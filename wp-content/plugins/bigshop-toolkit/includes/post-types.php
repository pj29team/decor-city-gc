<?php
/**
 * @version    1.0
 * @package    Fastshop_Toolkit
 * @author     KuteThemes
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 */

/**
 * Class Toolkit Post Type
 *
 * @since    1.0
 */
if ( !class_exists( 'Bigshop_Post_Type' ) ) {
	class Bigshop_Post_Type
	{

		public function __construct()
		{
			add_action( 'init', array( &$this, 'init' ), 9999 );
		}

		public static function init()
		{
			/*Mega menu */
			$args = array(
				'labels'              => array(
					'name'               => __( 'Mega Builder', 'bigshop-toolkit' ),
					'singular_name'      => __( 'Mega menu item', 'bigshop-toolkit' ),
					'add_new'            => __( 'Add new', 'bigshop-toolkit' ),
					'add_new_item'       => __( 'Add new menu item', 'bigshop-toolkit' ),
					'edit_item'          => __( 'Edit menu item', 'bigshop-toolkit' ),
					'new_item'           => __( 'New menu item', 'bigshop-toolkit' ),
					'view_item'          => __( 'View menu item', 'bigshop-toolkit' ),
					'search_items'       => __( 'Search menu items', 'bigshop-toolkit' ),
					'not_found'          => __( 'No menu items found', 'bigshop-toolkit' ),
					'not_found_in_trash' => __( 'No menu items found in trash', 'bigshop-toolkit' ),
					'parent_item_colon'  => __( 'Parent menu item:', 'bigshop-toolkit' ),
					'menu_name'          => __( 'Menu Builder', 'bigshop-toolkit' ),
				),
				'hierarchical'        => false,
				'description'         => __( 'Mega Menus.', 'bigshop-toolkit' ),
				'supports'            => array( 'title', 'editor' ),
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => 'bigshop_menu',
				'menu_position'       => 3,
				'show_in_nav_menus'   => true,
				'publicly_queryable'  => false,
				'exclude_from_search' => true,
				'has_archive'         => false,
				'query_var'           => true,
				'can_export'          => true,
				'rewrite'             => false,
				'capability_type'     => 'page',
				'menu_icon'           => 'dashicons-welcome-widgets-menus',
			);
			register_post_type( 'megamenu', $args );

			/* Footer */
			$args = array(
				'labels'              => array(
					'name'               => __( 'Footers', 'bigshop-toolkit' ),
					'singular_name'      => __( 'Footers', 'bigshop-toolkit' ),
					'add_new'            => __( 'Add New', 'bigshop-toolkit' ),
					'add_new_item'       => __( 'Add new footer', 'bigshop-toolkit' ),
					'edit_item'          => __( 'Edit footer', 'bigshop-toolkit' ),
					'new_item'           => __( 'New footer', 'bigshop-toolkit' ),
					'view_item'          => __( 'View footer', 'bigshop-toolkit' ),
					'search_items'       => __( 'Search template footer', 'bigshop-toolkit' ),
					'not_found'          => __( 'No template items found', 'bigshop-toolkit' ),
					'not_found_in_trash' => __( 'No template items found in trash', 'bigshop-toolkit' ),
					'parent_item_colon'  => __( 'Parent template item:', 'bigshop-toolkit' ),
					'menu_name'          => __( 'Footer Builder', 'bigshop-toolkit' ),
				),
				'hierarchical'        => false,
				'description'         => __( 'To Build Template Footer.', 'bigshop-toolkit' ),
				'supports'            => array( 'title', 'editor' ),
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => 'bigshop_menu',
				'menu_position'       => 4,
				'show_in_nav_menus'   => true,
				'publicly_queryable'  => false,
				'exclude_from_search' => true,
				'has_archive'         => false,
				'query_var'           => true,
				'can_export'          => true,
				'rewrite'             => false,
				'capability_type'     => 'page',
			);
			register_post_type( 'footer', $args );
		}
	}

	new Bigshop_Post_Type();
}
