<?php if ( !defined( 'ABSPATH' ) ) {
	die;
} // Cannot access pages directly.
if ( !class_exists( 'Bigshop_ThemeOption' ) ) {
	class Bigshop_ThemeOption
	{
		public function __construct()
		{
			$this->get_sidebars();
			$this->get_footer_options();
			$this->get_header_options();
			$this->get_footer_preview();
			$this->get_product_options();
			$this->get_social_option();
			$this->init_settings();
			add_action( 'admin_bar_menu', array( $this, 'bigshop_custom_menu' ), 1000 );
		}

		public function get_header_options()
		{
			$layoutDir      = get_template_directory() . '/templates/headers/';
			$header_options = array();

			if ( is_dir( $layoutDir ) ) {
				$files = scandir( $layoutDir );
				if ( $files && is_array( $files ) ) {
					foreach ( $files as $file ) {
						if ( $file != '.' && $file != '..' ) {
							$fileInfo = pathinfo( $file );
							if ( $fileInfo[ 'extension' ] == 'php' && $fileInfo[ 'basename' ] != 'index.php' ) {
								$file_data                    = get_file_data( $layoutDir . $file, array( 'Name' => 'Name' ) );
								$file_name                    = str_replace( 'header-', '', $fileInfo[ 'filename' ] );
								$header_options[ $file_name ] = array(
									'title'   => $file_data[ 'Name' ],
									'preview' => get_template_directory_uri() . '/templates/headers/header-' . $file_name . '.jpg',
								);
							}
						}
					}
				}
			}
			$this->header_options = $header_options;
		}

		public function get_product_options()
		{
			$layoutDir       = get_template_directory() . '/woocommerce/product-styles/';
			$product_options = array();

			if ( is_dir( $layoutDir ) ) {
				$files = scandir( $layoutDir );
				if ( $files && is_array( $files ) ) {
					$option = '';
					foreach ( $files as $file ) {
						if ( $file != '.' && $file != '..' ) {
							$fileInfo = pathinfo( $file );
							if ( $fileInfo[ 'extension' ] == 'php' && $fileInfo[ 'basename' ] != 'index.php' ) {
								$file_data                     = get_file_data( $layoutDir . $file, array( 'Name' => 'Name' ) );
								$file_name                     = str_replace( 'content-product-style-', '', $fileInfo[ 'filename' ] );
								$product_options[ $file_name ] = array(
									'title'   => $file_data[ 'Name' ],
									'preview' => get_template_directory_uri() . '/woocommerce/product-styles/content-product-style-' . $file_name . '.jpg',
								);
							}
						}
					}
				}
			}
			$this->product_options = $product_options;
		}

		public function get_sidebars()
		{
			$sidebars = array();
			global $wp_registered_sidebars;
			foreach ( $wp_registered_sidebars as $sidebar ) {
				$sidebars[ $sidebar[ 'id' ] ] = $sidebar[ 'name' ];
			}
			$this->sidebars = $sidebars;
		}

		public function bigshop_custom_menu()
		{
			global $wp_admin_bar;
			if ( !is_super_admin() || !is_admin_bar_showing() ) return;
			// Add Parent Menu
			$argsParent = array(
				'id'    => 'theme_option',
				'title' => esc_html__( 'Theme Options', 'bigshop' ),
				'href'  => admin_url( 'admin.php?page=bigshop' ),
			);
			$wp_admin_bar->add_menu( $argsParent );
		}

		public function get_social_option()
		{
			$socials     = array();
			$all_socials = cs_get_option( 'user_all_social' );
			$i           = 1;
			if ( $all_socials ) {
				foreach ( $all_socials as $social ) {
					$socials[ $i++ ] = $social[ 'title_social' ];
				}
			}
			$this->social_options = $socials;
		}

		public function get_footer_options()
		{
			$footer_options[ 'default' ] = array(
				'title'   => esc_html__( 'Default', 'bigshop' ),
				'preview' => '',
			);
			$layoutDir                   = get_template_directory() . '/templates/footers/';
			if ( is_dir( $layoutDir ) ) {
				$files = scandir( $layoutDir );
				if ( $files && is_array( $files ) ) {
					$option = '';
					foreach ( $files as $file ) {
						if ( $file != '.' && $file != '..' ) {
							$fileInfo = pathinfo( $file );
							if ( $fileInfo[ 'extension' ] == 'php' && $fileInfo[ 'basename' ] != 'index.php' ) {
								$file_data                    = get_file_data( $layoutDir . $file, array( 'Name' => 'Name' ) );
								$file_name                    = str_replace( 'footer-', '', $fileInfo[ 'filename' ] );
								$footer_options[ $file_name ] = array(
									'title'   => $file_data[ 'Name' ],
									'preview' => get_template_directory_uri() . '/templates/footers/footer-' . $file_name . '.jpg',
								);
							}
						}
					}
				}
			}
			$this->footer_options = $footer_options;
		}

		public function get_footer_preview()
		{
			$footer_preview = array();
			$template_style = '';
			$args           = array(
				'post_type'      => 'footer',
				'posts_per_page' => -1,
				'orderby'        => 'ASC',
			);
			$loop           = get_posts( $args );
			foreach ( $loop as $value ) {
				setup_postdata( $value );
				$data_meta = get_post_meta( $value->ID, '_custom_footer_options', true );
				if ( !empty( $data_meta ) ) {
					$template_style = $data_meta[ 'bigshop_footer_style' ];
				}
				$footer_preview[ $value->ID ] = array(
					'title'   => $value->post_title,
					'preview' => get_template_directory_uri() . '/templates/footers/footer-' . $template_style . '.jpg',
				);
			}
			$this->footer_preview = $footer_preview;
		}

		public function init_settings()
		{
			// ===============================================================================================
			// -----------------------------------------------------------------------------------------------
			// FRAMEWORK SETTINGS
			// -----------------------------------------------------------------------------------------------
			// ===============================================================================================
			$settings = array(
				'menu_title'      => esc_html__( 'Theme Options', 'bigshop' ),
				'menu_type'       => 'submenu', // menu, submenu, options, theme, etc.
				'menu_slug'       => 'bigshop',
				'ajax_save'       => true,
				'menu_parent'     => 'bigshop_menu',
				'show_reset_all'  => true,
				'menu_position'   => 2,
				'framework_title' => '<a href="http://bigshop.kutethemes.net/" target="_blank">Bigshop</a> <small>by <a href="https://themeforest.net/user/kutethemes/portfolio" target="_blank">KuteThemes</a></small>',
			);

			// ===============================================================================================
			// -----------------------------------------------------------------------------------------------
			// FRAMEWORK OPTIONS
			// -----------------------------------------------------------------------------------------------
			// ===============================================================================================
			$options = array();

			// ----------------------------------------
			// a option section for options overview  -
			// ----------------------------------------
			$options[] = array(
				'name'     => 'general',
				'title'    => esc_html__( 'General', 'bigshop' ),
				'icon'     => 'fa fa-wordpress',
				'sections' => array(
					array(
						'name'   => 'main_settings',
						'title'  => esc_html__( 'Main Settings', 'bigshop' ),
						'fields' => array(
							array(
								'id'    => 'bigshop_logo',
								'type'  => 'image',
								'title' => esc_html__( 'Logo', 'bigshop' ),
							),
							array(
								'id'      => 'bigshop_main_color',
								'type'    => 'color_picker',
								'title'   => esc_html__( 'Main Color', 'bigshop' ),
								'default' => '#f1b400',
								'rgba'    => true,
							),
							array(
								'id'      => 'bigshop_main_layout',
								'type'    => 'select',
								'options' => array(
									'boxed-layout'   => esc_html__( 'Boxed layout', 'bigshop' ),
									'wide-layout'    => esc_html__( 'Wide layout', 'bigshop' ),
								),
								'title'   => esc_html__( 'Choose main layout', 'bigshop' ),
								'default' => 'boxed-layout',
							),
							array(
								'id'      => 'enable_theme_options',
								'type'    => 'switcher',
								'title'   => esc_html__( 'Enable Meta Box Options', 'bigshop' ),
								'default' => false,
								'desc'    => esc_html__( 'Enable for using Themes setting each single page.', 'bigshop' ),
							),
							array(
								'id'      => 'bigshop_theme_lazy_load',
								'type'    => 'select',
								'options' => array(
									'yes' => 'Yes',
									'no'  => 'No',
								),
								'title'   => esc_html__( 'Use image Lazy Load', 'bigshop' ),
								'default' => 'yes',
							),
                            array(
								'id'      => 'enable_theme_dev_options',
								'type'    => 'switcher',
								'title'   => esc_html__( 'Enable Development Mode Options', 'bigshop' ),
								'default' => false,
								'desc'    => esc_html__( 'Enable Development Mode for more special settings like our demos.', 'bigshop' ),
							),
						),
					),

					array(
						'name'   => 'popup_settings',
						'title'  => esc_html__( 'Newsletter Settings', 'bigshop' ),
						'fields' => array(
							array(
								'id'      => 'bigshop_enable_popup',
								'type'    => 'switcher',
								'title'   => esc_html__( 'Enable Popup Newsletter', 'bigshop' ),
								'default' => false,
							),
							array(
								'id'         => 'bigshop_poppup_background',
								'type'       => 'image',
								'title'      => esc_html__( 'Popup Background', 'bigshop' ),
								'dependency' => array( 'bigshop_enable_popup', '==', '1' ),
							),
							array(
								'id'         => 'bigshop_popup_title',
								'type'       => 'text',
								'title'      => esc_html__( 'Title', 'bigshop' ),
								'default'    => 'SAVE 25%',
								'dependency' => array( 'bigshop_enable_popup', '==', '1' ),
							),
							array(
								'id'         => 'bigshop_popup_subtitle',
								'type'       => 'text',
								'title'      => esc_html__( 'Sub Title', 'bigshop' ),
								'default'    => 'SIGN UP FOR EMAILS',
								'dependency' => array( 'bigshop_enable_popup', '==', '1' ),
							),
							array(
								'id'         => 'bigshop_popup_description',
								'type'       => 'wysiwyg',
								'title'      => esc_html__( 'Description', 'bigshop' ),
								'settings'   => array(
									'textarea_rows' => 5,
									'tinymce'       => true,
									'media_buttons' => true,
								),
								'dependency' => array( 'bigshop_enable_popup', '==', '1' ),
							),
							array(
								'id'         => 'bigshop_popup_input_placeholder',
								'type'       => 'text',
								'title'      => esc_html__( 'Input placeholder text', 'bigshop' ),
								'default'    => 'Enter your email adress',
								'dependency' => array( 'bigshop_enable_popup', '==', '1' ),
							),
							array(
								'id'         => 'bigshop_popup_button_text',
								'type'       => 'text',
								'title'      => esc_html__( 'Input placeholder text', 'bigshop' ),
								'default'    => 'Sign-Up',
								'dependency' => array( 'bigshop_enable_popup', '==', '1' ),
							),
							array(
								'id'         => 'bigshop_popup_delay_time',
								'type'       => 'number',
								'title'      => esc_html__( 'Delay time', 'bigshop' ),
								'default'    => '0',
								'dependency' => array( 'bigshop_enable_popup', '==', '1' ),
							),
							array(
								'id'         => 'bigshop_enable_popup_mobile',
								'type'       => 'switcher',
								'title'      => esc_html__( 'Enable Poppup on Mobile', 'bigshop' ),
								'default'    => false,
								'dependency' => array( 'bigshop_enable_popup', '==', '1' ),
							),
						),
					),
					array(
						'name'   => 'widget_settings',
						'title'  => esc_html__( 'Widget Settings', 'bigshop' ),
						'fields' => array(
							array(
								'id'              => 'multi_widget',
								'type'            => 'group',
								'title'           => esc_html__( 'Multi Widget', 'bigshop' ),
								'button_title'    => esc_html__( 'Add Widget', 'bigshop' ),
								'accordion_title' => esc_html__( 'Add New Field', 'bigshop' ),
								'fields'          => array(
									array(
										'id'    => 'add_widget',
										'type'  => 'text',
										'title' => esc_html__( 'Name Widget', 'bigshop' ),
									),
								),
							),
						),
					),
					array(
						'name'   => 'theme_js_css',
						'title'  => esc_html__( 'Customs JS/CSS', 'bigshop' ),
						'fields' => array(
							array(
								'id'         => 'bigshop_custom_css',
								'title'      => esc_html__( 'Custom CSS', 'bigshop' ),
								'type'       => 'ace_editor',
								'attributes' => array(
									'data-theme' => 'twilight',  // the theme for ACE Editor
									'data-mode'  => 'css',     // the language for ACE Editor
								),
							),
							array(
								'id'         => 'bigshop_custom_js',
								'title'      => esc_html__( 'Custom JS', 'bigshop' ),
								'type'       => 'ace_editor',
								'attributes' => array(
									'data-theme' => 'twilight',  // the theme for ACE Editor
									'data-mode'  => 'javascript',     // the language for ACE Editor
								),
							),
						),
					),
				),
			);

			$options[] = array(
				'name'   => 'header',
				'title'  => esc_html__( 'Header Settings', 'bigshop' ),
				'icon'   => 'fa fa-folder-open-o',
                'sections' => array(
                    array(
                        'name'   => 'header_layout',
                        'title'  => esc_html__( 'Header Layout', 'bigshop' ),
                        'fields' => array(
                            array(
                                'id'      => 'bigshop_enable_sticky_menu',
                                'type'    => 'select',
                                'options' => array(
                                    'yes' => 'Yes',
                                    'no'  => 'No',
                                ),
                                'title'   => esc_html__( 'Main Menu Sticky', 'bigshop' ),
                            ),
                            array(
                                'id'         => 'bigshop_used_header',
                                'type'       => 'select_preview',
                                'title'      => esc_html__( 'Header Layout', 'bigshop' ),
                                'desc'       => esc_html__( 'Select a header layout', 'bigshop' ),
                                'options'    => $this->header_options,
                                'default'    => 'style-01',
                                'attributes' => array(
                                    'data-depend-id' => 'bigshop_used_header',
                                ),
                            ),
                            array(
								'title'    => 'Note on topbar off header',
								'id'       => 'bigshop_top_note',
								'type'     => 'textarea',
							),
                            array(
								'title'    => 'Title off phone number title on header',
								'id'       => 'bigshop_title_phonenumber',
								'type'     => 'text',
								'dependency' => array( 'bigshop_used_header', 'any', 'style-03' ),
							),
		                    array(
								'title'    => 'Phone number on header',
								'id'       => 'bigshop_top_phonenumber',
								'type'     => 'text',
								'dependency' => array( 'bigshop_used_header', 'any', 'style-02,style-03' ),
							),
							array(
								'title'    => 'Address on header',
								'id'       => 'bigshop_top_address',
								'type'     => 'text',
								'dependency' => array( 'bigshop_used_header', 'any', 'style-03' ),
							),
                        )
                    ),
                    array(
                        'name'   => 'header_vetical',
                        'title'  => esc_html__( 'Vertical menu', 'bigshop' ),
                        'fields' => array(
                            array(
                                'title'    => esc_html__('Use Vertical Menu', 'bigshop'),
                                'id'       => 'opt_enable_vertical_menu',
                                'type'     => 'switcher',
                                'default'  => '1',
                                'desc' => esc_html__( 'Use Vertical Menu on show any page', 'bigshop' ),
                            ),
                            array(
                                'title'    => 'Vertical Menu Title',
                                'id'       => 'opt_vertical_menu_title',
                                'type'     => 'text',
                                'default'  => esc_html__('All Category','bigshop'),
                                'dependency'   => array( 'opt_enable_vertical_menu', '==', 'true' ),
                            ),
                            array(
                                'title'    => 'Vertical Menu Button show all text',
                                'id'       => 'opt_vertical_menu_button_all_text',
                                'type'     => 'text',
                                'default'  => esc_html__('All Categories','bigshop'),
                                'dependency'   => array( 'opt_enable_vertical_menu', '==', 'true' ),
                            ),

                            array(
                                'title'    => 'Vertical Menu Button close text',
                                'id'       => 'opt_vertical_menu_button_close_text',
                                'type'     => 'text',
                                'default'  => esc_html__('Close','bigshop'),
                                'dependency'   => array( 'opt_enable_vertical_menu', '==', 'true' ),
                            ),
                            array(
                                'title'    => esc_html__('Collapse', 'bigshop'),
                                'id'       => 'opt_click_open_vertical_menu',
                                'type'     => 'switcher',
                                'default'  => '1',
                                'on'       => esc_html__('Enable', 'bigshop'),
                                'off'      => esc_html__('Disable', 'bigshop'),
                                'desc' => esc_html__('Vertical menu will expand on click', 'bigshop'),
                                'dependency'   => array( 'opt_enable_vertical_menu', '==', 'true' ),
                            ),

                            array(
                                'title'    => esc_html__('The number of visible vertical menu items', 'bigshop'),
                                'desc' => esc_html__('The number of visible vertical menu items', 'bigshop'),
                                'id'       => 'opt_vertical_item_visible',
                                'default'  => 10,
                                'type'     => 'text',
                                'validate' => 'numeric',
                                'dependency'   => array( 'opt_enable_vertical_menu', '==', 'true' ),
                            )
                        )
                    ),
                ),
			);

			$options[] = array(
				'name'   => 'footer',
				'title'  => esc_html__( 'Footer Settings', 'bigshop' ),
				'icon'   => 'fa fa-folder-open-o',
				'fields' => array(
					array(
						'id'      => 'bigshop_footer_options',
						'type'    => 'select_preview',
						'title'   => esc_html__( 'Select Footer Builder', 'bigshop' ),
						'options' => $this->footer_preview,
						'default' => 'default',
					),
				),
			);

			$options[] = array(
				'name'     => 'blog',
				'title'    => esc_html__( 'Blog Settings', 'bigshop' ),
				'icon'     => 'fa fa-rss',
				'sections' => array(
					array(
						'name'   => 'blog_page',
						'title'  => esc_html__( 'Blog Page', 'bigshop' ),
						'fields' => array(
							array(
								'id'      => 'sidebar_blog_layout',
								'type'    => 'image_select',
								'title'   => esc_html__( 'Single Post Sidebar Position', 'bigshop' ),
								'desc'    => esc_html__( 'Select sidebar position on Blog.', 'bigshop' ),
								'options' => array(
									'left'  => BIGSHOP_TOOLKIT_URL . '/includes/core/assets/images/left-sidebar.png',
									'right' => BIGSHOP_TOOLKIT_URL . '/includes/core/assets/images/right-sidebar.png',
									'full'  => BIGSHOP_TOOLKIT_URL . '/includes/core/assets/images/default-sidebar.png',
								),
								'default' => 'left',
							),
							array(
								'id'         => 'blog_sidebar',
								'type'       => 'select',
								'title'      => esc_html__( 'Blog Sidebar', 'bigshop' ),
								'options'    => $this->sidebars,
								'default'    => '',
								'dependency' => array( 'sidebar_blog_layout_full', '==', false ),
							),
                            array(
                                'id'    => 'using_placeholder',
                                'type'  => 'switcher',
                                'title' => esc_html__( 'Using Placeholder', 'bigshop' ),
                            ),
                            array(
                                'id'             => 'blog_list_style',
                                'type'           => 'select',
                                'title'          => 'Blog list Style',
                                'options'        => array(
                                    'standard' => 'Standard',
                                    'grid'     => 'Grid Style 01',
                                    'grid2'     => 'Grid Style 02',
                                ),
                                'default' => 'standard'
                            ),
                            /* Blog grid */
                            array(
                                'title'   => esc_html__('Items per row on Desktop', 'bigshop'),
                                'desc'    => esc_html__('(Screen resolution of device >= 1200px )', 'bigshop'),
                                'id'      => 'bigshop_blog_lg_items',
                                'type'    => 'select',
                                'default' => '4',
                                'options'   =>  array(
                                    '12'    =>  '1 item',
                                    '6'     =>  '2 items',
                                    '4'     =>  '3 items',
                                    '3'     =>  '4 items',
                                    '15'    =>  '5 items',
                                    '2'     =>  '6 items',
                                ),
                                'dependency'   => array( 'blog_list_style', 'any', 'grid,grid2' ),
                            ),
                            array(
                                'title'   => esc_html__('Items per row on landscape tablet', 'bigshop'),
                                'desc'    => esc_html__('(Screen resolution of device >=992px and < 1200px )', 'bigshop'),
                                'id'      => 'bigshop_blog_md_items',
                                'type'    => 'select',
                                'default' => '4',
                                'options'   =>  array(
                                    '12'    =>  '1 item',
                                    '6'     =>  '2 items',
                                    '4'     =>  '3 items',
                                    '3'     =>  '4 items',
                                    '15'    =>  '5 items',
                                    '2'     =>  '6 items',
                                ),
                                'dependency'   => array( 'blog_list_style', 'any', 'grid,grid2' ),
                            ),
                            array(
                                'title'   => esc_html__('Items per row on portrait tablet', 'bigshop'),
                                'desc'    => esc_html__('(Screen resolution of device >=768px and < 992px )', 'bigshop'),
                                'id'      => 'bigshop_blog_sm_items',
                                'type'    => 'select',
                                'default' => '4',
                                'options' => array(
                                    '12'    =>  '1 item',
                                    '6'     =>  '2 items',
                                    '4'     =>  '3 items',
                                    '3'     =>  '4 items',
                                    '15'    =>  '5 items',
                                    '2'     =>  '6 items',
                                ),
                                'dependency'   => array( 'blog_list_style', 'any', 'grid,grid2' ),
                            ),
                            array(
                                'title'   => esc_html__('Items per row on Mobile', 'bigshop'),
                                'desc'    => esc_html__('(Screen resolution of device >=480  add < 768px)', 'bigshop'),
                                'id'      => 'bigshop_blog_xs_items',
                                'type'    => 'select',
                                'default' => '6',
                                'options' => array(
                                    '12'    =>  '1 item',
                                    '6'     =>  '2 items',
                                    '4'     =>  '3 items',
                                    '3'     =>  '4 items',
                                    '15'    =>  '5 items',
                                    '2'     =>  '6 items',
                                ),
                                'dependency'   => array( 'blog_list_style', 'any', 'grid,grid2' ),
                            ),
                            array(
                                'title'   => esc_html__('Items per row on Mobile', 'bigshop'),
                                'desc'    => esc_html__('(Screen resolution of device < 480px)', 'bigshop'),
                                'id'      => 'bigshop_blog_ts_items',
                                'type'    => 'select',
                                'default' => '12',
                                'options' => array(
                                    '12'    =>  '1 item',
                                    '6'     =>  '2 items',
                                    '4'     =>  '3 items',
                                    '3'     =>  '4 items',
                                    '15'    =>  '5 items',
                                    '2'     =>  '6 items',
                                ),
                                'dependency'   => array( 'blog_list_style', 'any', 'grid,grid2' ),
                            ),
                           
						),
					),
					array(
						'name'   => 'single_post',
						'title'  => esc_html__( 'Single Post', 'bigshop' ),
						'fields' => array(
							array(
								'id'      => 'sidebar_single_post_position',
								'type'    => 'image_select',
								'title'   => esc_html__( 'Single Post Sidebar Position', 'bigshop' ),
								'desc'    => esc_html__( 'Select sidebar position on Single Post.', 'bigshop' ),
								'options' => array(
									'left'  => BIGSHOP_TOOLKIT_URL . '/includes/core/assets/images/left-sidebar.png',
									'right' => BIGSHOP_TOOLKIT_URL . '/includes/core/assets/images/right-sidebar.png',
									'full'  => BIGSHOP_TOOLKIT_URL . '/includes/core/assets/images/default-sidebar.png',
								),
								'default' => 'left',
							),
							array(
								'id'         => 'single_post_sidebar',
								'type'       => 'select',
								'title'      => esc_html__( 'Single Post Sidebar', 'bigshop' ),
								'options'    => $this->sidebars,
								'default'    => '',
								'dependency' => array( 'sidebar_single_post_position_full', '==', false ),
							),
						),
					),
				),
			);
			if ( class_exists( 'WooCommerce' ) ) {
				$options[] = array(
					'name'     => 'wooCommerce',
					'title'    => esc_html__( 'WooCommerce', 'bigshop' ),
					'icon'     => 'fa fa-shopping-bag',
					'sections' => array(
						array(
							'name'   => 'shop_product',
							'title'  => esc_html__( 'Shop Page', 'bigshop' ),
							'fields' => array(
								array(
									'id'    => 'enable_shop_banner',
									'type'  => 'switcher',
									'title' => esc_html__( 'Shop Banner', 'bigshop' ),
								),
								array(
									'id'         => 'woo_shop_banner',
									'type'       => 'image',
									'title'      => esc_html__( 'Shop Banner', 'bigshop' ),
									'add_title'  => esc_html__( 'Add Banner', 'bigshop' ),
									'dependency' => array( 'enable_shop_banner', '==', true ),
								),
								array(
									'type'    => 'subheading',
									'content' => esc_html__( 'Shop Settings', 'bigshop' ),
								),
								array(
									'id'      => 'product_newness',
									'type'    => 'number',
									'title'   => esc_html__( 'Products Newness', 'bigshop' ),
									'default' => '10',
								),
								array(
									'id'      => 'sidebar_shop_page_position',
									'type'    => 'image_select',
									'title'   => esc_html__( 'Shop Page Sidebar Position', 'bigshop' ),
									'desc'    => esc_html__( 'Select sidebar position on Shop Page.', 'bigshop' ),
									'options' => array(
										'left'  => BIGSHOP_TOOLKIT_URL . '/includes/core/assets/images/left-sidebar.png',
										'right' => BIGSHOP_TOOLKIT_URL . '/includes/core/assets/images/right-sidebar.png',
										'full'  => BIGSHOP_TOOLKIT_URL . '/includes/core/assets/images/default-sidebar.png',
									),
									'default' => 'left',
								),
								array(
									'id'         => 'shop_page_sidebar',
									'type'       => 'select',
									'title'      => esc_html__( 'Shop Sidebar', 'bigshop' ),
									'options'    => $this->sidebars,
									'default'    => '',
									'dependency' => array( 'sidebar_shop_page_position_full', '==', false ),
								),
								array(
									'id'      => 'shop_page_layout',
									'type'    => 'image_select',
									'title'   => esc_html__( 'Shop Default Layout', 'bigshop' ),
									'desc'    => esc_html__( 'Select default layout for shop, product category archive.', 'bigshop' ),
									'options' => array(
										'grid' => get_template_directory_uri() . '/assets/images/grid-display.png',
										'list' => get_template_directory_uri() . '/assets/images/list-display.png',
									),
									'default' => 'grid',
								),
								array(
									'id'      => 'product_per_page',
									'type'    => 'number',
									'title'   => esc_html__( 'Products perpage', 'bigshop' ),
									'desc'    => esc_html__( 'Number of products on shop page.', 'bigshop' ),
									'default' => '10',
								),
								array(
									'id'         => 'bigshop_shop_product_style',
									'type'       => 'select_preview',
									'title'      => esc_html__( 'Product Shop Layout', 'bigshop' ),
									'desc'       => esc_html__( 'Select a Product layout in shop page', 'bigshop' ),
									'options'    => $this->product_options,
									'default'    => '1',
									'attributes' => array(
										'data-depend-id' => 'bigshop_shop_product_style',
									),
								),
								array(
									'type'    => 'subheading',
									'content' => esc_html__( 'Responsive Settings', 'bigshop' ),
								),
								array(
									'title'   => esc_html__( 'Items per row on Desktop( For grid mode )', 'bigshop' ),
									'desc'    => esc_html__( '(Screen resolution of device >= 1200px < 1500px )', 'bigshop' ),
									'id'      => 'bigshop_woo_lg_items',
									'type'    => 'select',
									'default' => '4',
									'options' => array(
										'12' => esc_html__( '1 item', 'bigshop' ),
										'6'  => esc_html__( '2 items', 'bigshop' ),
										'4'  => esc_html__( '3 items', 'bigshop' ),
										'3'  => esc_html__( '4 items', 'bigshop' ),
										'15' => esc_html__( '5 items', 'bigshop' ),
										'2'  => esc_html__( '6 items', 'bigshop' ),
									),

								),
								array(
									'title'   => esc_html__( 'Items per row on landscape tablet( For grid mode )', 'bigshop' ),
									'desc'    => esc_html__( '(Screen resolution of device >=992px and < 1200px )', 'bigshop' ),
									'id'      => 'bigshop_woo_md_items',
									'type'    => 'select',
									'default' => '4',
									'options' => array(
										'12' => esc_html__( '1 item', 'bigshop' ),
										'6'  => esc_html__( '2 items', 'bigshop' ),
										'4'  => esc_html__( '3 items', 'bigshop' ),
										'3'  => esc_html__( '4 items', 'bigshop' ),
										'15' => esc_html__( '5 items', 'bigshop' ),
										'2'  => esc_html__( '6 items', 'bigshop' ),
									),

								),
								array(
									'title'   => esc_html__( 'Items per row on portrait tablet( For grid mode )', 'bigshop' ),
									'desc'    => esc_html__( '(Screen resolution of device >=768px and < 992px )', 'bigshop' ),
									'id'      => 'bigshop_woo_sm_items',
									'type'    => 'select',
									'default' => '4',
									'options' => array(
										'12' => esc_html__( '1 item', 'bigshop' ),
										'6'  => esc_html__( '2 items', 'bigshop' ),
										'4'  => esc_html__( '3 items', 'bigshop' ),
										'3'  => esc_html__( '4 items', 'bigshop' ),
										'15' => esc_html__( '5 items', 'bigshop' ),
										'2'  => esc_html__( '6 items', 'bigshop' ),
									),

								),
								array(
									'title'   => esc_html__( 'Items per row on Mobile( For grid mode )', 'bigshop' ),
									'desc'    => esc_html__( '(Screen resolution of device >=480  add < 768px)', 'bigshop' ),
									'id'      => 'bigshop_woo_xs_items',
									'type'    => 'select',
									'default' => '6',
									'options' => array(
										'12' => esc_html__( '1 item', 'bigshop' ),
										'6'  => esc_html__( '2 items', 'bigshop' ),
										'4'  => esc_html__( '3 items', 'bigshop' ),
										'3'  => esc_html__( '4 items', 'bigshop' ),
										'15' => esc_html__( '5 items', 'bigshop' ),
										'2'  => esc_html__( '6 items', 'bigshop' ),
									),

								),
								array(
									'title'   => esc_html__( 'Items per row on Mobile( For grid mode )', 'bigshop' ),
									'desc'    => esc_html__( '(Screen resolution of device < 480px)', 'bigshop' ),
									'id'      => 'bigshop_woo_ts_items',
									'type'    => 'select',
									'default' => '12',
									'options' => array(
										'12' => esc_html__( '1 item', 'bigshop' ),
										'6'  => esc_html__( '2 items', 'bigshop' ),
										'4'  => esc_html__( '3 items', 'bigshop' ),
										'3'  => esc_html__( '4 items', 'bigshop' ),
										'15' => esc_html__( '5 items', 'bigshop' ),
										'2'  => esc_html__( '6 items', 'bigshop' ),
									),
								),
							),
						),
						array(
							'name'   => 'single_product',
							'title'  => esc_html__( 'Single product', 'bigshop' ),
							'fields' => array(
								array(
									'id'      => 'sidebar_product_position',
									'type'    => 'image_select',
									'title'   => esc_html__( 'Single Product Sidebar Position', 'bigshop' ),
									'desc'    => esc_html__( 'Select sidebar position on single product page.', 'bigshop' ),
									'options' => array(
										'left'  => BIGSHOP_TOOLKIT_URL . '/includes/core/assets/images/left-sidebar.png',
										'right' => BIGSHOP_TOOLKIT_URL . '/includes/core/assets/images/right-sidebar.png',
										'full'  => BIGSHOP_TOOLKIT_URL . '/includes/core/assets/images/default-sidebar.png',
									),
									'default' => 'left',
								),
								array(
									'id'         => 'single_product_sidebar',
									'type'       => 'select',
									'title'      => esc_html__( 'Single Product Sidebar', 'bigshop' ),
									'options'    => $this->sidebars,
									'default'    => '',
									'dependency' => array( 'sidebar_product_position_full', '==', false ),
								),
								array(
									'id'      => 'enable_share_product',
									'type'    => 'switcher',
									'title'   => esc_html__( 'Enable Product Share', 'bigshop' ),
								),
                                array(
                                    'id'      => 'bigshop_enable_product_thumb_slide',
                                    'type'    => 'switcher',
                                    'title'   => esc_html__( 'Enable Thumbnail Slide', 'bigshop' ),
                                    'default' => true
                                ),
								array(
									'type'       => 'subheading',
									'content'    => esc_html__( 'Responsive Settings', 'bigshop' ),
									'dependency' => array( 'bigshop_enable_product_thumb_slide', '==', true ),
								),
								array(
									'title'      => esc_html__( 'Thumbnail items per row on Desktop', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >= 1200px )', 'bigshop' ),
									'id'         => 'bigshop_thumbnail_lg_items',
									'type'       => 'select',
									'default'    => '4',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
                                    'dependency' => array( 'bigshop_enable_product_thumb_slide', '==', true ),
								),
								array(
									'title'      => esc_html__( 'Thumbnail items per row on landscape tablet', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >=992px and < 1200px )', 'bigshop' ),
									'id'         => 'bigshop_thumbnail_md_items',
									'type'       => 'select',
									'default'    => '3',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
                                    'dependency' => array( 'bigshop_enable_product_thumb_slide', '==', true ),
								),
								array(
									'title'      => esc_html__( 'Thumbnail items per row on portrait tablet', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >=768px and < 992px )', 'bigshop' ),
									'id'         => 'bigshop_thumbnail_sm_items',
									'type'       => 'select',
									'default'    => '2',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
                                    'dependency' => array( 'bigshop_enable_product_thumb_slide', '==', true ),
								),
								array(
									'title'      => esc_html__( 'Thumbnail items per row on Mobile', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >=480  add < 768px)', 'bigshop' ),
									'id'         => 'bigshop_thumbnail_xs_items',
									'type'       => 'select',
									'default'    => '1',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
                                    'dependency' => array( 'bigshop_enable_product_thumb_slide', '==', true ),
								),
								array(
									'title'      => esc_html__( 'Thumbnail items per row on Mobile', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device < 480px)', 'bigshop' ),
									'id'         => 'bigshop_thumbnail_ts_items',
									'type'       => 'select',
									'default'    => '1',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
                                    'dependency' => array( 'bigshop_enable_product_thumb_slide', '==', true ),
								),
							),
						),
						array(
							'name'   => 'cross_sell',
							'title'  => esc_html__( 'Cross sell', 'bigshop' ),
							'fields' => array(
								array(
									'id'      => 'enable_cross_sell',
									'type'    => 'select',
									'options' => array(
										'yes' => 'Yes',
										'no'  => 'No',
									),
									'title'   => esc_html__( 'Enable Cross Sell', 'bigshop' ),
									'default' => 'yes',
								),
								array(
									'title'      => esc_html__( 'Cross sell title', 'bigshop' ),
									'id'         => 'bigshop_cross_sells_products_title',
									'type'       => 'text',
									'default'    => esc_html__( 'You may be interested in...', 'bigshop' ),
									'desc'       => esc_html__( 'Cross sell title', 'bigshop' ),
									'dependency' => array( 'enable_cross_sell', '==', 'yes' ),
								),
								array(
									'title'      => esc_html__( 'Cross sell items per row on Desktop', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >= 1200px < 1500px )', 'bigshop' ),
									'id'         => 'bigshop_woo_crosssell_lg_items',
									'type'       => 'select',
									'default'    => '3',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
									'dependency' => array( 'enable_cross_sell', '==', 'yes' ),
								),
								array(
									'title'      => esc_html__( 'Cross sell items per row on landscape tablet', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >=992px and < 1200px )', 'bigshop' ),
									'id'         => 'bigshop_woo_crosssell_md_items',
									'type'       => 'select',
									'default'    => '3',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
									'dependency' => array( 'enable_cross_sell', '==', 'yes' ),
								),
								array(
									'title'      => esc_html__( 'Cross sell items per row on portrait tablet', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >=768px and < 992px )', 'bigshop' ),
									'id'         => 'bigshop_woo_crosssell_sm_items',
									'type'       => 'select',
									'default'    => '2',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
									'dependency' => array( 'enable_cross_sell', '==', 'yes' ),
								),
								array(
									'title'      => esc_html__( 'Cross sell items per row on Mobile', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >=480  add < 768px)', 'bigshop' ),
									'id'         => 'bigshop_woo_crosssell_xs_items',
									'type'       => 'select',
									'default'    => '1',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
									'dependency' => array( 'enable_cross_sell', '==', 'yes' ),
								),
								array(
									'title'      => esc_html__( 'Cross sell items per row on Mobile', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device < 480px)', 'bigshop' ),
									'id'         => 'bigshop_woo_crosssell_ts_items',
									'type'       => 'select',
									'default'    => '1',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
									'dependency' => array( 'enable_cross_sell', '==', 'yes' ),
								),
							),
						),
						array(
							'name'   => 'related_product',
							'title'  => esc_html__( 'Related Products', 'bigshop' ),
							'fields' => array(
								array(
									'id'      => 'enable_relate_products',
									'type'    => 'select',
									'options' => array(
										'yes' => 'Yes',
										'no'  => 'No',
									),
									'title'   => esc_html__( 'Enable Related', 'bigshop' ),
									'default' => 'yes',
								),
								array(
									'title'      => esc_html__( 'Related products title', 'bigshop' ),
									'id'         => 'bigshop_related_products_title',
									'type'       => 'text',
									'default'    => esc_html__( 'Related Products', 'bigshop' ),
									'desc'       => esc_html__( 'Related products title', 'bigshop' ),
									'dependency' => array( 'enable_relate_products', '==', 'yes' ),
								),
								array(
									'title'      => esc_html__( 'Related products items per row on Desktop', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >= 1200px < 1500px )', 'bigshop' ),
									'id'         => 'bigshop_woo_related_lg_items',
									'type'       => 'select',
									'default'    => '4',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
									'dependency' => array( 'enable_relate_products', '==', 'yes' ),
								),
								array(
									'title'      => esc_html__( 'Related products items per row on landscape tablet', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >=992px and < 1200px )', 'bigshop' ),
									'id'         => 'bigshop_woo_related_md_items',
									'type'       => 'select',
									'default'    => '3',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
									'dependency' => array( 'enable_relate_products', '==', 'yes' ),
								),
								array(
									'title'      => esc_html__( 'Related product items per row on portrait tablet', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >=768px and < 992px )', 'bigshop' ),
									'id'         => 'bigshop_woo_related_sm_items',
									'type'       => 'select',
									'default'    => '2',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
									'dependency' => array( 'enable_relate_products', '==', 'yes' ),
								),
								array(
									'title'      => esc_html__( 'Related products items per row on Mobile', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >=480  add < 768px)', 'bigshop' ),
									'id'         => 'bigshop_woo_related_xs_items',
									'type'       => 'select',
									'default'    => '1',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
									'dependency' => array( 'enable_relate_products', '==', 'yes' ),
								),
								array(
									'title'      => esc_html__( 'Related products items per row on Mobile', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device < 480px)', 'bigshop' ),
									'id'         => 'bigshop_woo_related_ts_items',
									'type'       => 'select',
									'default'    => '1',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
									'dependency' => array( 'enable_relate_products', '==', 'yes' ),
								),
							),
						),
						array(
							'name'   => 'upsells_product',
							'title'  => esc_html__( 'Up sells Products', 'bigshop' ),
							'fields' => array(
								array(
									'id'      => 'enable_up_sell',
									'type'    => 'select',
									'options' => array(
										'yes' => 'Yes',
										'no'  => 'No',
									),
									'title'   => esc_html__( 'Enable Up Sell', 'bigshop' ),
									'default' => 'yes',
								),
								array(
									'title'      => esc_html__( 'Up sells title', 'bigshop' ),
									'id'         => 'bigshop_upsell_products_title',
									'type'       => 'text',
									'default'    => esc_html__( 'You may also like&hellip;', 'bigshop' ),
									'desc'       => esc_html__( 'Up sells products title', 'bigshop' ),
									'dependency' => array( 'enable_up_sell', '==', 'yes' ),
								),

								array(
									'title'      => esc_html__( 'Up sells items per row on Desktop', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >= 1200px < 1500px )', 'bigshop' ),
									'id'         => 'bigshop_woo_upsell_lg_items',
									'type'       => 'select',
									'default'    => '4',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
									'dependency' => array( 'enable_up_sell', '==', 'yes' ),
								),
								array(
									'title'      => esc_html__( 'Up sells items per row on landscape tablet', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >=992px and < 1200px )', 'bigshop' ),
									'id'         => 'bigshop_woo_upsell_md_items',
									'type'       => 'select',
									'default'    => '3',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
									'dependency' => array( 'enable_up_sell', '==', 'yes' ),
								),
								array(
									'title'      => esc_html__( 'Up sells items per row on portrait tablet', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >=768px and < 992px )', 'bigshop' ),
									'id'         => 'bigshop_woo_upsell_sm_items',
									'type'       => 'select',
									'default'    => '2',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
									'dependency' => array( 'enable_up_sell', '==', 'yes' ),
								),
								array(
									'title'      => esc_html__( 'Up sells items per row on Mobile', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device >=480  add < 768px)', 'bigshop' ),
									'id'         => 'bigshop_woo_upsell_xs_items',
									'type'       => 'select',
									'default'    => '2',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
									'dependency' => array( 'enable_up_sell', '==', 'yes' ),
								),
								array(
									'title'      => esc_html__( 'Up sells items per row on Mobile', 'bigshop' ),
									'desc'       => esc_html__( '(Screen resolution of device < 480px)', 'bigshop' ),
									'id'         => 'bigshop_woo_upsell_ts_items',
									'type'       => 'select',
									'default'    => '1',
									'options'    => array(
										'1' => esc_html__( '1 item', 'bigshop' ),
										'2' => esc_html__( '2 items', 'bigshop' ),
										'3' => esc_html__( '3 items', 'bigshop' ),
										'4' => esc_html__( '4 items', 'bigshop' ),
										'5' => esc_html__( '5 items', 'bigshop' ),
										'6' => esc_html__( '6 items', 'bigshop' ),
									),
									'dependency' => array( 'enable_up_sell', '==', 'yes' ),
								),
							),
						),
					),
				);
			}

			$options[] = array(
				'name'   => 'social_settings',
				'title'  => esc_html__( 'Social Settings', 'bigshop' ),
				'icon'   => 'fa fa-users',
				'fields' => array(
					array(
						'type'    => 'subheading',
						'content' => esc_html__( 'Social User', 'bigshop' ),
					),
					array(
						'id'              => 'user_all_social',
						'type'            => 'group',
						'title'           => esc_html__( 'Social', 'bigshop' ),
						'button_title'    => esc_html__( 'Add New Social', 'bigshop' ),
						'accordion_title' => esc_html__( 'Social Settings', 'bigshop' ),
						'fields'          => array(
							array(
								'id'      => 'title_social',
								'type'    => 'text',
								'title'   => esc_html__( 'Title Social', 'bigshop' ),
								'default' => 'Facebook',
							),
							array(
								'id'      => 'link_social',
								'type'    => 'text',
								'title'   => esc_html__( 'Link Social', 'bigshop' ),
								'default' => 'https://facebook.com',
							),
							array(
								'id'      => 'icon_social',
								'type'    => 'icon',
								'title'   => esc_html__( 'Icon Social', 'bigshop' ),
								'default' => 'fa fa-facebook',
							),
						),
					),
				),
			);
            $options[] = array(
                'name'   => 'google_map',
                'title'  => esc_html__( 'Google map', 'bigshop' ),
                'icon'   => 'fa fa-font',
                'fields' => array(
                    array(
                        'id'    => 'opt_gmap_api_key',
                        'type'  => 'text',
                        'title' => esc_html__('Google Map API Key', 'bigshop'),
                        'desc'  => wp_kses(sprintf(__('Enter your Google Map API key. <a href="%s" target="_blank">How to get?</a>', 'digitalworld'), 'https://developers.google.com/maps/documentation/javascript/get-api-key'), array('a' => array('href' => array(), 'target' => array()))),
                    ),
                ),
            );

			$options[] = array(
				'name'   => 'typography',
				'title'  => esc_html__( 'Typography Options', 'bigshop' ),
				'icon'   => 'fa fa-font',
				'fields' => array(
					array(
						'id'      => 'typography_font_family',
						'type'    => 'typography',
						'title'   => esc_html__( 'Font Family', 'bigshop' ),
						'default' => array(
							'family'  => 'Roboto',
							'variant' => '400',
						),
					),
					array(
						'id'      => 'typography_font_size',
						'type'    => 'number',
						'title'   => esc_html__( 'Font Size', 'bigshop' ),
						'default' => 17,
					),
					array(
						'id'      => 'typography_line_height',
						'type'    => 'number',
						'title'   => esc_html__( 'Line Height', 'bigshop' ),
						'default' => 30,
					),
				),
			);

			$options[] = array(
				'name'   => 'backup_option',
				'title'  => esc_html__( 'Backup Options', 'bigshop' ),
				'icon'   => 'fa fa-bold',
				'fields' => array(
					array(
						'type'  => 'backup',
						'title' => esc_html__( 'Backup Field', 'bigshop' ),
					),
				),
			);


			CSFramework::instance( $settings, $options );
		}
	}

	new Bigshop_ThemeOption();
}
