<?php if ( !defined( 'ABSPATH' ) ) {
	die;
} // Cannot access pages directly.

$data_meta = new Bigshop_ThemeOption();


$theme_options = get_option( CS_OPTION );

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// META BOX OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options = array();

// -----------------------------------------
// Page Meta box Options                   -
// -----------------------------------------
if( is_array( $theme_options ) && isset( $theme_options['enable_theme_options'] ) && $theme_options['enable_theme_options'] ){
    
    $default_color       = (isset( $theme_options['bigshop_main_color'] ) && $theme_options['bigshop_main_color'] != '' ) ? $theme_options['bigshop_main_color'] : '#ffffff' ;
    $default_main_layout = (isset( $theme_options['bigshop_main_layout'] ) && $theme_options['bigshop_main_layout'] != '' ) ? $theme_options['bigshop_main_layout'] : 'boxed-layout' ;
    $default_header = (isset( $theme_options['bigshop_used_header'] ) && $theme_options['bigshop_used_header'] != '' ) ? $theme_options['bigshop_used_header'] : 'style-01' ;
    $default_footer = (isset( $theme_options['bigshop_footer_options'] ) && $theme_options['bigshop_footer_options'] != '' ) ? $theme_options['bigshop_footer_options'] : 'default' ;
    $default_vertical_items = (isset( $theme_options['opt_vertical_item_visible'] ) && $theme_options['opt_vertical_item_visible'] != '' ) ? $theme_options['opt_vertical_item_visible'] : 8 ;
    
    
    $options[] = array(
    	'id'        => '_custom_metabox_theme_options',
    	'title'     => 'Custom Theme Options',
    	'post_type' => 'page',
    	'context'   => 'normal',
    	'priority'  => 'high',
    	'sections'  => array(
            array(
    			'name'   => 'page_page_options',
    			'title'  => 'Page Options',
    			'icon'   => 'fa fa-cog',
    			'fields' => array(
                    array(
    					'id'      => 'metabox_bigshop_home_page',
    					'type'    => 'select',
    					'options' => array(
    						'yes'   => esc_html__( 'Yes', 'bigshop' ),
    						'no'    => esc_html__( 'No', 'bigshop' ),
    					),
    					'title'   => esc_html__( 'Is home page', 'bigshop' ),
    					'default' => 'no',
    				)
    			),
    		),
    		array(
    			'name'   => 'page_theme_options',
    			'title'  => 'Theme Options',
    			'icon'   => 'fa fa-wordpress',
    			'fields' => array(
    				array(
    					'id'    => 'metabox_bigshop_logo',
    					'type'  => 'image',
    					'title' => 'Logo',
                        'default' => ''
    				),
    				array(
    					'id'      => 'metabox_bigshop_main_color',
    					'type'    => 'color_picker',
    					'title'   => 'Main Color',
    					'default' => $default_color,
    					'rgba'    => true,
    				),
                    array(
    					'id'      => 'metabox_bigshop_main_layout',
    					'type'    => 'select',
    					'options' => array(
    						'boxed-layout'   => esc_html__( 'Boxed layout', 'bigshop' ),
    						'wide-layout'    => esc_html__( 'Wide layout', 'bigshop' ),
    					),
    					'title'   => esc_html__( 'Choose main layout', 'bigshop' ),
    					'default' => $default_main_layout,
    				)
    			),
    		),
    		array(
    			'name'   => 'header_footer_theme_options',
    			'title'  => 'Header & Footer Settings',
    			'icon'   => 'fa fa-folder-open-o',
    			'fields' => array(
    				array(
    					'id'         => 'bigshop_metabox_used_header',
    					'type'       => 'select_preview',
    					'title'      => esc_html__( 'Header Layout', 'bigshop' ),
    					'desc'       => esc_html__( 'Select a header layout', 'bigshop' ),
    					'options'    => $data_meta->header_options,
    					'default'    => $default_header,
    					'attributes' => array(
    						'data-depend-id' => 'bigshop_metabox_used_header',
    					),
    				),
    				array(
                        'title'    => esc_html__('The number of visible vertical menu items', 'bigshop'),
                        'desc' => esc_html__('The number of visible vertical menu items', 'bigshop'),
                        'id'       => 'bigshop_metabox_vertical_menu_item_visible',
                        'default'  => $default_vertical_items,
                        'type'     => 'text',
                        'validate' => 'numeric',
                    ),
    				array(
    					'id'      => 'bigshop_metabox_footer_options',
    					'type'    => 'select_preview',
    					'title'   => esc_html__( 'Select Footer Builder', 'bigshop' ),
    					'options' => $data_meta->footer_preview,
    					'default' => $default_footer,
    				),
    			),
    		),
    	),
    );

}//End check enable metabox
// -----------------------------------------
// Page Footer Meta box Options            -
// -----------------------------------------

$options[] = array(
	'id'        => '_custom_footer_options',
	'title'     => 'Custom Footer Options',
	'post_type' => 'footer',
	'context'   => 'normal',
	'priority'  => 'high',
	'sections'  => array(
		array(
			'name'   => 'FOOTER STYLE',
			'fields' => array(
				array(
					'id'       => 'bigshop_footer_style',
					'type'     => 'select_preview',
					'title'    => esc_html__( 'Footer Style', 'bigshop' ),
					'subtitle' => esc_html__( 'Select a Footer Style', 'bigshop' ),
					'options'  => $data_meta->footer_options,
					'default'  => 'default',
				),
			),
		),
	),
);

// -----------------------------------------
// Page Side Meta box Options              -
// -----------------------------------------
$options[] = array(
	'id'        => '_custom_page_side_options',
	'title'     => 'Custom Page Side Options',
	'post_type' => 'page',
	'context'   => 'side',
	'priority'  => 'default',
	'sections'  => array(
		array(
			'name'   => 'page_option',
			'fields' => array(
				array(
					'id'      => 'sidebar_page_layout',
					'type'    => 'image_select',
					'title'   => 'Single Post Sidebar Position',
					'desc'    => 'Select sidebar position on Page.',
					'options' => array(
						'left'  => BIGSHOP_TOOLKIT_URL . '/includes/core/assets/images/left-sidebar.png',
						'right' => BIGSHOP_TOOLKIT_URL . '/includes/core/assets/images/right-sidebar.png',
						'full'  => BIGSHOP_TOOLKIT_URL . '/includes/core/assets/images/default-sidebar.png',
					),
					'default' => 'left',
				),
				array(
					'id'         => 'page_sidebar',
					'type'       => 'select',
					'title'      => 'Page Sidebar',
					'options'    => $data_meta->sidebars,
					'default'    => 'blue',
					'dependency' => array( 'sidebar_page_layout_full', '==', false ),
				),
				array(
					'id'    => 'page_extra_class',
					'type'  => 'text',
					'title' => 'Extra Class',
				),
			),
		),

	),
);

CSFramework_Metabox::instance( $options );
