<?php

if ( !defined( 'ABSPATH' ) ) {
	exit; // disable direct access
}

?>
<?php
define('BIGSHOP_IMAGE_DEMO_URL', plugins_url('/data/images/', __FILE__));
define('BIGSHOP_IMPORTER_URL', plugin_dir_url( __FILE__ ) );
define('BIGSHOP_IMPORTER_VERSION', '1.0.0' );

if (!class_exists('BIGSHOP_IMPORTER')) {

    class BIGSHOP_IMPORTER{

        public $data_demos = array();

        function __construct(){
            // SET DATA DEMOS
            $widget_file_url = plugins_url( '/data/widgets.wie',__FILE__ );
            $file_content_url= 'content.xml';
            
            $registed_menu   = array(
                'primary'        => esc_html__('Main Menu', 'bigshop'),
                'top_left_menu'  => esc_html__('Top Left Menu', 'bigshop'),
                'top_right_menu' => esc_html__('Top Right Menu', 'bigshop'),
                'vetical_menu'   => esc_html__('Vertical Menu', 'bigshop'),
            );
            
            $menu_location = array(
                'primary'        => esc_html__('Main Navigation', 'bigshop'),
                'top_left_menu'  => esc_html__('Top Left Menu', 'bigshop'),
                'top_right_menu' => esc_html__('Top Right Menu', 'bigshop'),
                'vetical_menu'   => esc_html__('Vertical Menu Demo', 'bigshop'),
            );
            
            $this->data_demos = array(
				array(
					'name'           => __( 'Demo 01', 'importer' ),
					'file_content'	 => $file_content_url,
					'revslider'      => plugins_url( '/data/revsliders/Bigshop-opt1.zip', __FILE__ ),
					'theme_option'   => plugins_url( '/data/theme_options/default.txt', __FILE__ ) ,
					'widget'         => $widget_file_url,
					'menus'          => $registed_menu,
					'homepage'       => 'Home 01',
					'blogpage'       => 'Blogs',
					'preview'        => plugins_url( '/data/previews/Demo1.jpg', __FILE__ ) ,
					'demo_link'      => 'http://bigshop.kutethemes.net/',
                    'menu_locations' => $menu_location
				),
                array(
					'name'           => __( 'Demo 02', 'importer' ),
					'file_content'	 => $file_content_url,
					'revslider'      => plugins_url( '/data/revsliders/Bigshop-opt2.zip', __FILE__ ),
					'theme_option'   => plugins_url( '/data/theme_options/default.txt', __FILE__ ) ,
					'widget'         => $widget_file_url,
					'menus'          => $registed_menu,
					'homepage'       => 'Home 02',
					'blogpage'       => 'Blogs',
					'preview'        => plugins_url( '/data/previews/Demo2.jpg', __FILE__ ) ,
					'demo_link'      => 'http://bigshop.kutethemes.net/home-02',
                    'menu_locations' => $menu_location
				),
                array(
					'name'           => __( 'Demo 03', 'importer' ),
					'file_content'	 => $file_content_url,
					'revslider'      => plugins_url( '/data/revsliders/Bigshop-opt3.zip', __FILE__ ),
					'theme_option'   => plugins_url( '/data/theme_options/default.txt', __FILE__ ) ,
					'widget'         => $widget_file_url,
					'menus'          => $registed_menu,
					'homepage'       => 'Home 03',
					'blogpage'       => 'Blogs',
					'preview'        => plugins_url( '/data/previews/Demo3.jpg', __FILE__ ) ,
					'demo_link'      => 'http://bigshop.kutethemes.net/home-03',
                    'menu_locations' => $menu_location
				),
                
			);
            
            // JS and css
            add_action('admin_enqueue_scripts', array($this, 'register_scripts'));
            
            /* Register ajax action */
            add_action('wp_ajax_bigshop_import_content', array($this, 'import_content'));
            add_action('wp_ajax_bigshop_import_revslider', array($this, 'import_revslider'));
            add_action('wp_ajax_bigshop_import_theme_options', array($this, 'import_theme_options'));
            add_action('wp_ajax_bigshop_import_widget', array($this, 'import_widget'));
            add_action('wp_ajax_bigshop_import_config', array($this, 'import_config'));
            
            add_action( 'init', array( $this, 'load_textdomain' ) );
            
        }
        
        function load_textdomain() {
            load_plugin_textdomain( 'bigshop-toolkit', false, plugin_dir_path( __FILE__ ) . '/languages' );
        }
        
        function register_scripts(){
            wp_enqueue_style('bigshop-admin-importer-style', BIGSHOP_IMPORTER_URL . 'css/style.css' );
            wp_enqueue_style('bigshop-admin-importer-circle', BIGSHOP_IMPORTER_URL . 'css/circle.css' );
            wp_enqueue_script('bigshop-admin-importer-script', BIGSHOP_IMPORTER_URL . 'js/script.js', array('jquery'), false, true);
            add_thickbox();
        }

        function importer_page_content(){
            ?>
            <div id="my-content-id" style="display:none;">
                <p>
                    This is my hidden content! It will appear in ThickBox when the link is clicked.
                </p>
            </div>

            <div class="bigshop-importer-wrapper">
                 <h2 class="heading">Bigshop Demos</h2>
                <div class="text">Bigshop brings for you a number of unique designs for your website. Our demos were carefully tested so you don’t have to create everything from scratch. With the theme demos you know exactly which predefined template is perfectly designed to start building upon. Each demo is fully customizable (fonts, colors and layouts)</div>
                <?php if ($this->data_demos) : ?>
                    <div class="options">
                        <?php $i = 0;
                            foreach ($this->data_demos as $key => $data): ?>
                                <div id="option-<?php echo $key; ?>" class="option">
                                    <div class="inner">
                                        <div class="bar-progress">
                                            <div class="meter">
                                                <span class="percent" style="width: 5%;"></span>
                                            </div>
                                            <div class="bar-progress-text">Installing...</div>
                                        </div>
                                        <div class="preview">
                                            <img src="<?php echo $data['preview']; ?>">
                                        </div>
                                        <div class="bigshop-control">
                                            <div class="control-inner">
                                                <h3 class="demo-name"><?php echo $data['name']; ?></h3>
                                                <div id="confirm-option-<?php echo $key; ?>" style="display:none;">
                                                    <div class="confirm-option-box">
                                                        <p>
                                                            Are you sure you want to install the full demo? It will import homepages, sample content with images, backgrounds, template layouts, fonts, colors.
                                                        </p>
                                                        <p>
                                                            <strong>Notice</strong>:
                                                            <ol style="margin: 0; margin-left: 15px;">
                                                                <li>Importer will help you build your site look like our demo. Importing data is recommended on fresh install.</li>
                                                                <li>Please ensure you have already installed and activated Bigshop Toolkit, WooCommerce, Visual Composer and Slider Revolution plugins</li>
                                                                <li>Please note that importing data only builds a frame for your website. It will not import all demo contents and images.</li>
                                                                <li>It can take a few minutes to complete. Please don't close your browser while importing.</li>
                                                            </ol>
                                                        </p>
                                                        <p><strong>Select the options below which you want to import:</strong></p>
                                                        <label for="demo-content-<?php echo $key; ?>">
                                                            <input id="demo-content-<?php echo $key; ?>" type="checkbox" value="1" checked="checked">
                                                            Demo content
                                                        </label>
                                                        <label for="config-<?php echo $key; ?>">
                                                            <input id="config-<?php echo $key; ?>" type="checkbox" value="1" checked="checked">
                                                            Config Demo
                                                        </label>
                                                        <label for="theme-option-<?php echo $key; ?>">
                                                            <input id="theme-option-<?php echo $key; ?>" type="checkbox" value="1" checked="checked">
                                                            Theme options
                                                        </label>
                                                        <label for="widget-<?php echo $key; ?>">
                                                            <input id="widget-<?php echo $key; ?>" type="checkbox" value="1" checked="checked">
                                                            Widget
                                                        </label>
                                                        <?php if ($data['revslider'] != ''): ?>
                                                            <label for="revslider-<?php echo $key; ?>">
                                                                <input id="revslider-<?php echo $key; ?>" type="checkbox" value="1" checked="checked">
                                                                Slider Revolution
                                                            </label>
                                                        <?php endif; ?>

                                                        <div class="confirm-buttons">
                                                            <button data-id="<?php echo $key; ?>" data-optionid="<?php echo $key; ?>" class="bigshop-button-import">Yes</button>
                                                            <button class="bigshop-button-close-import">No</button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="bottom-control">


                                                    <a title="Confirm Install" href="#TB_inline?width=480&height=395&inlineId=confirm-option-<?php echo $key; ?>" class="thickbox button zora-button-confirm">Install</a>
                                                    <a target="_blank" class="button more" href="<?php echo $data['demo_link']; ?>">Preview</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                    </div>
                <?php else: ?>
                    <p>No data import</p>
                <?php endif; ?>
            </div>
            <?php
        }

        /* DOWNLOAD FILE */
        function download($url = "", $file_name = ""){
            $filepath = "";
            if ($url != "") {
                $upload_dir = wp_upload_dir();
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $data = curl_exec($ch);
                curl_close($ch);
                $destination = $upload_dir['path'] . "/" . $file_name;
                $file = fopen($destination, "w+");
                fputs($file, $data);
                fclose($file);
                $filepath = $destination;
            }

            return $filepath;
        }

        /* Include Importer Classes */
        function include_importer_classes(){
            if (!class_exists('WP_Importer')) {
                include ABSPATH . 'wp-admin/includes/class-wp-importer.php';
            }
            if (!class_exists('BIGSHOP_WP_Import')) {
                if (file_exists(dirname(__FILE__) . '/inc/wordpress-importer.php')) {
                    include_once dirname(__FILE__) . '/inc/wordpress-importer.php';
                }

            }
        }

        /* Dont Resize image while importing */
        function no_resize_image($sizes){
            return array();
        }

        function before_content_import(){
            // Set some WooCommerce $attributes
            if (class_exists('WooCommerce')) {
                global $wpdb;

                if (current_user_can('administrator')) {
                    $attributes = array(
                        array(
                            'attribute_label'   => 'Color',
                            'attribute_name'    => 'color',
                            'attribute_type'    => 'select',
                            'attribute_orderby' => 'menu_order',
                            'attribute_public'  => '0'
                        ),
                        array(
                            'attribute_label'   => 'Size',
                            'attribute_name'    => 'size',
                            'attribute_type'    => 'select',
                            'attribute_orderby' => 'menu_order',
                            'attribute_public'  => '0'
                        ),
                    );

                    foreach ($attributes as $attribute):
                        if (empty($attribute['attribute_name']) || empty($attribute['attribute_label'])) {
                            return new WP_Error('error', __('Please, provide an attribute name and slug.', 'woocommerce'));
                        } elseif (($valid_attribute_name = $this->wc_valid_attribute_name($attribute['attribute_name'])) && is_wp_error($valid_attribute_name)) {
                            return $valid_attribute_name;
                        } elseif (taxonomy_exists(wc_attribute_taxonomy_name($attribute['attribute_name']))) {
                            return new WP_Error('error', sprintf(__('Slug "%s" is already in use. Change it, please.', 'woocommerce'), sanitize_title($attribute['attribute_name'])));
                        }

                        $wpdb->insert($wpdb->prefix . 'woocommerce_attribute_taxonomies', $attribute);

                        do_action('woocommerce_attribute_added', $wpdb->insert_id, $attribute);

                        $attribute_name = wc_sanitize_taxonomy_name('pa_' . $attribute['attribute_name']);

                        if (!taxonomy_exists($attribute_name)) {
                            $args = array(
                                'hierarchical' => true,
                                'show_ui'      => false,
                                'query_var'    => true,
                                'rewrite'      => false,
                            );
                            register_taxonomy($attribute_name, array('product'), $args);
                        }


                        flush_rewrite_rules();
                        delete_transient('wc_attribute_taxonomies');
                    endforeach;
                }
            }
        }

        function wc_valid_attribute_name($attribute_name){
            if (!class_exists('WooCommerce')) {
                return false;
            }

            if (strlen($attribute_name) >= 28) {
                return new WP_Error('error', sprintf(__('Slug "%s" is too long (28 characters max). Shorten it, please.', 'woocommerce'), sanitize_title($attribute_name)));
            } elseif (wc_check_if_attribute_name_is_reserved($attribute_name)) {
                return new WP_Error('error', sprintf(__('Slug "%s" is not allowed because it is a reserved term. Change it, please.', 'woocommerce'), sanitize_title($attribute_name)));
            }

            return true;
        }

        function import_content(){
            set_time_limit(0);
            if (!defined('WP_LOAD_IMPORTERS')) {
                define('WP_LOAD_IMPORTERS', true);
            }
            $optionid = isset($_POST['optionid']) ? $_POST['optionid'] : "";
            add_filter('intermediate_image_sizes_advanced', array($this, 'no_resize_image'));

            if ($optionid != "") {
                $demo = $this->data_demos[$optionid];
                if (is_array($demo)) {
                    $this->before_content_import();

                    echo $filepath = dirname(__FILE__) . '/data/' . $demo['file_content'];
                    if (file_exists($filepath)) {
                        try {
                            $this->include_importer_classes();
                            $importer = new BIGSHOP_WP_Import();
                            $importer->fetch_attachments = true;
                            $importer->import($filepath);
                            echo 'Successful Import Demo Content';
                        } catch (Exception $e) {
                            echo 'Caught exception: ', $e->getMessage(), "\n";
                        }

                    }

                }
            }
            wp_die();
        }

        function import_theme_options(){
            $optionid = isset($_POST['optionid']) ? $_POST['optionid'] : "";
            if ($optionid != "") {
                $demo = $this->data_demos[$optionid];
                if (!is_array($demo)) {
                    return;
                }
            }

            $url = $demo['theme_option'];


            $theme_options_url = untrailingslashit($url);
            $theme_options_txt = wp_remote_get($theme_options_url);


            if (empty($theme_options_txt)) {
                wp_die();
            }

            $theme_option_data = $theme_options_txt['body'];
            $options = $this->cs_decode_string($theme_option_data);
            if (!empty($options)) {
                update_option('_cs_options', $options);
            }

            wp_die();
        }
        function cs_decode_string( $string ) {
            return unserialize( gzuncompress( stripslashes( call_user_func( 'base'. '64' .'_decode', rtrim( strtr( $string, '-_', '+/' ), '=' ) ) ) ) );
        }
        /* import Sidebar Content */
        function import_widget(){
            $optionid = isset($_POST['optionid']) ? $_POST['optionid'] : "";
            if ($optionid != "") {
                $demo = $this->data_demos[$optionid];
                if (!is_array($demo)) {
                    return;
                }
            }

            $url = $demo['widget'];

            // Get file contents and decode
            $data = file_get_contents($url);
            $data = json_decode($data);

            global $wp_registered_sidebars;
            // Have valid data?
            // If no data or could not decode
            if (empty($data) || !is_object($data)) {
                wp_die();
            }
            // Hook before import
            do_action('wie_before_import');
            $data = apply_filters('wie_import_data', $data);

            // Get all available widgets site supports
            $available_widgets = $this->bigshop_available_widgets();

            // Get all existing widget instances
            $widget_instances = array();
            foreach ($available_widgets as $widget_data) {
                $widget_instances[$widget_data['id_base']] = get_option('widget_' . $widget_data['id_base']);
            }

            // Begin results
            $results = array();

            // Loop import data's sidebars
            foreach ($data as $sidebar_id => $widgets) {

                // Skip inactive widgets
                // (should not be in export file)
                if ('wp_inactive_widgets' == $sidebar_id) {
                    continue;
                }

                // Check if sidebar is available on this site
                // Otherwise add widgets to inactive, and say so
                if (isset($wp_registered_sidebars[$sidebar_id])) {
                    $sidebar_available = true;
                    $use_sidebar_id = $sidebar_id;
                    $sidebar_message_type = 'success';
                    $sidebar_message = '';
                } else {
                    $sidebar_available = false;
                    $use_sidebar_id = 'wp_inactive_widgets'; // add to inactive if sidebar does not exist in theme
                    $sidebar_message_type = 'error';
                    $sidebar_message = __('Sidebar does not exist in theme (using Inactive)', 'widget-importer-exporter');
                }

                // Result for sidebar
                $results[$sidebar_id]['name'] = !empty($wp_registered_sidebars[$sidebar_id]['name']) ? $wp_registered_sidebars[$sidebar_id]['name'] : $sidebar_id; // sidebar name if theme supports it; otherwise ID
                $results[$sidebar_id]['message_type'] = $sidebar_message_type;
                $results[$sidebar_id]['message'] = $sidebar_message;
                $results[$sidebar_id]['widgets'] = array();

                // Loop widgets
                foreach ($widgets as $widget_instance_id => $widget) {

                    $fail = false;

                    // Get id_base (remove -# from end) and instance ID number
                    $id_base = preg_replace('/-[0-9]+$/', '', $widget_instance_id);
                    $instance_id_number = str_replace($id_base . '-', '', $widget_instance_id);

                    // Does site support this widget?
                    if (!$fail && !isset($available_widgets[$id_base])) {
                        $fail = true;
                        $widget_message_type = 'error';
                        $widget_message = __('Site does not support widget', 'widget-importer-exporter'); // explain why widget not imported
                    }

                    // Filter to modify settings object before conversion to array and import
                    // Leave this filter here for backwards compatibility with manipulating objects (before conversion to array below)
                    // Ideally the newer wie_widget_settings_array below will be used instead of this
                    $widget = apply_filters('wie_widget_settings', $widget); // object

                    // Convert multidimensional objects to multidimensional arrays
                    // Some plugins like Jetpack Widget Visibility store settings as multidimensional arrays
                    // Without this, they are imported as objects and cause fatal error on Widgets page
                    // If this creates problems for plugins that do actually intend settings in objects then may need to consider other approach: https://wordpress.org/support/topic/problem-with-array-of-arrays
                    // It is probably much more likely that arrays are used than objects, however
                    $widget = json_decode(json_encode($widget), true);

                    // Filter to modify settings array
                    // This is preferred over the older wie_widget_settings filter above
                    // Do before identical check because changes may make it identical to end result (such as URL replacements)
                    $widget = apply_filters('wie_widget_settings_array', $widget);

                    // Does widget with identical settings already exist in same sidebar?
                    if (!$fail && isset($widget_instances[$id_base])) {

                        // Get existing widgets in this sidebar
                        $sidebars_widgets = get_option('sidebars_widgets');
                        $sidebar_widgets = isset($sidebars_widgets[$use_sidebar_id]) ? $sidebars_widgets[$use_sidebar_id] : array(); // check Inactive if that's where will go

                        // Loop widgets with ID base
                        $single_widget_instances = !empty($widget_instances[$id_base]) ? $widget_instances[$id_base] : array();
                        foreach ($single_widget_instances as $check_id => $check_widget) {

                            // Is widget in same sidebar and has identical settings?
                            if (in_array("$id_base-$check_id", $sidebar_widgets) && (array)$widget == $check_widget) {

                                $fail = true;
                                $widget_message_type = 'warning';
                                $widget_message = __('Widget already exists', 'widget-importer-exporter'); // explain why widget not imported

                                break;

                            }

                        }

                    }

                    // No failure
                    if (!$fail) {

                        // Add widget instance
                        $single_widget_instances = get_option('widget_' . $id_base); // all instances for that widget ID base, get fresh every time
                        $single_widget_instances = !empty($single_widget_instances) ? $single_widget_instances : array('_multiwidget' => 1); // start fresh if have to
                        $single_widget_instances[] = $widget; // add it

                        // Get the key it was given
                        end($single_widget_instances);
                        $new_instance_id_number = key($single_widget_instances);

                        // If key is 0, make it 1
                        // When 0, an issue can occur where adding a widget causes data from other widget to load, and the widget doesn't stick (reload wipes it)
                        if ('0' === strval($new_instance_id_number)) {
                            $new_instance_id_number = 1;
                            $single_widget_instances[$new_instance_id_number] = $single_widget_instances[0];
                            unset($single_widget_instances[0]);
                        }

                        // Move _multiwidget to end of array for uniformity
                        if (isset($single_widget_instances['_multiwidget'])) {
                            $multiwidget = $single_widget_instances['_multiwidget'];
                            unset($single_widget_instances['_multiwidget']);
                            $single_widget_instances['_multiwidget'] = $multiwidget;
                        }

                        // Update option with new widget
                        update_option('widget_' . $id_base, $single_widget_instances);

                        // Assign widget instance to sidebar
                        $sidebars_widgets = get_option('sidebars_widgets'); // which sidebars have which widgets, get fresh every time
                        $new_instance_id = $id_base . '-' . $new_instance_id_number; // use ID number from new widget instance
                        $sidebars_widgets[$use_sidebar_id][] = $new_instance_id; // add new instance to sidebar
                        update_option('sidebars_widgets', $sidebars_widgets); // save the amended data

                        // After widget import action
                        $after_widget_import = array(
                            'sidebar'           => $use_sidebar_id,
                            'sidebar_old'       => $sidebar_id,
                            'widget'            => $widget,
                            'widget_type'       => $id_base,
                            'widget_id'         => $new_instance_id,
                            'widget_id_old'     => $widget_instance_id,
                            'widget_id_num'     => $new_instance_id_number,
                            'widget_id_num_old' => $instance_id_number
                        );
                        do_action('wie_after_widget_import', $after_widget_import);

                        // Success message
                        if ($sidebar_available) {
                            $widget_message_type = 'success';
                            $widget_message = __('Imported', 'widget-importer-exporter');
                        } else {
                            $widget_message_type = 'warning';
                            $widget_message = __('Imported to Inactive', 'widget-importer-exporter');
                        }

                    }

                    // Result for widget instance
                    $results[$sidebar_id]['widgets'][$widget_instance_id]['name'] = isset($available_widgets[$id_base]['name']) ? $available_widgets[$id_base]['name'] : $id_base; // widget name or ID if name not available (not supported by site)
                    $results[$sidebar_id]['widgets'][$widget_instance_id]['title'] = !empty($widget['title']) ? $widget['title'] : __('No Title', 'widget-importer-exporter'); // show "No Title" if widget instance is untitled
                    $results[$sidebar_id]['widgets'][$widget_instance_id]['message_type'] = $widget_message_type;
                    $results[$sidebar_id]['widgets'][$widget_instance_id]['message'] = $widget_message;

                }

            }

            // Hook after import
            do_action('wie_after_import');

            // Return results
            return apply_filters('wie_import_results', $results);


            wp_die();
        }

        function bigshop_available_widgets() {

            global $wp_registered_widget_controls;

            $widget_controls = $wp_registered_widget_controls;

            $available_widgets = array();

            foreach ($widget_controls as $widget) {

                if (!empty($widget['id_base']) && !isset($available_widgets[$widget['id_base']])) { // no dupes

                    $available_widgets[$widget['id_base']]['id_base'] = $widget['id_base'];
                    $available_widgets[$widget['id_base']]['name'] = $widget['name'];

                }

            }

            return apply_filters('wie_available_widgets', $available_widgets);

        }

        /* Import Revolution Slider */
        function import_revslider(){
            if ( class_exists( 'RevSlider' ) ) {
                $rev_files = array();
                $rev_directory = dirname(__FILE__) . '/data/revsliders/';
                foreach (glob($rev_directory . '*.zip') as $filename) {
                    $filename = basename($filename);
                    $rev_files[] = $rev_directory . $filename;
                }
                
                $slider = new RevSlider();
                foreach( $rev_files as $filepath ){
                    $slider->importSliderFromPost(true,true,$filepath);  
                }
            }
            
            die();
            
        }

        function import_config(){
            $optionid = isset($_POST['optionid']) ? $_POST['optionid'] : "";
            if ($optionid != "") {
                $demo = $this->data_demos[$optionid];
                if (!is_array($demo)) {
                    return;
                }
            }

            $this->woocommerce_settings();
            $this->menu_locations($demo);
            $this->update_options($demo);
            wp_die();
        }

        /* WooCommerce Settings */
        function woocommerce_settings(){
            $woopages = array(
                'woocommerce_shop_page_id'      => 'Shop',
                'woocommerce_cart_page_id'      => 'Cart',
                'woocommerce_checkout_page_id'  => 'Checkout',
                'woocommerce_myaccount_page_id' => 'My account'
            );
            foreach ($woopages as $woo_page_name => $woo_page_title) {
                $woopage = get_page_by_title($woo_page_title);
                if (isset($woopage->ID) && $woopage->ID) {
                    update_option($woo_page_name, $woopage->ID);
                }
            }

            if (class_exists('YITH_Woocompare')) {
                update_option('yith_woocompare_compare_button_in_products_list', 'yes');
                update_option('yith_woocompare_is_button', 'button');
            }

            if (class_exists('WC_Admin_Notices')) {
                WC_Admin_Notices::remove_notice('install');
            }
            delete_transient('_wc_activation_redirect');
            /* UPDTE IMAGE SIZE */
            $catalog = array(
				'width' 	=> '370',	// px
				'height'	=> '417',	// px
				'crop'		=> 1 		// true
			);

			$single = array(
				'width' 	=> '600',	// px
				'height'	=> '676',	// px
				'crop'		=> 1 		// true
			);

			$thumbnail = array(
				'width' 	=> '180',	// px
				'height'	=> '203',	// px
				'crop'		=> 1 		// false
			);

            // Image sizes
            update_option('shop_catalog_image_size', $catalog);        // Product category thumbs
            update_option('shop_single_image_size', $single);        // Single product image
            update_option('shop_thumbnail_image_size', $thumbnail);    // Image gallery thumbs

            flush_rewrite_rules();
        }

        function menu_locations($demo){
            $menu_location = array();
        	$locations = get_theme_mod('nav_menu_locations');
            $menus = wp_get_nav_menus();
            
            if( isset( $demo['menu_locations'] ) && is_array( $demo['menu_locations'] ) ){
                if ($menus) {
                    foreach ($menus as $menu) {
                        foreach ($demo['menu_locations'] as $key => $value) {
                            if ($menu->name == $value) {
                                $menu_location[$key] = $menu->term_id;
                            }
                        }
                    }
                }
                
                set_theme_mod('nav_menu_locations', $menu_location);
            }else if ( isset( $demo['menus'] ) && is_array( $demo['menus'] ) ) {
                $menu_location = $locations;
                set_theme_mod('nav_menu_locations', $menu_location);
            }

        }

        /* Update Options */
        function update_options($demo){
            if (isset($demo['homepage']) && $demo['homepage'] != "") {
                // Home page
                $homepage = get_page_by_title($demo['homepage']);
                if (isset($homepage) && $homepage->ID) {
                    update_option('show_on_front', 'page');
                    update_option('page_on_front', $homepage->ID);
                }
            }

            // Blog page
            if (isset($demo['blogpage']) && $demo['blogpage'] != "") {
                $post_page = get_page_by_title($demo['blogpage']);
                if (isset($post_page) && $post_page->ID) {
                    update_option('show_on_front', 'page');
                    update_option('page_for_posts', $post_page->ID);
                }
            }
        }
    }
    new BIGSHOP_IMPORTER();
}