jQuery(document).ready(function($){
	"use strict";

	var bigshop_import_percent = 0, bigshop_import_percent_increase = 0, bigshop_import_index_request = 0;
	var bigshop_arr_import_request_data = new Array();
	var optionid ='';

	$(document).on('click','.bigshop-button-import',function(){
        var id = $(this).data('id');
        optionid = $(this).data('optionid');
        var thisOption  = $('#option-'+ optionid);
        $('.bigshop-importer-wrapper').find('.option').addClass('disable');
        thisOption.removeClass('disable');

		var import_demo_content = false,
			import_theme_options = false,
			import_widget = false,
			import_revslider = false,
			import_config = false;

		if($('#demo-content-'+id).is(':checked')){
			import_demo_content = true;
		}
		if($('#theme-option-'+id).is(':checked')){
			import_theme_options = true;
		}
		if($('#widget-'+id).is(':checked')){
			import_widget = true;
		}
		if($('#revslider-'+id).is(':checked')){
			import_revslider = true;
		}
		if($('#config-'+id).is(':checked')){
			import_config = true;
		}

       	$('#TB_closeWindowButton').trigger('click');
		
		// Demo content
		if( import_demo_content ){
			var data = {'action' : 'bigshop_import_content','optionid':optionid,'text':'Import Demo content...'};
			bigshop_arr_import_request_data.push( data );
		}

		if( import_theme_options ){
			bigshop_arr_import_request_data.push( {'action' : 'bigshop_import_theme_options','optionid':optionid,'text':'Import Theme options...'} );
		}
		
		if( import_widget ){
			bigshop_arr_import_request_data.push( {'action' : 'bigshop_import_widget','optionid':optionid,'text':'Import Widgets...'} );
		}
		
		if( import_revslider ){
			bigshop_arr_import_request_data.push( {'action' : 'bigshop_import_revslider','optionid':optionid,'text':'Import Revslider...'} );
		}
		
		if( import_config ){
			bigshop_arr_import_request_data.push( {'action' : 'bigshop_import_config','optionid':optionid,'text':'Config Demo...'} );
		}
		
		var total_ajaxs = bigshop_arr_import_request_data.length;
		
		if( total_ajaxs == 0 ){
			return;
		}
		
		bigshop_import_percent_increase = (100 / total_ajaxs);
		bigshop_import_ajax_handle();
	})

	function bigshop_import_ajax_handle(){

		if( bigshop_import_index_request == bigshop_arr_import_request_data.length ){
			 $('#option-'+optionid).addClass('complete');
            $('#option-'+optionid+'').find('.bar-progress .bar-progress-text').html('All done. Have fun!');
			return;
		}
        $('#option-'+optionid).find('.bar-progress').show();
		$.ajax({
			type: 'POST',
			url: ajaxurl,
			async: true,
			data: bigshop_arr_import_request_data[bigshop_import_index_request],
			complete: function(jqXHR, textStatus){
                $('#option-'+optionid+'').find('.bar-progress .bar-progress-text').html(bigshop_arr_import_request_data[bigshop_import_index_request]["text"]);
				bigshop_import_percent += bigshop_import_percent_increase;
				bigshop_progress_bar_handle();
				bigshop_import_index_request++;
				setTimeout(function(){
					bigshop_import_ajax_handle();
				}, 200);
			}
		});
	}

	function bigshop_progress_bar_handle(){

		if( bigshop_import_percent > 100 ){
			bigshop_import_percent = 100;
		}
		var progress_bar = $('#option-'+optionid).find('.bar-progress');
		progress_bar.find('.percent').css({'width': Math.ceil( bigshop_import_percent ) + '%'});
	}

	$(document).on('click','.bigshop-button-close-import',function () {
		$('#TB_closeWindowButton').trigger('click');
    })
});