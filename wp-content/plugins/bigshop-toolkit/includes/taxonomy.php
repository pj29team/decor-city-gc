<?php
function bigshop_toolkit_register_brands_taxonomy() {
    $labels = array(
        'name'                       => __( 'Brands', 'bigshop-toolkit' ),
        'singular_name'              => __( 'Brand', 'bigshop-toolkit' ),
        'menu_name'                  => __( 'Brands', 'bigshop-toolkit' ),
        'all_items'                  => __( 'All Brands', 'bigshop-toolkit' ),
        'edit_item'                  => __( 'Edit Brand', 'bigshop-toolkit' ),
        'view_item'                  => __( 'View Brand', 'bigshop-toolkit' ),
        'update_item'                => __( 'Update Brand', 'bigshop-toolkit' ),
        'add_new_item'               => __( 'Add New Brand', 'bigshop-toolkit' ),
        'new_item_name'              => __( 'New Brand Name', 'bigshop-toolkit' ),
        'parent_item'                => __( 'Parent Brand', 'bigshop-toolkit' ),
        'parent_item_colon'          => __( 'Parent Brand:', 'bigshop-toolkit' ),
        'search_items'               => __( 'Search Brands', 'bigshop-toolkit' ),
        'popular_items'              => __( 'Popular Brands', 'bigshop-toolkit' ),
        'separate_items_with_commas' => __( 'Separate brands with commas', 'bigshop-toolkit' ),
        'add_or_remove_items'        => __( 'Add or remove brands', 'bigshop-toolkit' ),
        'choose_from_most_used'      => __( 'Choose from the most used brands', 'bigshop-toolkit' ),
        'not_found'                  => __( 'No brands found', 'bigshop-toolkit' )
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'query_var'         => true,
        'public'            => true,
        'show_admin_column' => true,
        'rewrite'           => array(
            'slug'          => 'bigshop_product_brands',
            'hierarchical'  => true,
        )
    );

    register_taxonomy( 'bigshop_product_brands', array( 'product' ), $args );
    flush_rewrite_rules();
    
}

if ( in_array( 'woocommerce/woocommerce.php',get_option( 'active_plugins' )  ) ) {
    add_action( 'woocommerce_init', 'bigshop_toolkit_register_brands_taxonomy' );
}