<?php
    if( !class_exists('Bigshop_Wellcome') ){
        class Bigshop_Wellcome{

            public $tabs = array();

            public function __construct() {
                $this->set_tabs();
                // Add action to enqueue scripts.
                add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
                add_action( 'admin_menu', array( $this, 'admin_menu' ),9 );
            }

            public  function admin_menu(){
                if ( current_user_can( 'edit_theme_options' ) ) {
                    add_menu_page( 'Bigshop', 'Bigshop', 'manage_options', 'bigshop_menu', array( $this, 'wellcome' ),BIGSHOP_TOOLKIT_URL . '/assets/images/menu-icon.png', 2 );
                    add_submenu_page( 'bigshop_menu', 'Bigshop Dashboard', 'Dashboard', 'manage_options', 'bigshop_menu',  array( $this, 'wellcome' ) );
                }
            }

            public  function  enqueue_scripts(){
                wp_enqueue_style( 'chosen-admin', get_template_directory_uri() . '/assets/css/chosen.min.css' );
                wp_enqueue_style( 'bigshop-admin', get_template_directory_uri() . '/framework/assets/css/admin.css' );
                wp_enqueue_script( 'chosen-admin', get_template_directory_uri() . '/assets/js/chosen.jquery.min.js', array( 'jquery' ), false, true );
                wp_enqueue_script( 'bigshop-admin', get_template_directory_uri() . '/framework/assets/js/admin.js', array( 'jquery' ), false, true );
            }

            public function set_tabs(){
                $this->tabs = array(
                    'dashboard' => esc_html__('Wellcome','bigshop-toolkit'),
                    'demos' => esc_html__('Install demos','bigshop-toolkit'),
                    'plugins' => esc_html__('Install Plugins','bigshop-toolkit'),
                    'support' => esc_html__('Support','bigshop-toolkit')
                );

            }
            public  function active_plugin(){
                if (empty($_GET['magic_token']) || wp_verify_nonce($_GET['magic_token'], 'panel-plugins') === false) {
                    esc_html_e('Permission denied','bigshop-toolkit');
                    die;
                }

                if( isset($_GET['plugin_slug']) && $_GET['plugin_slug']!=""){
                    $plugin_slug = $_GET['plugin_slug'];
                    $plugins = TGM_Plugin_Activation::$instance->plugins;
                    foreach ($plugins as $plugin) {
                        if ($plugin['slug'] == $plugin_slug) {
                            activate_plugins($plugin['file_path']);
                            ?>
                            <script type="text/javascript">
                                window.location = "admin.php?page=bigshop_menu&tab=plugins";
                            </script>
                            <?php
                            break;
                        }
                    }
                }

            }

            public function deactivate_plugin(){
                if (empty($_GET['magic_token']) || wp_verify_nonce($_GET['magic_token'], 'panel-plugins') === false) {
                    esc_html_e('Permission denied','bigshop-toolkit');
                    die;
                }

                if( isset($_GET['plugin_slug']) && $_GET['plugin_slug']!=""){
                    $plugin_slug = $_GET['plugin_slug'];
                    $plugins = TGM_Plugin_Activation::$instance->plugins;
                    foreach ($plugins as $plugin) {
                        if ($plugin['slug'] == $plugin_slug) {
                            deactivate_plugins($plugin['file_path']);
                            ?>
                            <script type="text/javascript">
                                window.location = "admin.php?page=bigshop_menu&tab=plugins";
                            </script>
                            <?php
                            break;
                        }
                    }
                }

            }
            public  function intall_plugin(){

            }

            public function dashboard(){
                ?>
                <div class="dashboard">
                    <h1>Wellcome to Bigshop</h1>
                    <p class="about-text">Thanks for using our theme, we have worked very hard to release a great product and we will do our absolute best to support this theme and fix all the issues. </p>
                    <div class="dashboard-intro">
                        <div class="image">
                            <img src="<?php echo esc_url(get_template_directory_uri().'/screenshot.jpg');?>" alt="">
                        </div>
                        <div class="intro">
                            <p><strong>Bigshop</strong> is a modern, clean and professional WooCommerce Wordpress Theme, It is fully responsive, it looks stunning on all types of screens and devices.</p>
                            <h2>Quick Setings</h2>
                            <ul>
                                <li><a href="admin.php?page=bigshop_menu&tab=demos">Install Demos</a></li>
                                <li><a href="admin.php?page=bigshop_menu&tab=plugins">Install Plugins</a></li>
                                <li><a href="admin.php?page=bigshop_options">Theme Options</a></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <?php
                $this->support();
            }

            public function wellcome(){

                /* deactivate_plugin */
                if( isset($_GET['action']) && $_GET['action'] == 'deactivate_plugin'){
                    $this->deactivate_plugin();
                }
                /* deactivate_plugin */
                if( isset($_GET['action']) && $_GET['action'] == 'active_plugin'){
                    $this->active_plugin();
                }

                $tab = 'dashboard';
                if( isset($_GET['tab'])){
                    $tab = $_GET['tab'];
                }
                ?>
                <div class="bigshop-wrap">
                    <div id="tabs-container" role="tabpanel">
                        <div class="nav-tab-wrapper">
                            <?php foreach ($this->tabs as $key => $value ):?>
                                <a class="nav-tab bigshop-nav <?php if( $tab == $key ):?> active<?php endif;?>" href="admin.php?page=bigshop_menu&tab=<?php echo esc_attr($key);?>"><?php echo esc_html($value);?></a>
                            <?php endforeach;?>
                        </div>
                        <div class="tab-content">
                            <?php $this->$tab();?>
                        </div>
                    </div>
                </div>
                <?php
            }
            public static function demos(){
                if( class_exists('BIGSHOP_IMPORTER')){
                    $bigshop_importer = new BIGSHOP_IMPORTER();
                    $bigshop_importer->importer_page_content();
                }
            }
            public static function plugins(){
                $bigshop_tgm_theme_plugins = TGM_Plugin_Activation::$instance->plugins;
                $tgm =   TGM_Plugin_Activation::$instance;

                $status_class = "";
                ?>
                <div class="plugins rp-row">
                    <?php
                        $wp_plugin_list = get_plugins();
                        foreach ($bigshop_tgm_theme_plugins as $bigshop_tgm_theme_plugin ){
                            if( $tgm->is_plugin_active($bigshop_tgm_theme_plugin['slug'])){
                                $status_class = 'is-active';
                                if( $tgm->does_plugin_have_update($bigshop_tgm_theme_plugin['slug'])){
                                    $status_class = 'plugin-update';
                                }
                            }else if (isset($wp_plugin_list[$bigshop_tgm_theme_plugin['file_path']])) {
                                $status_class = 'plugin-inactive';
                            }else{
                                $status_class ='no-intall';
                            }
                            ?>
                            <div class="rp-col">
                                <div class="plugin <?php echo esc_attr($status_class);?>">
                                    <div class="preview">
                                        <?php if( isset($bigshop_tgm_theme_plugin['image']) && $bigshop_tgm_theme_plugin['image'] != "" ):?>
                                            <img src="<?php echo esc_url($bigshop_tgm_theme_plugin['image']);?>" alt="">
                                        <?php else:?>
                                            <img src="<?php echo esc_url(get_template_directory_uri().'/famework/assets/images/no-image.jpg');?>" alt="">
                                        <?php endif;?>
                                    </div>
                                    <div class="plugin-name">
                                        <h3 class="theme-name"><?php echo $bigshop_tgm_theme_plugin['name'] ?></h3>
                                    </div>
                                    <div class="actions">
                                        <a class="button button-primary button-install-plugin" href="<?php
                                            echo esc_url( wp_nonce_url(
                                                add_query_arg(
                                                    array(
                                                        'page'		  	=> urlencode(TGM_Plugin_Activation::$instance->menu),
                                                        'plugin'		=> urlencode($bigshop_tgm_theme_plugin['slug']),
                                                        'tgmpa-install' => 'install-plugin',
                                                    ),
                                                    admin_url('themes.php')
                                                ),
                                                'tgmpa-install',
                                                'tgmpa-nonce'
                                            ));
                                        ?>"><?php esc_html_e('Install','bigshop-toolkit');?></a>

                                        <a class="button button-primary button-update-plugin" href="<?php
                                            echo esc_url( wp_nonce_url(
                                                add_query_arg(
                                                    array(
                                                        'page'		  	=> urlencode(TGM_Plugin_Activation::$instance->menu),
                                                        'plugin'		=> urlencode($bigshop_tgm_theme_plugin['slug']),
                                                        'tgmpa-update' => 'update-plugin',
                                                    ),
                                                    admin_url('themes.php')
                                                ),
                                                'tgmpa-install',
                                                'tgmpa-nonce'
                                            ));
                                        ?>"><?php esc_html_e('Update','bigshop-toolkit');?></a>

                                        <a class="button button-primary button-activate-plugin" href="<?php
                                            echo esc_url(
                                                add_query_arg(
                                                    array(
                                                        'page'                   => urlencode('bigshop-toolkit'),
                                                        'plugin_slug' => urlencode($bigshop_tgm_theme_plugin['slug']),
                                                        'action'                 => 'active_plugin',
                                                        'magic_token'         => wp_create_nonce('panel-plugins')
                                                    ),
                                                    admin_url('admin.php')
                                                ));
                                        ?>""><?php esc_html_e('Activate','bigshop-toolkit');?></a>
                                        <a class="button button-secondary button-uninstall-plugin" href="<?php
                                            echo esc_url(
                                                add_query_arg(
                                                    array(
                                                        'page'                   => urlencode('bigshop-toolkit'),
                                                        'plugin_slug' => urlencode($bigshop_tgm_theme_plugin['slug']),
                                                        'action'                 => 'deactivate_plugin',
                                                        'magic_token'         => wp_create_nonce('panel-plugins')
                                                    ),
                                                    admin_url('admin.php')
                                                ));
                                        ?>""><?php esc_html_e('Deactivate','bigshop-toolkit');?></a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    ?>
                </div>
                <?php
            }
            public static function support(){
                ?>
                <div class="rp-row">
                    <div class="rp-col">
                        <div class="suport-item">
                            <h3><?php esc_html_e('Documentation','bigshop-toolkit');?></h3>
                            <p><?php esc_html_e('Here is our user guide for Bigshop, including basic setup steps, as well as Bigshop features and elements for your reference.','bigshop-toolkit');?></p>
                            <a target="_blank" href="#" class="button button-primary"><?php esc_html_e('Read Documentation','bigshop-toolkit');?></a>
                        </div>
                    </div>
                    <div class="rp-col closed">
                        <div class="suport-item">
                            <h3><?php esc_html_e('Video Tutorials','bigshop-toolkit');?></h3>
                            <p class="coming-soon"><?php esc_html_e('Video tutorials is the great way to show you how to setup Bigshop theme, make sure that the feature works as it\'s designed.','bigshop-toolkit');?></p>
                            <a href="#" class="button button-primary disabled"><?php esc_html_e('See Video','bigshop-toolkit');?></a>
                        </div>
                    </div>
                    <div class="rp-col">
                        <div class="suport-item">
                            <h3><?php esc_html_e('Forum','bigshop-toolkit');?></h3>
                            <p class="coming-soon"><?php esc_html_e('Can\'t find the solution on documentation? We\'re here to help, even on weekend. Just click here to start 1on1 chatting with us!','bigshop-toolkit');?></p>
                            <a target="_blank" href="#" class="button button-primary"><?php esc_html_e('Request Support','bigshop-toolkit');?></a>
                        </div>
                    </div>
                </div>

                <?php
            }
        }

        new Bigshop_Wellcome();
    }
