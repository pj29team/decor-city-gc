(function ($) {
    "use strict"; // Start of use strict
    /* CUSTOM VARIATIONS */
    function variations_custom() {
        $('.variations').find('.data-val').html('');
        $('.variations td.value').each(function () {
            var _this = $(this);
            var _attributetype = _this.find('select').data('attributetype');

            if( _attributetype == 'color'){
                _this.find('select').hide();
                _this.find('.data-val').show();
            }

            $(this).find('option').each(function () {

                var _ID        = $(this).parent().attr('id');
                var _data      = $(this).data(_ID);
                var _value     = $(this).attr('value');
                var _data_type = $(this).data('type');
                var itemclass = '';
                if( $(this).is(':selected')){
                    itemclass = itemclass + 'change-value active';
                }else{
                    itemclass ='change-value';
                }

                if ( _value !== '' ) {
                    if ( _data_type == 'color' || _data_type == 'photo' ) {
                        _this.find('.data-val').append('<a class="'+itemclass+'" href="#"  data-value="' + _value + '"><span style="background: ' + _data + ';background-size: cover; background-repeat: no-repeat;"></span></a>');
                    } else {
                        _this.find('.data-val').append('<a class="'+itemclass+' text" href="#" data-value="' + _value + '"><span>' + _value + '</span></a>');
                    }
                }
            });
        });
    };



    /* ---------------------------------------------
     Scripts ready
     --------------------------------------------- */
    $(document).ready(function () {
        variations_custom();
        $(document).on('change','.variations_form select',function () {
           variations_custom();
        });

        $(document).on('click', '.variations_form.cart .change-value', function () {
            var _this   = $(this);
            var _change = _this.data('value');

            _this.parent().parent().children('select').val(_change).trigger('change');
            _this.parent().find('.change-value').not(_this).removeClass('active');
            _this.addClass('active');
            return false;
        });
    });

})(jQuery); // End of use strict