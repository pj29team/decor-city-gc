<?php
/**
 *
 * Bigshop instagram
 *
 */
if ( !class_exists( 'Instagram_Widget' ) ) {
	class Instagram_Widget extends WP_Widget
	{
		function __construct()
		{
			$widget_ops = array(
				'classname'   => 'widget-instagram',
				'description' => 'Widget instagram.',
			);

			parent::__construct( 'widget_instagram', '1 - Bigshop Instagram', $widget_ops );
		}

		function widget( $args, $instance )
		{
			extract( $args );
			$user_name   = $instance[ 'user' ];
			$items_limit = $instance[ 'item' ];
			$token       = $instance[ 'token' ];

			echo $args[ 'before_widget' ];
			if ( !empty( $instance[ 'title' ] ) ) {
				echo $args[ 'before_title' ] . $instance[ 'title' ] . $args[ 'after_title' ];
			}
			?>
            <div class="content-instagram">
				<?php
				if ( intval( $user_name ) === 0 ) {
					esc_html_e( 'No user ID specified.', 'bigshop' );
				}
				$transient_var = $user_name . '_' . $items_limit;

				if ( false === ( $items = get_transient( $transient_var ) ) && !empty( $user_name ) && !empty( $token ) ) {
					$response = wp_remote_get( 'https://api.instagram.com/v1/users/' . esc_attr( $user_name ) . '/media/recent/?access_token=' . esc_attr( $token ) . '&count=' . esc_attr( $items_limit ) );
					if ( !is_wp_error( $response ) ) {
						$response_body = json_decode( $response[ 'body' ] );

						if ( $response_body->meta->code !== 200 ) {
							echo '<p>' . esc_html__( 'User ID and access token do not match. Please check again.', 'bigshop' ) . '</p>';
						}

						$items_as_objects = $response_body->data;
						$items            = array();

						foreach ( $items_as_objects as $item_object ) {
							$item[ 'link' ] = $item_object->link;
							$item[ 'src' ]  = $item_object->images->low_resolution->url;
							$items[]        = $item;
						}

						set_transient( $transient_var, $items, 60 * 60 );
					}
				}
				?>
				<?php if ( isset( $items ) && $items ): ?>
					<?php foreach ( $items as $item ): ?>
						<?php
						$size_add      = 's150x150';
						$item[ 'src' ] = str_replace( 's320x320', $size_add, $item[ 'src' ] );
						?>
                        <div class="item">
                            <a target="_blank" href="<?php echo esc_url( $item[ 'link' ] ) ?>">
                                <img class="img-responsive" src="<?php echo esc_url( $item[ 'src' ] ); ?>"
                                     alt="Instagram"/>
                            </a>
                        </div>
					<?php endforeach; ?>
				<?php endif; ?>
            </div>
			<?php
			echo $args[ 'after_widget' ];
		}

		function update( $new_instance, $old_instance )
		{

			$instance            = $old_instance;
			$instance[ 'user' ]  = $new_instance[ 'user' ];
			$instance[ 'token' ] = $new_instance[ 'token' ];
			$instance[ 'item' ]  = $new_instance[ 'item' ];
			$instance[ 'title' ] = $new_instance[ 'title' ];

			return $instance;

		}

		function form( $instance )
		{
			//
			// set defaults
			// -------------------------------------------------
			$instance = wp_parse_args(
				$instance,
				array(
					'user'  => '',
					'title' => '',
					'token' => '',
					'item'  => '',
				)
			);

			$title_value = $instance[ 'title' ];
			$title_field = array(
				'id'    => $this->get_field_name( 'title' ),
				'name'  => $this->get_field_name( 'title' ),
				'type'  => 'text',
				'title' => esc_html__( 'Title', 'bigshop' ),
			);

			echo cs_add_element( $title_field, $title_value );

			$user_value = $instance[ 'user' ];
			$user_field = array(
				'id'    => $this->get_field_name( 'user' ),
				'name'  => $this->get_field_name( 'user' ),
				'type'  => 'text',
				'title' => esc_html__( 'User', 'bigshop' ),
			);

			echo cs_add_element( $user_field, $user_value );

			$token_value = $instance[ 'token' ];
			$token_field = array(
				'id'    => $this->get_field_name( 'token' ),
				'name'  => $this->get_field_name( 'token' ),
				'type'  => 'text',
				'desc'  => wp_kses( sprintf( __( '<a href="%s" target="_blank">Get Token Instagram</a>', 'bigshop' ), 'http://instagram.pixelunion.net/' ), array( 'a' => array( 'href' => array(), 'target' => array() ) ) ),
				'title' => esc_html__( 'Token', 'bigshop' ),
			);

			echo cs_add_element( $token_field, $token_value );

			$item_value = $instance[ 'item' ];
			$item_field = array(
				'id'    => $this->get_field_name( 'item' ),
				'name'  => $this->get_field_name( 'item' ),
				'type'  => 'number',
				'title' => esc_html__( 'Number items', 'bigshop' ),
			);

			echo cs_add_element( $item_field, $item_value );
		}
	}
}

if ( !function_exists( 'Instagram_Widget_init' ) ) {
	function Instagram_Widget_init()
	{
		register_widget( 'Instagram_Widget' );
	}

	add_action( 'widgets_init', 'Instagram_Widget_init', 2 );
}