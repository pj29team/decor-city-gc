<?php
class Bigshop_Taxonomy_Widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'bigshop_taxonomy_widget', // Base ID
			esc_html__( '1 - Bigshop taxonomy', 'bigshop-toolkit' ), // 
			array( 'description' => esc_html__( 'Display list items of a taxonomy', 'bigshop-toolkit' ), ) // Args
		);
	}

	public function widget( $args, $instance ) {
	    $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Taxonomy', 'bigshop-toolkit' );
        $tax   = ! empty( $instance['tax'] ) ? $instance['tax'] : 'bigshop_product_brands';
        $hide_empty  = ! empty( $instance['hide_empty'] ) ? $instance['hide_empty'] : 'yes';
        $limit_items = ! empty( $instance['limit_items'] ) ? $instance['limit_items'] : 5;
        $orderby = ! empty( $instance['orderby'] ) ? $instance['orderby'] : 'name';
        $order   = ! empty( $instance['order'] ) ? $instance['order'] : 'ASC';
        
        $hide_if_empty = true;
        if( $hide_empty == 'no' ){
            $hide_if_empty = false;
        }
        
        $tax_args = array(
            'taxonomy' => $tax,
            'orderby'  => $orderby,
            'order'    => $order,
            'hide_empty' => $hide_if_empty,
            'number'     => $limit_items
        );
        
        
        
        $terms = get_terms( $tax_args );
        
		echo $args['before_widget'];
        echo '<div class="widget_product_categories">';
        
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		
        ?>
            <ul class="product-categories">
                
                <?php if( is_array( $terms ) && !empty( $terms ) ) : ?>
                    <?php foreach($terms as $val ) : ?>
                        <?php $term_link = get_term_link($val->slug, $tax_args['taxonomy'] ); ?>
                        <li class="cat-item cat-item-<?php echo esc_attr( $val->term_id ); ?>">
                            <a href="<?php echo esc_url( $term_link ); ?>"><?php echo esc_html( $val->name ); ?></a> <span class="count">(<?php echo esc_html( $val->count ); ?>)</span>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        
        <?php
        echo '</div>';
		echo $args['after_widget'];
	}

	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Taxonomy', 'bigshop-toolkit' );
        $tax   = ! empty( $instance['tax'] ) ? $instance['tax'] : 'bigshop_product_brands';
        $hide_empty  = ! empty( $instance['hide_empty'] ) ? $instance['hide_empty'] : 'yes';
        $limit_items = ! empty( $instance['limit_items'] ) ? $instance['limit_items'] : 5;
        $orderby = ! empty( $instance['orderby'] ) ? $instance['orderby'] : 'name';
        $order   = ! empty( $instance['order'] ) ? $instance['order'] : 'ASC';
        
        $taxonomies = array(
            'bigshop_product_brands' => esc_html__( 'Product Brands', 'bigshop-toolkit' ),
            'product_cat' => esc_html__( 'Product Categories', 'bigshop-toolkit' ),
            'product_tag' => esc_html__( 'Product Tags', 'bigshop-toolkit' ),
        );
        
		?>
		<p>
		  <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'bigshop-toolkit' ); ?></label> 
		  <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
        
        <p>
		  <label for="<?php echo esc_attr( $this->get_field_id( 'tax' ) ); ?>"><?php esc_attr_e( 'Taxonomy:', 'bigshop-toolkit' ); ?></label> 
		  
          <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'tax' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'tax' ) ); ?>" >
             <?php 
                 foreach( $taxonomies as $key => $val ){
                    ?>
                    <option value="<?php echo esc_attr( $key );  ?>" <?php selected( $key, $tax ); ?> ><?php echo $val; ?></option>
                    <?php
                 }
             ?>
          </select>
        </p>
        
        <p>
		  <label for="<?php echo esc_attr( $this->get_field_id( 'hide_empty' ) ); ?>"><?php esc_attr_e( 'Hide empty:', 'bigshop-toolkit' ); ?></label> 
		  
          <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'hide_empty' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'hide_empty' ) ); ?>" >
             <option value="yes" <?php selected( $hide_empty, 'yes' ); ?> ><?php echo esc_html__( 'Yes', 'bigshop-toolkit' ); ?></option>
             <option value="no" <?php selected( $hide_empty, 'no' ); ?> ><?php echo esc_html__( 'No', 'bigshop-toolkit' ); ?></option>
          </select>
        </p>
        
        <p>
		  <label for="<?php echo esc_attr( $this->get_field_id( 'limit_items' ) ); ?>"><?php esc_attr_e( 'Limit items:', 'bigshop-toolkit' ); ?></label> 
		  <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'limit_items' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'limit_items' ) ); ?>" type="text" value="<?php echo esc_attr( $limit_items ); ?>" />
		</p>
        
        <p>
		  <label for="<?php echo esc_attr( $this->get_field_id( 'orderby' ) ); ?>"><?php esc_attr_e( 'Order by:', 'bigshop-toolkit' ); ?></label> 
		  
          <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'orderby' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'orderby' ) ); ?>" >
             <option value="name" <?php selected( $orderby, 'name' ); ?> ><?php echo esc_html__( 'Name', 'bigshop-toolkit' ); ?></option>
             <option value="slug"  <?php selected( $orderby, 'slug' ); ?> ><?php  echo esc_html__( 'Slug', 'bigshop-toolkit' ); ?></option>
             <option value="term_group" <?php selected( $orderby, 'term_group' ); ?> ><?php echo esc_html__( 'Term group', 'bigshop-toolkit' ); ?></option>
             <option value="term_id"  <?php selected( $orderby, 'term_id' ); ?> ><?php  echo esc_html__( 'Term ID', 'bigshop-toolkit' ); ?></option>
             <option value="id" <?php selected( $orderby, 'id' ); ?> ><?php echo esc_html__( 'ID', 'bigshop-toolkit' ); ?></option>
             <option value="description"  <?php selected( $orderby, 'description' ); ?> ><?php  echo esc_html__( 'Description', 'bigshop-toolkit' ); ?></option>
             
          </select>
        </p>
        
        <p>
		  <label for="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>"><?php esc_attr_e( 'Order:', 'bigshop-toolkit' ); ?></label> 
		  
          <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'order' ) ); ?>" >
             <option value="ASC" <?php selected( $order, 'ASC' ); ?> ><?php echo esc_html__( 'ASC', 'bigshop-toolkit' ); ?></option>
             <option value="DESC" <?php selected( $order, 'DESC' ); ?> ><?php echo esc_html__( 'DESC', 'bigshop-toolkit' ); ?></option>
          </select>
        </p>
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title']        = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['tax']          = ( ! empty( $new_instance['tax'] ) ) ? strip_tags( $new_instance['tax'] ) : 'bigshop_product_brands';
        $instance['hide_empty']   = ( ! empty( $new_instance['hide_empty'] ) ) ? strip_tags( $new_instance['hide_empty'] ) : 'yes';
        $instance['limit_items']  = ( ! empty( $new_instance['limit_items'] ) ) ? strip_tags( $new_instance['limit_items'] ) : 5;
        $instance['orderby']      = ( ! empty( $new_instance['orderby'] ) ) ? strip_tags( $new_instance['orderby'] ) : 'name';
        $instance['order']        = ( ! empty( $new_instance['order'] ) ) ? strip_tags( $new_instance['order'] ) : 'ASC';
        

		return $instance;
	}

}

function bigshop_register_taxonomy_widget() {
    register_widget( 'Bigshop_Taxonomy_Widget' );
}
add_action( 'widgets_init', 'bigshop_register_taxonomy_widget' );