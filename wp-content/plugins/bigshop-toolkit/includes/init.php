<?php
include_once( 'classes/welcome.php' );
include_once( 'core/cs-framework.php' );
/* MAILCHIP */
include_once( 'classes/mailchimp/MCAPI.class.php' );
include_once( 'classes/mailchimp/mailchimp-settings.php' );
include_once( 'classes/mailchimp/mailchimp.php' );
/* WIDGET */
include_once( 'widgets/widget-socials.php' );
include_once( 'widgets/widget-post.php' );
include_once( 'widgets/widget-instagram.php' );


if ( !function_exists( 'bigshop_toolkit_vc_param' ) ) {
	function bigshop_toolkit_vc_param( $key = false, $value = false )
	{
		if ( class_exists( 'Vc_Manager' ) ) {
			return vc_add_shortcode_param( $key, $value );
		}
	}

	add_action( 'init', 'bigshop_toolkit_vc_param' );
}
if ( class_exists( 'WooCommerce' ) ) {
	include_once( 'widgets/widget-products.php' );
	include_once( 'widgets/widget-woo-layered-nav.php' );
	include_once( 'classes/woo-attributes-swatches/woo-term.php' );
	include_once( 'classes/woo-attributes-swatches/woo-product-attribute-meta.php' );
    include_once( 'widgets/widget-taxonomy.php' );
	/* SHARE SINGLE PRODUCT */
	if ( !function_exists( 'bigshop_single_product_share' ) ) {
		function bigshop_single_product_share()
		{
			global $post;
			$enable_share_product  = bigshop_get_option( 'enable_share_product' );
			$share_image_url       = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
			$share_link_url        = get_permalink( $post->ID );
			$share_link_title      = get_the_title();
			$share_twitter_summary = get_the_excerpt();

			if ( $enable_share_product != 1 ) {
				return false;
			}
			?>
            <div class="bigshop-single-product-socials">
                <a target="_blank" class="facebook"
                   href="https://www.facebook.com/sharer.php?s=100&amp;p%5Btitle%5D=<?php echo $share_link_title ?>&amp;p%5Burl%5D=<?php echo urlencode( $share_link_url ) ?>"
                   title="<?php _e( 'Facebook', 'bigshop' ) ?>">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/facebook.png' ?>" alt="">
                </a>
                <a target="_blank" class="googleplus"
                   href="https://plus.google.com/share?url=<?php echo urlencode( $share_link_url ) ?>&amp;title=<?php echo $share_link_title ?>"
                   title="<?php _e( 'Google+', 'bigshop' ) ?>"
                   onclick='javascript:window.open(this.href, "", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600");return false;'>
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/gplus.png' ?>" alt="">
                </a>
                <a target="_blank" class="pinterest"
                   href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode( $share_link_url ) ?>&amp;description=<?php echo $share_twitter_summary ?>&amp;media=<?php echo $share_image_url ?>"
                   title="<?php _e( 'Pinterest', 'bigshop' ) ?>"
                   onclick="window.open(this.href); return false;">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/share.png' ?>" alt="">
                </a>
                <a target="_blank" class="twitter"
                   href="https://twitter.com/share?url=<?php echo urlencode( $share_link_url ) ?>&amp;text=<?php echo $share_twitter_summary ?>"
                   title="<?php _e( 'Twitter', 'bigshop' ) ?>">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/tweet.png' ?>" alt="">
                </a>
            </div>
			<?php
		}
	}
	add_action( 'woocommerce_share', 'bigshop_single_product_share' );
	/* SHARE SINGLE PRODUCT */
}

if( !function_exists( 'bigshop_toolkit_output' ) ){
    function bigshop_toolkit_output( $data, $echo = false ){
        if( $echo ){
            $allowed_tags = wp_kses_allowed_html( 'post' );
            echo wp_kses( $data, $allowed_tags );
        }else{
            return $data;
        }
    }
}

if( !function_exists( 'bigshop_toolkit_resize_image' ) ){
    function bigshop_toolkit_resize_image( $attach_id = null, $img_url = null, $width, $height, $crop = false, $place_hold = true, $use_real_img_hold = true, $solid_img_color = null ) {
        /*If is singular and has post thumbnail and $attach_id is null, so we get post thumbnail id automatic*/
        if ( is_singular() && !$attach_id ) {
            if ( has_post_thumbnail() && !post_password_required() ) {
                $attach_id = get_post_thumbnail_id();
            }
        }
        /*this is an attachment, so we have the ID*/
        $image_src = array();
        if ( $attach_id ) {
            $image_src = wp_get_attachment_image_src( $attach_id, 'full' );
            $actual_file_path = get_attached_file( $attach_id );
            /*this is not an attachment, let's use the image url*/
        } else if ( $img_url ) {
            $file_path = str_replace( get_site_url(), get_home_path(), $img_url );
            $actual_file_path = rtrim( $file_path, '/' );
            if ( !file_exists( $actual_file_path ) ) {
                $file_path = parse_url( $img_url );
                $actual_file_path = rtrim( ABSPATH, '/' ) . $file_path['path'];
            }
            if ( file_exists( $actual_file_path ) ) {
                $orig_size = getimagesize( $actual_file_path );
                $image_src[0] = $img_url;
                $image_src[1] = $orig_size[0];
                $image_src[2] = $orig_size[1];
            }
            else{
                $image_src[0] = '';
                $image_src[1] = 0;
                $image_src[2] = 0;
            }
        }
        if ( ! empty( $actual_file_path ) && file_exists( $actual_file_path ) ) {
            $file_info = pathinfo( $actual_file_path );
            $extension = '.' . $file_info['extension'];
            /*the image path without the extension*/
            $no_ext_path = $file_info['dirname'] . '/' . $file_info['filename'];
            $cropped_img_path = $no_ext_path . '-' . $width . 'x' . $height . $extension;
            /*checking if the file size is larger than the target size*/
            /*if it is smaller or the same size, stop right here and return*/
            if ( $image_src[1] > $width || $image_src[2] > $height ) {
                /*the file is larger, check if the resized version already exists (for $crop = true but will also work for $crop = false if the sizes match)*/
                if ( file_exists( $cropped_img_path ) ) {
                    $cropped_img_url = str_replace( basename( $image_src[0] ), basename( $cropped_img_path ), $image_src[0] );
                    $vt_image = array(
                        'url' => $cropped_img_url,
                        'width' => $width,
                        'height' => $height
                    );
                    return $vt_image;
                }

                if ( $crop == false ) {
                    /*calculate the size proportionaly*/
                    $proportional_size = wp_constrain_dimensions( $image_src[1], $image_src[2], $width, $height );
                    $resized_img_path = $no_ext_path . '-' . $proportional_size[0] . 'x' . $proportional_size[1] . $extension;
                    /*checking if the file already exists*/
                    if ( file_exists( $resized_img_path ) ) {
                        $resized_img_url = str_replace( basename( $image_src[0] ), basename( $resized_img_path ), $image_src[0] );
                        $vt_image = array(
                            'url' => $resized_img_url,
                            'width' => $proportional_size[0],
                            'height' => $proportional_size[1]
                        );
                        return $vt_image;
                    }
                }
                /*no cache files - let's finally resize it*/
                $img_editor = wp_get_image_editor( $actual_file_path );
                if ( is_wp_error( $img_editor ) || is_wp_error( $img_editor->resize( $width, $height, $crop ) ) ) {
                    return array(
                        'url' => '',
                        'width' => '',
                        'height' => ''
                    );
                }
                $new_img_path = $img_editor->generate_filename();
                if ( is_wp_error( $img_editor->save( $new_img_path ) ) ) {
                    return array(
                        'url' => '',
                        'width' => '',
                        'height' => ''
                    );
                }
                if ( ! is_string( $new_img_path ) ) {
                    return array(
                        'url' => '',
                        'width' => '',
                        'height' => ''
                    );
                }
                $new_img_size = getimagesize( $new_img_path );
                $new_img = str_replace( basename( $image_src[0] ), basename( $new_img_path ), $image_src[0] );
                /*resized output*/
                $vt_image = array(
                    'url' => $new_img,
                    'width' => $new_img_size[0],
                    'height' => $new_img_size[1]
                );
                return $vt_image;
            }
            /*default output - without resizing*/
            $vt_image = array(
                'url' => $image_src[0],
                'width' => $image_src[1],
                'height' => $image_src[2]
            );
            return $vt_image;
        }
        else {
            if ( $place_hold ) {
                $width = intval( $width );
                $height = intval( $height );
                /*Real image place hold (https://unsplash.it/)*/
                if ( $use_real_img_hold ) {
                    $random_time = time() + rand( 1, 100000 );
                    $vt_image = array(
                        'url' => 'https://unsplash.it/' . $width . '/' . $height . '?random&time=' . $random_time,
                        'width' => $width,
                        'height' => $height
                    );
                }
                else{
                    $vt_image = array(
                        'url' => 'http://placehold.it/' . $width . 'x' . $height,
                        'width' => $width,
                        'height' => $height
                    );
                }
                return $vt_image;
            }
        }
        return false;
    }
}