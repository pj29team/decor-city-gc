<?php

if ( !class_exists( 'Bigshop_Shortcode_Brands' ) ) {
	class Bigshop_Shortcode_Brands extends Bigshop_Shortcode
	{
		/**
		 * Shortcode name.
		 *
		 * @var  string
		 */
		public $shortcode = 'brands';

		/**
		 * Default $atts .
		 *
		 * @var  array
		 */
		public $default_atts = array();


		public static function generate_css( $atts ){
			$css = '';

			return $css;
		}


		public function output_html( $atts, $content = null )
		{
			$atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'bigshop_brands', $atts ) : $atts;

			// Extract shortcode parameters.
			extract( $atts );


			$css_class = array( 'bigshop-brands' );
			$css_class[] = $atts[ 'el_class' ];
			$css_class[] = $atts[ 'brands_custom_id' ];

			if ( function_exists( 'vc_shortcode_custom_css_class' ) ) {
				$css_class[] = ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts[ 'css' ], ' ' ), '', $atts );
			}
            $brands = vc_param_group_parse_atts( $atts['brands'] );
            $brand_item_class = array('brand');
            $brand_item_class[] = 'col-lg-' . $atts[ 'boostrap_lg_items' ];
            $brand_item_class[] = 'col-md-' . $atts[ 'boostrap_md_items' ];
            $brand_item_class[] = 'col-sm-' . $atts[ 'boostrap_sm_items' ];
            $brand_item_class[] = 'col-xs-' . $atts[ 'boostrap_xs_items' ];
            $brand_item_class[] = 'col-ts-' . $atts[ 'boostrap_ts_items' ];
            if( isset($atts['block_link']) && $atts['block_link']){
                $block_link =  vc_build_link( $atts['block_link']);
            }else{
                $block_link = array('url'=>'', 'title'=>'', 'target'=>'', 'rel'=>'') ;
            }

			ob_start();
			?>
            <div class="<?php echo esc_attr( implode( ' ', $css_class ) ); ?>">
				<?php if ( $atts[ 'title' ] ) : ?>
                    <?php
                    $block_title = '';
                    $title = explode(' ',$atts['title']);

                    if( count($title) >0 ){
                        $i = 0;
                        foreach ($title as $text){
                            if( $i == 0 ){
                                $block_title .='<span>'.$text.'</span>';
                            }else{
                                $block_title .=' '.$text;
                            }

                            $i++;
                        }
                    }
                    ?>
                    <h2 class="block-title">
                        <?php echo $block_title; ?>
                        <a class="link" href="<?php echo esc_url($block_link['url']) ?>" <?php if($block_link['target']): ?> target="<?php echo esc_html($block_link['target']) ?>" <?php endif; ?>  <?php if($block_link['rel']): ?> rel="<?php echo esc_attr($block_link['rel']) ; ?>" <?php endif; ?>><?php echo esc_html($block_link['title']);?></a>
                    </h2>
				<?php endif; ?>
                <div class="block-content row">
					<?php if( $brands && !empty($brands)):?>
                        <?php foreach ($brands as $brand):?>
                            <div class="<?php echo esc_attr( implode( ' ', $brand_item_class ) ); ?>">
                                <a href="<?php echo esc_url( $brand['link']);?>" title="<?php echo esc_attr($brand['name'])?>">
                                    <?php echo wp_get_attachment_image($brand['logo'],'full');?>
                                </a>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                </div>
            </div>
			<?php
			$html = ob_get_clean();

			return apply_filters( 'Bigshop_Shortcode_brands', force_balance_tags( $html ), $atts, $content );
		}
	}
}