<?php

if ( !class_exists( 'Bigshop_Shortcode_Googlemap' ) ) {
	class Bigshop_Shortcode_Googlemap extends Bigshop_Shortcode
	{
		/**
		 * Shortcode name.
		 *
		 * @var  string
		 */
		public $shortcode = 'googlemap';


		/**
		 * Default $atts .
		 *
		 * @var  array
		 */
		public $default_atts = array();


		public static function generate_css( $atts )
		{
			// Extract shortcode parameters.
			extract( $atts );
			$css = '';
			if ( $atts[ 'map_height' ] > 0 ) {
				$css .= '.' . $atts[ 'googlemap_custom_id' ] . ' .bigshop-google-maps { min-height:' . $atts[ 'map_height' ] . 'px;} ';
			}

			return $css;
		}


		public function output_html( $atts, $content = null )
		{
			$atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'bigshop_googlemap', $atts ) : $atts;

			// Extract shortcode parameters.
			extract( $atts );


			$css_class   = array( 'widget google-map' );
			$css_class[] = $atts[ 'el_class' ];
			$css_class[] = $atts[ 'googlemap_custom_id' ];

			if ( function_exists( 'vc_shortcode_custom_css_class' ) ) {
				$css_class[] = ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
			}
			ob_start();
			?>
            <div class="<?php echo esc_attr( implode( ' ', $css_class ) ); ?>">
                <?php if( $title!='' ):?>
                <h2 class="widgettitle"><?php echo esc_html( $title ) ?></h2>
                <?php endif;?>
                <?php if( $subtitle !='' ):?>
                <div class="subtitle"><?php echo esc_html( $subtitle ) ?></div>
                <?php endif;?>
                <div class                  =   "bigshop-google-maps" id="<?php echo esc_attr($atts[ 'googlemap_custom_id' ]);?>"
                     data-hue               =   ""
                     data-lightness         =   "1"
                     data-map-style         =   ""
                     data-saturation        =   "-100"
                     data-modify-coloring   =   "false"
                     data-title_maps        =   ""
                     data-phone             =   ""
                     data-email             =   ""
                     data-address           =   ""
                     data-longitude         =   "<?php echo esc_html( $longitude ) ?>"
                     data-latitude          =   "<?php echo esc_html( $latitude ) ?>"
                     data-pin-icon          =   ""
                     data-zoom              =   "<?php echo esc_html( $zoom ) ?>"
                     data-map-type          =   "<?php echo esc_attr( $map_type ) ?>">
                </div>
            </div>

			<?php
			$html = ob_get_clean();

			return apply_filters( 'Bigshop_Shortcode_googlemap', force_balance_tags( $html ), $atts, $content );
		}
	}
}