<?php
if (!class_exists('Bigshop_Shortcode_Teammember')){
    class Bigshop_Shortcode_Teammember extends  Bigshop_Shortcode{
        /**
         * Shortcode name.
         *
         * @var  string
         */
        public $shortcode = 'team_member';


        /**
         * Default $atts .
         *
         * @var  array
         */
        public  $default_atts  = array(
        );


        public static function generate_css( $atts ){
            extract( $atts );
            $css = '';
            if( $atts['owl_navigation_position'] == 'nav-top-left' || $atts['owl_navigation_position'] == 'nav-top-right'){
                $css .= '.'.$atts['slider_custom_id'] .' .owl-nav{ top:'.$atts['owl_navigation_position_top'].'px;} ';
            }

            return $css;
        }


        public function output_html( $atts, $content = null ){
            $atts = function_exists('vc_map_get_attributes') ? vc_map_get_attributes('bigshop_team_member', $atts) : $atts;

            // Extract shortcode parameters.
            extract($atts);
            $get_team_members = vc_param_group_parse_atts( $team_members );

            $css_class = array('bigshop-team-members');
            $css_class[] = $atts['style'];
            $css_class[] = $atts['el_class'];
            $css_class[] =  $atts['team_member_custom_id'];

            if ( function_exists( 'vc_shortcode_custom_css_class' ) ){
                $css_class[] = ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
            }
            $owl_settings = $this->generate_carousel_data_attributes('', $atts);
            ob_start();
            ?>
            <div class="<?php echo esc_attr( implode(' ', $css_class) );?>">
                <div class="owl-carousel <?php echo esc_attr( $owl_navigation_position );?>" <?php echo $owl_settings;?>>
                    <?php if( is_array( $get_team_members ) && !empty( $get_team_members ) ) : ?>
                        <?php foreach( $get_team_members as $member ) : ?>
                            <?php if( empty( $member ) ){ continue; } ?>
                            <div class="member_item">
                                <?php
                                    if( isset( $member['avatar'] ) && !empty( $member['avatar'] ) ){
                                        $member_avatar = wp_get_attachment_image_src( $member['avatar'], 'full', false );
                                        if( isset( $member_avatar[0] ) && $member_avatar[0] != '' ){
                                            ?>
                                            <div class="member_avatar">
                                                <img src="<?php echo esc_url( $member_avatar[0] ); ?>" alt="" />
                                            </div>
                                            <?php
                                        }
                                    }
                                ?>
                                <?php if( isset( $member['name'] ) && !empty( $member['name'] ) ) : ?>
                                    <h3 class="member_name"><?php echo esc_html( $member['name'] ); ?></h3>
                                <?php endif; ?>
                                <?php if( isset( $member['position'] ) && !empty( $member['position'] ) ) : ?>
                                    <div class="member_position"><?php echo esc_html( $member['position'] ); ?></div>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>

            </div>
            <?php
            $html = ob_get_clean();

            return apply_filters( 'bigshop_toolkit_shortcode_team_member', force_balance_tags( $html ), $atts ,$content );
        }
    }
}