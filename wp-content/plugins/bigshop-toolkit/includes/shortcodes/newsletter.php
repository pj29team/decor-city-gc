<?php

if ( !class_exists( 'Bigshop_Shortcode_Newsletter' ) ) {

	class Bigshop_Shortcode_Newsletter extends Bigshop_Shortcode
	{
		/**
		 * Shortcode name.
		 *
		 * @var  string
		 */
		public $shortcode = 'newsletter';


		/**
		 * Default $atts .
		 *
		 * @var  array
		 */
		public $default_atts = array();


		public static function generate_css( $atts )
		{
			// Extract shortcode parameters.
			extract( $atts );
			$css = '';

			return $css;
		}


		public function output_html( $atts, $content = null )
		{
			$atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'bigshop_newsletter', $atts ) : $atts;
			// Extract shortcode parameters.
			extract( $atts );

			$css_class   = array( 'bigshop-newsletter' );
			$css_class[] = $atts[ 'el_class' ];
			$css_class[] = $atts[ 'style' ];
			$css_class[] = $atts[ 'newsletter_custom_id' ];

			if ( function_exists( 'vc_shortcode_custom_css_class' ) ) {
				$css_class[] = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
			}

			ob_start();
			?>
            <div class="<?php echo esc_attr( implode( ' ', $css_class ) ); ?>">
                <div class="content-newletter">
					<div class="inner">
                        <div class="head">
                            <?php if ( $atts[ 'title' ] ): ?>
                                <?php
                                $block_title = '';
                                $title = explode(' ',$atts['title']);

                                if( count($title) >0 ){
                                    $i = 0;
                                    foreach ($title as $text){
                                        if( $i == 0 ){
                                            $block_title .='<span>'.$text.'</span>';
                                        }else{
                                            $block_title .=' '.$text;
                                        }

                                        $i++;
                                    }
                                }
                                ?>
                                <h3 class="title"><?php echo $block_title; ?></h3>
                            <?php endif; ?>
                            <?php if ( $atts[ 'subtitle' ] ): ?>
                                <div class="sub-title"><?php echo force_balance_tags( $atts[ 'subtitle' ] ); ?></div>
                            <?php endif; ?>
                        </div>
                        <div class="content">
                            <div class="newsletter-form-wrap">
                                <div class="input-wapper">
                                    <input class="email email-newsletter" type="email" name="email" placeholder="<?php echo esc_attr( $atts[ 'placeholder_text' ] ); ?>">
                                </div>
                                <div class="button-wapper">
                                    <a href="javascript:void(0);" class="button btn-submit submit-newsletter">
                                        <?php echo esc_html( $atts[ 'button_text' ] ); ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php
			$html = ob_get_clean();

			return apply_filters( 'Bigshop_Shortcode_newsletter', force_balance_tags( $html ), $atts, $content );
		}
	}
}