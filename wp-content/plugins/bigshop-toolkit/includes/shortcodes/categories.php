<?php

if (!class_exists('Bigshop_Shortcode_Categories')){

    class Bigshop_Shortcode_Categories extends  Bigshop_Shortcode{
        /**
         * Shortcode name.
         *
         * @var  string
         */
        public $shortcode = 'categories';

        /**
         * Default $atts .
         *
         * @var  array
         */
        public  $default_atts  = array(

        );
        public function output_html( $atts, $content = null ){
            $atts = function_exists('vc_map_get_attributes') ? vc_map_get_attributes('bigshop_categories', $atts) : $atts;
            // Extract shortcode parameters.
            extract($atts);

            $css_class = array('bigshop-sc-categories');
            $css_class[] = $atts['el_class'];
            $css_class[] = $atts['style'];
            $css_class[] =  $atts['categories_custom_id'];

            if ( function_exists( 'vc_shortcode_custom_css_class' ) ){
                $css_class[] = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
            }

            
            
            $taxnonomy_lists = array();
            if( $atts['taxonomy'] != '' ){
                $taxnonomy_lists = explode( ',', $atts['taxonomy'] );
            }
            
            $list_tax = array();

            if( is_array( $taxnonomy_lists ) && !empty( $taxnonomy_lists ) ){
                foreach( $taxnonomy_lists as $tax ){
                    $term_link =  get_term_link( $tax,'product_cat');
                    $term_info =  get_term_by('slug', $tax, 'product_cat');
                    
                    if( $term_link != '' && isset( $term_info->name ) && $term_info->name != '' ){
                        $list_tax[] = array(
                            'name' => $term_info->name,
                            'link' => $term_link
                        );
                    }
                }
            }

            $bg_url = '';
            if( isset( $atts['bg_cat'] ) && $atts['bg_cat'] != '' ){
                $bg_url = wp_get_attachment_url( $atts['bg_cat'] );
            }
            
            ob_start();

            ?>
            <?php if( $style == 'style2'):?>
                
            <?php else:?>
                <div class="<?php echo esc_attr( implode(' ', $css_class) );?>">
                	<div class="category-thumb">
                        <?php if( $bg_url ) : ?>
                            <img src="<?php echo esc_url( $bg_url ); ?>" alt="" />
                        <?php endif; ?>
                    </div>
                    <div class="category-info">
                        <?php if( isset( $atts['title'] ) && $atts['title'] != '' ) : ?>
                            <h3 class="title"><?php echo esc_html( $atts['title'] ); ?></h3>
                        <?php endif; ?>

                        <?php if( is_array( $list_tax ) && !empty( $list_tax ) ) : ?>
                            <ul class="list-category-brand">
                                <?php foreach( $list_tax as $val ) : ?>
                                    <li class="item">
                                        <a href="<?php echo esc_url( $val['link'] ); ?>"><?php echo esc_html( $val['name'] ); ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>

                    </div>
                </div>
            <?php endif;?>
            <?php
            $html = ob_get_clean();
            return apply_filters( 'bigshop_toolkit_shortcode_categories', force_balance_tags( $html ), $atts ,$content );
        }
    }
}