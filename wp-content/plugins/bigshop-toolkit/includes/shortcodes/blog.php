<?php

if ( !class_exists( 'Bigshop_Shortcode_blog' ) ) {
	class Bigshop_Shortcode_blog extends Bigshop_Shortcode
	{
		/**
		 * Shortcode name.
		 *
		 * @var  string
		 */
		public $shortcode = 'blog';


		/**
		 * Default $atts .
		 *
		 * @var  array
		 */
		public $default_atts = array();


		public function output_html( $atts, $content = null )
		{
			$atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'bigshop_blog', $atts ) : $atts;

			// Extract shortcode parameters.
			extract( $atts );
           
			$css_class = array( 'bigshop-blog' );
			$css_class[] = $atts[ 'style' ];
			$css_class[] = $atts[ 'el_class' ];
			$css_class[] = $atts[ 'blog_custom_id' ];
            
			if ( function_exists( 'vc_shortcode_custom_css_class' ) ) {
				$css_class[] = ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts[ 'css' ], ' ' ), '', $atts );
			}
            
            
			$owl_settings = $this->generate_carousel_data_attributes( 'owl_', $atts );
            
			$args = array(
				'post_type'           => 'post',
				'post_status'         => 'publish',
				'ignore_sticky_posts' => 1,
				'posts_per_page'      => $atts[ 'per_page' ],
				'suppress_filter'     => true,
				'orderby'             => $atts[ 'orderby' ],
				'order'               => $atts[ 'order' ],
			);
			if ( !empty( $ids_post ) ) {
				$args[ 'p' ] = $ids_post;
			}

			if ( $atts[ 'category_slug' ] ) {
				$idObj = get_category_by_slug( $atts[ 'category_slug' ] );
				if ( is_object( $idObj ) ) {
					$args[ 'cat' ] = $idObj->term_id;
				}
			}

			$loop_posts = new WP_Query( apply_filters( 'bigshop_shortcode_posts_query', $args, $atts ) );
            
            $excerpt_length = (isset( $atts['excerpt_length']) && $atts['excerpt_length'] > 0 ) ? $atts['excerpt_length'] : 15;
            $readmore_text  = (isset( $atts['readmore_text']) && $atts['readmore_text'] != '' ) ? $atts['readmore_text'] : esc_html__( 'Read more...', 'bigshop-toolkit' );
            
			ob_start();
			?>
            <div class="<?php echo esc_attr( implode( ' ', $css_class ) ); ?>">
            	<?php if( isset( $atts['title'] ) && $atts['title'] != '' ) : ?>
			     <h3 class="title"><?php echo esc_html( $atts['title'] ); ?></h3>
                <?php endif; ?>
				<?php if ( $loop_posts->have_posts() ) : ?>
                    <?php if( $atts[ 'style' ] && $atts[ 'style' ] == 'style-1' ) : ?>
                        <div class="blog-owl owl-carousel nav-top-right " <?php bigshop_toolkit_output( $owl_settings, true ); ?>>
    						<?php
                                $i = 1;
                                $total_post = $loop_posts->post_count;
                            ?>
                            <div class="owl-one-row">
                                <?php while ( $loop_posts->have_posts() ) : $loop_posts->the_post() ?>
                                    <div <?php post_class( 'blog-item layout01 ' . $atts[ 'owl_rows_space' ] . ' ' ); ?>>
        								<div class="blog-thumb">
                                            <?php if( has_post_thumbnail() ){ ?>
                                                <?php if( isset( $atts['crop_image'] ) && $atts['crop_image'] == 'yes' ){ ?>
                                                    <?php
                                                        $image_crop_width  = (isset( $atts['crop_width'] ) && $atts['crop_width'] > 0 ) ? $atts['crop_width'] : 270;
                                                        $image_crop_height = (isset( $atts['crop_height'] ) && $atts['crop_height'] > 0 ) ? $atts['crop_height'] : 186;
                                                        
                                                        $image_crop = bigshop_toolkit_resize_image( get_post_thumbnail_id( get_the_ID() ), null,$image_crop_width, $image_crop_height  );
                                                        if( is_array( $image_crop ) && !empty( $image_crop['url'] ) ){
                                                            echo sprintf( '<img src="%s" alt="%s" width="%s" height="%s" />', esc_url( $image_crop['url'] ), get_the_title(), $image_crop['width'], $image_crop['height'] );
                                                        }
                                                    ?>
                                                <?php }else{ ?>
                                                    <?php the_post_thumbnail(); ?>
                                                <?php }//End else ?>
                                            <?php } ?>
                                        </div>
                                        
                                        <div class="blog-info">
                                            <div class="meta">
                                                <span class="author"><?php the_author(); ?></span>
                                                <span class="slash ">&#47;</span>
                                                <span class="time"><?php the_time( 'd M Y'); ?></span>
                                            </div>
                                            <h3 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                                            <div class="excerpt">
                                                <?php echo wp_trim_words( get_the_excerpt(), $excerpt_length, '' ); ?>
                                            </div>
                                            <p class="readmore">
                                                <a href="<?php the_permalink(); ?>"><?php echo bigshop_toolkit_output( $readmore_text ); ?></a>
                                            </p>
                                        </div>
                                    </div>
                                    <?php
                                        if( $i % $owl_number_row == 0 && $i < $total_post ){
                                            echo '</div><div class="owl-one-row">';
                                        }
                                        $i++; 
                                    ?>
        						<?php endwhile; ?>
                            </div>
                        </div>
                    <?php elseif( $atts[ 'style' ] && $atts[ 'style' ] == 'style-2' ): ?>    
                        <div class="blog-owl owl-carousel nav-top-right " <?php bigshop_toolkit_output( $owl_settings, true ); ?>>
    						<?php
                                $i = 1;
                                $total_post = $loop_posts->post_count;
                            ?>
                            <div class="owl-one-row">
                                <?php while ( $loop_posts->have_posts() ) : $loop_posts->the_post() ?>
                                    <div <?php post_class( 'blog-item layout02 ' .  $atts[ 'owl_rows_space' ] . ' ' ); ?>>
        								<div class="blog-thumb">
                                            <?php if( has_post_thumbnail() ){ ?>
                                                <?php if( isset( $atts['crop_image'] ) && $atts['crop_image'] == 'yes' ){ ?>
                                                    <?php
                                                        $image_crop_width  = (isset( $atts['crop_width'] ) && $atts['crop_width'] > 0 ) ? $atts['crop_width'] : 80;
                                                        $image_crop_height = (isset( $atts['crop_height'] ) && $atts['crop_height'] > 0 ) ? $atts['crop_height'] : 55;
                                                        
                                                        $image_crop = bigshop_toolkit_resize_image( get_post_thumbnail_id( get_the_ID() ), null,$image_crop_width, $image_crop_height  );
                                                        if( is_array( $image_crop ) && !empty( $image_crop['url'] ) ){
                                                            echo sprintf( '<img src="%s" alt="%s" width="%s" height="%s" />', esc_url( $image_crop['url'] ), get_the_title(), $image_crop['width'], $image_crop['height'] );
                                                        }
                                                    ?>
                                                <?php }else{ ?>
                                                    <?php the_post_thumbnail(); ?>
                                                <?php }//End else ?>
                                            <?php } ?>
                                        </div>
                                        
                                        <div class="blog-info">
                                            <div class="meta">
                                                <span class="author"><?php the_author(); ?></span>
                                                <span class="slash ">&#47;</span>
                                                <span class="time"><?php the_time( 'd M Y'); ?></span>
                                            </div>
                                            <?php if( isset( $atts['title'] ) ) : ?>
                                            <h3 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                                            <?php endif; ?>
                                            
                                        </div>
                                    </div>
                                    <?php
                                        if( $i % $owl_number_row == 0 && $i < $total_post ){
                                            echo '</div><div class="owl-one-row">';
                                        }
                                        $i++;
                                    ?>
        						<?php endwhile; ?>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="blog-owl owl-carousel nav-top-right " <?php bigshop_toolkit_output( $owl_settings, true ); ?>>
    						<?php
                                $i = 1;
                                $total_post = $loop_posts->post_count;
                            ?>
                            <div class="owl-one-row">
                                <?php while ( $loop_posts->have_posts() ) : $loop_posts->the_post() ?>
                                    <div <?php post_class( 'blog-item layout03 ' . $atts[ 'owl_rows_space' ] . ' ' ); ?>>
        								<div class="blog-thumb">
                                            <?php if( has_post_thumbnail() ){ ?>
                                                <?php if( isset( $atts['crop_image'] ) && $atts['crop_image'] == 'yes' ){ ?>
                                                    <?php
                                                        $image_crop_width  = (isset( $atts['crop_width'] ) && $atts['crop_width'] > 0 ) ? $atts['crop_width'] : 405;
                                                        $image_crop_height = (isset( $atts['crop_height'] ) && $atts['crop_height'] > 0 ) ? $atts['crop_height'] : 279;
                                                        
                                                        $image_crop = bigshop_toolkit_resize_image( get_post_thumbnail_id( get_the_ID() ), null,$image_crop_width, $image_crop_height  );
                                                        if( is_array( $image_crop ) && !empty( $image_crop['url'] ) ){
                                                            echo sprintf( '<img src="%s" alt="%s" width="%s" height="%s" />', esc_url( $image_crop['url'] ), get_the_title(), $image_crop['width'], $image_crop['height'] );
                                                        }
                                                    ?>
                                                <?php }else{ ?>
                                                    <?php the_post_thumbnail(); ?>
                                                <?php }//End else ?>
                                            <?php } ?>
                                        </div>
                                        
                                        <div class="blog-info">
                                            <div class="meta">
                                                <span class="author"><?php the_author(); ?></span>
                                                <span class="slash ">&#47;</span>
                                                <span class="time"><?php the_time( 'd M Y'); ?></span>
                                            </div>
                                            <?php if( isset( $atts['title'] ) ) : ?>
                                            <h3 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                                            <?php endif; ?>
                                            <div class="excerpt">
                                                <?php echo wp_trim_words( get_the_excerpt(), $excerpt_length, '' ); ?>
                                            </div>
                                            <p class="readmore">
                                                <a href="<?php the_permalink(); ?>"><?php echo bigshop_toolkit_output( $readmore_text ); ?></a>
                                            </p>
                                        </div>
                                    </div>
                                    <?php
                                        if( $i % $owl_number_row == 0 && $i < $total_post ){
                                            echo '</div><div class="owl-one-row">';
                                        }
                                        $i++; 
                                    ?>
        						<?php endwhile; ?>
                            </div>
                        </div>
                    <?php endif;//Style ?>
					<?php wp_reset_postdata(); ?>
				<?php else : ?>
					<?php esc_html_e( 'No post found!', 'bigshop-toolkit' ); ?>
				<?php endif; ?>
            </div>
			<?php
			$html = ob_get_clean();

			return apply_filters( 'Bigshop_Shortcode_blog', force_balance_tags( $html ), $atts, $content );
		}
	}
}