<?php
    if (!class_exists('Bigshop_Shortcode_Progressbar')){
        class Bigshop_Shortcode_Progressbar extends  Bigshop_Shortcode{
            /**
             * Shortcode name.
             *
             * @var  string
             */
            public $shortcode = 'progress_bar';


            /**
             * Default $atts .
             *
             * @var  array
             */
            public  $default_atts  = array(
            );


            public static function generate_css( $atts ){
                extract( $atts );
                $css = '';
                return $css;
            }


            public function output_html( $atts, $content = null ){
                $atts = function_exists('vc_map_get_attributes') ? vc_map_get_attributes('bigshop_progress_bar', $atts) : $atts;

                // Extract shortcode parameters.
                extract($atts);
                $progress_bars = vc_param_group_parse_atts( $atts['progress_bars'] );

                $css_class = array('bigshop-progress-bar');
                $css_class[] = $atts['style'];
                $css_class[] =  $atts['progress_bar_custom_id'];

                if ( function_exists( 'vc_shortcode_custom_css_class' ) ){
                    $css_class[] = ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
                }

                ob_start();
                ?>
                <div class="<?php echo esc_attr( implode(' ', $css_class) );?>">
                    <?php if( $progress_bars && !empty($progress_bars)):?>
                        <?php foreach ( $progress_bars as $bar):?>
                            <div class="bar-wapper">
                                <?php if( $bar['label']):?>
                                    <div class="title"><?php echo $bar['label'];?></div>
                                <?php endif;?>
                                <div class="bar">
                                    <div style="width: <?php echo $bar['value'];?>%; background-color: <?php echo $bar['bar_background'];?>;" class="progress"><span class="value"><?php echo $bar['value'];?>%</span></div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                </div>
                <?php
                $html = ob_get_clean();

                return apply_filters( 'bigshop_toolkit_shortcode_progress_bar', force_balance_tags( $html ), $atts ,$content );
            }
        }
    }