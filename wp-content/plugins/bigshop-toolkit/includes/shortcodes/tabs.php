<?php

if ( !class_exists( 'Bigshop_Shortcode_Tabs' ) ) {
	class Bigshop_Shortcode_Tabs extends Bigshop_Shortcode
	{
		/**
		 * Shortcode name.
		 *
		 * @var  string
		 */
		public $shortcode = 'tabs';


		/**
		 * Default $atts .
		 *
		 * @var  array
		 */
		public $default_atts = array(
			'style'          => '',
			'css_animation'  => '',
			'el_class'       => '',
			'css'            => '',
			'ajax_check'     => '',
			'tabs_custom_id' => '',
			'active_section' => '',
		);


		public static function generate_css( $atts )
		{
			// Extract shortcode parameters.
			extract( $atts );
			$css = '';

			return $css;
		}

		public function output_html( $atts, $content = null )
		{
			$atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'bigshop_tabs', $atts ) : $atts;
			// Extract shortcode parameters.
			extract(
				shortcode_atts(
					$this->default_atts,
					$atts
				)
			);

			$css_class   = array( 'bigshop-tabs' );
			$css_class[] = $atts[ 'el_class' ];
			$css_class[] = $atts[ 'style' ];
			$css_class[] = $atts[ 'tabs_custom_id' ];

			if ( function_exists( 'vc_shortcode_custom_css_class' ) ) {
				$css_class [] = ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), '', $atts );
			}

			$sections = $this->get_all_attributes( 'vc_tta_section', $content );
			ob_start();
			?>
            <div class="<?php echo esc_attr( implode( ' ', $css_class ) ); ?>">
				<?php if ( $sections && is_array( $sections ) && count( $sections ) > 0 ): ?>
                    <div class="tab-head">
                        <?php if( $atts['tab_title']):?>
                        <?php
                        $block_title = '';
                        $title = explode(' ',$atts['tab_title']);

                        if( count($title) >0 ){
                            $i = 0;
                            foreach ($title as $text){
                                if( $i == 0 ){
                                    $block_title .='<span>'.$text.'</span>';
                                }else{
                                    $block_title .=' '.$text;
                                }

                                $i++;
                            }
                        }
                        ?>
                        <h3 class="tab-title"><?php echo $block_title;?></h3>
                        <?php endif;?>
                        <ul class="tab-link">
							<?php $i = 0; ?>
							<?php foreach ( $sections as $section ): ?>
								<?php
								$content_shortcode = '';
								$i++;
								if ( $atts[ 'ajax_check' ] == 1 ) {
									$content_shortcode = base64_encode( $section[ 'content' ] );
								}

								/* Get icon from section tabs */
								$type_icon = isset( $section[ 'i_type' ] ) ? $section[ 'i_type' ] : '';
								$add_icon  = isset( $section[ 'add_icon' ] ) ? $section[ 'add_icon' ] : '';

								if ( $type_icon == 'fontflaticon' ) {
									$class_icon = isset( $section[ 'icon_bigshopcustomfonts' ] ) ? $section[ 'icon_bigshopcustomfonts' ] : '';
								} else {
									$class_icon = isset( $section[ 'icon_fontawesome' ] ) ? $section[ 'icon_fontawesome' ] : '';
								}

								$position_icon = isset( $section[ 'i_position' ] ) ? $section[ 'i_position' ] : '';
								?>
                                <li class="<?php if ( $i == $atts[ 'active_section' ] ): ?>active<?php endif; ?>">
                                    <a <?php if ( $i == $atts[ 'active_section' ] ) {
										echo 'class="loaded"';
									} ?> data-ajax="<?php echo esc_attr( $atts[ 'ajax_check' ] ) ?>"
                                         data-shortcode='<?php echo esc_attr( $content_shortcode ); ?>'
                                         data-animate="<?php echo esc_attr( $atts[ 'css_animation' ] ); ?>"
                                         data-toggle="tab"
                                         href="#bigshop_<?php echo esc_attr( $section[ 'tab_id' ] ); ?>">
										<?php if ( $add_icon == true && $position_icon != 'right' ) : ?><i
                                            class="before-icon <?php echo esc_attr( $class_icon ); ?>"></i><?php endif; ?>
										<?php echo esc_html( $section[ 'title' ] ); ?>
										<?php if ( $add_icon == true && $position_icon == 'right' ) : ?><i
                                            class="after-icon <?php echo esc_attr( $class_icon ); ?>"></i><?php endif; ?>
                                    </a>
                                </li>
							<?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="tab-container">
						<?php $i = 0; ?>
						<?php foreach ( $sections as $section ): ?>
							<?php $i++; ?>
                            <div class="tab-panel <?php if ( $i == $atts[ 'active_section' ] ): ?>active<?php endif; ?>"
                                 id="bigshop_<?php echo esc_attr( $section[ 'tab_id' ] ); ?>">
								<?php
								if ( $atts[ 'ajax_check' ] == '1' ) {
									if ( $i == $atts[ 'active_section' ] ) {
										echo do_shortcode( $section[ 'content' ] );
									}
								} else {
									echo do_shortcode( $section[ 'content' ] );
								}
								?>
                            </div>
						<?php endforeach; ?>
                    </div>
				<?php endif; ?>
            </div>
			<?php
			$html = ob_get_clean();

			return apply_filters( 'Bigshop_Shortcode_tabs', force_balance_tags( $html ), $atts, $content );
		}
	}
}