<?php
    if ( !class_exists( 'Bigshop_Shortcode_Storeinfo' ) ) {
        class Bigshop_Shortcode_Storeinfo extends Bigshop_Shortcode
        {
            /**
             * Shortcode name.
             *
             * @var  string
             */
            public $shortcode = 'storeinfo';


            /**
             * Default $atts .
             *
             * @var  array
             */
            public $default_atts = array();


            public static function generate_css( $atts )
            {
                // Extract shortcode parameters.
                extract( $atts );
                $css = '';

                return $css;
            }


            public function output_html( $atts, $content = null )
            {
                $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'bigshop_storeinfo', $atts ) : $atts;

                // Extract shortcode parameters.
                extract( $atts );

                $css_class   = array( 'bigshop-storeinfo widget' );
                $css_class[] = $atts[ 'el_class' ];
                $css_class[] = $atts[ 'layout' ];
                $css_class[] = $atts[ 'storeinfo_custom_id' ];

                if ( function_exists( 'vc_shortcode_custom_css_class' ) ) {
                    $css_class[] = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
                }
                ob_start();
                ?>
                <div class="<?php echo esc_attr( implode( ' ', $css_class ) ); ?>">
                    <?php if( $atts['title'] ):?>
                    <h2 class="widgettitle"><?php echo esc_html( $atts['title']);?></h2>
                    <?php endif;?>
                    <div class="content">
                        <?php if( $atts['address'] ):?>
                            <div class="content-item-wapper">
                                <div class="content-item address">
                                    <div class="inner">
                                        <span class="icon"></span>
                                        <span class="text"><?php echo esc_html( $atts['address']);?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if( $atts['phone'] ):?>
                            <div class="content-item-wapper">
                                <div class="content-item phone">
                                    <div class="inner">
                                        <span class="icon"></span>
                                        <span class="text"><?php echo esc_html( $atts['phone']);?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if( $atts['email'] ):?>
                            <div class="content-item-wapper">
                                <div class="content-item email">
                                    <div class="inner">
                                        <span class="icon"></span>
                                        <span class="text"><?php echo esc_html( $atts['email']);?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
                <?php
                $html = ob_get_clean();

                return apply_filters( 'Bigshop_Shortcode_Storeinfo', force_balance_tags( $html ), $atts, $content );
            }
        }
    }