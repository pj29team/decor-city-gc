<?php

if ( !class_exists( 'Bigshop_Shortcode_Products' ) ) {
	class Bigshop_Shortcode_Products extends Bigshop_Shortcode
	{
		/**
		 * Shortcode name.
		 *
		 * @var  string
		 */
		public $shortcode = 'products';

		/**
		 * Default $atts .
		 *
		 * @var  array
		 */
		public $default_atts = array();


		public static function generate_css( $atts )
		{
            extract( $atts );
            $css = '';
            if( $atts['owl_navigation_position'] == 'nav-top-left' || $atts['owl_navigation_position'] == 'nav-top-right'){
                $css .= '.'.$atts['products_custom_id'] .' .owl-nav{ top:'.$atts['owl_navigation_position_top'].'px;} ';
            }
            if( $atts['owl_navigation_offset_right']!=''){
                $css .= '.'.$atts['products_custom_id'] .' .owl-nav{ right:'.$atts['owl_navigation_offset_right'].'px;} ';
            }
            if( $atts['owl_navigation_offset_left']!=''){
                $css .= '.'.$atts['products_custom_id'] .' .owl-nav{ left:'.$atts['owl_navigation_offset_left'].'px;} ';
            }

            return $css;
		}

		public function output_html( $atts, $content = null )
		{
			$atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'bigshop_products', $atts ) : $atts;

			extract( $atts );
			$css_class   = array( 'bigshop-products' );
			$css_class[] = $atts[ 'el_class' ];
			$css_class[] = $atts[ 'products_custom_id' ];
			$css_class[] = 'style-' . $atts[ 'product_style' ];
			$css_class[] = 'product-list-width-style-' . $atts[ 'product_style' ];

			if ( function_exists( 'vc_shortcode_custom_css_class' ) ) {
				$css_class[] = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts[ 'css' ], ' ' ), '', $atts );
			}


			/* Product Size */

			if ( $atts[ 'product_image_size' ] ) {
				if ( $atts[ 'product_image_size' ] == 'custom' ) {
					$thumb_width  = $atts[ 'product_custom_thumb_width' ];
					$thumb_height = $atts[ 'product_custom_thumb_height' ];
				} else {
					$product_image_size = explode( "x", $atts[ 'product_image_size' ] );
					$thumb_width        = $product_image_size[ 0 ];
					$thumb_height       = $product_image_size[ 1 ];
				}
				if ( $thumb_width > 0 ) {
					add_filter( 'bigshop_shop_pruduct_thumb_width', create_function( '', 'return ' . $thumb_width . ';' ) );
				}
				if ( $thumb_height > 0 ) {
					add_filter( 'bigshop_shop_pruduct_thumb_height', create_function( '', 'return ' . $thumb_height . ';' ) );
				}
			}
			$products      = $this->getProducts( $atts );
			$total_product = $products->post_count;

			$product_item_class   = array( 'product-item', $atts[ 'target' ] );
			$product_item_class[] = 'style-' . $atts[ 'product_style' ];

			$product_list_class = array();
			$owl_settings       = '';
			if ( $atts[ 'productsliststyle' ] == 'grid' ) {
			    if( $grid_layout == '' ){
			        $grid_layout = 'default';
			    }
                $css_class[] = 'grid-layout ' . $grid_layout;
				$product_list_class[] = 'product-list-grid row auto-clear equal-container ';

				$product_item_class[] = $atts[ 'boostrap_rows_space' ];
				$product_item_class[] = 'col-lg-' . $atts[ 'boostrap_lg_items' ];
				$product_item_class[] = 'col-md-' . $atts[ 'boostrap_md_items' ];
				$product_item_class[] = 'col-sm-' . $atts[ 'boostrap_sm_items' ];
				$product_item_class[] = 'col-xs-' . $atts[ 'boostrap_xs_items' ];
				$product_item_class[] = 'col-ts-' . $atts[ 'boostrap_ts_items' ];
			}
			if ( $atts[ 'productsliststyle' ] == 'owl' ) {
			    if( $carousel_layout == '' ){
			        $carousel_layout = 'default';
			    }
			    $css_class[] = 'owl-layout ' . $carousel_layout;
				if ( $total_product < $atts[ 'owl_lg_items' ] ) {
					$atts[ 'owl_loop' ] = 'false';
				}
				$product_list_class[] = 'owl-products owl-carousel nav-style2 equal-container ' . $atts[ 'owl_navigation_position' ];

				$product_item_class[] = $atts[ 'owl_rows_space' ];

				$owl_settings = $this->generate_carousel_data_attributes( 'owl_', $atts );
                
			}
            
            if( $atts['product_style']  == '6' ){
                if ( $atts['product6_thumb_width'] > 0 ) {
					add_filter( 'bigshop_product6_list_thumb_width', create_function( '', 'return ' . $atts['product6_thumb_width'] . ';' ) );
				}
				if ( $atts['product6_thumb_height'] > 0 ) {
					add_filter( 'bigshop_product6_list_thumb_height', create_function( '', 'return ' . $atts['product6_thumb_height'] . ';' ) );
				}
            }

			ob_start();
			?>
            <div class="<?php echo esc_attr( implode( ' ', $css_class ) ); ?>">
                <?php if ( $atts[ 'the_title' ] ) : ?>
                    <?php
                    $block_title = '';
                    $title = explode(' ',$atts['the_title']);

                    if( count($title) >0 ){
                        $i = 0;
                        foreach ($title as $text){
                            if( $i == 0 ){
                                $block_title .='<span>'.$text.'</span>';
                            }else{
                                $block_title .=' '.$text;
                            }

                            $i++;
                        }
                    }
                    ?>
                    <h2 class="title"><?php echo $block_title; ?></h2>
                <?php endif; ?>
				<div class="product-list-content">
                    <?php if ( $products->have_posts() ): ?>
                        <?php if ( $atts[ 'productsliststyle' ] == 'grid' ): ?>
                            <ul class="<?php echo esc_attr( implode( ' ', $product_list_class ) ); ?>">
                                <?php while ( $products->have_posts() ) : $products->the_post(); ?>
                                    <li <?php post_class( $product_item_class ); ?>>
                                        <?php wc_get_template_part( 'product-styles/content-product-style', $atts[ 'product_style' ] ); ?>
                                    </li>
                                <?php endwhile; ?>
                            </ul>
                            <!-- OWL Products -->
                        <?php elseif( $productsliststyle == 'owl'):?>
                            <?php
                            $i = 1;
                            $toal_product = $products->post_count;
                            ?>
                            <div class="<?php echo esc_attr( implode(' ', $product_list_class) );?>" <?php echo force_balance_tags($owl_settings);?>>
                                <div class="owl-one-row">
                                    <?php while ( $products->have_posts() ) : $products->the_post();  ?>
                                        <div <?php post_class( $product_item_class );?>>
                                            <?php wc_get_template_part('product-styles/content-product-style', $product_style); ?>
                                        </div>
                                        <?php
                                        if( $i % $owl_number_row == 0 && $i < $toal_product ){
                                            echo '</div><div class="owl-one-row">';
                                        }
                                        $i++;
                                        ?>
                                    <?php endwhile;?>
                                </div>
                            </div>
                        <?php endif;?>
                    <?php else: ?>
                        <p>
                            <strong><?php esc_html_e( 'No Product', 'bigshop-toolkit' ); ?></strong>
                        </p>
                    <?php endif; ?>
                </div>
            </div>
			<?php
			wp_reset_postdata();
			$html = ob_get_clean();

			return apply_filters( 'Bigshop_Shortcode_products', force_balance_tags( $html ), $atts, $content );
		}
	}
}