<?php

if ( !class_exists( 'Bigshop_Shortcode_Testimonials' ) ) {
	class Bigshop_Shortcode_Testimonials extends Bigshop_Shortcode
	{
		/**
		 * Shortcode name.
		 *
		 * @var  string
		 */
		public $shortcode = 'testimonials';


		/**
		 * Default $atts .
		 *
		 * @var  array
		 */
		public $default_atts = array();


		public function output_html( $atts, $content = null )
		{
			$atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'bigshop_testimonials', $atts ) : $atts;

			// Extract shortcode parameters.
			extract( $atts );
           
			$css_class = array( 'bigshop-testimonials' );
			$css_class[] = $atts[ 'style' ];
			$css_class[] = $atts[ 'el_class' ];
			$css_class[] = $atts[ 'testimonials_custom_id' ];
            
			if ( function_exists( 'vc_shortcode_custom_css_class' ) ) {
				$css_class[] = ' ' . apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts[ 'css' ], ' ' ), '', $atts );
			}

			$owl_settings = $this->generate_carousel_data_attributes( 'owl_', $atts );
            $testimonial_content = vc_param_group_parse_atts( $atts['testimonials'] );
            
            
			ob_start();
			?>
            <div class="<?php echo esc_attr( implode( ' ', $css_class ) ); ?>">
                <?php if( isset( $atts['title'] ) ) : ?>
			     <h3 class="title"><?php echo esc_html( $atts['title'] ); ?></h3>
                <?php endif; ?>
                
                <div class="testimonial-content owl-carousel nav-top-right" <?php bigshop_toolkit_output( $owl_settings, true ); ?>>
                    <?php
                        if( is_array( $testimonial_content ) &&  !empty( $testimonial_content ) ){
                            foreach( $testimonial_content as $content ){
                                
                                ?>
                                <div class="item">
                                    <div class="main-des">
                                        <?php if( isset( $content['content'] ) && $content['content'] != '' ){ bigshop_toolkit_output( $content['content'], true ); } ?>
                                    </div>
                                    <div class="main-info">
                                        <div class="avatar">
                                            <?php if( isset( $content['avatar'] ) && $content['avatar'] != '' ) : ?>
                                                <?php 
                                                    $avatar = wp_get_attachment_url( absint( $content['avatar'] ) );
                                                    if( $avatar ){
                                                        echo sprintf( '<img src="%s" alt="" class="img-avatar" />', esc_url( $avatar ) );
                                                    }
                                                ?>
                                            <?php endif; ?>
                                        </div>
                                        <div class="info">
                                            <?php if( isset( $content['name'] ) && $content['name'] != '' ) : ?>
                                                <p class="name"><?php echo esc_html( $content['name'] ); ?></p>
                                            <?php endif; ?>
                                            <?php if( isset( $content['position'] ) && $content['position'] != '' ) : ?>
                                                <p class="position-job"><?php echo esc_html( $content['position'] ); ?></p>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                    ?>
                </div>
            </div>
			<?php
			$html = ob_get_clean();

			return apply_filters( 'Bigshop_Shortcode_Testimonials', force_balance_tags( $html ), $atts, $content );
		}
	}
}