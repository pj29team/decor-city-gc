<?php
/**
 * Plugin Name: BigShop Toolkit
 * Plugin URI:  http://kutethemes.net
 * Description: BigShop toolkit for BigShop theme. Currently supports the following theme functionality: shortcodes, CPT.
 * Version:     1.0
 * Author:      Kutethemes Team
 * Author URI:  http://kutethemes.net
 * License:     GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: bigshop-toolkit
 */

// Define url to this plugin file.
define( 'BIGSHOP_TOOLKIT_URL', plugin_dir_url( __FILE__ ) );

// Define path to this plugin file.
define( 'BIGSHOP_TOOLKIT_PATH', plugin_dir_path( __FILE__ ) );

// Include function plugins if not include.
if ( !function_exists( 'is_plugin_active' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}

/**
 * Load plugin textdomain.
 *
 * @since 1.0.0
 */
function bigshop_toolkit_load_textdomain()
{
	load_plugin_textdomain( 'bigshop-toolkit', false, BIGSHOP_TOOLKIT_PATH . '/languages' );
}

add_action( 'init', 'bigshop_toolkit_load_textdomain' );

// Run shortcode in widget text
add_filter( 'widget_text', 'do_shortcode' );

// Register custom post types
include_once( BIGSHOP_TOOLKIT_PATH . '/includes/post-types.php' );
include_once( BIGSHOP_TOOLKIT_PATH . '/includes/taxonomy.php' );
include_once( BIGSHOP_TOOLKIT_PATH . '/includes/vc_templates.php' );
// Register init

if ( !function_exists( 'bigshop_toolkit_init' ) ) {
	function bigshop_toolkit_init()
	{
		include_once( BIGSHOP_TOOLKIT_PATH . '/includes/init.php' );
		include_once( BIGSHOP_TOOLKIT_PATH . '/includes/importer/init.php' );
		if ( class_exists( 'Vc_Manager' ) ) {
			// Register custom shortcodes
			include_once( BIGSHOP_TOOLKIT_PATH . '/includes/shortcode.php' );
		}
	}
}

add_action( 'bigshop_toolkit_init', 'bigshop_toolkit_init' );

if ( !function_exists( 'bigshop_toolkit_install' ) ) {
	function bigshop_toolkit_install()
	{
		do_action( 'bigshop_toolkit_init' );
	}
}
add_action( 'plugins_loaded', 'bigshop_toolkit_install', 11 );
