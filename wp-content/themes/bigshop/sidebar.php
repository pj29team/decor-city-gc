<?php
$bigshop_blog_used_sidebar = bigshop_get_option( 'blog_sidebar', 'widget-area' );
if ( is_single() ) {
	$bigshop_blog_used_sidebar = bigshop_get_option( 'single_post_sidebar', 'widget-area' );
}
?>
<?php if ( is_active_sidebar( $bigshop_blog_used_sidebar ) ) : ?>
    <div id="widget-area" class="widget-area sidebar-blog">
		<?php dynamic_sidebar( $bigshop_blog_used_sidebar ); ?>
    </div><!-- .widget-area -->
<?php endif; ?>