<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
// Ensure visibility
if ( empty( $product ) || !$product->is_visible() ) {
	return;
}


// Custom columns
$bigshop_woo_lg_items = bigshop_get_option( 'bigshop_woo_lg_items', 4 );
$bigshop_woo_md_items = bigshop_get_option( 'bigshop_woo_md_items', 4 );
$bigshop_woo_sm_items = bigshop_get_option( 'bigshop_woo_sm_items', 6 );
$bigshop_woo_xs_items = bigshop_get_option( 'bigshop_woo_xs_items', 6 );
$bigshop_woo_ts_items = bigshop_get_option( 'bigshop_woo_ts_items', 12 );

$bigshop_woo_product_style = bigshop_get_option( 'bigshop_shop_product_style', 1 );

$shop_display_mode = bigshop_get_option( 'shop_page_layout', 'grid' );
if ( isset( $_SESSION[ 'shop_display_mode' ] ) ) {
	$shop_display_mode = $_SESSION[ 'shop_display_mode' ];
}

$classes[] = 'product-item rows-space-30';
if ( $shop_display_mode == "grid" ) {
	$classes[] = 'col-lg-' . $bigshop_woo_lg_items;
	$classes[] = 'col-md-' . $bigshop_woo_md_items;
	$classes[] = 'col-sm-' . $bigshop_woo_sm_items;
	$classes[] = 'col-xs-' . $bigshop_woo_xs_items;
	$classes[] = 'col-ts-' . $bigshop_woo_ts_items;

} else {
	$classes[] = 'list col-sm-12';
}

$template_style = 'style-' . $bigshop_woo_product_style;

$classes[] = 'style-' . $bigshop_woo_product_style;
?>

<li <?php post_class( $classes ); ?>>
	<?php if ( $shop_display_mode == "grid" ): ?>
		<?php wc_get_template_part( 'product-styles/content-product', $template_style ); ?>
	<?php else: ?>
		<?php wc_get_template_part( 'content-product', 'list' ); ?>
	<?php endif; ?>
</li>
