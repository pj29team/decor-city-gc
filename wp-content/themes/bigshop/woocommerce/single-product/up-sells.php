<?php
/**
 * Single Product Up-Sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/up-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     4.0.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit;
}
$classes                   = array();
$bigshop_enable_up_sell    = bigshop_get_option( 'enable_up_sell', 'yes' );
if ( $bigshop_enable_up_sell == 'no' ) {
	return;
}

$classes[] = 'product-item';

$woo_upsell_lg_items = bigshop_get_option( 'bigshop_woo_upsell_lg_items', 3 );
$woo_upsell_md_items = bigshop_get_option( 'bigshop_woo_upsell_md_items', 3 );
$woo_upsell_sm_items = bigshop_get_option( 'bigshop_woo_upsell_sm_items', 2 );
$woo_upsell_xs_items = bigshop_get_option( 'bigshop_woo_upsell_xs_items', 1 );
$woo_upsell_ts_items = bigshop_get_option( 'bigshop_woo_upsell_ts_items', 1 );

$data_reponsive = array(
    '0'=>array(
        'items'=>$woo_upsell_ts_items
    ),
    '480'=>array(
        'items'=>$woo_upsell_xs_items
    ),
    '768'=>array(
        'items'=>$woo_upsell_sm_items
    ),
    '992'=>array(
        'items'=>$woo_upsell_md_items
    ),
    '1200'=>array(
        'items'=>$woo_upsell_lg_items
    ),
);
$data_reponsive = json_encode($data_reponsive);
$loop = 'false';

$woo_upsell_sell_title = bigshop_get_option( 'bigshop_upsell_products_title', 'You may also like&hellip;' );
$block_title = '';
$title = explode(' ',$woo_upsell_sell_title);

if( count($title) >0 ){
    $i = 0;
    foreach ($title as $text){
        if( $i == 0 ){
            $block_title .='<span>'.$text.'</span>';
        }else{
            $block_title .=' '.$text;
        }

        $i++;
    }
}
if ( $upsells ) : ?>
    <section class="up-sells upsells products product-grid equal-container">

        <h2 class="product-grid-title"><?php echo balanceTags($block_title); ?></h2>
        <div class="row">
            <div class="owl-carousel owl-products nav-top-right nav-top-right nav-style2" data-margin="1" data-nav="true" data-dots="false" data-responsive="<?php echo esc_attr( $data_reponsive ); ?>">
                <?php foreach ( $upsells as $upsell ) : ?>
                    <div <?php post_class( $classes ) ?>>
                        <?php
                            $post_object = get_post( $upsell->get_id() );

                            setup_postdata( $GLOBALS[ 'post' ] =& $post_object );

                            wc_get_template_part( 'product-styles/content-product', 'style-1' ); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

    </section>

<?php endif;

wp_reset_postdata();
