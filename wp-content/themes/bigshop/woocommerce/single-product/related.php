<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     4.0.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit;
}
$classes                        = array();
$bigshop_enable_relate_products = bigshop_get_option( 'enable_relate_products', 'yes' );
if ( $bigshop_enable_relate_products == 'no' ) {
	return;
}

$classes[] = 'product-item';

$woo_related_lg_items = bigshop_get_option( 'bigshop_woo_related_lg_items', 3 );
$woo_related_md_items = bigshop_get_option( 'bigshop_woo_related_md_items', 3 );
$woo_related_sm_items = bigshop_get_option( 'bigshop_woo_related_sm_items', 2 );
$woo_related_xs_items = bigshop_get_option( 'bigshop_woo_related_xs_items', 1 );
$woo_related_ts_items = bigshop_get_option( 'bigshop_woo_related_ts_items', 1 );

$data_reponsive = array(
    '0'=>array(
        'items'=>$woo_related_ts_items
    ),
    '480'=>array(
        'items'=>$woo_related_xs_items
    ),
    '768'=>array(
        'items'=>$woo_related_sm_items
    ),
    '992'=>array(
        'items'=>$woo_related_md_items
    ),
    '1200'=>array(
        'items'=>$woo_related_lg_items
    ),
);
$data_reponsive = json_encode($data_reponsive);
$loop = 'false';

$woo_related_title = bigshop_get_option( 'bigshop_related_products_title', 'Related Products' );
$block_title = '';
$title = explode(' ',$woo_related_title);

if( count($title) >0 ){
    $i = 0;
    foreach ($title as $text){
        if( $i == 0 ){
            $block_title .='<span>'.$text.'</span>';
        }else{
            $block_title .=' '.$text;
        }

        $i++;
    }
}

if ( $related_products ) : ?>
    <section class="related products product-grid equal-container">
        <h2 class="product-grid-title"><?php echo balanceTags($block_title); ?></h2>
        <div class="row">
            <div class="owl-carousel owl-products nav-top-right nav-style2" data-margin="1" data-nav="true" data-dots="false" data-responsive="<?php echo esc_attr( $data_reponsive ); ?>">
                <?php foreach ( $related_products as $related_product ) : ?>
                    <div <?php post_class( $classes ) ?>>
                        <?php
                            $post_object = get_post( $related_product->get_id() );

                            setup_postdata( $GLOBALS[ 'post' ] =& $post_object );

                            wc_get_template_part( 'product-styles/content-product', 'style-1' ); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

<?php endif;

wp_reset_postdata();
