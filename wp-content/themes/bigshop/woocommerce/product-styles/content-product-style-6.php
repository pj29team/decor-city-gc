<?php
/*
     Name: Product style 6
     Slug: content-product-style-6
*/
?>
<div class="product-inner bigshop-product06">
	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );
    ?>
    <div class="left-content">
        <div class="product-thumb">
            <div class="thumb-inner bigshop_product6_main_thum">
                <?php
                /**
                 * woocommerce_before_shop_loop_item_title hook.
                 *
                 * @hooked woocommerce_show_product_loop_sale_flash - 10
                 * @hooked woocommerce_template_loop_product_thumbnail - 10
                 */
                do_action( 'woocommerce_before_shop_loop_item_title' );
                ?>
                <div class="product-loop-thumbs">
                    <?php do_action( 'bigshop_function_shop_loop_product_thumbnails' ); ?>
                </div>
            </div>
            <?php do_action('bigshop_function_shop_loop_item_quickview');?>
        </div>
    </div>
    <div class="right-content">
        <div class="product-info">
            <?php
                do_action( 'bigshop_display_product_countdown_in_loop' );
                /**
                 * woocommerce_shop_loop_item_title hook.
                 *
                 * @hooked woocommerce_template_loop_product_title - 10
                 */
                do_action( 'woocommerce_shop_loop_item_title' );
                /**
                 * woocommerce_after_shop_loop_item_title hook.
                 *
                 * @hooked woocommerce_template_loop_rating - 5
                 * @hooked woocommerce_template_loop_price - 10
                 */
                add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 11 );
                do_action( 'woocommerce_after_shop_loop_item_title' );
                remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 11 );
                
                
            ?>

            <?php do_action( 'bigshop_product_loop_excerpt' ); ?>
            
            <div class="group-button-inner product-bottom">
                <?php
                    /**
                     * woocommerce_after_shop_loop_item hook.
                     *
                     * @hooked woocommerce_template_loop_product_link_close - 5
                     * @hooked woocommerce_template_loop_add_to_cart - 10
                     */
                    do_action( 'woocommerce_after_shop_loop_item' );
                    
                ?>
                <div class="box-extra-btn">
                    <?php
                        do_action('bigshop_function_shop_loop_item_wishlist');
                        do_action('bigshop_function_shop_loop_item_compare');
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>