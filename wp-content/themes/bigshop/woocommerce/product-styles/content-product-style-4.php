<?php
    /*
         Name: Product style 04
         Slug: content-product-style-4
    */
    add_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_rating',15);
?>
<div class="product-inner bigshop-product04">
    <?php
        /**
         * woocommerce_before_shop_loop_item hook.
         *
         * @hooked woocommerce_template_loop_product_link_open - 10
         */
        do_action( 'woocommerce_before_shop_loop_item' );
    ?>
    <div class="product-thumb">
        <div class="thumb-inner">
            <?php
                /**
                 * woocommerce_before_shop_loop_item_title hook.
                 *
                 * @hooked woocommerce_show_product_loop_sale_flash - 10
                 * @hooked woocommerce_template_loop_product_thumbnail - 10
                 */
                do_action( 'woocommerce_before_shop_loop_item_title' );
            ?>
        </div>
    </div>
    <div class="product-info">
        <?php
            /**
             * woocommerce_shop_loop_item_title hook.
             *
             * @hooked woocommerce_template_loop_product_title - 10
             */
            do_action( 'woocommerce_shop_loop_item_title' );
            /**
             * woocommerce_after_shop_loop_item_title hook.
             *
             * @hooked woocommerce_template_loop_rating - 5
             * @hooked woocommerce_template_loop_price - 10
             */
            do_action( 'woocommerce_after_shop_loop_item_title' );
        ?>
    </div>
</div>
<?php
    remove_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_rating',15);
?>