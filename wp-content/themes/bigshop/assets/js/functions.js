(function ($) {
    "use strict"; // Start of use strict

    /* ---------------------------------------------
     MENU REPONSIIVE
     --------------------------------------------- */
    function bigshop_init_menu_reposive() {
        var kt_is_mobile = (Modernizr.touch) ? true : false;
        if (kt_is_mobile === true) {
            $(document).on('click', '.bigshop-nav .menu-item-has-children >a,.bigshop-nav .menu-item-has-children>.toggle-submenu', function (e) {
                e.preventDefault();
                var licurrent = $(this).closest('li');
                var liItem = $('.bigshop-nav .menu-item-has-children');
                if (!licurrent.hasClass('show-submenu')) {
                    liItem.removeClass('show-submenu');
                    licurrent.parents().each(function () {
                        if ($(this).hasClass('menu-item-has-children')) {
                            $(this).addClass('show-submenu');
                        }
                        if ($(this).hasClass('main-menu')) {
                            return false;
                        }
                    })
                    licurrent.addClass('show-submenu');
                    // Close all child submenu if parent submenu is closed
                    if (!licurrent.hasClass('show-submenu')) {
                        licurrent.find('li').removeClass('show-submenu');
                    }
                    return false;
                } else {
                    licurrent.removeClass('show-submenu');
                    var href = $(this).attr('href');
                    if ($.trim(href) == '' || $.trim(href) == '#') {
                        licurrent.toggleClass('show-submenu');
                    }
                    else {
                        window.location = href;
                    }
                }
                // Close all child submenu if parent submenu is closed
                if (!licurrent.hasClass('show-submenu')) {
                    licurrent.find('li').removeClass('show-submenu');
                }
                e.stopPropagation();
            });
            $(document).on('click', function (e) {
                var target = $(e.target);
                if (!target.closest('.show-submenu').length || !target.closest('.bigshop-nav').length) {
                    $('.show-submenu').removeClass('show-submenu');
                }
            });
            // On Desktop
        } else {
            $(document).on('mousemove', '.bigshop-nav .menu-item-has-children', function () {
                $(this).addClass('show-submenu');
                if ($(this).closest('.bigshop-nav').hasClass('main-menu')) {
                    $('body').addClass('is-show-menu');
                }
            })

            $(document).on('mouseout', '.bigshop-nav .menu-item-has-children', function () {
                $(this).removeClass('show-submenu');
                $('body').removeClass('is-show-menu');
            })
        }
    }

    /* ---------------------------------------------
     Resize mega menu
     --------------------------------------------- */
    function bigshop_resizeMegamenu() {
        var window_size = jQuery('body').innerWidth();
        window_size += bigshop_get_scrollbar_width();
        if (window_size > 1024) {
            if ($('#header-sticky-menu .main-menu-wapper').length > 0) {
                var container = $('#header-sticky-menu .main-menu-wapper');
                if (container != 'undefined') {
                    var container_width = 0;
                    container_width = container.innerWidth();
                    var container_offset = container.offset();
                    setTimeout(function () {
                        $('.main-menu .item-megamenu').each(function (index, element) {
                            $(element).children('.megamenu').css({'max-width': container_width + 'px'});
                            var sub_menu_width = $(element).children('.megamenu').outerWidth();
                            var item_width = $(element).outerWidth();
                            $(element).children('.megamenu').css({'left': '-' + (sub_menu_width / 2 - item_width / 2) + 'px'});
                            var container_left = container_offset.left;
                            var container_right = (container_left + container_width);
                            var item_left = $(element).offset().left;
                            var overflow_left = (sub_menu_width / 2 > (item_left - container_left));
                            var overflow_right = ((sub_menu_width / 2 + item_left) > container_right);
                            if (overflow_left) {
                                var left = (item_left - container_left);
                                $(element).children('.megamenu').css({'left': -left + 'px'});
                            }
                            if (overflow_right && !overflow_left) {
                                var left = (item_left - container_left);
                                left = left - ( container_width - sub_menu_width );
                                $(element).children('.megamenu').css({'left': -left + 'px'});
                            }
                        })
                    }, 100);
                }
            }
        }
    }

    function bigshop_get_scrollbar_width() {
        var $inner = jQuery('<div style="width: 100%; height:200px;">test</div>'),
            $outer = jQuery('<div style="width:200px;height:150px; position: absolute; top: 0; left: 0; visibility: hidden; overflow:hidden;"></div>').append($inner),
            inner = $inner[0],
            outer = $outer[0];
        jQuery('body').append(outer);
        var width1 = inner.offsetWidth;
        $outer.css('overflow', 'scroll');
        var width2 = outer.clientWidth;
        $outer.remove();
        return (width1 - width2);
    }


    function bigshop_init_carousel() {
        $(".owl-carousel").each(function (index, el) {
            var config = $(this).data();
            config.navText = ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'];
            var animateOut = $(this).data('animateout');
            var animateIn = $(this).data('animatein');
            var slidespeed = $(this).data('slidespeed');
            if (typeof animateOut != 'undefined') {
                config.animateOut = animateOut;
            }
            if (typeof animateIn != 'undefined') {
                config.animateIn = animateIn;
            }
            if (typeof (slidespeed) != 'undefined') {
                config.smartSpeed = slidespeed;
            }

            if ($('body').hasClass('rtl')) {
                config.rtl = true;
            }
            if (bigshop_global_script.bigshop_enable_lazy == '1') {
                //config.lazyLoad = true;
            }

            var owl = $(this);
            owl.on('initialized.owl.carousel', function (event) {
                var total_active = owl.find('.owl-item.active').length;
                var i = 0;
                owl.find('.owl-item').removeClass('item-first item-last');
                setTimeout(function () {
                    owl.find('.owl-item.active').each(function () {
                        i++;
                        if (i == 1) {
                            $(this).addClass('item-first');
                        }
                        if (i == total_active) {
                            $(this).addClass('item-last');
                        }
                    });
                    
                    //lazy load for item that is cloned
                    bigshop_init_lazy_load();
                }, 100);
            })
            owl.on('refreshed.owl.carousel', function (event) {
                var total_active = owl.find('.owl-item.active').length;
                var i = 0;
                owl.find('.owl-item').removeClass('item-first item-last');
                setTimeout(function () {
                    owl.find('.owl-item.active').each(function () {
                        i++;
                        if (i == 1) {
                            $(this).addClass('item-first');
                        }
                        if (i == total_active) {
                            $(this).addClass('item-last');
                        }
                    });

                }, 100);
            })
            owl.on('change.owl.carousel', function (event) {
                var total_active = owl.find('.owl-item.active').length;
                var i = 0;
                owl.find('.owl-item').removeClass('item-first item-last');
                setTimeout(function () {
                    owl.find('.owl-item.active').each(function () {
                        i++;
                        if (i == 1) {
                            $(this).addClass('item-first');
                        }
                        if (i == total_active) {
                            $(this).addClass('item-last');
                        }
                    });

                }, 100);


            })
            owl.owlCarousel(config);

        });
    }

    /* ---------------------------------------------
     COUNTDOWN
     --------------------------------------------- */
    function bigshop_countdown() {
        if ($('.bigshop-countdown').length > 0) {
            var labels = ['Years', 'Months', 'Weeks', 'Days', 'Hrs', 'Mins', 'Secs'];
            var layout = '<span class="box-count day"><span class="number">{dnn}</span> <span class="text">Days</span></span><span class="dot">:</span><span class="box-count hrs"><span class="number">{hnn}</span> <span class="text">Hours</span></span><span class="dot">:</span><span class="box-count min"><span class="number">{mnn}</span> <span class="text">Mins</span></span><span class="dot">:</span><span class="box-count secs"><span class="number">{snn}</span> <span class="text">Secs</span></span>';
            $('.bigshop-countdown').each(function () {
                var austDay = new Date($(this).data('y'), $(this).data('m') - 1, $(this).data('d'), $(this).data('h'), $(this).data('i'), $(this).data('s'));
                $(this).countdown({
                    until: austDay,
                    labels: labels,
                    layout: layout
                });
            });
        }
    };

    /* ---------------------------------------------
     Woocommerce Quantily
     --------------------------------------------- */
    function bigshop_woo_quantily() {
        $('body').on('click', '.quantity .quantity-plus', function () {
            var obj_qty = $(this).closest('.quantity').find('input.qty'),
                val_qty = parseInt(obj_qty.val()),
                min_qty = parseInt(obj_qty.data('min')),
                max_qty = parseInt(obj_qty.data('max')),
                step_qty = parseInt(obj_qty.data('step'));
            val_qty = val_qty + step_qty;
            if (max_qty && val_qty > max_qty) {
                val_qty = max_qty;
            }
            obj_qty.val(val_qty);
            obj_qty.trigger("change");
            return false;
        });

        $('body').on('click', '.quantity .quantity-minus', function () {
            var obj_qty = $(this).closest('.quantity').find('input.qty'),
                val_qty = parseInt(obj_qty.val()),
                min_qty = parseInt(obj_qty.data('min')),
                max_qty = parseInt(obj_qty.data('max')),
                step_qty = parseInt(obj_qty.data('step'));
            val_qty = val_qty - step_qty;
            if (min_qty && val_qty < min_qty) {
                val_qty = min_qty;
            }
            if (!min_qty && val_qty < 0) {
                val_qty = 0;
            }
            obj_qty.val(val_qty);
            obj_qty.trigger("change");
            return false;
        });
    }

    function bigshop_init_lazy_load() {
        if ($("img.lazy").length > 0) {
            $("img.lazy").lazyload(
                {
                    effect: "fadeIn"
                }
            );
        }
        
    }

    function bigshop_ajax_lazy_load(contain) {
        if (contain != null) {
            var _lazy_img = $(contain).find('img.lazy');
        } else {
            var _lazy_img = $('img.lazy');
        }

        if (_lazy_img.length > 0) {
            _lazy_img.each(function () {
                if ($(this).data('original')) {
                    $(this).attr('src', $(this).data('original'));
                }
            });
        }
    }

    /* ---------------------------------------------
     TAB EFFECT
     --------------------------------------------- */
    function bigshop_tab_fade_effect() {
        // effect click
        $(document).on('click', '.bigshop-tabs .tab-link a', function () {
           
            var tab_id = $(this).attr('href');
            var tab_animated = $(this).attr('data-animate');

            tab_animated = ( tab_animated == undefined || tab_animated == "" ) ? '' : tab_animated;
            if (tab_animated == "") {
                return false;
            }

            $(tab_id).find('.product-list-owl .slick-active, .product-list-grid .product-item').each(function (i) {

                var t = $(this);
                var style = $(this).attr("style");
                style = ( style == undefined ) ? '' : style;
                var delay = i * 400;
                t.attr("style","-webkit-animation-delay:" + delay + "ms;"
                    + "-moz-animation-delay:" + delay + "ms;"
                    + "-o-animation-delay:" + delay + "ms;"
                    + "animation-delay:" + delay + "ms;" + style
                ).addClass(tab_animated + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    t.removeClass(tab_animated + ' animated');
                    t.attr("style", style);
                });
            })
            
        })
    }

    function bigshop_product_thumb_slider() {
        var div_data = $('.flex-control-nav.flex-control-thumbs');

        if (div_data.length > 0 && div_data.html() != "" && bigshop_global_script.bigshop_enable_product_thumb_slide == '1') {
            var config = [];
            if ($('.flex-control-nav.flex-control-thumbs').length > 0) {
                config.navText = ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'];
                var animateOut = $(this).data('animateout');
                var animateIn = $(this).data('animatein');
                var slidespeed = $(this).data('slidespeed');
                if (typeof animateOut != 'undefined') {
                    config.animateOut = animateOut;
                }
                if (typeof animateIn != 'undefined') {
                    config.animateIn = animateIn;
                }
                if (typeof (slidespeed) != 'undefined') {
                    config.smartSpeed = slidespeed;
                }

                if ($('body').hasClass('rtl')) {
                    config.rtl = true;
                }
                if (bigshop_global_script.bigshop_enable_lazy == '1') {
                    //config.lazyLoad = true;
                }
                config.nav = true;
                config.margin = 10;
                var responsive = {
                    0: {
                        items: bigshop_global_script.bigshop_thumbnail_ts_items
                    },
                    480: {
                        items: bigshop_global_script.bigshop_thumbnail_xs_items
                    },
                    768: {
                        items: bigshop_global_script.bigshop_thumbnail_sm_items
                    },
                    992: {
                        items: bigshop_global_script.bigshop_thumbnail_md_items
                    },
                    1200: {
                        items: bigshop_global_script.bigshop_thumbnail_lg_items
                    }
                };
                config.responsive = responsive;

                var owl = $('.flex-control-nav.flex-control-thumbs');
                owl.addClass('owl-carousel thumbnails_carousel nav-center');
                owl.owlCarousel(config);
            }
        }
    }

    /* ---------------------------------------------
     Ajax Tab
     --------------------------------------------- */
    $(document).on('click', '[data-ajax="1"]', function () {
        if (!$(this).hasClass('loaded')) {
            var atts = $(this).data('shortcode');
            var tab_id = $(this).attr('href');
            var t = $(this);

            $(tab_id).closest('.tab-container').append('<div class="cssload-wapper" style="min-height: 300px;position: relative"><div class="cssload-square"><div class="animation-tab"></div></div></div>');
            $(tab_id).closest('.panel-collapse').append('<div class="cssload-wapper" style="min-height: 300px;position: relative"><div class="cssload-square"><div class="animation-tab"></div></div></div>');
            $.ajax({
                type: 'POST',
                url: bigshop_ajax_frontend.ajaxurl,
                data: {
                    action: 'bigshop_ajax_tabs',
                    security: bigshop_ajax_frontend.security,
                    atts: atts
                },
                success: function (response) {
                    if (response['success'] == 'ok') {
                        $(tab_id).closest('.tab-container').find('.cssload-wapper').remove();
                        $(tab_id).closest('.panel-collapse').find('.cssload-wapper').remove();
                        $(tab_id).html(response['html']);
                        t.addClass('loaded');
                    }
                },
                complete: function () {
                    bigshop_init_carousel();
                    bigshop_tab_fade_effect();
                    bigshop_better_equal_elems();
                    bigshop_ajax_lazy_load(tab_id);
                }
            });
        }
    });

    function bigshop_google_maps() {
        if ($('.bigshop-google-maps').length <= 0) {
            return;
        }
        $('.bigshop-google-maps').each(function () {
            var $this = $(this),
                $id = $this.attr('id'),
                $title_maps = $this.attr('data-title_maps'),
                $phone = $this.attr('data-phone'),
                $email = $this.attr('data-email'),
                $zoom = parseInt($this.attr('data-zoom')),
                $latitude = $this.attr('data-latitude'),
                $longitude = $this.attr('data-longitude'),
                $address = $this.attr('data-address'),
                $map_type = $this.attr('data-map-type'),
                $pin_icon = $this.attr('data-pin-icon'),
                $modify_coloring = $this.attr('data-modify-coloring') === "true" ? true : false,
                $saturation = $this.attr('data-saturation'),
                $hue = $this.attr('data-hue'),
                $map_style = $this.data('map-style'),
                $styles;

            if ($modify_coloring == true) {
                var $styles = [
                    {
                        stylers: [
                            {hue: $hue},
                            {invert_lightness: false},
                            {saturation: $saturation},
                            {lightness: 1},
                            {
                                featureType: "landscape.man_made",
                                stylers: [{
                                    visibility: "on"
                                }]
                            }
                        ]
                    }, {
                        featureType: 'water',
                        elementType: 'geometry',
                        stylers: [
                            {color: '#46bcec'}
                        ]
                    }
                ];
            }
            var map;
            var bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                zoom: $zoom,
                panControl: true,
                zoomControl: true,
                mapTypeControl: true,
                scaleControl: true,
                draggable: true,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId[$map_type],
                styles: $styles
            };

            map = new google.maps.Map(document.getElementById($id), mapOptions);
            map.setTilt(45);

            // Multiple Markers
            var markers = [];
            var infoWindowContent = [];

            if ($latitude != '' && $longitude != '') {
                markers[0] = [$address, $latitude, $longitude];
                infoWindowContent[0] = [$address];
            }

            var infoWindow = new google.maps.InfoWindow(), marker, i;

            for (i = 0; i < markers.length; i++) {
                var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: markers[i][0],
                    icon: $pin_icon
                });
                if ($map_style == '1') {

                    if (infoWindowContent[i][0].length > 1) {
                        infoWindow.setContent(
                            '<div style="background-color:#fff; padding: 30px 30px 10px 25px; width:290px;line-height: 22px" class="bigshop-map-info">' +
                            '<h4 class="map-title">' + $title_maps + '</h4>' +
                            '<div class="map-field"><i class="fa fa-map-marker"></i><span>&nbsp;' + $address + '</span></div>' +
                            '<div class="map-field"><i class="fa fa-phone"></i><span>&nbsp;' + $phone + '</span></div>' +
                            '<div class="map-field"><i class="fa fa-envelope"></i><span><a href="mailto:' + $email + '">&nbsp;' + $email + '</a></span></div> ' +
                            '</div>'
                        );
                    }

                    infoWindow.open(map, marker);

                }
                if ($map_style == '2') {
                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            if (infoWindowContent[i][0].length > 1) {
                                infoWindow.setContent(
                                    '<div style="background-color:#fff; padding: 30px 30px 10px 25px; width:290px;line-height: 22px" class="bigshop-map-info">' +
                                    '<h4 class="map-title">' + $title_maps + '</h4>' +
                                    '<div class="map-field"><i class="fa fa-map-marker"></i><span>&nbsp;' + $address + '</span></div>' +
                                    '<div class="map-field"><i class="fa fa-phone"></i><span>&nbsp;' + $phone + '</span></div>' +
                                    '<div class="map-field"><i class="fa fa-envelope"></i><span><a href="mailto:' + $email + '">&nbsp;' + $email + '</a></span></div> ' +
                                    '</div>'
                                );
                            }

                            infoWindow.open(map, marker);
                        }
                    })(marker, i));
                }

                map.fitBounds(bounds);
            }

            var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
                this.setZoom($zoom);
                google.maps.event.removeListener(boundsListener);
            });
        });
    }

    //EQUAL ELEM
    function bigshop_better_equal_elems() {
        setTimeout(function () {
            $('.equal-container').each(function () {
                var $this = $(this);
                if ($this.find('.equal-elem').length) {
                    $this.find('.equal-elem').css({
                        'height': 'auto'
                    });
                    var elem_height = 0;
                    $this.find('.equal-elem').each(function () {
                        var this_elem_h = $(this).height();
                        if (elem_height < this_elem_h) {
                            elem_height = this_elem_h;
                        }
                    });
                    $this.find('.equal-elem').height(elem_height);
                }
            });
        }, 1000);
    }


    /* JAVASCRIPT SHOP PAGE */

    function bigshop_ajax_woocommerce() {
        /* AJAX REMOVE */
        $(document).on('click', '.bigshop-mini-cart .mini_cart_item a.remove', function (e) {
            var $this = $(this);
            var thisItem = $this.closest('.bigshop-mini-cart');
            var remove_url = $this.attr('href');
            var product_id = $this.attr('data-product_id');

            if (thisItem.is('.loading')) {
                return false;
            }

            if ($.trim(remove_url) !== '' && $.trim(remove_url) !== '#') {

                thisItem.addClass('loading');

                var nonce = bigshop_get_url_var('_wpnonce', remove_url);
                var cart_item_key = bigshop_get_url_var('remove_item', remove_url);

                var data = {
                    action: 'bigshop_remove_cart_item_via_ajax',
                    product_id: product_id,
                    cart_item_key: cart_item_key,
                    nonce: nonce
                };
                thisItem.find('.minicart-content').append('<div class="ajax-loader-wapper"><div class="ajax-loader"></div></div>');
                thisItem.find('.minicart-content').fadeTo("slow", 0.3);

                $.post(bigshop_ajax_frontend['ajaxurl'], data, function (response) {

                    if (response['err'] != 'yes') {
                        $('.bigshop-mini-cart').html(response['mini_cart_html']);
                    }
                    thisItem.removeClass('loading');
                    thisItem.find('.ajax-loader-wapper').remove();
                    thisItem.find('.minicart-content').fadeTo("slow", 1);
                    thisItem.find('.shopcart-description').addClass('open');


                });

                e.preventDefault();
            }

            return false;

        });

        function bigshop_get_url_var(key, url) {
            var result = new RegExp(key + "=([^&]*)", "i").exec(url);
            return result && result[1] || "";
        }

        /* SINGLE ADD TO CART */
        $(document).on('click', '.single_add_to_cart_button', function (e) {
            e.preventDefault();
            var _this = $(this);
            var _product_id = _this.val();
            var _form = _this.closest('form');
            var _form_data = _form.serialize();

            if (_product_id != '') {
                var _data = 'add-to-cart=' + _product_id + '&' + _form_data;
            } else {
                var _data = _form_data;
            }
            _this.addClass('loading');
            $.post(wc_add_to_cart_params.wc_ajax_url.toString().replace('wc-ajax=%%endpoint%%', ''), _data, function (response) {
                $(document.body).trigger('wc_fragment_refresh');
                _this.removeClass('loading');
            });
        });

        /* UPDATE WISHLIST */
        var bigshop_update_wishlist_count = function () {
            $.ajax({
                data: {
                    action: 'bigshop_update_wishlist_count'
                },
                success: function (data) {
                    //do something
                    $('.block-wishlist .count').text(data);
                },

                url: bigshop_ajax_frontend['ajaxurl']
            });
        };

        $('body').on('added_to_wishlist removed_from_wishlist', bigshop_update_wishlist_count);

        /*  VIEW GRID LIST */
        $(document).on('click', '.display-mode', function () {
            var _mode = $(this).data('mode');
            var _url = window.location.href;
            var xhttp;

            _url += _url.indexOf("?") === -1 ? "?" : "&" + 'shop_display_mode=' + _mode;

            $(this).addClass('active').siblings().removeClass('active');
            $('body').append('<div class="ajax-loader-wapper"><div class="ajax-loader"></div></div>');
            $('body').fadeTo("slow", 0.3);

            if (window.XMLHttpRequest)
                xhttp = new XMLHttpRequest();
            else
                xhttp = new ActiveXObject("Microsoft.XMLHTTP");
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    $('.product-grid').html($(xhttp.responseText).find('.product-grid').html());
                    $('body').find('.ajax-loader-wapper').remove();
                    $('body').fadeTo("slow", 1);
                    bigshop_ajax_lazy_load(null);
                }
            };
            xhttp.open("POST", _url, true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send("shop_display_mode=" + _mode);
            return false;
        });
    }

    /* JAVASCRIPT SHOP PAGE */

    /* ---------------------------------------------
     Init popup
     --------------------------------------------- */
    function bigshop_init_popup() {
        if (bigshop_global_script.bigshop_enable_popup_mobile != 1) {
            if ($(window).width() + bigshop_get_scrollbar_width() < 768) {
                return false;
            }
        }
        var disabled_popup_by_user = getCookie('bigshop_disabled_popup_by_user');
        if (disabled_popup_by_user == 'true') {
            return false;
        } else {
            if ($('body').hasClass('home') && bigshop_global_script.bigshop_enable_popup == 1) {
                setTimeout(function () {
                    $('#popup-newsletter').modal({
                        keyboard: false
                    })
                }, bigshop_global_script.bigshop_popup_delay_time);

            }
        }
    }

    $(document).on('change', '.bigshop_disabled_popup_by_user', function () {
        if ($(this).is(":checked")) {
            setCookie("bigshop_disabled_popup_by_user", 'true', 7);
        } else {
            setCookie("bigshop_disabled_popup_by_user", '', 0);
        }
    });

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    /* BACK TO TOP */
    $(document).on('click', 'a.backtotop', function () {
        $('html, body').animate({scrollTop: 0}, 800);
    });

    function bigshop_show_other_item_vertical_menu() {
        var window_size = jQuery('body').innerWidth();
        window_size += bigshop_get_scrollbar_width();
        if ($('.block-nav-categori').length > 0) {
            $('.block-nav-categori').each(function () {
                var all_item = 0;
                var limit_item = $(this).data('items') - 1;
                if (window_size <= 1024) {
                    limit_item = $(this).data('tabletitems') - 1;
                }
                all_item = $(this).find('.vertical-menu>li').length;

                if (all_item > (limit_item + 1)) {
                    $(this).addClass('show-button-all');
                }
                $(this).find('.vertical-menu>li').each(function (i) {
                    all_item = all_item + 1;
                    if (i > limit_item) {
                        $(this).addClass('link-orther');
                    }
                })
            })
        }
    }

    // Clone all Zan Menus for mobile
    function bigshop_clone_all_menus() {

        if (!$('.bigshop-clone-wrap').length && $('.clone-mobile-menu').length > 0) {
            $('body').prepend('<div class="bigshop-clone-wrap">' +
                '<div class="bigshop-panels-actions-wrap"><a class="bigshop-close-btn bigshop-close-panels" href="#">x</a></div>' +
                '<div class="bigshop-panels"></div>' +
                '</div>');
        }

        var i                = 0;
        var panels_html_args = Array();
        $('.clone-mobile-menu').each(function () {
            var $this              = $(this);
            var thisMenu           = $this;
            var this_menu_id       = thisMenu.attr('id');
            var this_menu_clone_id = 'bigshop-clone-' + this_menu_id;

            if (!$('#' + this_menu_clone_id).length) {
                var thisClone = $this.clone(true); // Clone Wrap
                thisClone.find('.menu-item').addClass('clone-menu-item');

                thisClone.find('[id]').each(function () {
                    // Change all tab links with href = this id
                    thisClone.find('.vc_tta-panel-heading a[href="#' + $(this).attr('id') + '"]').attr('href', '#' + bigshop_add_string_prefix($(this).attr('id'), 'bigshop-clone-'));
                    thisClone.find('.bigshop-tabs .tabs-link a[href="#' + $(this).attr('id') + '"]').attr('href', '#' + bigshop_add_string_prefix($(this).attr('id'), 'bigshop-clone-'));
                    $(this).attr('id', bigshop_add_string_prefix($(this).attr('id'), 'bigshop-clone-'));
                });
                thisClone.find('.bigshop-menu').addClass('bigshop-menu-clone');

                // Create main panel if not exists
                if (!$('.bigshop-clone-wrap .bigshop-panels #bigshop-panel-' + this_menu_id).length) {
                    $('.bigshop-clone-wrap .bigshop-panels').append('<div id="bigshop-panel-' + this_menu_id + '" class="bigshop-panel bigshop-main-panel"><ul class="depth-01"></ul></div>');
                }
                var thisMainPanel = $('.bigshop-clone-wrap .bigshop-panels #bigshop-panel-' + this_menu_id + ' ul');
                thisMainPanel.html(thisClone.html());

                bigshop_insert_children_panels_html_by_elem(thisMainPanel, i);

            }

        });
    }


    // i: For next nav target
    function bigshop_insert_children_panels_html_by_elem($elem, i) {

        if ($elem.find('.menu-item-has-children').length) {
            $elem.find('.menu-item-has-children').each(function () {
                var thisChildItem = $(this);
                bigshop_insert_children_panels_html_by_elem(thisChildItem, i);

                var next_nav_target = 'bigshop-panel-' + i;

                // Make sure there is no duplicate panel id
                while ($('#' + next_nav_target).length) {
                    i++;
                    next_nav_target = 'bigshop-panel-' + i;
                }

                // Insert Next Nav
                thisChildItem.prepend('<a class="bigshop-next-panel" href="#' + next_nav_target + '" data-target="#' + next_nav_target + '"></a>');

                // Get sub menu html
                var submenu_html = $('<div>').append(thisChildItem.find('> .submenu').clone()).html();
                thisChildItem.find('> .submenu').remove();

                $('.bigshop-clone-wrap .bigshop-panels').append('<div id="' + next_nav_target + '" class="bigshop-panel bigshop-sub-panel bigshop-hidden">' + submenu_html + '</div>');

            });
        }

    }

    function bigshop_add_string_prefix(str, prefix) {
        return prefix + str;
    }


    // Open next panel
    $(document).on('click', '.bigshop-next-panel', function (e) {
        var $this     = $(this);
        var thisItem  = $this.closest('.menu-item');
        var thisPanel = $this.closest('.bigshop-panel');
        var target_id = $this.attr('href');

        if ($(target_id).length) {
            thisPanel.addClass('bigshop-sub-opened');
            $(target_id).addClass('bigshop-panel-opened').removeClass('bigshop-hidden').attr('data-parent-panel', thisPanel.attr('id'));
            // Insert current panel title
            var item_title = thisItem.find('.bigshop-item-title').attr('title');
            var firstItemTitle = '';
            if( $('.bigshop-panels-actions-wrap .bigshop-current-panel-title').length >0){
                firstItemTitle = $('.bigshop-panels-actions-wrap .bigshop-current-panel-title').html();
            }


            if (typeof item_title != 'undefined' && typeof item_title != false) {
                if (!$('.bigshop-panels-actions-wrap .bigshop-current-panel-title').length) {
                    $('.bigshop-panels-actions-wrap').prepend('<span class="bigshop-current-panel-title"></span>');
                }
                $('.bigshop-panels-actions-wrap .bigshop-current-panel-title').html(item_title);
            }
            else {
                $('.bigshop-panels-actions-wrap .bigshop-current-panel-title').remove();
            }

            // Back to previous panel
            $('.bigshop-panels-actions-wrap .bigshop-prev-panel').remove();
            $('.bigshop-panels-actions-wrap').prepend('<a data-prenttitle="'+firstItemTitle+'" class="bigshop-prev-panel" href="#' + thisPanel.attr('id') + '" data-cur-panel="' + target_id + '" data-target="#' + thisPanel.attr('id') + '"></a>');
        }
        $('.bigshop-next-panel').closest('.bigshop-panels').find('.bigshop-panel').animate({scrollTop: 0}, 500);
        e.preventDefault();
    });

    // Go to previous panel
    $(document).on('click', '.bigshop-prev-panel', function (e) {
        var $this        = $(this);
        var cur_panel_id = $this.attr('data-cur-panel');
        var target_id    = $this.attr('href');

        $(cur_panel_id).removeClass('bigshop-panel-opened').addClass('bigshop-hidden');
        $(target_id).addClass('bigshop-panel-opened').removeClass('bigshop-sub-opened');

        // Set new back button
        var new_parent_panel_id = $(target_id).attr('data-parent-panel');
        if (typeof new_parent_panel_id == 'undefined' || typeof new_parent_panel_id == false) {
            $('.bigshop-panels-actions-wrap .bigshop-prev-panel').remove();
            $('.bigshop-panels-actions-wrap .bigshop-current-panel-title').remove();
        }
        else {
            $('.bigshop-panels-actions-wrap .bigshop-prev-panel').attr('href', '#' + new_parent_panel_id).attr('data-cur-panel', target_id).attr('data-target', '#' + new_parent_panel_id);
            // Insert new panel title
            var item_title = $('#' + new_parent_panel_id).find('.bigshop-next-panel[data-target="' + target_id + '"]').closest('.menu-item').find('.bigshop-item-title').attr('data-title');
            item_title = $(this).data('prenttitle');
            if (typeof item_title != 'undefined' && typeof item_title != false) {
                if (!$('.bigshop-panels-actions-wrap .bigshop-current-panel-title').length) {
                    $('.bigshop-panels-actions-wrap').prepend('<span class="bigshop-current-panel-title"></span>');
                }
                $('.bigshop-panels-actions-wrap .bigshop-current-panel-title').html(item_title);
            }
            else {
                $('.bigshop-panels-actions-wrap .bigshop-current-panel-title').remove();
            }
        }

        e.preventDefault();

    });

    // BOX MOBILE MENU
    $(document).on('click','.mobile-navigation',function(){
        $('.bigshop-clone-wrap').addClass('open');
        return false;
    });
    // Close box menu
    $(document).on('click','.bigshop-clone-wrap .bigshop-close-panels',function(){
        $('.bigshop-clone-wrap').removeClass('open');
        return false;
    });

    //STICKY MENU
    function bigshop_sticky_menu() {
        //Sticky Menu
        if (bigshop_global_script.bigshop_enable_sticky_menu == 'yes') {
            var window_size = jQuery('body').innerWidth();
            window_size += bigshop_get_scrollbar_width();
            if (window_size > 991) {
                var max_h = $('#header').innerHeight();
                var vertical_menu_height = 0;
                var vertical_menu_title_height = 0;
                var topSpacing = 0;

                if ($('.block-nav-categori.always-open').length > 0) {
                    vertical_menu_height = $('.block-nav-categori.always-open .block-content').innerHeight();
                    vertical_menu_title_height = $('.block-nav-categori.always-open .block-title').innerHeight();
                }

                if (vertical_menu_height > 0) {
                    topSpacing = '-' + ( ( max_h + vertical_menu_height) - vertical_menu_title_height);
                }

                $('.header-sticky').sticky({topSpacing: topSpacing});
                $('.header-sticky').on('sticky-start', function () {
                    $('#header').find('.block-nav-categori').removeClass('has-open').removeClass('is-home').removeClass('open-on-home');
                });
                $('.header-sticky').on('sticky-end', function () {
                    if ($('#header').find('.block-nav-categori').hasClass('always-open')) {
                        $('#header').find('.block-nav-categori').addClass('open-on-home').addClass('is-home');
                    }
                });

                $('.header.style3').find('.main-menu').clone().appendTo('.sticky-wrapper .header-nav-inner');
            }
        }
    }

    //Product style 6 thumb slider
    function bigshop_product6_list_thumb_slider(){
        $(document).on('click', '.bigshop_product6_loop_thumbs .img_thumb img', function(){
            console.log( 'clicked' );
            var $this = $(this);
            var large_url = $this.attr('data-large_url');
            
            $('.bigshop_product6_loop_thumbs .img_thumb img').removeClass('current');
            
            $this.closest( '.bigshop-product06' ).find( '.bigshop_product6_main_thum .thumb-inner img' ).attr( 'src', large_url );
            $this.addClass('current');
        });
    }
    
    function bigshop_is_top_header_empty(){
        var is_content_left_empty = $.trim($('.header .topbar-content.left').html()).length;
        var is_top_note_empty = $('.header .top-note').length;
        var is_nav_right_empty = $('.header .bigshop-nav.right').length;
        
        if( is_content_left_empty == 0 && is_top_note_empty == 0 && is_nav_right_empty == 0 ){
            $('.header .top-header').addClass( 'header_empty' );
        }
    }

    function bigshop_remove_empty_wpml_submenu(){
        if ( $('.wpml-ls-sub-menu').children().length == 0 ) {
            $('.wpml-ls-sub-menu').hide();
        }
    }

    /* ---------------------------------------------
     Scripts After Ajax
     --------------------------------------------- */

    $(document).ajaxComplete(function (event, xhr, settings) {

    });

    /* ---------------------------------------------
     Scripts bind
     --------------------------------------------- */

    $(window).bind("load", function () {
        bigshop_better_equal_elems();
        bigshop_init_carousel();
    });

    /* ---------------------------------------------
     Scripts load
     --------------------------------------------- */

    $(window).load(function () {

        bigshop_product_thumb_slider();
    });

    /* ---------------------------------------------
     Scripts resize
     --------------------------------------------- */

    $(window).on("resize", function () {

        bigshop_better_equal_elems();
        bigshop_resizeMegamenu();
    });

    /* ---------------------------------------------
     Scripts scroll
     --------------------------------------------- */

    $(window).scroll(function () {
        if ($(window).scrollTop() > 500) {
            $('.backtotop').show();
        } else {
            $('.backtotop').hide();
        }
    });

    /* ---------------------------------------------
     Scripts ready
     --------------------------------------------- */
    $(document).ready(function () {
        bigshop_clone_all_menus();
        bigshop_init_popup();
        bigshop_countdown();
        bigshop_woo_quantily();
        bigshop_tab_fade_effect();
        bigshop_google_maps();
        bigshop_resizeMegamenu();
        bigshop_ajax_woocommerce();
        bigshop_show_other_item_vertical_menu();
        bigshop_init_menu_reposive();
        bigshop_init_lazy_load();
        bigshop_sticky_menu();
        bigshop_product6_list_thumb_slider();
        bigshop_show_cart();
        bigshop_is_top_header_empty();
        bigshop_remove_empty_wpml_submenu();

        if ($('.shop-top-control select').length > 0) {
            $('.shop-top-control select').chosen();
        }
        if ($('.category-search-option').length > 0) {
            $('.category-search-option').chosen();
        }

        function bigshop_show_cart() {
            var window_size = jQuery('body').innerWidth();
                window_size += bigshop_get_scrollbar_width();

            if (window_size >= 640) {
                $(document).on('click', '.block-minicart .shopcart-link', function () {
                    $(this).closest('.block-minicart').find('.shopcart-description').addClass('open');
                    return false;
                })
                $(document).on('click', '.block-minicart .close-minicart', function () {
                    $(this).closest('.block-minicart').find('.shopcart-description').removeClass('open');
                    return false;
                }) 
            }
        }

        $(document).on('click', function (e) {
            var target = $(e.target);
            if (!target.closest('.block-minicart').length) {
                $('.block-minicart').find('.shopcart-description').removeClass('open');
            }
        });

        /*  [ All Categorie ]
         - - - - - - - - - - - - - - - - - - - - */
        $(document).on('click', '.open-cate', function (e) {
            e.preventDefault();
            $(this).closest('.block-nav-categori').find('li.link-orther').each(function () {
                $(this).slideDown();
            });
            var closetext = $(this).data('closetext');
            $(this).addClass('close-cate').removeClass('open-cate').html(closetext);
        })
        /* Close Categorie */
        $(document).on('click', '.close-cate', function () {
            $(this).closest('.block-nav-categori').find('li.link-orther').each(function () {
                $(this).slideUp();
            });
            var alltext = $(this).data('alltext');
            $(this).addClass('open-cate').removeClass('close-cate').html(alltext);
            return false;
        })
        $(".block-nav-categori .block-title").on('click', function () {
            $(this).toggleClass('active');
            $(this).parent().toggleClass('has-open');
            $("body").toggleClass("categori-open");
            return false;
        });

        $(document).on('vc-full-width-row-single', function () {
            setTimeout(function () {
                bigshop_init_carousel();
            },100);
        });
    });
})(jQuery); // End of use strict