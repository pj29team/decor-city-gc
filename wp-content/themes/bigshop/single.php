<?php
    /**
     * The main template file.
     *
     * This is the most generic template file in a WordPress theme
     * and one of the two required files for a theme (the other being style.css).
     * It is used to display a page when nothing more specific matches a query.
     * E.g., it puts together the home page when no home.php file exists.
     *
     * @link https://codex.wordpress.org/Template_Hierarchy
     *
     * @package Bigshop
     */
?>
<?php get_header(); ?>
<?php

    /* Blog Layout */
    $bigshop_blog_layout = bigshop_get_option( 'sidebar_blog_layout', 'left' );

    if ( is_single() ) {
        /*Single post layout*/
        $bigshop_blog_layout = bigshop_get_option( 'sidebar_single_post_position', 'left' );
    }

    /* Blog Style */
    $bigshop_blog_style = bigshop_get_option('blog_list_style','standard');

    $bigshop_container_class = array( 'main-container' );

    if ( $bigshop_blog_layout == 'full' ) {
        $bigshop_container_class[] = 'no-sidebar';
    } else {
        $bigshop_container_class[] = $bigshop_blog_layout . '-sidebar';
    }
    $bigshop_content_class   = array();
    $bigshop_content_class[] = 'main-content';
    if ( $bigshop_blog_layout == 'full' ) {
        $bigshop_content_class[] = 'col-sm-12';
    } else {
        $bigshop_content_class[] = 'col-lg-9 col-md-8';
    }

    $bigshop_slidebar_class   = array();
    $bigshop_slidebar_class[] = 'sidebar';
    if ( $bigshop_blog_layout != 'full' ) {
        $bigshop_slidebar_class[] = 'col-lg-3 col-md-4';
    }

?>
<?php get_template_part( 'template-parts/part', 'breadcrumb' ); ?>
<div class="<?php echo esc_attr( implode( ' ', $bigshop_container_class ) ); ?>">
    <h3 class="page-title">Blog Post</h3>
    <div class="container">
        <div class="row">
            <div class="<?php echo esc_attr( implode( ' ', $bigshop_content_class ) ); ?>">
                <?php
                    while (have_posts()): the_post();
                        get_template_part( 'templates/blogs/single' );

                        /*If comments are open or we have at least one comment, load up the comment template.*/
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;
                    endwhile;
                ?>
            </div>
            <?php if ( $bigshop_blog_layout != "full" ): ?>
                <div class="<?php echo esc_attr( implode( ' ', $bigshop_slidebar_class ) ); ?>">
                    <?php get_sidebar(); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
