<?php
    /**
     * Template Name: Page No Title
     *
     * @package WordPress
     * @subpackage bigshop
     * @since bigshop 1.0
     */
get_header(); ?>
<?php
    /* Data MetaBox */
    $data_meta = get_post_meta( get_the_ID(), '_custom_page_side_options', true );

    if ( empty( $data_meta ) ) {
        /*Default page layout*/
        $bigshop_page_extra_class = '';
        $bigshop_page_layout      = 'left';
        $bigshop_page_sidebar     = 'widget-area';
    } else {
        /*Default page layout*/
        $bigshop_page_extra_class = $data_meta[ 'page_extra_class' ];
        $bigshop_page_layout      = $data_meta[ 'sidebar_page_layout' ];
        $bigshop_page_sidebar     = $data_meta[ 'page_sidebar' ];
    }

    /*Main container class*/
    $bigshop_main_container_class   = array();
    $bigshop_main_container_class[] = $bigshop_page_extra_class;
    $bigshop_main_container_class[] = 'main-container';
    if ( $bigshop_page_layout == 'full' ) {
        $bigshop_main_container_class[] = 'no-sidebar';
    } else {
        $bigshop_main_container_class[] = $bigshop_page_layout . '-sidebar';
    }
    $bigshop_main_content_class   = array();
    $bigshop_main_content_class[] = 'main-content';
    if ( $bigshop_page_layout == 'full' ) {
        $bigshop_main_content_class[] = 'col-sm-12';
    } else {
        $bigshop_main_content_class[] = 'col-lg-9 col-md-8';
    }
    $bigshop_slidebar_class   = array();
    $bigshop_slidebar_class[] = 'sidebar';
    if ( $bigshop_page_layout != 'full' ) {
        $bigshop_slidebar_class[] = 'col-lg-3 col-md-4';
    }
?>
<?php get_template_part( 'template-parts/part', 'breadcrumb' ); ?>
    <main class="site-main <?php echo esc_attr( implode( ' ', $bigshop_main_container_class ) ); ?>">
        <div class="container">

            <div class="row">
                <div class="<?php echo esc_attr( implode( ' ', $bigshop_main_content_class ) ); ?>">
                    <?php
                        if ( have_posts() ) {
                            while ( have_posts() ) {
                                the_post();
                                ?>
                                <div class="page-main-content">
                                    <?php
                                        the_content();
                                        wp_link_pages( array(
                                                'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'bigshop' ) . '</span>',
                                                'after'       => '</div>',
                                                'link_before' => '<span>',
                                                'link_after'  => '</span>',
                                                'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'bigshop' ) . ' </span>%',
                                                'separator'   => '<span class="screen-reader-text">, </span>',
                                            )
                                        );
                                    ?>
                                </div>
                                <?php
                                // If comments are open or we have at least one comment, load up the comment template.
                                if ( comments_open() || get_comments_number() ) :
                                    comments_template();
                                endif;
                                ?>
                                <?php
                            }
                        }
                    ?>
                </div>
                <?php if ( $bigshop_page_layout != "full" ): ?>
                    <?php if ( is_active_sidebar( $bigshop_page_sidebar ) ) : ?>
                        <div id="widget-area"
                             class="widget-area <?php echo esc_attr( implode( ' ', $bigshop_slidebar_class ) ); ?>">
                            <?php dynamic_sidebar( $bigshop_page_sidebar ); ?>
                        </div><!-- .widget-area -->
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </main>
<?php get_footer(); ?>