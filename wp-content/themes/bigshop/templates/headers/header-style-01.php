<?php
/*
 Name:  Header style 01
 */

?>

<header id="header" class="header style1">
    <div class="top-header">
        <div class="container">
            <?php  $bigshop_top_note = bigshop_get_option('bigshop_top_note', '');
                if( $bigshop_top_note != '' ) : ?>
                <div class="top-note"><?php  echo balanceTags( trim( $bigshop_top_note ) ); ?></div>
                <?php endif;?>
                <div class="topbar-content left">
                <?php
                    do_action('wcml_currency_switcher', array(
                      'format' => '%code%',
                    ));
                    
                    do_action('icl_language_selector');
                    wp_nav_menu( array(
                            'menu'            => 'top_left_menu',
                            'theme_location'  => 'top_left_menu',
                            'depth'           => 1,
                            'container'       => '',
                            'container_class' => '',
                            'container_id'    => '',
                            'menu_class'      => 'top-bar-menu left',
                            'fallback_cb'     => 'Bigshop_navwalker::fallback',
                            'walker'          => new Bigshop_navwalker(),
                        )
                    );?>
                </div>
                <?php 
                wp_nav_menu( array(
                        'menu'            => 'top_right_menu',
                        'theme_location'  => 'top_right_menu',
                        'depth'           => 1,
                        'container'       => '',
                        'container_class' => '',
                        'container_id'    => '',
                        'menu_class'      => 'bigshop-nav top-bar-menu right',
                        'fallback_cb'     => 'Bigshop_navwalker::fallback',
                        'walker'          => new Bigshop_navwalker(),
                    )
                );
            ?>
        </div>
    </div>
    <div class="main-header">
        <div class="container">
            <div class="header-content">
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <div class="logo">
                            <?php bigshop_get_logo(); ?>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-5 col-xs-9">
                        <?php get_template_part('template-parts/search-form');?>
                    </div>
                    <div class="col-sm-2 col-md-3 col-xs-3">
                        <?php if( class_exists( 'WooCommerce' ) ):?>
                            <div class="block-minicart bigshop-mini-cart"><?php bigshop_mini_cart(); ?></div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        $opt_enable_vertical_menu = bigshop_get_option('opt_enable_vertical_menu', '');
        $opt_click_open_vertical_menu = bigshop_get_option('opt_click_open_vertical_menu', '');
        $opt_vertical_item_visible = apply_filters('bigshop_header_vertical_menu_limit_items', bigshop_get_option('opt_vertical_item_visible', 10) );
        $header_nav_class = array('header-nav header-sticky');
        if( $opt_enable_vertical_menu == '1' ){
            $header_nav_class[] = 'has-vertical-menu';
        }

    ?>

    <div class="<?php echo esc_attr( implode(' ', $header_nav_class) );?>">
        <div class="container">
            <div class="header-nav-inner">
                <?php if( $opt_enable_vertical_menu == '1'):?>
                    <?php
                    $block_vertical_class = array('vertical-wapper block-nav-categori');
                    if( ($opt_click_open_vertical_menu == '' ) && ( is_front_page() || ( isset($_GET['bigshop_is_home']) && $_GET['bigshop_is_home'] == 'true' ) || bigshop_single_page_is_homepage() )){
                        $block_vertical_class[] = 'open-on-home is-home always-open';
                    }
                    $opt_vertical_menu_title  = bigshop_get_option('opt_vertical_menu_title','All Category');
                    $opt_vertical_menu_button_all_text  = bigshop_get_option('opt_vertical_menu_button_all_text','All Categories');
                    $opt_vertical_menu_button_close_text  = bigshop_get_option('opt_vertical_menu_button_close_text','Close');
                    ?>
                    <!-- block categori -->
                    <div data-items="<?php echo esc_attr( $opt_vertical_item_visible );?>" class="<?php echo esc_attr( implode(' ', $block_vertical_class) );?>">
                        <div class="block-title">
                            <span class="icon-bar">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                            <span class="text"><?php echo esc_html($opt_vertical_menu_title);?></span>
                        </div>
                        <div class="block-content verticalmenu-content">
                            <?php
                                wp_nav_menu( array(
                                    'menu'            => 'vetical_menu',
                                    'theme_location'  => 'vetical_menu',
                                    'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => 'clone-mobile-menu bigshop-nav vertical-menu ',
                                    'fallback_cb'     => 'Bigshop_navwalker::fallback',
                                    'walker'          => new Bigshop_navwalker()
                                ));
                            ?>
                            <div class="view-all-categori">
                                <a href="#" data-closetext="<?php echo esc_attr( $opt_vertical_menu_button_close_text );?>" data-alltext="<?php echo esc_attr( $opt_vertical_menu_button_all_text )?>" class="btn-view-all open-cate"><?php echo esc_html( $opt_vertical_menu_button_all_text )?></a>
                            </div>
                        </div>
                    </div><!-- block categori -->
                <?php endif;?>
                <div class="box-header-nav">
                    <a class="menu-bar mobile-navigation" href="#">
                        <span class="icon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                        <span class="text"><?php esc_html_e('Main Menu','bigshop');?></span>
                    </a>
                    <?php
                        wp_nav_menu( array(
                            'menu'            => 'primary',
                            'theme_location'  => 'primary',
                            'container'       => '',
                            'container_class' => '',
                            'container_id'    => '',
                            'menu_class'      => 'clone-mobile-menu bigshop-nav main-menu',
                            'fallback_cb'     => 'Bigshop_navwalker::fallback',
                            'walker'          => new Bigshop_navwalker()
                        ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</header>