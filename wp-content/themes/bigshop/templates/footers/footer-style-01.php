<?php
/*
 Name:  Style 01
 */
?>
<footer class="bigshop-footer-layout1 footer-id-<?php echo get_the_ID(); ?>">
    <div class="footer layout1">
        <div class="container">
            <?php the_content();?>
        </div>
    </div>
</footer>

