<div class="single-post-detail">
    <article <?php post_class('post-item');?>>
        <?php bigshop_post_thumbnail();?>
        <div class="post-info">
            <div class="metas">
                <div class="inner">
                    <?php
                        if ( is_sticky() && is_home() && ! is_paged() ) {
                            printf( '<span class="sticky-post">%s</span>', esc_html__( 'Sticky', 'bigshop' ) );
                        }
                    ?>
                    <span class="author"><?php the_author();?></span>
                    <span class="time"><?php echo get_the_date();?></span>
                </div>
            </div>
            <h3 class="post-name"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
            <div class="post-content"><?php the_content(); ?></div>
            
            <div class="meta-info">
                <?php if( has_tag() ):?>
                <div class="tags">
                    <label><?php esc_html_e('Tags:','bigshop');?></label>
                    <?php the_tags( '', ', ' ); ?>
                </div>
                <?php endif;?>
            </div>
        </div>
        <?php
            wp_link_pages( array(
                'before'      => '<div class="page-links">',
                'after'       => '</div>',
                'link_before' => '<span>',
                'link_after'  => '</span>',
                'pagelink'    => '%',
                'separator'   => '',
            ) );
        ?>
    </article>
</div>