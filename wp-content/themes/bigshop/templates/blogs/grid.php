<?php
    $bigshop_blog_lg_items = bigshop_get_option('bigshop_blog_lg_items',4);
    $bigshop_blog_md_items = bigshop_get_option('bigshop_blog_md_items',4);
    $bigshop_blog_sm_items = bigshop_get_option('bigshop_blog_sm_items',3);
    $bigshop_blog_xs_items = bigshop_get_option('bigshop_blog_xs_items',6);
    $bigshop_blog_ts_items = bigshop_get_option('bigshop_blog_ts_items',12);

    $blog_item_class = 'post-item';
    $blog_item_class_col = '';
    $blog_item_class_col .=' col-lg-'.$bigshop_blog_lg_items;
    $blog_item_class_col .=' col-md-'.$bigshop_blog_md_items;
    $blog_item_class_col .=' col-sm-'.$bigshop_blog_sm_items;
    $blog_item_class_col .=' col-xs-'.$bigshop_blog_xs_items;
    $blog_item_class_col .=' col-ts-'.$bigshop_blog_ts_items;
?>
<div class="blog-posts grid row auto-clear ">
    <?php if( have_posts()):?>
        <?php
        while( have_posts()){
            the_post();
            ?>
            <div class="<?php echo esc_attr($blog_item_class_col);?>">
                <article <?php post_class('post-item');?>>
                    <?php get_template_part('templates/blogs/loop');?>
                </article>
            </div>

            <?php
        }
        ?>
    <?php else:?>
        <?php get_template_part( 'content', 'none' );?>
    <?php endif;?>
</div>