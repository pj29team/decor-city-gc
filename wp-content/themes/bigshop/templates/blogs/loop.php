<?php bigshop_post_thumbnail();?>
<div class="post-info">
    <div class="metas">
       <div class="inner">
           <?php
               if ( is_sticky() && is_home() && ! is_paged() ) {
                   printf( '<span class="sticky-post">%s</span>', esc_html__( 'Sticky', 'bigshop' ) );
               }
           ?>
           <span class="author"><?php the_author();?></span>
           <span class="time"><?php echo get_the_date();?></span>
       </div>
    </div>
    <h3 class="post-name"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
    <div class="post-excerpt"><?php echo wp_trim_words(apply_filters('the_excerpt', get_the_excerpt()), 25, __('...', 'bigshop')); ?></div>
</div>
<div class="post-footer clear-list">
    <span class="comments">
        <?php comments_number( '0 Comment', '1 Comment', '% Comments' ); ?>.
    </span>
    <a href="<?php the_permalink();?>" class="button primary readmore"><?php esc_html_e('Read more','bigshop');?></a>
</div>
