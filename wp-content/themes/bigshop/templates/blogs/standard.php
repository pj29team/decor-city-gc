<div class="blog-posts standard">
    <?php if( have_posts()):?>
    <?php
        while( have_posts()){
            the_post();
            ?>
            <article <?php post_class('post-item');?>>
                <?php get_template_part('templates/blogs/loop');?>
            </article>
            <?php
        }
    ?>
    <?php else:?>
        <?php get_template_part( 'content', 'none' );?>
    <?php endif;?>
</div>