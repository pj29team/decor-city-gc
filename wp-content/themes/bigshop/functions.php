<?php
if ( !isset( $content_width ) ) $content_width = 900;
if ( !class_exists( 'Bigshop_Functions' ) ) {
	class Bigshop_Functions
	{
		/**
		 * Instance of the class.
		 *
		 * @since   1.0.0
		 *
		 * @var   object
		 */
		protected static $instance = null;

		/**
		 * Initialize the plugin by setting localization and loading public scripts
		 * and styles.
		 *
		 * @since    1.0.0
		 */
		public function __construct()
		{
			add_action( 'after_setup_theme', array( $this, 'bigshop_settup' ) );
			add_action( 'widgets_init', array( $this, 'widgets_init' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );
			add_filter( 'get_default_comment_status', array( $this, 'open_default_comments_for_page' ), 10, 3 );
			add_filter( 'comment_form_fields', array( &$this, 'bigshop_move_comment_field_to_bottom' ), 10, 3 );
			$this->includes();
		}

		public function bigshop_settup()
		{
			/*
			* Make theme available for translation.
			* Translations can be filed in the /languages/ directory.
			* If you're building a theme based on boutique, use a find and replace
			* to change 'bigshop' to the name of your theme in all the template files
			*/
			load_theme_textdomain( 'bigshop', get_template_directory() . '/languages' );
			add_theme_support( 'automatic-feed-links' );
			/*
			 * Let WordPress manage the document title.
			 * By adding theme support, we declare that this theme does not use a
			 * hard-coded <title> tag in the document head, and expect WordPress to
			 * provide it for us.
			 */
			add_theme_support( 'title-tag' );
			/*
			 * Enable support for Post Thumbnails on posts and pages.
			 *
			 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
			 */
			add_theme_support( 'post-thumbnails' );
			add_theme_support( "custom-header" );
			add_theme_support( 'custom-background' );


			/*This theme uses wp_nav_menu() in two locations.*/
			register_nav_menus( array(
					'primary'        => esc_html__('Primary Menu', 'bigshop'),
                    'vetical_menu'   => esc_html__('Vertical Menu', 'bigshop'),
                    'top_left_menu'  => esc_html__('Top Left Menu', 'bigshop'),
                    'top_right_menu' => esc_html__( 'Top Right Menu', 'bigshop' ),
				)
			);
			/*
			 * Switch default core markup for search form, comment form, and comments
			 * to output valid HTML5.
			 */
			add_theme_support( 'html5', array(
					'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
				)
			);
			/*Support woocommerce*/
			add_theme_support( 'woocommerce' );
			add_theme_support( 'wc-product-gallery-lightbox' );
			add_theme_support( 'wc-product-gallery-slider' );
			add_theme_support( 'wc-product-gallery-zoom' );
		}

		public function bigshop_move_comment_field_to_bottom( $fields )
		{
			$comment_field = $fields[ 'comment' ];
			unset( $fields[ 'comment' ] );
			$fields[ 'comment' ] = $comment_field;

			return $fields;
		}

		/**
		 * Register widget area.
		 *
		 * @since bigshop 1.0
		 *
		 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
		 */
		function widgets_init()
		{
			$opt_multi_slidebars = bigshop_get_option( 'multi_widget', '' );

			if ( is_array( $opt_multi_slidebars ) && count( $opt_multi_slidebars ) > 0 ) {
				foreach ( $opt_multi_slidebars as $value ) {
					if ( $value && $value != '' ) {
						register_sidebar( array(
								'name'          => $value[ 'add_widget' ],
								'id'            => 'custom-sidebar-' . sanitize_key( $value[ 'add_widget' ] ),
								'before_widget' => '<div id="%1$s" class="widget block-sidebar %2$s">',
								'after_widget'  => '</div>',
								'before_title'  => '<div class="title-widget widgettitle"><strong>',
								'after_title'   => '</strong></div>',
							)
						);
					}
				}
			}
			register_sidebar( array(
					'name'          => esc_html__( 'Widget Area', 'bigshop' ),
					'id'            => 'widget-area',
					'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'bigshop' ),
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<h2 class="widgettitle">',
					'after_title'   => '<span class="arow"></span></h2>',
				)
			);
			register_sidebar( array(
					'name'          => esc_html__( 'Shop Widget Area', 'bigshop' ),
					'id'            => 'shop-widget-area',
					'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'bigshop' ),
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<h2 class="widgettitle">',
					'after_title'   => '<span class="arow"></span></h2>',
				)
			);
			register_sidebar( array(
					'name'          => esc_html__( 'Product Widget Area', 'bigshop' ),
					'id'            => 'product-widget-area',
					'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'bigshop' ),
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<h2 class="widgettitle">',
					'after_title'   => '<span class="arow"></span></h2>',
				)
			);
			register_sidebar( array(
					'name'          => esc_html__( 'Page Widget Area', 'bigshop' ),
					'id'            => 'page-widget-area',
					'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'bigshop' ),
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<h2 class="widgettitle">',
					'after_title'   => '<span class="arow"></span></h2>',
				)
			);
		}

		/**
		 * Register custom fonts.
		 */
		function bigshop_fonts_url() {

			$font_families = array();

			$font_families[] = 'Lato:100,100i,300,300i,400,400i,700,700i,900,900i';
			$font_families[] = 'Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i';
			$font_families[] = 'Montserrat:400,500,600,700';

			$query_args = array(
				'family' => urlencode( implode( '|', $font_families ) ),
				'subset' => urlencode( 'latin,latin-ext' ),
			);

			$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );

			return esc_url_raw( $fonts_url );
		}

		/**
		 * Enqueue scripts and styles.
		 *
		 * @since bigshop 1.0
		 */
		function scripts()
		{
			// Load fonts
			wp_enqueue_style('bigshop-googlefonts', $this->bigshop_fonts_url(), array(), null);

			/*Load our main stylesheet.*/
			wp_enqueue_style( 'animate', get_theme_file_uri( '/assets/css/animate.min.css' ), array(), '1.0' );
			wp_enqueue_style( 'bootstrap', get_theme_file_uri( '/assets/css/bootstrap.min.css' ), array(), '1.0' );
			wp_enqueue_style( 'font-awesome', get_theme_file_uri( '/assets/css/font-awesome.min.css' ), array(), '1.0' );
            wp_enqueue_style( 'chosen',get_theme_file_uri( '/assets/css/chosen.min.css' ), array(), '1.0');
            wp_enqueue_style( 'owl-carousel',get_theme_file_uri( '/assets/css/owl.carousel.min.css' ));
			wp_enqueue_style( 'bigshop-main', get_stylesheet_uri() );


			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				wp_enqueue_script( 'comment-reply' );
			}
			/*Load lib js*/
			wp_enqueue_script( 'bootstrap', get_theme_file_uri( '/assets/js/bootstrap.min.js' ), array('jquery'), '1.0' );
			wp_enqueue_script( 'Modernizr', get_theme_file_uri( '/assets/js/Modernizr.min.js' ), array('jquery'), '1.0' );
			wp_enqueue_script( 'lazyload', get_theme_file_uri( '/assets/js/jquery.lazyload.min.js' ), array('jquery'), '1.0' );
			wp_enqueue_script( 'chosen', get_theme_file_uri( '/assets/js/chosen.jquery.min.js' ), array('jquery'), '1.0' );
			wp_enqueue_script( 'actual', get_theme_file_uri( '/assets/js/jquery.actual.min.js' ), array('jquery'), '1.0' );
			wp_enqueue_script( 'owl-carousel', get_theme_file_uri( '/assets/js/owl.carousel.min.js' ), array('jquery'), '1.0' );
			wp_enqueue_script( 'jquery-plugin', get_theme_file_uri( '/assets/js/jquery.plugin.min.js' ), array('jquery'), '1.0' );
			wp_enqueue_script( 'countdown', get_theme_file_uri( '/assets/js/jquery.countdown.min.js' ), array('jquery'), '1.0' );
			wp_enqueue_script( 'jquery-sticky', get_theme_file_uri( '/assets/js/jquery.sticky.min.js' ), array('jquery'), '1.0' );

			$gmap_api_key = bigshop_get_option( 'opt_gmap_api_key', '' );
			$gmap_api_key = trim( $gmap_api_key );
			if ( $gmap_api_key != '' ) {
				wp_enqueue_script( 'maps', esc_url( 'https://maps.googleapis.com/maps/api/js?key=' . $gmap_api_key ), array( 'jquery' ), null, true );
			}
			wp_enqueue_script( 'bigshop-script', get_theme_file_uri( '/assets/js/functions.min.js' ), array('jquery'), '1.0' );

			wp_localize_script( 'bigshop-script', 'bigshop_ajax_frontend', array(
					'ajaxurl'  => admin_url( 'admin-ajax.php' ),
					'security' => wp_create_nonce( 'bigshop_ajax_frontend' ),
				)
			);

			$bigshop_enable_lazy              = bigshop_get_option( 'bigshop_theme_lazy_load', 'yes' );
			$bigshop_enable_product_thumb_slide = bigshop_get_option( 'bigshop_enable_product_thumb_slide', '' );
			$bigshop_enable_popup             = bigshop_get_option( 'bigshop_enable_popup' );
			$bigshop_enable_popup_mobile      = bigshop_get_option( 'bigshop_enable_popup_mobile' );
			$bigshop_popup_delay_time         = bigshop_get_option( 'bigshop_popup_delay_time', '1' );
			$bigshop_enable_sticky_menu       = bigshop_get_option( 'bigshop_enable_sticky_menu', 'yes' );

			$bigshop_thumbnail_lg_items = bigshop_get_option('bigshop_thumbnail_lg_items','4');
			$bigshop_thumbnail_md_items = bigshop_get_option('bigshop_thumbnail_md_items','3');
			$bigshop_thumbnail_sm_items = bigshop_get_option('bigshop_thumbnail_sm_items','2');
			$bigshop_thumbnail_xs_items = bigshop_get_option('bigshop_thumbnail_xs_items','2');
			$bigshop_thumbnail_ts_items = bigshop_get_option('bigshop_thumbnail_ts_items','1');

			wp_localize_script( 'bigshop-script', 'bigshop_global_script', array(
					'bigshop_enable_lazy'              => $bigshop_enable_lazy,
                    'bigshop_enable_product_thumb_slide' => $bigshop_enable_product_thumb_slide,
                    'bigshop_enable_popup'             => $bigshop_enable_popup,
                    'bigshop_enable_popup_mobile'      => $bigshop_enable_popup_mobile,
                    'bigshop_popup_delay_time'         => $bigshop_popup_delay_time,
                    'bigshop_enable_sticky_menu'       => $bigshop_enable_sticky_menu,
                    'bigshop_thumbnail_lg_items'       => (int) $bigshop_thumbnail_lg_items,
                    'bigshop_thumbnail_md_items'       => (int) $bigshop_thumbnail_md_items,
                    'bigshop_thumbnail_sm_items'       => (int)$bigshop_thumbnail_sm_items,
                    'bigshop_thumbnail_xs_items'       => (int) $bigshop_thumbnail_xs_items,
                    'bigshop_thumbnail_ts_items'       => (int) $bigshop_thumbnail_ts_items,
				)
			);
		}

		/**
		 * Filter whether comments are open for a given post type.
		 *
		 * @param string $status Default status for the given post type,
		 *                             either 'open' or 'closed'.
		 * @param string $post_type Post type. Default is `post`.
		 * @param string $comment_type Type of comment. Default is `comment`.
		 * @return string (Maybe) filtered default status for the given post type.
		 */
		function open_default_comments_for_page( $status, $post_type, $comment_type )
		{
			if ( 'page' == $post_type ) {
				return 'open';
			}

			return $status;
			/*You could be more specific here for different comment types if desired*/
		}


		public function includes()
		{
			include_once( get_template_directory() . '/framework/framework.php' );
			define( 'CS_ACTIVE_FRAMEWORK', true );
			define( 'CS_ACTIVE_METABOX', true );
			define( 'CS_ACTIVE_TAXONOMY', true );
			define( 'CS_ACTIVE_SHORTCODE', false );
			define( 'CS_ACTIVE_CUSTOMIZE', false );
		}
	}

	new  Bigshop_Functions();
}



