<?php
    $selected = '';
    
    if( isset( $_GET['product_cat']) && $_GET['product_cat'] ){
        $selected = $_GET['product_cat'];
    }
    
    $args = array(
        'show_option_none' => esc_html__( 'All Categories', 'bigshop' ),
        'taxonomy'          => 'product_cat',
        'class'             => 'category-search-option',
        'hide_empty'        => 1,
        'orderby'           => 'name',
        'order'             => "asc",
        'tab_index'         => true,
        'hierarchical'      => true,
        'id'                => rand(),
        'name'              => 'product_cat',
        'value_field'       => 'slug',
        'selected'          => $selected,
        'option_none_value' => '0',
    );

?>
<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ) ?>" class="form-search style2">
    <?php if ( class_exists( 'WooCommerce' ) ): ?>
        <input type="hidden" name="post_type" value="product"/>
        <div class="category">
            <?php wp_dropdown_categories( $args ); ?>
        </div>
    <?php endif; ?>
    <input type="text" class="input" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" placeholder="<?php echo esc_html__( 'Search here . . .', 'bigshop' ); ?>"/>
    <button class="btn-search" type="submit"><?php echo esc_html__( 'Search', 'bigshop' ); ?></button>
</form><!-- block search -->