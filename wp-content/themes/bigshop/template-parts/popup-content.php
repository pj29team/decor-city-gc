<?php
$bigshop_enable_popup      = bigshop_get_option( 'bigshop_enable_popup' );
$bigshop_popup_title       = bigshop_get_option( 'bigshop_popup_title', 'SAVE 25%' );
$bigshop_popup_subtitle    = bigshop_get_option( 'bigshop_popup_subtitle', 'SIGN UP FOR EMAILS' );
$bigshop_popup_description = bigshop_get_option( 'bigshop_popup_description', '' );

$bigshop_popup_input_placeholder = bigshop_get_option( 'bigshop_popup_input_placeholder', 'Enter your email adress' );
$bigshop_popup_butotn_text       = bigshop_get_option( 'bigshop_popup_button_text', 'Sign-Up' );
$bigshop_poppup_background       = bigshop_get_option( 'bigshop_poppup_background', '' );

if ( $bigshop_enable_popup == 1 ) :
	?>
    <!--  Popup Newsletter-->
    <div class="modal fade" id="popup-newsletter" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content" <?php if ( $bigshop_poppup_background ): ?> style="background-image: url(<?php echo esc_url( wp_get_attachment_image_url( $bigshop_poppup_background, 'full' ) ); ?>)" <?php endif; ?>>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle"></i>
                </button>
                <div class="modal-inner">
					<?php if ( $bigshop_popup_title ): ?>
                        <h2 class="title"><?php echo esc_html( $bigshop_popup_title ); ?></h2>
					<?php endif; ?>
					<?php if ( $bigshop_popup_subtitle ): ?>
                        <h3 class="sub-title"><?php echo esc_html( $bigshop_popup_subtitle ); ?></h3>
					<?php endif; ?>
					<?php if ( $bigshop_popup_description ): ?>
                        <div class="description"><?php echo htmlspecialchars_decode( $bigshop_popup_description ); ?></div>
					<?php endif; ?>
                    <div class="newsletter-form-wrap">
                        <input class="email" type="email" name="email"
                               placeholder="<?php echo esc_html( $bigshop_popup_input_placeholder ); ?>">
                        <button type="submit" name="submit_button" class="btn-submit submit-newsletter">
							<?php echo esc_html( $bigshop_popup_butotn_text ); ?>
                        </button>
                    </div>
                    <div class="checkbox btn-checkbox">
                        <label>
                            <input class="bigshop_disabled_popup_by_user" type="checkbox">
                            <span><?php echo esc_html__( 'Don&rsquo;t show this popup again', 'bigshop' ); ?></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div><!--  Popup Newsletter-->
<?php endif;