<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ) ?>" class="form-search">
    <?php if ( class_exists( 'WooCommerce' ) ): ?>
        <input type="hidden" name="post_type" value="product"/>
    <?php endif; ?>
    <input type="text" class="input" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" placeholder="<?php echo esc_html__( 'Search here . . .', 'bigshop' ); ?>">
    <button class="btn-search" type="submit"><?php echo esc_html__( 'Search', 'bigshop' ); ?></button>
</form><!-- block search -->