<?php
global $woocommerce;
$header_style         = bigshop_get_option( 'bigshop_used_header', 'style-01' );
$enable_theme_options = bigshop_get_option( 'enable_theme_options' );
$data_meta            = get_post_meta( get_the_ID(), '_custom_metabox_theme_options', true );

if ( !empty( $data_meta ) && $enable_theme_options == 1 ) {
	$header_style = $data_meta[ 'bigshop_metabox_used_header' ];
}
?>
<a class="shopcart-link" href="<?php echo esc_url( wc_get_cart_url() ); ?>">
    <span class="text"><?php esc_html_e('My Cart :','bigshop');?></span>
    <span class="cart-icon">
        <span class="count"><?php echo WC()->cart->cart_contents_count ?></span>
    </span>
</a>
<?php if ( !WC()->cart->is_empty() ) : ?>
    <div class="shopcart-description">
        <div class="content-wrap">
            <div class="title">
				<?php echo esc_html__( 'Shopping cart', 'bigshop' ); ?>
				<span><?php printf( esc_html__( ' (%1$s) items', 'bigshop' ), WC()->cart->cart_contents_count ); ?></span>
                <a class="close-minicart" href="#"><i class="fa fa-times" aria-hidden="true"></i></a>
            </div>
            <div class="minicart-content">
                <div class="minicart-items">
                    <ol class="product-items">
                        <?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ): ?>
                            <?php $bag_product = apply_filters( 'woocommerce_cart_item_product', $cart_item[ 'data' ], $cart_item, $cart_item_key ); ?>
                            <?php $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item[ 'product_id' ], $cart_item, $cart_item_key ); ?>

                            <?php if ( $bag_product && $bag_product->exists() && $cart_item[ 'quantity' ] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ): ?>

                                <?php $product_name = apply_filters( 'woocommerce_cart_item_name', $bag_product->get_title(), $cart_item, $cart_item_key ); ?>
                                <?php $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $bag_product->get_image( 'shop_thumbnail' ), $cart_item, $cart_item_key ); ?>
                                <?php $product_price = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $bag_product ), $cart_item, $cart_item_key ); ?>
                                <li class="product-cart mini_cart_item">
                                    <div class="thumb">
                                        <a class="product-media" href="<?php echo esc_url( get_permalink( $cart_item[ 'product_id' ] ) ) ?>">
                                            <?php echo balanceTags( $bag_product->get_image( array( 100, 100 ) ) ); ?>
                                        </a>
                                    </div>
                                    <div class="product-details">
                                        <h3 class="product-name">
                                            <a href="<?php echo esc_url( get_permalink( $cart_item[ 'product_id' ] ) ) ?>"><?php echo esc_html( $product_name ); ?></a>
                                        </h3>
                                        <span class="product-quantity"><?php printf( esc_html__( '%1$s x', 'bigshop' ), $cart_item[ 'quantity' ] ); ?></span>
                                        <?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="qty product-price">' . sprintf( '%s', $product_price ) . '</span>', $cart_item, $cart_item_key ); ?>
                                    </div>
                                    <?php
                                        echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                            '<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s"><i class="fa fa-trash-o" aria-hidden="true"></i></a>',
                                            esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
                                            esc_html__( 'Remove this item', 'bigshop' ),
                                            esc_attr( $product_id ),
                                            esc_attr( $bag_product->get_sku() )
                                        ), $cart_item_key
                                        );
                                    ?>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ol>
                </div>

                <div class="minicart-bottom">
                    <div class="subtotal">
                        <span class="total-title"><?php echo esc_html__( 'Cart Total: ', 'bigshop' ); ?></span>
                        <span class="total-price"><?php echo balanceTags( $woocommerce->cart->get_cart_subtotal() ); ?></span>
                    </div>
                    <?php do_action( 'woocommerce_widget_shopping_cart_before_buttons' ); ?>
                    <div class="actions">
                        <a class="button button-viewcart" href="<?php echo esc_url( wc_get_cart_url() ); ?>">
                            <span><?php esc_html_e( 'View cart', 'bigshop' ); ?></span>
                        </a>
                        <a href="<?php echo esc_url( wc_get_checkout_url() ); ?>"
                           class="button button-checkout"><span><?php echo esc_html__( 'Checkout', 'bigshop' ); ?></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>