<?php

if ( !class_exists( 'WooCommerce' ) ) : ?>
	<?php if ( is_user_logged_in() ): ?>
		<?php wp_register(); ?>
	<?php else: ?>
        <li><?php wp_loginout(); ?></li>
	<?php endif; ?>
<?php else:
	$myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
	$myaccount_link = get_permalink( get_option( 'woocommerce_myaccount_page_id' ) );
	$woocommerce_enable_myaccount_registration = get_option( 'woocommerce_enable_myaccount_registration' );

	$logout_url = "#";
	if ( $myaccount_page_id ) {
		if ( function_exists( 'woocommerce_get_page_id' ) ) {
			if ( function_exists( 'wc_get_page_id' ) ) {
				$logout_url = wp_logout_url( get_permalink( wc_get_page_id( 'shop' ) ) );
			} else {
				$logout_url = wp_logout_url( get_permalink( woocommerce_get_page_id( 'shop' ) ) );
			}

			if ( get_option( 'woocommerce_force_ssl_checkout' ) == 'yes' ) {
				$logout_url = str_replace( 'http:', 'https:', $logout_url );
			}
		}

	}
	?>
	<?php if ( is_user_logged_in() ): ?>
	<?php
	$currentUser = wp_get_current_user();
	?>
    <li>
        <a href="<?php echo esc_url( $myaccount_link ); ?>">
            <span class="icon fa fa-user-o"></span>
			<?php esc_html_e( 'Hi, ', 'bigshop' );
			echo esc_html( $currentUser->display_name ); ?>
        </a>
    </li>
    <?php else: ?>
    <li>
        <a href="<?php echo esc_url( $myaccount_link ); ?>">
            <span class="icon fa fa-unlock-alt"></span>
            <?php if ( $woocommerce_enable_myaccount_registration == "yes" ) : ?>
                <?php echo esc_html__( 'Sign up / Register', 'bigshop' ); ?>
            <?php else: ?>
                <?php echo esc_html__( 'Login', 'bigshop' ); ?>
            <?php endif; ?>
        </a>
    </li>
    <?php endif; ?>
<?php endif; ?>