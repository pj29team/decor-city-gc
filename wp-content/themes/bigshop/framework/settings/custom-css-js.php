<?php

if ( !function_exists( 'bigshop_custom_css' ) ) {
	function bigshop_custom_css()
	{
		$css = bigshop_get_option( 'bigshop_custom_css', '' );
		$css .= bigshop_theme_color();
		$css .= bigshop_vc_custom_css_footer();
		wp_enqueue_style( 'bigshop-main', get_theme_file_uri( '/style.css' ), array(), '1.0' );
		wp_add_inline_style( 'bigshop-main', $css );
	}
}
add_action( 'wp_enqueue_scripts', 'bigshop_custom_css', 999 );

if ( !function_exists( 'bigshop_theme_color' ) ) {
	function bigshop_theme_color()
	{
		$main_color           = bigshop_get_option( 'bigshop_main_color', '#f1b400' );
		$main_color2           = bigshop_get_option( 'bigshop_main_color2', '#f3772c' );
		$enable_theme_options = bigshop_get_option( 'enable_theme_options' );

		$meta_data = get_post_meta( get_the_ID(), '_custom_metabox_theme_options', true );

		if ( !empty( $meta_data ) && $enable_theme_options == 1 ) {
			$main_color = $meta_data[ 'metabox_bigshop_main_color' ];
		}

		$typography_font_family = bigshop_get_option( 'typography_font_family' );
		$typography_font_size   = bigshop_get_option( 'typography_font_size', 14 );
		$typography_line_height = bigshop_get_option( 'typography_line_height', 24 );
		/* Body Css */
		$css = '';
//		$css .= 'body {';
//		if ( !empty( $typography_font_family ) ) {
//			$css .= 'font-family: ' . $typography_font_family[ 'family' ] . ', sans-serif;';
//			$css .= 'font-weight: ' . $typography_font_family[ 'variant' ] . ';';
//		}
//		if ( $typography_font_size ) {
//			$css .= 'font-size: ' . $typography_font_size . 'px;';
//		}
//		if ( $typography_line_height ) {
//			$css .= 'line-height: ' . $typography_line_height . 'px;';
//		}
//		$css .= '}';
		/* Main color */
		$css .= '
		    a:hover,
		    a:active,
		    a:focus,
		    .main-menu>li:hover>a,
		    .main-menu>li.active>a,
		    .main-menu>li:hover>a,
		    .main-menu>li.active>a,
		    .top-bar-menu>li>a:hover,
		    .post-item .post-name:hover a,
		    .tagcloud a:hover,
		    .breadcrumb .active span,
		    .breadcrumb a:hover,
		    .woocommerce-breadcrumb a:hover,
		    .woocommerce-breadcrumb,
		    .widget_layered_nav li:hover>a,
            .widget_layered_nav li.chosen>a,
            .widget_layered_nav li:hover>.count,
            .widget_layered_nav li.chosen>.count,
            .shopcart-link .cart-icon::before,
            .vertical-menu>li:hover>a,
            .product-grid-title span,
            .bigshop-tabs .tab-title span,
            .bigshop-products .title span,
            .product_list_widget  .product-name:hover a,
            .bigshop-newsletter  .head .title span,
            .footer .coppyright,
            .bigshop-brands .block-title span,
            .quantity .control .btn-number:hover,
            .header.style2 .top-header .top-note span,
            .header.style3 .top-header .top-note span,
            .top_phonenumber,
            .header.style3 .main-menu>li:hover>a,
            .header.style3 .main-menu>li.active>a,
            .top-contact .icon,
            .widget_categories li:hover > a,
            .widget_categories li a:hover,
            .widget_pages li:hover > a,
            .widget_pages li a:hover,
            .widget_nav_menu li:hover > a,
            .widget_nav_menu li a:hover,
            .widget_archive li:hover > a,
            .widget_archive li a:hover,
            .product-item.list .product-bottom .add_to_cart_button,
            .bigshop-iconbox .iconbox-inner:hover .icon,
            .bigshop-sc-categories .category-info .title,
            .widget_product_categories li:hover>a,
            .widget_product_categories li:hover>span,
            .product-item.style-6 .bigshop_product6_loop_thumbs.owl-carousel .owl-nav .owl-next:hover,
			.product-item.style-6 .bigshop_product6_loop_thumbs.owl-carousel  .owl-nav .owl-prev:hover,
			.bigshop-products.owl-layout.layout2 .owl-carousel .owl-nav .owl-prev:hover,
			.bigshop-products.owl-layout.layout2 .owl-carousel .owl-nav .owl-next:hover,
			.bigshop-tabs.style02 .tab-link li:hover>a, 
			.bigshop-tabs.style02 .tab-link li.active>a,
			.bigshop-testimonials .owl-carousel .owl-nav .owl-next:hover, 
			.bigshop-testimonials .owl-carousel .owl-nav .owl-prev:hover,
			.bigshop-blog .owl-carousel .owl-nav .owl-next:hover, 
			.bigshop-blog .owl-carousel .owl-nav .owl-prev:hover,
			.blog-item .readmore a ,
			.wcml_currency_switcher.wcml-dropdown li:hover > a,
			.footer.layout1 .bigshop-custommenu ul li>a:hover,
			.footer.layout1 .bigshop-socials .social-item:hover,
			.blog-item .title:hover a,
			.product-item.style-7 .price,
			.wpml-ls-legacy-dropdown a:hover, 
			.wpml-ls-legacy-dropdown a:focus, 
			.wpml-ls-legacy-dropdown .wpml-ls-current-language:hover>a,
			.wpml-ls-legacy-dropdown-click a:hover, 
			.wpml-ls-legacy-dropdown-click a:focus, 
			.wpml-ls-legacy-dropdown-click .wpml-ls-current-language:hover>a,
			.tag-sticky-2.post-item .post-name a::after,
			.bigshop-brands .block-title .link:hover,
			.sidebar-home3 .bigshop-products.owl-layout.layout2 .title span  {
		        color:'.$main_color.';
		    }
		    .form-search  .btn-search,
		    .vertical-wapper .block-title,
		    .button:hover,
            .button:focus,
            .button:active,
            input[type="submit"]:hover,
            input[type="submit"]:focus,
            input[type="submit"]:active,
            .button.primary,
            .widget .widgettitle:after,
            .comments-title:after,
            .comment-reply-title:after,
            .product-item .product-bottom .button,
            .product-item .product-bottom .added_to_cart,
            .product-item .yith-wcwl-add-to-wishlist>div a:hover,
            .product-item .product-bottom .compare.button:hover,
            .product-item .flash>span.onsale,
            .shop-top-control .modes-mode:hover,
            .shop-top-control .modes-mode.active,
            .minicart-content .subtotal .total-title:after,
            .minicart-content .minicart-bottom .button.button-checkout,
            .minicart-content .minicart-bottom .button:hover,
            .summary .button,
            .summary .yith-wcwl-add-to-wishlist>div >a:hover,
            .summary .compare.button:hover,
            .single-left  .onsale,
            .wc-tabs li.active>a,
            .owl-carousel .owl-nav .owl-next:hover,
            .owl-carousel  .owl-nav .owl-prev:hover,
            .product-item.style-3 .product-info .button,
            .product-item.style-3 .product-info .added_to_cart,
            .backtotop,
            .bigshop-title:after,
            .bigshop-section-title .title:after,
            .table-title:after,
            .button.checkout-button,
            .place-order .button,
            #customer_login .u-column1 > h2::after,
			#customer_login .u-column2 > h2::after,
			.product-item.list .product-bottom .add_to_cart_button:hover,
			.page-title::after,
			.bigshop-newsletter.layout02 .newsletter-form-wrap .button,
			.bigshop-tabs.style03  .tab-link li.active,
			#yith-quick-view-content .onsale,
			.widget_shopping_cart .woocommerce-mini-cart__buttons a.button:hover,
			.bigshop-newsletter .newsletter-form-wrap .button ,
			.header.style3 .header-nav,
			#rev_slider_1_1 .bigshop-nav-1.tparrows:hover,
			#rev_slider_2_1 .bigshop-nav-1.tparrows:hover,
			#rev_slider_3_1 .bigshop-nav-1.tparrows:hover {
		         background-color:'.$main_color.';
		    }
		    .form-search,
		    .verticalmenu-content,
		    .tagcloud a:hover,
		    .vertical-menu .submenu,
		    .summary .yith-wcwl-add-to-wishlist>div >a:hover,
            .summary .compare.button:hover,
            .variations .change-value.active,
            .owl-carousel .owl-nav .owl-next:hover,
            .owl-carousel  .owl-nav .owl-prev:hover,
            .bigshop-newsletter .newsletter-form-wrap .email-newsletter,
            .product-item.list .product-bottom .add_to_cart_button,
            .bigshop-products.grid-layout.layout2,
            .blog-item.layout03:hover,
            .bigshop-tabs.style03 .tab-link
            {
		        border-color:'.$main_color.';
		    }
		    .product-item .flash>span.onsale:before,
		    #yith-quick-view-content .onsale:before,
		    .single-left  .onsale:before{
		         border-color: transparent '.$main_color.' transparent transparent;
		    }
		    .bigshop-tabs.style03  .tab-link li.active::after {
		    	border-color: transparent transparent '.$main_color.' '.$main_color.';
		    }
		    .tab-container .cssload-wapper .cssload-square::before {
		    	border-color: '.$main_color.' transparent transparent '.$main_color.';
		    }
		    .tab-container .cssload-wapper .cssload-square::after {
		    	border-color:  transparent transparent '.$main_color.' '.$main_color.';
		    }

		';
		$css .= '
			.header.style3 .top_phonenumber .content .number,
			.footer.layout1 .widget .widgettitle, .footer.layout1 .bigshop-blog > .title {
				color:'.$main_color2.';
			}
			.header.style3 .vertical-wapper .block-title,
			.header.style3 .form-search  .btn-search,
			.header.style3 .block-minicart .shopcart-link {
				background-color:'.$main_color2.';
			}
			.header.style3 .form-search {
				border-color:'.$main_color2.';
			}
		';
		return $css;
	}
}

if ( !function_exists( 'bigshop_vc_custom_css_footer' ) ) {
	function bigshop_vc_custom_css_footer()
	{
		$bigshop_footer_options = bigshop_get_option( 'bigshop_footer_options', '' );
		$enable_theme_options   = bigshop_get_option( 'enable_theme_options' );
		$data_option_meta       = get_post_meta( get_the_ID(), '_custom_metabox_theme_options', true );
		if ( !empty( $data_option_meta ) && $enable_theme_options == 1 ) {
			$bigshop_footer_options = $data_option_meta[ 'bigshop_metabox_footer_options' ];
		}

		$shortcodes_custom_css = get_post_meta( $bigshop_footer_options, '_wpb_post_custom_css', true );
		$shortcodes_custom_css .= get_post_meta( $bigshop_footer_options, '_wpb_shortcodes_custom_css', true );
		$shortcodes_custom_css .= get_post_meta( $bigshop_footer_options, '_bigshop_Shortcode_custom_css', true );

		return $shortcodes_custom_css;
	}
}

if ( !function_exists( 'bigshop_write_custom_js ' ) ) {
	function bigshop_write_custom_js()
	{
		$bigshop_custom_js = bigshop_get_option( 'bigshop_custom_js', '' );
		wp_enqueue_script( 'bigshop-script', get_theme_file_uri( '/assets/js/functions.js' ), array(), '1.0' );
		wp_add_inline_script( 'bigshop-script', $bigshop_custom_js );
	}
}
add_action( 'wp_enqueue_scripts', 'bigshop_write_custom_js' );