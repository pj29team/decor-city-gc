/**
 * Created by khanh on 1/10/2017.
 */
(function ($) {
    "use strict"; // Start of use strict

    function bigshop_autocomplete_taxonomy() {
        $('.bigshop_vc_taxonomy').each(function () {
            if( $(this).length > 0){
                $(this).chosen();
            }
        })
    }
    /* ---------------------------------------------
     Scripts ready
     --------------------------------------------- */
    $(document).ready(function() {
        bigshop_autocomplete_taxonomy();
        $(document).on('change','.bigshop_select_preview',function(){
            var url = $(this).find(':selected').data('img');
            $(this).parent('.container-select_preview').find('.image-preview img').attr('src',url);
        });
    });
    $(document).ajaxComplete(function (event, xhr, settings) {
        bigshop_autocomplete_taxonomy();
    });

    /* ---------------------------------------------
     Scripts initialization
     --------------------------------------------- */
    $(window).load(function () {

    });
    $(window).bind("load", function () {

    });
})(jQuery); // End of use strict