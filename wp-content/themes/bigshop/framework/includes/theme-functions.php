<?php
/* Session Start */
if ( !function_exists( 'Bigshop_StartSession' ) ) {
	function Bigshop_StartSession()
	{
		if ( !session_id() ) {
			session_start();
		}
	}
}
add_action( 'init', 'Bigshop_StartSession', 1 );

/**
 *
 * Get option
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if ( !function_exists( 'bigshop_get_option' ) ) {
	function bigshop_get_option( $option_name = '', $default = '' )
	{
		$get_value = isset( $_GET[ $option_name ] ) ? $_GET[ $option_name ] : '';

		$cs_option = null;

		if ( defined( 'CS_VERSION' ) ) {
			$cs_option = get_option( CS_OPTION );
		}
		if ( isset( $_GET[ $option_name ] ) ) {
			$cs_option = $get_value;
			$default   = $get_value;
		}

		$options = apply_filters( 'cs_get_option', $cs_option, $option_name, $default );

		if ( !empty( $option_name ) && !empty( $options[ $option_name ] ) ) {
			return $options[ $option_name ];
		} else {
			return ( !empty( $default ) ) ? $default : null;
		}

	}
}

if( !function_exists('bigshop_init')){
    function bigshop_init() {
        $enable_dev_mode = bigshop_get_option('enable_theme_dev_options',false);
        
        $dev_mode = false;
        if( $enable_dev_mode ){
            $dev_mode = true;
        }
        define( 'BIGSHOP_DEV_MODE', $dev_mode );
    }
}


add_action( 'init', 'bigshop_init' );

/* BODY CLASS */
add_filter( 'body_class', 'bigshop_body_class' );
if ( !function_exists( 'bigshop_body_class' ) ) {

	function bigshop_body_class( $classes )
	{
		$my_theme  = wp_get_theme();
		$classes[] = $my_theme->get( 'Name' ) . "-" . $my_theme->get( 'Version' );

		return $classes;
	}
}

/* SET POST VIEW */
function bigshop_set_post_views( $postID )
{
	$count_key = 'bigshop_post_views_count';
	$count     = get_post_meta( $postID, $count_key, true );
	if ( $count == '' ) {
		$count = 0;
		delete_post_meta( $postID, $count_key );
		add_post_meta( $postID, $count_key, '0' );
	} else {
		$count++;
		update_post_meta( $postID, $count_key, $count );
	}
}

/* GET REVO SLIDE */
if ( !function_exists( 'bigshop_rev_slide_options' ) ) {
	function bigshop_rev_slide_options()
	{
		$bigshop_rev_slide_options = array( '' => esc_html__( '--- Choose Revolution Slider ---', 'bigshop' ) );
		if ( class_exists( 'RevSlider' ) ) {
			global $wpdb;
			if ( shortcode_exists( 'rev_slider' ) ) {
				$rev_sql  = $wpdb->prepare(
					"SELECT *
                FROM {$wpdb->prefix}revslider_sliders
                WHERE %d", 1
				);
				$rev_rows = $wpdb->get_results( $rev_sql );
				if ( count( $rev_rows ) > 0 ) {
					foreach ( $rev_rows as $rev_row ):
						$bigshop_rev_slide_options[ $rev_row->alias ] = $rev_row->title;
					endforeach;
				}
			}
		}

		return $bigshop_rev_slide_options;
	}
}

/* CUSTOM BLOG NAV */
if ( !function_exists( 'bigshop_paging_nav' ) ) {
	/**
	 * Display navigation to next/previous set of posts when applicable.
	 *
	 * @since Fastshop 1.0
	 *
	 * @global WP_Query $wp_query WordPress Query object.
	 * @global WP_Rewrite $wp_rewrite WordPress Rewrite object.
	 */
	function bigshop_paging_nav()
	{
		global $wp_query;
		// Don't print empty markup if there's only one page.
		if ( $wp_query->max_num_pages < 2 ) {
			return;
		}
		echo get_the_posts_pagination( array(
				'screen_reader_text' => '&nbsp;',
				'before_page_number' => '',
				'prev_text'          => esc_html__( 'Prev', 'bigshop' ),
				'next_text'          => esc_html__( 'Next', 'bigshop' ),
			)
		);
	}
}

if ( !function_exists( 'bigshop_resize_image' ) ) {
	/**
	 * @param int $attach_id
	 * @param string $img_url
	 * @param int $width
	 * @param int $height
	 * @param bool $crop
	 * @param bool $use_lazy
	 *
	 * @since 1.0
	 * @return array
	 */
    function bigshop_resize_image( $attach_id = null, $img_url = null, $width, $height, $crop = false, $place_hold = true, $use_real_img_hold = true, $solid_img_color = null ) {
        /*If is singular and has post thumbnail and $attach_id is null, so we get post thumbnail id automatic*/
        if ( is_singular() && !$attach_id ) {
            if ( has_post_thumbnail() && !post_password_required() ) {
                $attach_id = get_post_thumbnail_id();
            }
        }
        /*this is an attachment, so we have the ID*/
        $image_src = array();
        if ( $attach_id ) {
            $image_src = wp_get_attachment_image_src( $attach_id, 'full' );
            $actual_file_path = get_attached_file( $attach_id );
            /*this is not an attachment, let's use the image url*/
        } else if ( $img_url ) {
            $file_path = str_replace( get_site_url(), get_home_path(), $img_url );
            $actual_file_path = rtrim( $file_path, '/' );
            if ( !file_exists( $actual_file_path ) ) {
                $file_path = parse_url( $img_url );
                $actual_file_path = rtrim( ABSPATH, '/' ) . $file_path['path'];
            }
            if ( file_exists( $actual_file_path ) ) {
                $orig_size = getimagesize( $actual_file_path );
                $image_src[0] = $img_url;
                $image_src[1] = $orig_size[0];
                $image_src[2] = $orig_size[1];
            }
            else{
                $image_src[0] = '';
                $image_src[1] = 0;
                $image_src[2] = 0;
            }
        }
        if ( ! empty( $actual_file_path ) && file_exists( $actual_file_path ) ) {
            $file_info = pathinfo( $actual_file_path );
            $extension = '.' . $file_info['extension'];
            /*the image path without the extension*/
            $no_ext_path = $file_info['dirname'] . '/' . $file_info['filename'];
            $cropped_img_path = $no_ext_path . '-' . $width . 'x' . $height . $extension;
            /*checking if the file size is larger than the target size*/
            /*if it is smaller or the same size, stop right here and return*/
            if ( $image_src[1] > $width || $image_src[2] > $height ) {
                /*the file is larger, check if the resized version already exists (for $crop = true but will also work for $crop = false if the sizes match)*/
                if ( file_exists( $cropped_img_path ) ) {
                    $cropped_img_url = str_replace( basename( $image_src[0] ), basename( $cropped_img_path ), $image_src[0] );
                    $vt_image = array(
                        'url' => $cropped_img_url,
                        'width' => $width,
                        'height' => $height
                    );
                    return $vt_image;
                }

                if ( $crop == false ) {
                    /*calculate the size proportionaly*/
                    $proportional_size = wp_constrain_dimensions( $image_src[1], $image_src[2], $width, $height );
                    $resized_img_path = $no_ext_path . '-' . $proportional_size[0] . 'x' . $proportional_size[1] . $extension;
                    /*checking if the file already exists*/
                    if ( file_exists( $resized_img_path ) ) {
                        $resized_img_url = str_replace( basename( $image_src[0] ), basename( $resized_img_path ), $image_src[0] );
                        $vt_image = array(
                            'url' => $resized_img_url,
                            'width' => $proportional_size[0],
                            'height' => $proportional_size[1]
                        );
                        return $vt_image;
                    }
                }
                /*no cache files - let's finally resize it*/
                $img_editor = wp_get_image_editor( $actual_file_path );
                if ( is_wp_error( $img_editor ) || is_wp_error( $img_editor->resize( $width, $height, $crop ) ) ) {
                    return array(
                        'url' => '',
                        'width' => '',
                        'height' => ''
                    );
                }
                $new_img_path = $img_editor->generate_filename();
                if ( is_wp_error( $img_editor->save( $new_img_path ) ) ) {
                    return array(
                        'url' => '',
                        'width' => '',
                        'height' => ''
                    );
                }
                if ( ! is_string( $new_img_path ) ) {
                    return array(
                        'url' => '',
                        'width' => '',
                        'height' => ''
                    );
                }
                $new_img_size = getimagesize( $new_img_path );
                $new_img = str_replace( basename( $image_src[0] ), basename( $new_img_path ), $image_src[0] );
                /*resized output*/
                $vt_image = array(
                    'url' => $new_img,
                    'width' => $new_img_size[0],
                    'height' => $new_img_size[1]
                );
                return $vt_image;
            }
            /*default output - without resizing*/
            $vt_image = array(
                'url' => $image_src[0],
                'width' => $image_src[1],
                'height' => $image_src[2]
            );
            return $vt_image;
        }
        else {
            if ( $place_hold ) {
                $width = intval( $width );
                $height = intval( $height );
                /*Real image place hold (https://unsplash.it/)*/
                if ( $use_real_img_hold ) {
                    $random_time = time() + rand( 1, 100000 );
                    $vt_image = array(
                        'url' => 'https://unsplash.it/' . $width . '/' . $height . '?random&time=' . $random_time,
                        'width' => $width,
                        'height' => $height
                    );
                }
                else{
                    $vt_image = array(
                        'url' => 'http://placehold.it/' . $width . 'x' . $height,
                        'width' => $width,
                        'height' => $height
                    );
                }
                return $vt_image;
            }
        }
        return false;
    }
}

/* GET LOGO */
if ( !function_exists( 'bigshop_get_logo' ) ) {
	/**
	 * Function get the site logo
	 *
	 * @since bigshop 1.0
	 * @author KuteTheme
	 **/
	function bigshop_get_logo()
	{
		$enable_theme_options = bigshop_get_option( 'enable_theme_options' );
		$meta_data            = get_post_meta( get_the_ID(), '_custom_metabox_theme_options', true );

		$logo_url = get_template_directory_uri() . '/assets/images/logo.png';
		$logo     = bigshop_get_option( 'bigshop_logo' );

		if ( !empty( $meta_data ) && $enable_theme_options == 1 ) {
			$logo = $meta_data[ 'metabox_bigshop_logo' ];
		}
		if ( $logo != '' ) {
			$logo_url = wp_get_attachment_image_url( $logo, 'full' );
		}

		$html = '<a href="' . esc_url( home_url( '/' ) ) . '"><img alt="' . esc_attr( get_bloginfo( 'name' ) ) . '" src="' . esc_url( $logo_url ) . '" class="_rw" /></a>';
		echo apply_filters( 'bigshop_site_logo', $html );
	}
}

/* SET POST THUMBNAIL */
if ( !function_exists( 'bigshop_post_thumbnail' ) ) {
	function bigshop_post_thumbnail()
	{
		$sidebar_blog_layout = bigshop_get_option( 'sidebar_blog_layout', 'left' );
        $bigshop_blog_style = bigshop_get_option('blog_list_style','standard');
		$using_placeholder   = bigshop_get_option( 'using_placeholder',0 );
        $sidebar_single_post_position = bigshop_get_option( 'sidebar_single_post_position', 'left' );

		if ( !$using_placeholder && !has_post_thumbnail() ){
			return false;
		}
        $thumb_w = 870;
        $thumb_h = 600;
		$crop = true;
		if( $sidebar_blog_layout == 'full'){
            $thumb_w = 1170;
            $thumb_h = 600;
        }
        if( $bigshop_blog_style =='grid' || $bigshop_blog_style =='grid2' ){
            $thumb_w = 420;
            $thumb_h = 289;
        }

        if( is_single()){

            $thumb_w = 870;
            $thumb_h = 600;
            if( $sidebar_single_post_position =='full'){
                $thumb_w = 1170;
                $thumb_h = 600;
            }
        }

        $image = bigshop_resize_image( get_post_thumbnail_id(), null, $thumb_w, $thumb_h, $crop, true, false );
		?>
        <div class="post-thumb">
            <?php if( is_single()):?>
                <img width="<?php echo esc_attr( $image['width'] ); ?>" height="<?php echo esc_attr( $image['height'] ); ?>" class="attachment-post-thumbnail wp-post-image" src="<?php echo esc_attr( $image['url'] ) ?>" alt="<?php the_title(); ?>" />
            <?php else:?>
                <a class="thumb-link banner-effect banner-effect9" href="<?php the_permalink(); ?>">
                    <img width="<?php echo esc_attr( $image['width'] ); ?>" height="<?php echo esc_attr( $image['height'] ); ?>" class="attachment-post-thumbnail wp-post-image" src="<?php echo esc_attr( $image['url'] ) ?>" alt="<?php the_title(); ?>" />
                </a>
            <?php endif;?>
        </div>
		<?php
	}
}

/* GET HEADER */

if ( !function_exists( 'bigshop_get_header' ) ) {
	/**
	 * Function get the header form template
	 *
	 * @since bigshop 1.0
	 * @author KuteTheme
	 **/
	function bigshop_get_header()
	{
		/* Data MetaBox */
		$enable_theme_options = bigshop_get_option( 'enable_theme_options' );
		$bigshop_used_header  = bigshop_get_option( 'bigshop_used_header', 'style-01' );
		$data_meta            = get_post_meta( get_the_ID(), '_custom_metabox_theme_options', true );
        
		if ( !empty( $data_meta ) && $enable_theme_options == 1 ) {
			$bigshop_used_header = $data_meta[ 'bigshop_metabox_used_header' ];
		}
        
		get_template_part( 'templates/headers/header', $bigshop_used_header );
	}
}

/* GET HEADER */

/* GET FOOTER */

if ( !function_exists( 'bigshop_get_footer' ) ) {
	function bigshop_get_footer()
	{
		$bigshop_footer_options = bigshop_get_option( 'bigshop_footer_options', '' );

		/* Data MetaBox */
		$enable_theme_options = bigshop_get_option( 'enable_theme_options' );
		$data_option_meta     = get_post_meta( get_the_ID(), '_custom_metabox_theme_options', true );

		if ( !empty( $data_option_meta ) && $enable_theme_options == 1 ) {
			$bigshop_footer_options = $data_option_meta[ 'bigshop_metabox_footer_options' ];
		}
		$data_meta = get_post_meta( $bigshop_footer_options, '_custom_footer_options', true );
		if ( empty( $data_meta ) ) {
			$bigshop_template_style = 'default';
		} else {
			$bigshop_template_style = $data_meta[ 'bigshop_footer_style' ];
		}

		ob_start();
		$query = new WP_Query( array( 'p' => $bigshop_footer_options, 'post_type' => 'footer', 'posts_per_page' => 1 ) );
		if ( $query->have_posts() ):
			while ( $query->have_posts() ): $query->the_post(); ?>
				<?php if ( $bigshop_template_style == 'default' ): ?>
                    <footer class="footer default">
                        <div class="container">
							<?php the_content(); ?>
                        </div>
                    </footer>
				<?php else: ?>
					<?php get_template_part( 'templates/footers/footer', $bigshop_template_style ); ?>
				<?php endif; ?>
			<?php endwhile;
		endif;
		wp_reset_postdata();
		echo ob_get_clean();
	}
}

/* GET FOOTER */


if( !function_exists('bigshop_custom_comment') ){
    /**
     * Display comment teplate.
     *
     * @since Genova 1.0
     * @author RedApple
     */
    function bigshop_custom_comment($comment, $args, $depth)
    {
        if ( 'div' === $args['style'] ) {
            $tag       = 'div';
            $add_below = 'comment';
        } else {
            $tag       = 'li';
            $add_below = 'div-comment';
        }
        ?>
        <<?php echo esc_attr($tag); ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
        <?php if ( 'div' != $args['style'] ) : ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>
        <div class="author-avatar">
            <?php if ($args['avatar_size'] != 0){
                echo get_avatar($comment, $args['avatar_size']);
            }
            ?>
        </div>

        <div class="comment-content">
            <div class="head">
                <?php printf( __( '<span class="author">%s</span>' ,'bigshop'), get_comment_author() ); ?>
                <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
                        <?php
                            /* translators: 1: date, 2: time */
                            printf( esc_html__('%1$s at %2$s','bigshop'), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( esc_html__( '(Edit)','bigshop' ), '  ', '' );
                    ?>
                </div>
            </div>
            <div class="coment-text">
                <?php if ( $comment->comment_approved == '0' ) : ?>
                    <em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.','bigshop' ); ?></em>
                    <br />
                <?php endif; ?>
                <?php comment_text(); ?>
            </div>
            <div class="reply">
                <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
            </div>
        </div>
        <?php if ( 'div' != $args['style'] ) : ?>
        </div>
    <?php endif; ?>
        <?php
    }
}

add_filter( 'wp_nav_menu_items', 'bigshop_custom_menuright_item', 99, 2 );
if( !function_exists('bigshop_custom_menuright_item')){
    function bigshop_custom_menuright_item ( $items, $args ) {

        if ( $args->theme_location == 'top_right_menu') {
            ob_start();
            
            get_template_part( 'template-parts/header','userlink');
            $html = ob_get_clean();
            $items .= $html;
        }
        return $items;
    }
}

add_filter('pre_option_posts_per_page', 'bigshop_limit_posts_per_page');
if( !function_exists('bigshop_limit_posts_per_page')){
    function bigshop_limit_posts_per_page( $posts_per_page ) {
        if( isset($_GET['posts_per_page']) && is_numeric($_GET['posts_per_page']) && $_GET['posts_per_page'] > 0 ){
            return $_GET['posts_per_page'];
        }

        return $posts_per_page;
    }
}

//Filter body classes
if( !function_exists( 'bigshop_layout_style_body_class' ) ){
    function bigshop_layout_style_body_class( $classes ){
        $bigshop_main_layout = bigshop_get_option( 'bigshop_main_layout', 'boxed-layout' );
        
        if( is_page() ){
            $meta_data = get_post_meta( get_the_ID(), '_custom_metabox_theme_options', true );
            
            if( isset( $meta_data['metabox_bigshop_main_layout'] ) && $meta_data['metabox_bigshop_main_layout'] != '' ){
                $bigshop_page_main_layout = $meta_data['metabox_bigshop_main_layout'];
                if( $bigshop_page_main_layout != '' ){
                    $bigshop_main_layout = $bigshop_page_main_layout;
                }
                
                if( bigshop_single_page_is_homepage() ){
                    $classes[] = 'home';
                }
            }
        }
        
        $classes[] = $bigshop_main_layout;
        return $classes;
    }
}
add_filter( 'body_class', 'bigshop_layout_style_body_class', 30 );
    
if( !function_exists( 'bigshop_single_page_is_homepage' ) ){
    function bigshop_single_page_is_homepage(){
        
        if( is_page() ){
            $meta_data = get_post_meta( get_the_ID(), '_custom_metabox_theme_options', true );
            
            if( isset( $meta_data['metabox_bigshop_home_page'] ) && $meta_data['metabox_bigshop_home_page'] != '' ){
                $is_homepage = $meta_data['metabox_bigshop_home_page'];
                if( $is_homepage == 'yes' ){
                    return true;
                }
            }
        }
        return false;
    }
}

add_filter( 'bigshop_header_vertical_menu_limit_items', 'bigshop_header_vertical_menu_limit_items_callback' );
if( !function_exists( 'bigshop_header_vertical_menu_limit_items_callback' ) ){
    function bigshop_header_vertical_menu_limit_items_callback( $limit ){
        if( is_page( ) ){
            $meta_data = get_post_meta( get_the_ID(), '_custom_metabox_theme_options', true );
            if( isset( $meta_data['bigshop_metabox_vertical_menu_item_visible'] ) && $meta_data['bigshop_metabox_vertical_menu_item_visible'] != '' ){
                $limit_item = $meta_data['bigshop_metabox_vertical_menu_item_visible'];
                if( $limit_item != '' && is_numeric( $limit_item ) && $limit_item != 0 ){
                    $limit = $limit_item;
                }
            }
        }
        return $limit;
    }
}

//SET MENU DEMO
if( !function_exists( 'bigshop_demo_menu_args' ) ){
    function bigshop_demo_menu_args( $args ){
        if( defined( 'BIGSHOP_DEV_MODE' ) && BIGSHOP_DEV_MODE == 1 ){
            $page_slug = get_post_field( 'post_name', get_post() );
            if( is_page( $page_slug ) ){
                $menu_configs_file = get_template_directory() . '/framework/settings/menu-configs.php';
                if( file_exists( $menu_configs_file ) ){
                    $menu_configs = @include( $menu_configs_file );
                    if( is_array( $menu_configs ) && !empty( $menu_configs ) ){
                        if( array_key_exists( $page_slug, $menu_configs ) ){

                            $page_menu_config = $menu_configs[$page_slug];
                            foreach( $page_menu_config as $menu_theme_location=>$menu_menu_id ){
                                if( $args['theme_location'] == $menu_theme_location ){
                                    if( empty( $args['menu'] ) ){
                                        $args['menu'] = new stdClass();
                                    }

                                    $menu_data      = get_term_by( 'id', $menu_menu_id, 'nav_menu', OBJECT );
                                    $args['menu']   = $menu_data;
                                }
                            }

                        }
                    }
                }
            }

        }

        return $args;
    }
}
add_filter( 'wp_nav_menu_args', 'bigshop_demo_menu_args' );