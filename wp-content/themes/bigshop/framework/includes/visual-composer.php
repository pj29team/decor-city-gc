<?php
if ( !class_exists( 'Bigshop_Visual_Composer' ) ) {
	class Bigshop_Visual_Composer
	{
		public function __construct()
		{
			$this->define_constants();
			add_filter( 'vc_google_fonts_get_fonts_filter', array( $this, 'vc_fonts' ) );
			add_action( 'vc_after_mapping', array( &$this, 'params' ) );
			add_action( 'vc_after_mapping', array( &$this, 'autocomplete' ) );
			/* Custom font Icon*/
			add_filter( 'vc_iconpicker-type-bigshopcustomfonts', array( &$this, 'iconpicker_type_bigshop_customfonts' ) );
			$this->map_shortcode();
		}

		/**
		 * Define  Constants.
		 */
		private function define_constants()
		{
			$this->define( 'BIGSHOP_SHORTCODE_PREVIEW', get_theme_file_uri( '/framework/assets/images/shortcode-previews/' ) );
			$this->define( 'BIGSHOP_PRODUCT_STYLE_PREVIEW', get_theme_file_uri( '/woocommerce/product-styles/' ) );
		}

		/**
		 * Define constant if not already set.
		 *
		 * @param  string $name
		 * @param  string|bool $value
		 */
		private function define( $name, $value )
		{
			if ( !defined( $name ) ) {
				define( $name, $value );
			}
		}

		function params()
		{
			if ( function_exists( 'bigshop_toolkit_vc_param' ) ) {
				bigshop_toolkit_vc_param( 'taxonomy', array( &$this, 'taxonomy_field' ) );
				bigshop_toolkit_vc_param( 'uniqid', array( &$this, 'uniqid_field' ) );
				bigshop_toolkit_vc_param( 'select_preview', array( &$this, 'select_preview_field' ) );
				bigshop_toolkit_vc_param( 'number', array( &$this, 'number_field' ) );
			}
		}

		/**
		 * load param autocomplete render
		 * */
		public function autocomplete()
		{
			add_filter( 'vc_autocomplete_bigshop_products_ids_callback', array( &$this, 'productIdAutocompleteSuggester' ), 10, 1 );
			add_filter( 'vc_autocomplete_bigshop_products_ids_render', array( &$this, 'productIdAutocompleteRender' ), 10, 1 );
		}

		/*
         * taxonomy_field
         * */
		public function taxonomy_field( $settings, $value )
		{
			$dependency = '';
			$value_arr  = $value;
			if ( !is_array( $value_arr ) ) {
				$value_arr = array_map( 'trim', explode( ',', $value_arr ) );
			}
			$output = '';
			if ( isset( $settings[ 'hide_empty' ] ) && $settings[ 'hide_empty' ] ) {
				$settings[ 'hide_empty' ] = 1;
			} else {
				$settings[ 'hide_empty' ] = 0;
			}
			if ( !empty( $settings[ 'taxonomy' ] ) ) {
				$terms_fields = array();
				if ( isset( $settings[ 'placeholder' ] ) && $settings[ 'placeholder' ] ) {
					$terms_fields[] = "<option value=''>" . $settings[ 'placeholder' ] . "</option>";
				}
				$terms = get_terms( $settings[ 'taxonomy' ], array( 'hide_empty' => false, 'parent' => $settings[ 'parent' ], 'hide_empty' => $settings[ 'hide_empty' ] ) );
				if ( $terms && !is_wp_error( $terms ) ) {
					foreach ( $terms as $term ) {
						$selected       = ( in_array( $term->slug, $value_arr ) ) ? ' selected="selected"' : '';
						$terms_fields[] = "<option value='{$term->slug}' {$selected}>{$term->name}</option>";
					}
				}
				$size     = ( !empty( $settings[ 'size' ] ) ) ? 'size="' . $settings[ 'size' ] . '"' : '';
				$multiple = ( !empty( $settings[ 'multiple' ] ) ) ? 'multiple="multiple"' : '';
				$uniqeID  = uniqid();
				$output   = '<select style="width:100%;" id="vc_taxonomy-' . $uniqeID . '" ' . $multiple . ' ' . $size . ' name="' . $settings[ 'param_name' ] . '" class="bigshop_vc_taxonomy wpb_vc_param_value wpb-input wpb-select ' . $settings[ 'param_name' ] . ' ' . $settings[ 'type' ] . '_field" ' . $dependency . '>'
					. implode( $terms_fields )
					. '</select>';
			}

			return $output;
		}

		public function uniqid_field( $settings, $value )
		{
			if ( !$value ) {
				$value = uniqid( hash( 'crc32', $settings[ 'param_name' ] ) . '-' );
			}
			$output = '<input type="text" class="wpb_vc_param_value textfield" name="' . $settings[ 'param_name' ] . '" value="' . esc_attr( $value ) . '" />';

			return $output;
		}

		public function number_field( $settings, $value )
		{
			$dependency = '';
			$param_name = isset( $settings[ 'param_name' ] ) ? $settings[ 'param_name' ] : '';
			$type       = isset( $settings[ 'type ' ] ) ? $settings[ 'type' ] : '';
			$min        = isset( $settings[ 'min' ] ) ? $settings[ 'min' ] : '';
			$max        = isset( $settings[ 'max' ] ) ? $settings[ 'max' ] : '';
			$suffix     = isset( $settings[ 'suffix' ] ) ? $settings[ 'suffix' ] : '';
			$class      = isset( $settings[ 'class' ] ) ? $settings[ 'class' ] : '';
			if ( !$value && isset( $settings[ 'std' ] ) ) {
				$value = $settings[ 'std' ];
			}
			$output = '<input type="number" min="' . esc_attr( $min ) . '" max="' . esc_attr( $max ) . '" class="wpb_vc_param_value textfield ' . $param_name . ' ' . $type . ' ' . $class . '" name="' . $param_name . '" value="' . esc_attr( $value ) . '" ' . $dependency . ' style="max-width:100px; margin-right: 10px;" />' . $suffix;

			return $output;
		}

		public function select_preview_field( $settings, $value )
		{
			ob_start();
			// Get menus list
			$options = $settings[ 'value' ];
			$default = $settings[ 'default' ];
			if ( is_array( $options ) && count( $options ) > 0 ) {
				$uniqeID = uniqid();
				$i       = 0;
				?>
                <div class="container-select_preview">
                    <select id="bigshop_select_preview-<?php echo esc_attr( $uniqeID ); ?>"
                            name="<?php echo esc_attr( $settings[ 'param_name' ] ); ?>"
                            class="bigshop_select_preview vc_select_image wpb_vc_param_value wpb-input wpb-select <?php echo esc_attr( $settings[ 'param_name' ] ); ?> <?php echo esc_attr( $settings[ 'type' ] ); ?>_field">
						<?php foreach ( $options as $k => $option ): ?>
							<?php
							if ( $i == 0 ) {
								$first_value = $k;
							}
							$i++;
							?>
							<?php $selected = ( $k == $value ) ? ' selected="selected"' : ''; ?>
                            <option data-img="<?php echo esc_url( $option[ 'img' ] ); ?>"
                                    value='<?php echo esc_attr( $k ) ?>' <?php echo esc_attr( $selected ) ?>><?php echo esc_attr( $option[ 'alt' ] ) ?></option>
						<?php endforeach; ?>
                    </select>
                    <div class="image-preview">
						<?php if ( isset( $options[ $value ] ) && $options[ $value ] && ( isset( $options[ $value ][ 'img' ] ) ) ): ?>
                            <img style="margin-top: 10px; max-width: 100%;height: auto;"
                                 src="<?php echo esc_url( $options[ $value ][ 'img' ] ); ?>" alt="">
						<?php else: ?>
                            <img style="margin-top: 10px; max-width: 100%;height: auto;"
                                 src="<?php echo esc_url( $options[ $default ][ 'img' ] ); ?>" alt="">
						<?php endif; ?>
                    </div>
                </div>
				<?php
			}

			return ob_get_clean();
		}

		/**
		 * Suggester for autocomplete by id/name/title/sku
		 * @since 1.0
		 *
		 * @param $query
		 * @author Reapple
		 * @return array - id's from products with title/sku.
		 */
		public function productIdAutocompleteSuggester( $query )
		{
			global $wpdb;
			$product_id      = (int)$query;
			$post_meta_infos = $wpdb->get_results( $wpdb->prepare( "SELECT a.ID AS id, a.post_title AS title, b.meta_value AS sku
    					FROM {$wpdb->posts} AS a
    					LEFT JOIN ( SELECT meta_value, post_id  FROM {$wpdb->postmeta} WHERE `meta_key` = '_sku' ) AS b ON b.post_id = a.ID
    					WHERE a.post_type = 'product' AND ( a.ID = '%d' OR b.meta_value LIKE '%%%s%%' OR a.post_title LIKE '%%%s%%' )", $product_id > 0 ? $product_id : -1, stripslashes( $query ), stripslashes( $query )
			), ARRAY_A
			);
			$results         = array();
			if ( is_array( $post_meta_infos ) && !empty( $post_meta_infos ) ) {
				foreach ( $post_meta_infos as $value ) {
					$data            = array();
					$data[ 'value' ] = $value[ 'id' ];
					$data[ 'label' ] = esc_html__( 'Id', 'bigshop' ) . ': ' . $value[ 'id' ] . ( ( strlen( $value[ 'title' ] ) > 0 ) ? ' - ' . esc_html__( 'Title', 'bigshop' ) . ': ' . $value[ 'title' ] : '' ) . ( ( strlen( $value[ 'sku' ] ) > 0 ) ? ' - ' . esc_html__( 'Sku', 'bigshop' ) . ': ' . $value[ 'sku' ] : '' );
					$results[]       = $data;
				}
			}

			return $results;
		}

		/**
		 * Find product by id
		 * @since 1.0
		 *
		 * @param $query
		 * @author Reapple
		 *
		 * @return bool|array
		 */
		public function productIdAutocompleteRender( $query )
		{
			$query = trim( $query[ 'value' ] ); // get value from requested
			if ( !empty( $query ) ) {
				// get product
				$product_object = wc_get_product( (int)$query );
				if ( is_object( $product_object ) ) {
					$product_sku         = $product_object->get_sku();
					$product_title       = $product_object->get_title();
					$product_id          = $product_object->get_id();
					$product_sku_display = '';
					if ( !empty( $product_sku ) ) {
						$product_sku_display = ' - ' . esc_html__( 'Sku', 'bigshop' ) . ': ' . $product_sku;
					}
					$product_title_display = '';
					if ( !empty( $product_title ) ) {
						$product_title_display = ' - ' . esc_html__( 'Title', 'bigshop' ) . ': ' . $product_title;
					}
					$product_id_display = esc_html__( 'Id', 'bigshop' ) . ': ' . $product_id;
					$data               = array();
					$data[ 'value' ]    = $product_id;
					$data[ 'label' ]    = $product_id_display . $product_title_display . $product_sku_display;

					return !empty( $data ) ? $data : false;
				}

				return false;
			}

			return false;
		}

		public function vc_fonts( $fonts_list )
		{
			/* Gotham */
			$Gotham              = new stdClass();
			$Gotham->font_family = "Gotham";
			$Gotham->font_styles = "100,300,400,600,700";
			$Gotham->font_types  = "300 Light:300:light,400 Normal:400:normal";

			$fonts = array( $Gotham );

			return array_merge( $fonts_list, $fonts );
		}

		/* Custom Font icon*/
		function iconpicker_type_bigshop_customfonts( $icons )
		{
			$icons[ 'Flaticon' ] = array(
				array( 'flaticon-loop' => '01'),
                array('flaticon-shopping-bag-1' => '02'),
                array('flaticon-shopping-bag' => '03'),
                array('flaticon-exchange' => '04'),
                array('flaticon-search' => '05'),
                array('flaticon-like' => '06'),
                array('flaticon-social' => '07'),
                array('flaticon-clock' => '08'),
                array('flaticon-transport' => '09' ),
			);

			return $icons;
		}

		public static function map_shortcode()
		{
			/* ADD PARAM*/
			vc_add_params(
				'vc_single_image',
				array(
					array(
						'param_name' => 'image_effect',
						'heading'    => esc_html__( 'Effect', 'bigshop' ),
						'group'      => esc_html__( 'Image Effect', 'bigshop' ),
						'type'       => 'dropdown',
						'value'      => array(
							esc_html__( 'Normal Effect', 'bigshop' )    => 'normal-effect',
							esc_html__( 'Plus Zoom', 'bigshop' )        => 'plus-zoom',
							esc_html__( 'Underline Center', 'bigshop' ) => 'underline-center',
							esc_html__( 'None', 'bigshop' )             => 'none',
						),
						'sdt'        => 'none',
					),
				)
			);

			// Map new Tabs element.
			vc_map(
				array(
					'name'                    => esc_html__( 'Bigshop: Tabs', 'bigshop' ),
					'base'                    => 'bigshop_tabs',
					'icon'                    => 'icon-wpb-ui-tab-content',
					'is_container'            => true,
					'show_settings_on_create' => false,
					'as_parent'               => array(
						'only' => 'vc_tta_section',
					),
					'category'                => esc_html__( 'Bigshop Elements', 'bigshop' ),
					'description'             => esc_html__( 'Tabs content', 'bigshop' ),
					'params'                  => array(
                        array(
							'type'        => 'select_preview',
							'heading'     => esc_html__( 'Select style', 'bigshop' ),
							'value'       => array(
								'default' => array(
									'alt' => 'Default',
									'img' => BIGSHOP_SHORTCODE_PREVIEW . 'tab/default.jpg',
								),
                                'style02' => array(
									'alt' => 'Style 02',
									'img' => BIGSHOP_SHORTCODE_PREVIEW . 'tab/style02.png',
								),
                                'style03' => array(
									'alt' => 'Style 03',
									'img' => BIGSHOP_SHORTCODE_PREVIEW . 'tab/style03.jpg',
								),
							),
							'default'     => 'default',
							'admin_label' => true,
							'param_name'  => 'style',
						),
						
                        array(
                            "type"       => "textfield",
                            "heading"    => esc_html__( "Title", 'bigshop' ),
                            "param_name" => "tab_title",
                            "value"      => '',
                        ),
						vc_map_add_css_animation(),
						array(
							'param_name' => 'ajax_check',
							'heading'    => esc_html__( 'Using Ajax Tabs', 'bigshop' ),
							'type'       => 'dropdown',
							'value'      => array(
								esc_html__( 'Yes', 'bigshop' ) => '1',
								esc_html__( 'No', 'bigshop' )  => '0',
							),
							'std'        => '0',
						),
						array(
							'type'       => 'textfield',
							'heading'    => esc_html__( 'Active Section', 'bigshop' ),
							'param_name' => 'active_section',
							'std'        => '1',
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Extra class name', 'bigshop' ),
							'param_name'  => 'el_class',
							'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bigshop' ),
						),
						array(
							'type'       => 'css_editor',
							'heading'    => esc_html__( 'CSS box', 'bigshop' ),
							'param_name' => 'css',
							'group'      => esc_html__( 'Design Options', 'bigshop' ),
						),
						array(
							'param_name'       => 'tabs_custom_id',
							'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
							'type'             => 'uniqid',
							'edit_field_class' => 'hidden',
						),
						array(
							'type'             => 'checkbox',
							'param_name'       => 'collapsible_all',
							'heading'          => esc_html__( 'Allow collapse all?', 'bigshop' ),
							'description'      => esc_html__( 'Allow collapse all accordion sections.', 'bigshop' ),
							'edit_field_class' => 'hidden',
						),
					),
					'js_view'                 => 'VcBackendTtaTabsView',
					'custom_markup'           => '
                    <div class="vc_tta-container" data-vc-action="collapse">
                        <div class="vc_general vc_tta vc_tta-tabs vc_tta-color-backend-tabs-white vc_tta-style-flat vc_tta-shape-rounded vc_tta-spacing-1 vc_tta-tabs-position-top vc_tta-controls-align-left">
                            <div class="vc_tta-tabs-container">'
						. '<ul class="vc_tta-tabs-list">'
						. '<li class="vc_tta-tab" data-vc-tab data-vc-target-model-id="{{ model_id }}" data-element_type="vc_tta_section"><a href="javascript:;" data-vc-tabs data-vc-container=".vc_tta" data-vc-target="[data-model-id=\'{{ model_id }}\']" data-vc-target-model-id="{{ model_id }}"><span class="vc_tta-title-text">{{ section_title }}</span></a></li>'
						. '</ul>
                            </div>
                            <div class="vc_tta-panels vc_clearfix {{container-class}}">
                              {{ content }}
                            </div>
                        </div>
                    </div>',
					'default_content'         => '
                        [vc_tta_section title="' . sprintf( '%s %d', esc_html__( 'Tab', 'bigshop' ), 1 ) . '"][/vc_tta_section]
                        [vc_tta_section title="' . sprintf( '%s %d', esc_html__( 'Tab', 'bigshop' ), 2 ) . '"][/vc_tta_section]
                    ',
					'admin_enqueue_js'        => array(
						vc_asset_url( 'lib/vc_tabs/vc-tabs.min.js' ),
					),
				)
			);

			/* new icon box*/
			vc_map(
				array(
					'name'     => esc_html__( 'Bigshop: Icon Box', 'bigshop' ),
					'base'     => 'bigshop_iconbox',
					'category' => esc_html__( 'Bigshop Elements', 'bigshop' ),
					'params'   => array(
						array(
							'type'        => 'select_preview',
							'heading'     => esc_html__( 'Select style', 'bigshop' ),
							'value'       => array(
								'style1'  => array(
									'alt' => 'Style 1',
									'img' => BIGSHOP_SHORTCODE_PREVIEW . 'iconbox/style1.jpg',
								),
								'style2'  => array(
									'alt' => 'Style 2',
									'img' => BIGSHOP_SHORTCODE_PREVIEW . 'iconbox/style2.jpg',
								),
							),
							'default'     => 'style1',
							'admin_label' => true,
							'param_name'  => 'style',
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Title', 'bigshop' ),
							'param_name'  => 'title',
							'admin_label' => true,
						),
						array(
							'param_name'  => 'text_content',
							'heading'     => esc_html__( 'Content', 'bigshop' ),
							'type'        => 'textarea',
							'admin_label' => true,
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Link to", 'bigshop' ),
							"param_name"  => "link_iconbox",
							'admin_label' => true,
						),
						array(
							'param_name' => 'icon_type',
							'heading'    => esc_html__( 'Icon Library', 'bigshop' ),
							'type'       => 'dropdown',
							'value'      => array(
								esc_html__( 'Font Awesome', 'bigshop' ) => 'fontawesome',
								esc_html__( 'Other', 'bigshop' )        => 'bigshopcustomfonts',
							),
						),
						array(
							'param_name'  => 'icon_bigshopcustomfonts',
							'heading'     => esc_html__( 'Icon', 'bigshop' ),
							'description' => esc_html__( 'Select icon from library.', 'bigshop' ),
							'type'        => 'iconpicker',
							'settings'    => array(
								'emptyIcon' => true,
								'type'      => 'bigshopcustomfonts',
							),
							'dependency'  => array(
								'element' => 'icon_type',
								'value'   => 'bigshopcustomfonts',
							),
						),
						array(
							'param_name'  => 'icon_fontawesome',
							'heading'     => esc_html__( 'Icon', 'bigshop' ),
							'description' => esc_html__( 'Select icon from library.', 'bigshop' ),
							'type'        => 'iconpicker',
							'settings'    => array(
								'emptyIcon'    => true,
								'iconsPerPage' => 4000,
							),
							'dependency'  => array(
								'element' => 'icon_type',
								'value'   => 'fontawesome',
							),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Extra class name', 'bigshop' ),
							'param_name'  => 'el_class',
							'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'bigshop' ),
						),
						array(
							'type'       => 'css_editor',
							'heading'    => esc_html__( 'CSS box', 'bigshop' ),
							'param_name' => 'css',
							'group'      => esc_html__( 'Design Options', 'bigshop' ),
						),
						array(
							'param_name'       => 'iconbox_custom_id',
							'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
							'type'             => 'uniqid',
							'edit_field_class' => 'hidden',
						),
					),
				)
			);
			// Map new Products
			// CUSTOM PRODUCT SIZE
			$product_size_width_list = array();
			$width                   = 300;
			$height                  = 300;
			$crop                    = 1;
			if ( function_exists( 'wc_get_image_size' ) ) {
				$size   = wc_get_image_size( 'shop_catalog' );
				$width  = isset( $size[ 'width' ] ) ? $size[ 'width' ] : $width;
				$height = isset( $size[ 'height' ] ) ? $size[ 'height' ] : $height;
				$crop   = isset( $size[ 'crop' ] ) ? $size[ 'crop' ] : $crop;
			}
			for ( $i = 100; $i < $width; $i = $i + 10 ) {
				array_push( $product_size_width_list, $i );
			}
			$product_size_list                           = array();
			$product_size_list[ $width . 'x' . $height ] = $width . 'x' . $height;
			foreach ( $product_size_width_list as $k => $w ) {
				$w = intval( $w );
				if ( isset( $width ) && $width > 0 ) {
					$h = round( $height * $w / $width );
				} else {
					$h = $w;
				}
				$product_size_list[ $w . 'x' . $h ] = $w . 'x' . $h;
			}
			$product_size_list[ 'Custom' ] = 'custom';
			$attributes_tax                = array();
			if ( function_exists( 'wc_get_attribute_taxonomies' ) ) {
				$attributes_tax = wc_get_attribute_taxonomies();
			}

			$attributes = array();
			if ( is_array( $attributes_tax ) && count( $attributes_tax ) > 0 ) {
				foreach ( $attributes_tax as $attribute ) {
					$attributes[ $attribute->attribute_label ] = $attribute->attribute_name;
				}
			}

			vc_map(
				array(
					'name'        => esc_html__( 'Bigshop: Products', 'bigshop' ),
					'base'        => 'bigshop_products', // shortcode
					'class'       => '',
					'category'    => esc_html__( 'Bigshop Elements', 'bigshop' ),
					'description' => esc_html__( 'Display a product list.', 'bigshop' ),
					'params'      => array(
                    
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Title', 'bigshop' ),
							'param_name'  => 'the_title',
							'admin_label' => true,
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Product List style', 'bigshop' ),
							'param_name'  => 'productsliststyle',
							'value'       => array(
								esc_html__( 'Grid Bootstrap', 'bigshop' ) => 'grid',
								esc_html__( 'Owl Carousel', 'bigshop' )   => 'owl',
							),
							'description' => esc_html__( 'Select a style for list', 'bigshop' ),
							'admin_label' => true,
							'std'         => 'grid',
						),
                        array(
							'heading'     => esc_html__( 'Box Grid layout', 'bigshop' ),
                            'type'        => 'select_preview',
							'value'       => array(
								'default' => array(
									'alt' => 'Default',
                                    'img' => ''
								),
                                'layout2' => array(
									'alt' => 'Layout 02',
									'img' => BIGSHOP_SHORTCODE_PREVIEW . 'product-layout/grid-layout2.jpg',
								),
							),
							'default'     => 'default',
							'admin_label' => true,
							'param_name'  => 'grid_layout',
                            'dependency'  => array(
                                'element' => 'productsliststyle',
                                'value'   => array( 'grid' )
                            )
						),
                        array(
							'heading'     => esc_html__( 'Box Carousel layout', 'bigshop' ),
                            'type'        => 'select_preview',
							'value'       => array(
								'default' => array(
									'alt' => 'Default',
									'img' => BIGSHOP_SHORTCODE_PREVIEW . 'product-layout/owl-default.jpg',
								),
                                'layout2' => array(
									'alt' => 'Layout 02',
									'img' => BIGSHOP_SHORTCODE_PREVIEW . 'product-layout/owl-layout2.jpg',
								),
							),
							'default'     => 'default',
							'admin_label' => true,
							'param_name'  => 'carousel_layout',
                            'dependency'  => array(
                                'element' => 'productsliststyle',
                                'value'   => array( 'owl' )
                            )
						),
						array(
							'type'        => 'select_preview',
							'heading'     => esc_html__( 'Product style', 'bigshop' ),
							'value'       => array(
								'1' => array(
									'alt' => esc_html__( 'Style 01', 'bigshop' ),
									'img' => BIGSHOP_PRODUCT_STYLE_PREVIEW . 'content-product-style-1.jpg',
								),
								'2' => array(
									'alt' => esc_html__( 'Style 02', 'bigshop' ),
									'img' => BIGSHOP_PRODUCT_STYLE_PREVIEW . 'content-product-style-2.jpg',
								),
                                '3' => array(
                                    'alt' => esc_html__( 'Style 03', 'bigshop' ),
                                    'img' => BIGSHOP_PRODUCT_STYLE_PREVIEW . 'content-product-style-3.jpg',
                                ),
                                '4' => array(
                                    'alt' => esc_html__( 'Style 04', 'bigshop' ),
                                    'img' => BIGSHOP_PRODUCT_STYLE_PREVIEW . 'content-product-style-4.jpg',
                                ),
                                '5' => array(
                                    'alt' => esc_html__( 'Style 05', 'bigshop' ),
                                    'img' => BIGSHOP_PRODUCT_STYLE_PREVIEW . 'content-product-style-5.jpg',
                                ),
                                '6' => array(
                                    'alt' => esc_html__( 'Style 06', 'bigshop' ),
                                    'img' => BIGSHOP_PRODUCT_STYLE_PREVIEW . 'content-product-style-6.jpg',
                                ),
                                '7' => array(
                                    'alt' => esc_html__( 'Style 07', 'bigshop' ),
                                    'img' => BIGSHOP_PRODUCT_STYLE_PREVIEW . 'content-product-style-7.jpg',
                                ),
                                '8' => array(
                                    'alt' => esc_html__( 'Style 08', 'bigshop' ),
                                    'img' => BIGSHOP_PRODUCT_STYLE_PREVIEW . 'content-product-style-8.jpg',
                                ),
							),
							'default'     => '1',
							'admin_label' => true,
							'param_name'  => 'product_style',
							'description' => esc_html__( 'Select a style for product item', 'bigshop' ),
						),
                        array(
							"type"       => "textfield",
							"heading"    => esc_html__( "Thumbnail Width", 'bigshop' ),
							"param_name" => "product6_thumb_width",
							"value"      => '60',
							"suffix"     => esc_html__( "px", 'bigshop' ),
							"dependency" => array( "element" => "product_style", "value" => array( '6' ) ),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "Thumbanail Height", 'bigshop' ),
							"param_name" => "product6_thumb_height",
							"value"      => '68',
							"suffix"     => esc_html__( "px", 'bigshop' ),
							"dependency" => array( "element" => "product_style", "value" => array( '6' ) ),
						),
                        
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Image size', 'bigshop' ),
							'param_name'  => 'product_image_size',
							'value'       => $product_size_list,
							'description' => esc_html__( 'Select a size for product', 'bigshop' ),
							'admin_label' => true,
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "Width", 'bigshop' ),
							"param_name" => "product_custom_thumb_width",
							"value"      => $width,
							"suffix"     => esc_html__( "px", 'bigshop' ),
							"dependency" => array( "element" => "product_image_size", "value" => array( 'custom' ) ),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "Height", 'bigshop' ),
							"param_name" => "product_custom_thumb_height",
							"value"      => $height,
							"suffix"     => esc_html__( "px", 'bigshop' ),
							"dependency" => array( "element" => "product_image_size", "value" => array( 'custom' ) ),
						),
						/*Products */
						array(
							"type"        => "taxonomy",
							"taxonomy"    => "product_cat",
							"class"       => "",
							"heading"     => esc_html__( "Product Category", 'bigshop' ),
							"param_name"  => "taxonomy",
							"value"       => '',
							'parent'      => '',
							'multiple'    => true,
							'hide_empty'  => false,
							'placeholder' => esc_html__( 'Choose category', 'bigshop' ),
							"description" => esc_html__( "Note: If you want to narrow output, select category(s) above. Only selected categories will be displayed.", 'bigshop' ),
							'std'         => '',
							'group'       => esc_html__( 'Products options', 'bigshop' ),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Target', 'bigshop' ),
							'param_name'  => 'target',
							'value'       => array(
								esc_html__( 'Best Selling Products', 'bigshop' ) => 'best-selling',
								esc_html__( 'Top Rated Products', 'bigshop' )    => 'top-rated',
								esc_html__( 'Recent Products', 'bigshop' )       => 'recent-product',
								esc_html__( 'Product Category', 'bigshop' )      => 'product-category',
								esc_html__( 'Products', 'bigshop' )              => 'products',
								esc_html__( 'Featured Products', 'bigshop' )     => 'featured_products',
								esc_html__( 'On Sale', 'bigshop' )               => 'on_sale',
								esc_html__( 'On New', 'bigshop' )                => 'on_new',
							),
							'description' => esc_html__( 'Choose the target to filter products', 'bigshop' ),
							'std'         => 'recent-product',
							'group'       => esc_html__( 'Products options', 'bigshop' ),
						),
						array(
							"type"        => "dropdown",
							"heading"     => esc_html__( "Order by", 'bigshop' ),
							"param_name"  => "orderby",
							"value"       => array(
								'',
								esc_html__( 'Date', 'bigshop' )          => 'date',
								esc_html__( 'ID', 'bigshop' )            => 'ID',
								esc_html__( 'Author', 'bigshop' )        => 'author',
								esc_html__( 'Title', 'bigshop' )         => 'title',
								esc_html__( 'Modified', 'bigshop' )      => 'modified',
								esc_html__( 'Random', 'bigshop' )        => 'rand',
								esc_html__( 'Comment count', 'bigshop' ) => 'comment_count',
								esc_html__( 'Menu order', 'bigshop' )    => 'menu_order',
								esc_html__( 'Sale price', 'bigshop' )    => '_sale_price',
							),
							'std'         => 'date',
							"description" => esc_html__( "Select how to sort.", 'bigshop' ),
							"dependency"  => array( "element" => "target", "value" => array( 'top-rated', 'recent-product', 'product-category', 'featured_products', 'on_sale', 'on_new', 'product_attribute' ) ),
							'group'       => esc_html__( 'Products options', 'bigshop' ),
						),
						array(
							"type"        => "dropdown",
							"heading"     => esc_html__( "Order", 'bigshop' ),
							"param_name"  => "order",
							"value"       => array(
								esc_html__( 'ASC', 'bigshop' )  => 'ASC',
								esc_html__( 'DESC', 'bigshop' ) => 'DESC',
							),
							'std'         => 'DESC',
							"description" => esc_html__( "Designates the ascending or descending order.", 'bigshop' ),
							"dependency"  => array( "element" => "target", "value" => array( 'top-rated', 'recent-product', 'product-category', 'featured_products', 'on_sale', 'on_new', 'product_attribute' ) ),
							'group'       => esc_html__( 'Products options', 'bigshop' ),
						),
						array(
							'type'       => 'textfield',
							'heading'    => esc_html__( 'Product per page', 'bigshop' ),
							'param_name' => 'per_page',
							'value'      => 6,
							"dependency" => array( "element" => "target", "value" => array( 'best-selling', 'top-rated', 'recent-product', 'product-category', 'featured_products', 'product_attribute', 'on_sale', 'on_new' ) ),
							'group'      => esc_html__( 'Products options', 'bigshop' ),
						),
						array(
							'type'        => 'autocomplete',
							'heading'     => esc_html__( 'Products', 'bigshop' ),
							'param_name'  => 'ids',
							'settings'    => array(
								'multiple'      => true,
								'sortable'      => true,
								'unique_values' => true,
							),
							'save_always' => true,
							'description' => esc_html__( 'Enter List of Products', 'bigshop' ),
							"dependency"  => array( "element" => "target", "value" => array( 'products' ) ),
							'group'       => esc_html__( 'Products options', 'bigshop' ),
						),
						/* OWL Settings */
						array(
							'type'       => 'dropdown',
							'value'      => array(
								esc_html__( '1 Row', 'bigshop' )  => '1',
								esc_html__( '2 Rows', 'bigshop' ) => '2',
								esc_html__( '3 Rows', 'bigshop' ) => '3',
								esc_html__( '4 Rows', 'bigshop' ) => '4',
								esc_html__( '5 Rows', 'bigshop' ) => '5',
							),
							'std'        => '1',
							'heading'    => esc_html__( 'The number of rows which are shown on block', 'bigshop' ),
							'param_name' => 'owl_number_row',
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency" => array(
								"element" => "productsliststyle", "value" => array( 'owl' ),
							),
						),
						array(
							'type'       => 'dropdown',
							'heading'    => esc_html__( 'Rows space', 'bigshop' ),
							'param_name' => 'owl_rows_space',
							'value'      => array(
								esc_html__( 'Default', 'bigshop' ) => 'rows-space-0',
								esc_html__( '10px', 'bigshop' )    => 'rows-space-10',
								esc_html__( '15px', 'bigshop' )    => 'rows-space-15',
								esc_html__( '20px', 'bigshop' )    => 'rows-space-20',
								esc_html__( '25px', 'bigshop' )    => 'rows-space-25',
								esc_html__( '30px', 'bigshop' )    => 'rows-space-30',
								esc_html__( '35px', 'bigshop' )    => 'rows-space-35',
								esc_html__( '40px', 'bigshop' )    => 'rows-space-40',
								esc_html__( '45px', 'bigshop' )    => 'rows-space-45',
								esc_html__( '50px', 'bigshop' )    => 'rows-space-50',
								esc_html__( '60px', 'bigshop' )    => 'rows-space-60',
								esc_html__( '70px', 'bigshop' )    => 'rows-space-70',
								esc_html__( '80px', 'bigshop' )    => 'rows-space-80',
								esc_html__( '90px', 'bigshop' )    => 'rows-space-90',
								esc_html__( '100px', 'bigshop' )   => 'rows-space-100',
							),
							'std'        => 'rows-space-0',
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency" => array(
								"element" => "owl_number_row", "value" => array( '2', '3', '4', '5' ),
							),
						),
						array(
							'type'       => 'dropdown',
							'value'      => array(
								esc_html__( 'Yes', 'bigshop' ) => 'true',
								esc_html__( 'No', 'bigshop' )  => 'false',
							),
							'std'        => false,
							'heading'    => esc_html__( 'AutoPlay', 'bigshop' ),
							'param_name' => 'owl_autoplay',
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency" => array(
								"element" => "productsliststyle", "value" => array( 'owl' ),
							),
						),
						array(
							'type'        => 'dropdown',
							'value'       => array(
								esc_html__( 'No', 'bigshop' )  => 'false',
								esc_html__( 'Yes', 'bigshop' ) => 'true',
							),
							'std'         => false,
							'heading'     => esc_html__( 'Dots', 'bigshop' ),
							'param_name'  => 'owl_dots',
							'description' => esc_html__( "Show dots.", 'bigshop' ),
							'group'       => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency"  => array(
								"element" => "productsliststyle", "value" => array( 'owl' ),
							),
                            'default' =>'false'
						),
						array(
							'type'        => 'dropdown',
							'value'       => array(
								esc_html__( 'No', 'bigshop' )  => 'false',
								esc_html__( 'Yes', 'bigshop' ) => 'true',
							),
							'std'         => false,
							'heading'     => esc_html__( 'Navigation', 'bigshop' ),
							'param_name'  => 'owl_navigation',
							'description' => esc_html__( "Show buton 'next' and 'prev' buttons.", 'bigshop' ),
							'group'       => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency"  => array(
								"element" => "productsliststyle", "value" => array( 'owl' ),
							),
						),
						array(
							'type'       => 'dropdown',
							'heading'    => esc_html__( 'Navigation position', 'bigshop' ),
							'param_name' => 'owl_navigation_position',
							'value'      => array(
								esc_html__( 'Default', 'bigshop' ) => '',
                                esc_html__('Center', 'bigshop')    => 'nav-center',
                                esc_html__('Top Left', 'bigshop')  => 'nav-top-left',
                                esc_html__('Top Right', 'bigshop') => 'nav-top-right',
							),
							'std'        => '',
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency" => array( "element" => "owl_navigation", "value" => array( 'true' ) ),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "Postion Top", 'bigshop' ),
							"param_name" => "owl_navigation_position_top",
							"std"        => "0",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency" => array(
								"element" => "owl_navigation_position", "value" => array( 'nav-top-left', 'nav-top-right' ),
							),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "Offset Right", 'bigshop' ),
							"param_name" => "owl_navigation_offset_right",
							"std"        => "",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency" => array(
								"element" => "owl_navigation_position", "value" => array( 'nav-top-right' ),
							),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "Offset Left", 'bigshop' ),
							"param_name" => "owl_navigation_offset_left",
							"std"        => "",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency" => array(
								"element" => "owl_navigation_position", "value" => array( 'nav-top-left' ),
							),
						),
						array(
							'type'        => 'dropdown',
							'value'       => array(
								esc_html__( 'Yes', 'bigshop' ) => 'true',
								esc_html__( 'No', 'bigshop' )  => 'false',
							),
							'std'         => false,
							'heading'     => esc_html__( 'Loop', 'bigshop' ),
							'param_name'  => 'owl_loop',
							'description' => esc_html__( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'bigshop' ),
							'group'       => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency"  => array(
								"element" => "productsliststyle", "value" => array( 'owl' ),
							),
                            'default'=> 'false'
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Slide Speed", 'bigshop' ),
							"param_name"  => "owl_slidespeed",
							"value"       => "200",
							"suffix"      => esc_html__( "milliseconds", 'bigshop' ),
							"description" => esc_html__( 'Slide speed in milliseconds', 'bigshop' ),
							'group'       => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency"  => array(
								"element" => "productsliststyle", "value" => array( 'owl' ),
							),
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Margin", 'bigshop' ),
							"param_name"  => "owl_margin",
							"value"       => "0",
							"description" => esc_html__( 'Distance( or space) between 2 item', 'bigshop' ),
							'group'       => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency"  => array(
								"element" => "productsliststyle", "value" => array( 'owl' ),
							),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "The items on desktop (Screen resolution of device >= 1200px and < 1500px )", 'bigshop' ),
							"param_name" => "owl_lg_items",
							"std"        => "4",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency" => array(
								"element" => "productsliststyle", "value" => array( 'owl' ),
							),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "The items on desktop (Screen resolution of device >= 992px < 1200px )", 'bigshop' ),
							"param_name" => "owl_md_items",
							"std"        => "3",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency" => array(
								"element" => "productsliststyle", "value" => array( 'owl' ),
							),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "The items on tablet (Screen resolution of device >=768px and < 992px )", 'bigshop' ),
							"param_name" => "owl_sm_items",
							"std"        => "2",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency" => array(
								"element" => "productsliststyle", "value" => array( 'owl' ),
							),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "The items on mobile landscape(Screen resolution of device >=480px and < 768px)", 'bigshop' ),
							"param_name" => "owl_xs_items",
							"std"        => "2",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency" => array(
								"element" => "productsliststyle", "value" => array( 'owl' ),
							),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "The items on mobile (Screen resolution of device < 480px)", 'bigshop' ),
							"param_name" => "owl_ts_items",
							"std"        => "1",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency" => array(
								"element" => "productsliststyle", "value" => array( 'owl' ),
							),
						),
                        array(
                            'type'          => 'param_group',
                            'param_name'    => 'owl_responsive_custom',
                            "heading"       => esc_html__("Responsive custom settings", 'bigshop'),
                            "dependency"    => array(
                                 "element"      => "productsliststyle", "value" => array('owl')
                             ),
                            'group'         => esc_html__( 'Carousel settings', 'bigshop' ),
                            'params'        => array(
                                array(
                                    'type'          => 'textfield',
                                    'value'         => '',
                                    'heading'       => esc_html__("Device width", 'bigshop'),
                                    'param_name'    => 'device_width',
                                ),
                                array(
                                    'type'          => 'textfield',
                                    'value'         => '',
                                    'heading'       => esc_html__("Item number", 'bigshop'),
                                    'param_name'    => 'item_number',
                                ),
                                array(
                                    'type'          => 'textfield',
                                    'value'         => '',
                                    'heading'       => esc_html__("Item margin", 'bigshop'),
                                    'param_name'    => 'item_margin',
                                )
                            )
                        ),
						/* Bostrap setting */
						array(
							'type'       => 'dropdown',
							'heading'    => esc_html__( 'Rows space', 'bigshop' ),
							'param_name' => 'boostrap_rows_space',
							'value'      => array(
                                esc_html__( 'Default', 'bigshop' ) => 'rows-space-0',
                                esc_html__( '10px', 'bigshop' )    => 'rows-space-10',
                                esc_html__( '15px', 'bigshop' )    => 'rows-space-15',
                                esc_html__( '20px', 'bigshop' )    => 'rows-space-20',
                                esc_html__( '25px', 'bigshop' )    => 'rows-space-25',
                                esc_html__( '30px', 'bigshop' )    => 'rows-space-30',
                                esc_html__( '35px', 'bigshop' )    => 'rows-space-35',
                                esc_html__( '40px', 'bigshop' )    => 'rows-space-40',
                                esc_html__( '45px', 'bigshop' )    => 'rows-space-45',
                                esc_html__( '50px', 'bigshop' )    => 'rows-space-50',
                                esc_html__( '60px', 'bigshop' )    => 'rows-space-60',
                                esc_html__( '70px', 'bigshop' )    => 'rows-space-70',
                                esc_html__( '80px', 'bigshop' )    => 'rows-space-80',
                                esc_html__( '90px', 'bigshop' )    => 'rows-space-90',
                                esc_html__( '100px', 'bigshop' )   => 'rows-space-100',
							),
							'std'        => 'rows-space-0',
							'group'      => esc_html__( 'Boostrap settings', 'bigshop' ),
							"dependency" => array(
								"element" => "productsliststyle", "value" => array( 'grid' ),
							),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Items per row on Desktop', 'bigshop' ),
							'param_name'  => 'boostrap_lg_items',
							'value'       => array(
								esc_html__( '1 item', 'bigshop' )  => '12',
								esc_html__( '2 items', 'bigshop' ) => '6',
								esc_html__( '3 items', 'bigshop' ) => '4',
								esc_html__( '4 items', 'bigshop' ) => '3',
								esc_html__( '5 items', 'bigshop' ) => '15',
								esc_html__( '6 items', 'bigshop' ) => '2',
							),
							'description' => esc_html__( '(Item per row on screen resolution of device >= 1200px and < 1500px )', 'bigshop' ),
							'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
							'std'         => '3',
							"dependency"  => array(
								"element" => "productsliststyle", "value" => array( 'grid' ),
							),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Items per row on landscape tablet', 'bigshop' ),
							'param_name'  => 'boostrap_md_items',
							'value'       => array(
								esc_html__( '1 item', 'bigshop' )  => '12',
								esc_html__( '2 items', 'bigshop' ) => '6',
								esc_html__( '3 items', 'bigshop' ) => '4',
								esc_html__( '4 items', 'bigshop' ) => '3',
								esc_html__( '5 items', 'bigshop' ) => '15',
								esc_html__( '6 items', 'bigshop' ) => '2',
							),
							'description' => esc_html__( '(Item per row on screen resolution of device >=992px and < 1200px )', 'bigshop' ),
							'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
							'std'         => '3',
							"dependency"  => array(
								"element" => "productsliststyle", "value" => array( 'grid' ),
							),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Items per row on portrait tablet', 'bigshop' ),
							'param_name'  => 'boostrap_sm_items',
							'value'       => array(
								esc_html__( '1 item', 'bigshop' )  => '12',
								esc_html__( '2 items', 'bigshop' ) => '6',
								esc_html__( '3 items', 'bigshop' ) => '4',
								esc_html__( '4 items', 'bigshop' ) => '3',
								esc_html__( '5 items', 'bigshop' ) => '15',
								esc_html__( '6 items', 'bigshop' ) => '2',
							),
							'description' => esc_html__( '(Item per row on screen resolution of device >=768px and < 992px )', 'bigshop' ),
							'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
							'std'         => '4',
							"dependency"  => array(
								"element" => "productsliststyle", "value" => array( 'grid' ),
							),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Items per row on Mobile', 'bigshop' ),
							'param_name'  => 'boostrap_xs_items',
							'value'       => array(
								esc_html__( '1 item', 'bigshop' )  => '12',
								esc_html__( '2 items', 'bigshop' ) => '6',
								esc_html__( '3 items', 'bigshop' ) => '4',
								esc_html__( '4 items', 'bigshop' ) => '3',
								esc_html__( '5 items', 'bigshop' ) => '15',
								esc_html__( '6 items', 'bigshop' ) => '2',
							),
							'description' => esc_html__( '(Item per row on screen resolution of device >=480  add < 768px )', 'bigshop' ),
							'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
							'std'         => '6',
							"dependency"  => array(
								"element" => "productsliststyle", "value" => array( 'grid' ),
							),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Items per row on Mobile', 'bigshop' ),
							'param_name'  => 'boostrap_ts_items',
							'value'       => array(
								esc_html__( '1 item', 'bigshop' )  => '12',
								esc_html__( '2 items', 'bigshop' ) => '6',
								esc_html__( '3 items', 'bigshop' ) => '4',
								esc_html__( '4 items', 'bigshop' ) => '3',
								esc_html__( '5 items', 'bigshop' ) => '15',
								esc_html__( '6 items', 'bigshop' ) => '2',
							),
							'description' => esc_html__( '(Item per row on screen resolution of device < 480px)', 'bigshop' ),
							'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
							'std'         => '12',
							"dependency"  => array(
								"element" => "productsliststyle", "value" => array( 'grid' ),
							),
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Extra class name", "bigshop" ),
							"param_name"  => "el_class",
							"description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "bigshop" ),
						),
						array(
							'type'       => 'css_editor',
							'heading'    => esc_html__( 'Css', 'bigshop' ),
							'param_name' => 'css',
							'group'      => esc_html__( 'Design Options', 'bigshop' ),
						),
						array(
							'param_name'       => 'products_custom_id',
							'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
							'type'             => 'uniqid',
							'edit_field_class' => 'hidden',
						),
					),
				)
			);
            
            /* Map categories */
            vc_map(
                array(
                    'name'     => esc_html__( 'Bigshop: Categories', 'bigshop' ),
                    'base'     => 'bigshop_categories', // shortcode
                    'class'    => '',
                    'category' => esc_html__( 'Bigshop Elements', 'bigshop' ),
                    'description'   =>  esc_html__( 'Display Categories list.', 'bigshop' ),
                    'params'   => array(
                        array(
                            'type' => 'select_preview',
                            'heading' => esc_html__( 'Layout', 'bigshop' ),
                            'value' => array(
                                'default' => array(
                                    'alt' => 'Default',
                                    'img' => BIGSHOP_SHORTCODE_PREVIEW.'categories/default.jpg'
                                ),
                                
                            ),
                            'default'       =>'default',
                            'admin_label' => true,
                            'param_name' => 'style',
                        ),
                        array(
                            "type"        => "taxonomy",
                            "taxonomy"    => "product_cat",
                            "class"       => "",
                            "heading"     => esc_html__("Product Category", 'bigshop'),
                            "param_name"  => "taxonomy",
                            "value"       => '',
                            'parent'      => '',
                            'multiple'    => true,
                            'hide_empty'  => false,
                            'placeholder' => esc_html__('Choose category', 'bigshop'),
                            "description" => esc_html__( "Select a product category.", 'bigshop' ),
                            'std'         => '',
                            "admin_label" => true,
                        ),
                        array(
                            "type"        => "attach_image",
                            "heading"     => __( "Background", "bigshop" ),
                            "param_name"  => "bg_cat",
                            "admin_label" => false,
                        ),
                        
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__( "Title", "bigshop" ),
                            "description" => esc_html__( "Leave it blank if you want to display the origin category title", "bigshop" ),
                            "param_name"  => "title",
                        ),
                        
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__("Extra class name", "bigshop"),
                            "param_name"  => "el_class",
                            "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "bigshop")
                        ),
                        array(
                            'type'       => 'css_editor',
                            'heading'    => esc_html__('Css', 'bigshop'),
                            'param_name' => 'css',
                            'group'      => esc_html__('Design Options', 'bigshop'),
                        ),
                        array(
                            'param_name'       => 'categories_custom_id',
                            'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
                            'type'             => 'uniqid',
                            'edit_field_class' => 'hidden',
                        )
                    )
                )
            );

			/* Map New blog */
			$categories_array = array(
				esc_html__( 'All', 'bigshop' ) => '',
			);
			$args             = array();
			$categories       = get_categories( $args );
			foreach ( $categories as $category ) {
				$categories_array[ $category->name ] = $category->slug;
			}

			vc_map(
				array(
					'name'        => esc_html__( 'Bigshop: Blog', 'bigshop' ),
					'base'        => 'bigshop_blog',
					'class'       => '',
					'category'    => esc_html__( 'Bigshop Elements', 'bigshop' ),
					'description' => esc_html__( 'Display a blog lists.', 'bigshop' ),
					'params'      => array(
						array(
							'type'        => 'select_preview',
							'heading'     => esc_html__( 'Select style', 'bigshop' ),
							'value'       => array(
								'style-1' => array(
									'alt' => 'Style 01',
									'img' => BIGSHOP_SHORTCODE_PREVIEW . 'blog/style1.jpg',
								),
                                'style-2' => array(
									'alt' => 'Style 02',
									'img' => BIGSHOP_SHORTCODE_PREVIEW . 'blog/style2.jpg',
								),
                                'style-3' => array(
									'alt' => 'Style 03',
									'img' => BIGSHOP_SHORTCODE_PREVIEW . 'blog/style3.jpg',
								),
							),
							'default'     => 'style-1',
							'admin_label' => true,
							'param_name'  => 'style',
						),
                        array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Title', 'bigshop' ),
							'param_name'  => 'title',
							'admin_label' => true,
							'description' => esc_html__( 'Title for block', 'bigshop' ),
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Number Post', 'bigshop' ),
							'param_name'  => 'per_page',
							'std'         => 3,
							'admin_label' => false,
							'description' => esc_html__( 'Number post in a slide', 'bigshop' ),
						),
						array(
							'param_name'  => 'category_slug',
							'type'        => 'dropdown',
							'value'       => $categories_array,
							'heading'     => esc_html__( 'Category filter:', 'bigshop' ),
							"admin_label" => false,
						),
						array(
							"type"        => "dropdown",
							"heading"     => esc_html__( "Order by", 'bigshop' ),
							"param_name"  => "orderby",
							"value"       => array(
								esc_html__( 'None', 'bigshop' )     => 'none',
								esc_html__( 'ID', 'bigshop' )       => 'ID',
								esc_html__( 'Author', 'bigshop' )   => 'author',
								esc_html__( 'Name', 'bigshop' )     => 'name',
								esc_html__( 'Date', 'bigshop' )     => 'date',
								esc_html__( 'Modified', 'bigshop' ) => 'modified',
								esc_html__( 'Rand', 'bigshop' )     => 'rand',
							),
							'std'         => 'date',
							"description" => esc_html__( "Select how to sort retrieved posts.", 'bigshop' ),
						),
						array(
							"type"        => "dropdown",
							"heading"     => esc_html__( "Order", 'bigshop' ),
							"param_name"  => "order",
							"value"       => array(
								esc_html__( 'ASC', 'bigshop' )  => 'ASC',
								esc_html__( 'DESC', 'bigshop' ) => 'DESC',
							),
							'std'         => 'DESC',
							"description" => esc_html__( "Designates the ascending or descending order.", 'bigshop' ),
						),
                        array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Except length", 'bigshop' ),
							"param_name"  => "excerpt_length",
							"value"       => 14,
							"description" => esc_html__( 'Limit exceprt length', 'bigshop' ),
                            "dependency"  => array(
                                "element" => "style",
                                "value"   => array(
                                    "style-1", 'style-3'
                                )
                            )
						),
                        array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Read more text", 'bigshop' ),
							"param_name"  => "readmore_text",
							"value"       => 'Read more . . .',
							"description" => esc_html__( 'Read more text', 'bigshop' ),
                            "dependency"  => array(
                                "element" => "style",
                                "value"   => array(
                                    "style-1", 'style-3'
                                )
                            )
						),
                        array(
							"type"        => "dropdown",
							"heading"     => esc_html__( "Crop image", 'bigshop' ),
							"param_name"  => "crop_image",
							"value"       => array(
								esc_html__( 'Yes', 'bigshop' )  => 'yes',
								esc_html__( 'No', 'bigshop' )   => 'no',
							),
							'std'         => 'no',
							"description" => esc_html__( "Allow crop thumbnail image with custom width x height", 'bigshop' ),
						),
                        array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Width", 'bigshop' ),
							"param_name"  => "crop_width",
							"value"       => "270",
							"description" => esc_html__( 'Crop width in px', 'bigshop' ),
                            "dependency"  => array(
                                "element" => "crop_image",
                                "value"   => array( "yes" )
                            )
						),
                        array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Height", 'bigshop' ),
							"param_name"  => "crop_height",
							"value"       => "186",
							"description" => esc_html__( 'Crop height in px', 'bigshop' ),
                            "dependency"  => array(
                                "element" => "crop_image",
                                "value"   => array( "yes" )
                            )
						),
						/* Owl */
						array(
							'type'       => 'dropdown',
							'value'      => array(
								esc_html__( '1 Row', 'bigshop' )  => '1',
								esc_html__( '2 Rows', 'bigshop' ) => '2',
								esc_html__( '3 Rows', 'bigshop' ) => '3',
								esc_html__( '4 Rows', 'bigshop' ) => '4',
								esc_html__( '5 Rows', 'bigshop' ) => '5',
							),
							'std'        => '1',
							'heading'    => esc_html__( 'The number of rows which are shown on block', 'bigshop' ),
							'param_name' => 'owl_number_row',
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							'type'       => 'dropdown',
							'heading'    => esc_html__( 'Rows space', 'bigshop' ),
							'param_name' => 'owl_rows_space',
							'value'      => array(
								esc_html__( 'Default', 'bigshop' ) => 'rows-space-0',
								esc_html__( '10px', 'bigshop' )    => 'rows-space-10',
								esc_html__( '20px', 'bigshop' )    => 'rows-space-20',
								esc_html__( '30px', 'bigshop' )    => 'rows-space-30',
								esc_html__( '40px', 'bigshop' )    => 'rows-space-40',
								esc_html__( '50px', 'bigshop' )    => 'rows-space-50',
								esc_html__( '60px', 'bigshop' )    => 'rows-space-60',
								esc_html__( '70px', 'bigshop' )    => 'rows-space-70',
								esc_html__( '80px', 'bigshop' )    => 'rows-space-80',
								esc_html__( '90px', 'bigshop' )    => 'rows-space-90',
								esc_html__( '100px', 'bigshop' )   => 'rows-space-100',
							),
							'std'        => 'rows-space-0',
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency" => array(
								"element" => "owl_number_row", "value" => array( '2', '3', '4', '5' ),
							),
						),
						array(
							'type'       => 'dropdown',
							'value'      => array(
								esc_html__( 'Yes', 'bigshop' ) => 'true',
								esc_html__( 'No', 'bigshop' )  => 'false',
							),
							'std'        => 'false',
							'heading'    => esc_html__( 'AutoPlay', 'bigshop' ),
							'param_name' => 'owl_autoplay',
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						
						array(
							'type'        => 'dropdown',
							'value'       => array(
								esc_html__( 'No', 'bigshop' )  => 'false',
								esc_html__( 'Yes', 'bigshop' ) => 'true',
							),
							'std'         => 'false',
							'heading'     => esc_html__( 'Navigation', 'bigshop' ),
							'param_name'  => 'owl_navigation',
							'description' => esc_html__( "Show buton 'next' and 'prev' buttons.", 'bigshop' ),
							'group'       => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						
						array(
							'type'        => 'dropdown',
							'value'       => array(
								esc_html__( 'Yes', 'bigshop' ) => 'true',
								esc_html__( 'No', 'bigshop' )  => 'false',
							),
							'std'         => 'false',
							'heading'     => esc_html__( 'Loop', 'bigshop' ),
							'param_name'  => 'owl_loop',
							'description' => esc_html__( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'bigshop' ),
							'group'       => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Slide Speed", 'bigshop' ),
							"param_name"  => "owl_slidespeed",
							"value"       => "200",
							"suffix"      => esc_html__( "milliseconds", 'bigshop' ),
							"description" => esc_html__( 'Slide speed in milliseconds', 'bigshop' ),
							'group'       => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Margin", 'bigshop' ),
							"param_name"  => "owl_margin",
							"value"       => "0",
							"description" => esc_html__( 'Distance( or space) between 2 item', 'bigshop' ),
							'group'       => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "The items on desktop (Screen resolution of device >= 1200px and < 1500px )", 'bigshop' ),
							"param_name" => "owl_lg_items",
							"std"        => "1",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "The items on desktop (Screen resolution of device >= 992px < 1200px )", 'bigshop' ),
							"param_name" => "owl_md_items",
							"std"        => "1",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "The items on tablet (Screen resolution of device >=768px and < 992px )", 'bigshop' ),
							"param_name" => "owl_sm_items",
							"std"        => "1",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "The items on mobile landscape(Screen resolution of device >=480px and < 768px)", 'bigshop' ),
							"param_name" => "owl_xs_items",
							"std"        => "1",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "The items on mobile (Screen resolution of device < 480px)", 'bigshop' ),
							"param_name" => "owl_ts_items",
							"std"        => "1",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Extra class name", "bigshop" ),
							"param_name"  => "el_class",
							"description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "bigshop" ),
						),
						array(
							'type'       => 'css_editor',
							'heading'    => esc_html__( 'Css', 'bigshop' ),
							'param_name' => 'css',
							'group'      => esc_html__( 'Design Options', 'bigshop' ),
						),
						array(
							'param_name'       => 'blog_custom_id',
							'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
							'type'             => 'uniqid',
							'edit_field_class' => 'hidden',
						),
					),
				)
			);

            /* Map testimonial */
            vc_map(
				array(
					'name'        => esc_html__( 'Bigshop: Testimonials', 'bigshop' ),
					'base'        => 'bigshop_testimonials',
					'class'       => '',
					'category'    => esc_html__( 'Bigshop Elements', 'bigshop' ),
					'description' => esc_html__( 'Display a testimonials list.', 'bigshop' ),
					'params'      => array(
						array(
							'type'        => 'select_preview',
							'heading'     => esc_html__( 'Select style', 'bigshop' ),
							'value'       => array(
								'style-1' => array(
									'alt' => 'Style 01',
									'img' => BIGSHOP_SHORTCODE_PREVIEW . 'testimonials/default.jpg',
								),
							),
							'default'     => 'style-1',
							'admin_label' => true,
							'param_name'  => 'style',
						),
                        array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Title', 'bigshop' ),
							'param_name'  => 'title',
							'admin_label' => true,
							'description' => esc_html__( 'Title for block', 'bigshop' ),
						),
                        
                        array(
							'type'        => 'param_group',
							'heading'     => esc_html__( 'Testimonials', 'bigshop' ),
							'param_name'  => 'testimonials',
							'admin_label' => false,
							'description' => esc_html__( 'Title for block', 'bigshop' ),
                            'params'      => array(
                                array(
        							'type'        => 'attach_image',
        							'heading'     => esc_html__( 'Avatar', 'bigshop' ),
        							'param_name'  => 'avatar'
        						),
                                array(
        							'type'        => 'textfield',
        							'heading'     => esc_html__( 'Name', 'bigshop' ),
        							'param_name'  => 'name'
        						),
                                array(
        							'type'        => 'textfield',
        							'heading'     => esc_html__( 'Position/Job', 'bigshop' ),
        							'param_name'  => 'position',
        						),
                                array(
        							'type'        => 'textarea',
        							'heading'     => esc_html__( 'Content', 'bigshop' ),
        							'param_name'  => 'content',
        						)
                                
                            )
						),
						
						/* Owl */
						array(
							'type'       => 'dropdown',
							'value'      => array(
								esc_html__( '1 Row', 'bigshop' )  => '1',
								esc_html__( '2 Rows', 'bigshop' ) => '2',
								esc_html__( '3 Rows', 'bigshop' ) => '3',
								esc_html__( '4 Rows', 'bigshop' ) => '4',
								esc_html__( '5 Rows', 'bigshop' ) => '5',
							),
							'std'        => '1',
							'heading'    => esc_html__( 'The number of rows which are shown on block', 'bigshop' ),
							'param_name' => 'owl_number_row',
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							'type'       => 'dropdown',
							'heading'    => esc_html__( 'Rows space', 'bigshop' ),
							'param_name' => 'owl_rows_space',
							'value'      => array(
								esc_html__( 'Default', 'bigshop' ) => 'rows-space-0',
								esc_html__( '10px', 'bigshop' )    => 'rows-space-10',
								esc_html__( '20px', 'bigshop' )    => 'rows-space-20',
								esc_html__( '30px', 'bigshop' )    => 'rows-space-30',
								esc_html__( '40px', 'bigshop' )    => 'rows-space-40',
								esc_html__( '50px', 'bigshop' )    => 'rows-space-50',
								esc_html__( '60px', 'bigshop' )    => 'rows-space-60',
								esc_html__( '70px', 'bigshop' )    => 'rows-space-70',
								esc_html__( '80px', 'bigshop' )    => 'rows-space-80',
								esc_html__( '90px', 'bigshop' )    => 'rows-space-90',
								esc_html__( '100px', 'bigshop' )   => 'rows-space-100',
							),
							'std'        => 'rows-space-0',
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
							"dependency" => array(
								"element" => "owl_number_row", "value" => array( '2', '3', '4', '5' ),
							),
						),
						array(
							'type'       => 'dropdown',
							'value'      => array(
								esc_html__( 'Yes', 'bigshop' ) => 'true',
								esc_html__( 'No', 'bigshop' )  => 'false',
							),
							'std'        => 'false',
							'heading'    => esc_html__( 'AutoPlay', 'bigshop' ),
							'param_name' => 'owl_autoplay',
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						
						array(
							'type'        => 'dropdown',
							'value'       => array(
								esc_html__( 'No', 'bigshop' )  => 'false',
								esc_html__( 'Yes', 'bigshop' ) => 'true',
							),
							'std'         => 'false',
							'heading'     => esc_html__( 'Navigation', 'bigshop' ),
							'param_name'  => 'owl_navigation',
							'description' => esc_html__( "Show buton 'next' and 'prev' buttons.", 'bigshop' ),
							'group'       => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						
						array(
							'type'        => 'dropdown',
							'value'       => array(
								esc_html__( 'Yes', 'bigshop' ) => 'true',
								esc_html__( 'No', 'bigshop' )  => 'false',
							),
							'std'         => 'false',
							'heading'     => esc_html__( 'Loop', 'bigshop' ),
							'param_name'  => 'owl_loop',
							'description' => esc_html__( "Inifnity loop. Duplicate last and first items to get loop illusion.", 'bigshop' ),
							'group'       => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Slide Speed", 'bigshop' ),
							"param_name"  => "owl_slidespeed",
							"value"       => "200",
							"suffix"      => esc_html__( "milliseconds", 'bigshop' ),
							"description" => esc_html__( 'Slide speed in milliseconds', 'bigshop' ),
							'group'       => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Margin", 'bigshop' ),
							"param_name"  => "owl_margin",
							"value"       => "0",
							"description" => esc_html__( 'Distance( or space) between 2 item', 'bigshop' ),
							'group'       => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "The items on desktop (Screen resolution of device >= 1200px and < 1500px )", 'bigshop' ),
							"param_name" => "owl_lg_items",
							"std"        => "1",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "The items on desktop (Screen resolution of device >= 992px < 1200px )", 'bigshop' ),
							"param_name" => "owl_md_items",
							"std"        => "1",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "The items on tablet (Screen resolution of device >=768px and < 992px )", 'bigshop' ),
							"param_name" => "owl_sm_items",
							"std"        => "1",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "The items on mobile landscape(Screen resolution of device >=480px and < 768px)", 'bigshop' ),
							"param_name" => "owl_xs_items",
							"std"        => "1",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "The items on mobile (Screen resolution of device < 480px)", 'bigshop' ),
							"param_name" => "owl_ts_items",
							"std"        => "1",
							'group'      => esc_html__( 'Carousel settings', 'bigshop' ),
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Extra class name", "bigshop" ),
							"param_name"  => "el_class",
							"description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "bigshop" ),
						),
						array(
							'type'       => 'css_editor',
							'heading'    => esc_html__( 'Css', 'bigshop' ),
							'param_name' => 'css',
							'group'      => esc_html__( 'Design Options', 'bigshop' ),
						),
						array(
							'param_name'       => 'testimonials_custom_id',
							'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
							'type'             => 'uniqid',
							'edit_field_class' => 'hidden',
						),
					),
				)
			);

			/*Map new Container */
//			vc_map(
//				array(
//					'name'                    => esc_html__( 'Bigshop: Container', 'bigshop' ),
//					'base'                    => 'bigshop_container',
//					'category'                => esc_html__( 'Bigshop Elements', 'bigshop' ),
//					'content_element'         => true,
//					'show_settings_on_create' => true,
//					'is_container'            => true,
//					'js_view'                 => 'VcColumnView',
//					'params'                  => array(
//						array(
//							'param_name'  => 'content_width',
//							'heading'     => esc_html__( 'Content width', 'bigshop' ),
//							'type'        => 'dropdown',
//							'value'       => array(
//								esc_html__( 'Default', 'bigshop' )         => 'container',
//								esc_html__( 'Custom Boostrap', 'bigshop' ) => 'custom_col',
//								esc_html__( 'Custom Width', 'bigshop' )    => 'custom_width',
//							),
//							'admin_label' => true,
//							'std'         => 'container',
//						),
//						array(
//							'type'        => 'dropdown',
//							'heading'     => esc_html__( 'Percent width row on Desktop', 'bigshop' ),
//							'param_name'  => 'boostrap_bg_items',
//							'value'       => array(
//								esc_html__( '12 column - 12/12', 'bigshop' ) => '12',
//								esc_html__( '11 column - 11/12', 'bigshop' ) => '11',
//								esc_html__( '10 column - 10/12', 'bigshop' ) => '10',
//								esc_html__( '9 column - 9/12', 'bigshop' )   => '9',
//								esc_html__( '8 column - 8/12', 'bigshop' )   => '8',
//								esc_html__( '7 column - 7/12', 'bigshop' )   => '7',
//								esc_html__( '6 column - 6/12', 'bigshop' )   => '6',
//								esc_html__( '5 column - 5/12', 'bigshop' )   => '5',
//								esc_html__( '4 column - 4/12', 'bigshop' )   => '4',
//								esc_html__( '3 column - 3/12', 'bigshop' )   => '3',
//								esc_html__( '2 column - 2/12', 'bigshop' )   => '2',
//								esc_html__( '1 column - 1/12', 'bigshop' )   => '1',
//								esc_html__( '4 column 5 - 4/5', 'bigshop' )  => '45',
//								esc_html__( '3 column 5 - 3/5', 'bigshop' )  => '35',
//								esc_html__( '2 column 5 - 2/5', 'bigshop' )  => '25',
//								esc_html__( '1 column 5 - 1/5', 'bigshop' )  => '15',
//							),
//							'description' => esc_html__( '(Percent width row on screen resolution of device >= 1500px )', 'bigshop' ),
//							'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
//							'std'         => '15',
//							'dependency'  => array(
//								'element' => 'content_width',
//								'value'   => array( 'custom_col' ),
//							),
//						),
//						array(
//							'type'        => 'dropdown',
//							'heading'     => esc_html__( 'Percent width row on Desktop', 'bigshop' ),
//							'param_name'  => 'boostrap_lg_items',
//							'value'       => array(
//								esc_html__( '12 column - 12/12', 'bigshop' ) => '12',
//								esc_html__( '11 column - 11/12', 'bigshop' ) => '11',
//								esc_html__( '10 column - 10/12', 'bigshop' ) => '10',
//								esc_html__( '9 column - 9/12', 'bigshop' )   => '9',
//								esc_html__( '8 column - 8/12', 'bigshop' )   => '8',
//								esc_html__( '7 column - 7/12', 'bigshop' )   => '7',
//								esc_html__( '6 column - 6/12', 'bigshop' )   => '6',
//								esc_html__( '5 column - 5/12', 'bigshop' )   => '5',
//								esc_html__( '4 column - 4/12', 'bigshop' )   => '4',
//								esc_html__( '3 column - 3/12', 'bigshop' )   => '3',
//								esc_html__( '2 column - 2/12', 'bigshop' )   => '2',
//								esc_html__( '1 column - 1/12', 'bigshop' )   => '1',
//								esc_html__( '4 column 5 - 4/5', 'bigshop' )  => '45',
//								esc_html__( '3 column 5 - 3/5', 'bigshop' )  => '35',
//								esc_html__( '2 column 5 - 2/5', 'bigshop' )  => '25',
//								esc_html__( '1 column 5 - 1/5', 'bigshop' )  => '15',
//							),
//							'description' => esc_html__( '(Percent width row on screen resolution of device >= 1200px and < 1500px )', 'bigshop' ),
//							'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
//							'std'         => '12',
//							'dependency'  => array(
//								'element' => 'content_width',
//								'value'   => array( 'custom_col' ),
//							),
//						),
//						array(
//							'type'        => 'dropdown',
//							'heading'     => esc_html__( 'Percent width row on landscape tablet', 'bigshop' ),
//							'param_name'  => 'boostrap_md_items',
//							'value'       => array(
//								esc_html__( '12 column - 12/12', 'bigshop' ) => '12',
//								esc_html__( '11 column - 11/12', 'bigshop' ) => '11',
//								esc_html__( '10 column - 10/12', 'bigshop' ) => '10',
//								esc_html__( '9 column - 9/12', 'bigshop' )   => '9',
//								esc_html__( '8 column - 8/12', 'bigshop' )   => '8',
//								esc_html__( '7 column - 7/12', 'bigshop' )   => '7',
//								esc_html__( '6 column - 6/12', 'bigshop' )   => '6',
//								esc_html__( '5 column - 5/12', 'bigshop' )   => '5',
//								esc_html__( '4 column - 4/12', 'bigshop' )   => '4',
//								esc_html__( '3 column - 3/12', 'bigshop' )   => '3',
//								esc_html__( '2 column - 2/12', 'bigshop' )   => '2',
//								esc_html__( '1 column - 1/12', 'bigshop' )   => '1',
//								esc_html__( '4 column 5 - 4/5', 'bigshop' )  => '45',
//								esc_html__( '3 column 5 - 3/5', 'bigshop' )  => '35',
//								esc_html__( '2 column 5 - 2/5', 'bigshop' )  => '25',
//								esc_html__( '1 column 5 - 1/5', 'bigshop' )  => '15',
//							),
//							'description' => esc_html__( '(Percent width row on screen resolution of device >=992px and < 1200px )', 'bigshop' ),
//							'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
//							'std'         => '12',
//							'dependency'  => array(
//								'element' => 'content_width',
//								'value'   => array( 'custom_col' ),
//							),
//						),
//						array(
//							'type'        => 'dropdown',
//							'heading'     => esc_html__( 'Percent width row on portrait tablet', 'bigshop' ),
//							'param_name'  => 'boostrap_sm_items',
//							'value'       => array(
//								esc_html__( '12 column - 12/12', 'bigshop' ) => '12',
//								esc_html__( '11 column - 11/12', 'bigshop' ) => '11',
//								esc_html__( '10 column - 10/12', 'bigshop' ) => '10',
//								esc_html__( '9 column - 9/12', 'bigshop' )   => '9',
//								esc_html__( '8 column - 8/12', 'bigshop' )   => '8',
//								esc_html__( '7 column - 7/12', 'bigshop' )   => '7',
//								esc_html__( '6 column - 6/12', 'bigshop' )   => '6',
//								esc_html__( '5 column - 5/12', 'bigshop' )   => '5',
//								esc_html__( '4 column - 4/12', 'bigshop' )   => '4',
//								esc_html__( '3 column - 3/12', 'bigshop' )   => '3',
//								esc_html__( '2 column - 2/12', 'bigshop' )   => '2',
//								esc_html__( '1 column - 1/12', 'bigshop' )   => '1',
//								esc_html__( '4 column 5 - 4/5', 'bigshop' )  => '45',
//								esc_html__( '3 column 5 - 3/5', 'bigshop' )  => '35',
//								esc_html__( '2 column 5 - 2/5', 'bigshop' )  => '25',
//								esc_html__( '1 column 5 - 1/5', 'bigshop' )  => '15',
//							),
//							'description' => esc_html__( '(Percent width row on screen resolution of device >=768px and < 992px )', 'bigshop' ),
//							'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
//							'std'         => '12',
//							'dependency'  => array(
//								'element' => 'content_width',
//								'value'   => array( 'custom_col' ),
//							),
//						),
//						array(
//							'type'        => 'dropdown',
//							'heading'     => esc_html__( 'Percent width row on Mobile', 'bigshop' ),
//							'param_name'  => 'boostrap_xs_items',
//							'value'       => array(
//								esc_html__( '12 column - 12/12', 'bigshop' ) => '12',
//								esc_html__( '11 column - 11/12', 'bigshop' ) => '11',
//								esc_html__( '10 column - 10/12', 'bigshop' ) => '10',
//								esc_html__( '9 column - 9/12', 'bigshop' )   => '9',
//								esc_html__( '8 column - 8/12', 'bigshop' )   => '8',
//								esc_html__( '7 column - 7/12', 'bigshop' )   => '7',
//								esc_html__( '6 column - 6/12', 'bigshop' )   => '6',
//								esc_html__( '5 column - 5/12', 'bigshop' )   => '5',
//								esc_html__( '4 column - 4/12', 'bigshop' )   => '4',
//								esc_html__( '3 column - 3/12', 'bigshop' )   => '3',
//								esc_html__( '2 column - 2/12', 'bigshop' )   => '2',
//								esc_html__( '1 column - 1/12', 'bigshop' )   => '1',
//								esc_html__( '4 column 5 - 4/5', 'bigshop' )  => '45',
//								esc_html__( '3 column 5 - 3/5', 'bigshop' )  => '35',
//								esc_html__( '2 column 5 - 2/5', 'bigshop' )  => '25',
//								esc_html__( '1 column 5 - 1/5', 'bigshop' )  => '15',
//							),
//							'description' => esc_html__( '(Percent width row on screen resolution of device >=480  add < 768px )', 'bigshop' ),
//							'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
//							'std'         => '12',
//							'dependency'  => array(
//								'element' => 'content_width',
//								'value'   => array( 'custom_col' ),
//							),
//						),
//						array(
//							'type'        => 'dropdown',
//							'heading'     => esc_html__( 'Percent width row on Mobile', 'bigshop' ),
//							'param_name'  => 'boostrap_ts_items',
//							'value'       => array(
//								esc_html__( '12 column - 12/12', 'bigshop' ) => '12',
//								esc_html__( '11 column - 11/12', 'bigshop' ) => '11',
//								esc_html__( '10 column - 10/12', 'bigshop' ) => '10',
//								esc_html__( '9 column - 9/12', 'bigshop' )   => '9',
//								esc_html__( '8 column - 8/12', 'bigshop' )   => '8',
//								esc_html__( '7 column - 7/12', 'bigshop' )   => '7',
//								esc_html__( '6 column - 6/12', 'bigshop' )   => '6',
//								esc_html__( '5 column - 5/12', 'bigshop' )   => '5',
//								esc_html__( '4 column - 4/12', 'bigshop' )   => '4',
//								esc_html__( '3 column - 3/12', 'bigshop' )   => '3',
//								esc_html__( '2 column - 2/12', 'bigshop' )   => '2',
//								esc_html__( '1 column - 1/12', 'bigshop' )   => '1',
//								esc_html__( '4 column 5 - 4/5', 'bigshop' )  => '45',
//								esc_html__( '3 column 5 - 3/5', 'bigshop' )  => '35',
//								esc_html__( '2 column 5 - 2/5', 'bigshop' )  => '25',
//								esc_html__( '1 column 5 - 1/5', 'bigshop' )  => '15',
//							),
//							'description' => esc_html__( '(Percent width row on screen resolution of device < 480px)', 'bigshop' ),
//							'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
//							'std'         => '12',
//							'dependency'  => array(
//								'element' => 'content_width',
//								'value'   => array( 'custom_col' ),
//							),
//						),
//						array(
//							'param_name'  => 'number_width',
//							'heading'     => esc_html__( 'width', 'bigshop' ),
//							"description" => esc_html__( "you can width by px or %, ex: 100%", "bigshop" ),
//							'std'         => '50%',
//							'admin_label' => true,
//							'type'        => 'textfield',
//							'dependency'  => array(
//								'element' => 'content_width',
//								'value'   => array( 'custom_width' ),
//							),
//						),
//						'css' => array(
//							'type'       => 'css_editor',
//							'heading'    => esc_html__( 'Css', 'bigshop' ),
//							'param_name' => 'css',
//							'group'      => esc_html__( 'Design Options', 'bigshop' ),
//						),
//						array(
//							"type"        => "textfield",
//							"heading"     => esc_html__( "Extra class name", "bigshop" ),
//							"param_name"  => "el_class",
//							"description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "bigshop" ),
//						),
//						array(
//							'param_name'       => 'container_custom_id',
//							'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
//							'type'             => 'uniqid',
//							'edit_field_class' => 'hidden',
//						),
//					),
//				)
//			);

			/*Map New Newsletter*/
			vc_map(
				array(
					'name'        => esc_html__( 'Bigshop: Newsletter', 'bigshop' ),
					'base'        => 'bigshop_newsletter', // shortcode
					'category'    => esc_html__( 'Bigshop Elements', 'bigshop' ),
					'description' => esc_html__( 'Display a newsletter box.', 'bigshop' ),
					'params'      => array(
						array(
							'type'        => 'select_preview',
							'heading'     => esc_html__( 'Layout', 'bigshop' ),
							'value'       => array(
								'default' => array(
									'alt' => 'default',
									'img' => BIGSHOP_SHORTCODE_PREVIEW . 'newsletter/default.jpg',
								),
                                'layout02' => array(
									'alt' => 'Layout 02',
									'img' => BIGSHOP_SHORTCODE_PREVIEW . 'newsletter/layout02.jpg',
								),
							),
							'default'     => 'default',
							'admin_label' => true,
							'param_name'  => 'style',
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Title', 'bigshop' ),
							'param_name'  => 'title',
							'description' => esc_html__( 'The title of shortcode', 'bigshop' ),
							'admin_label' => true,
							'std'         => '',
						),
						array(
							'type'       => 'textarea',
							'heading'    => esc_html__( 'Sub title', 'bigshop' ),
							'param_name' => 'subtitle',
							'std'        => '',
                            
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "Placeholder text", 'bigshop' ),
							"param_name" => "placeholder_text",
							'std'        => 'Your email address',
						),
						array(
							"type"       => "textfield",
							"heading"    => esc_html__( "Button text", 'bigshop' ),
							"param_name" => "button_text",
							'std'        => 'SUBSCRIBE',
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Extra class name", "bigshop" ),
							"param_name"  => "el_class",
							"description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "bigshop" ),
						),
						array(
							'type'       => 'css_editor',
							'heading'    => esc_html__( 'Css', 'bigshop' ),
							'param_name' => 'css',
							'group'      => esc_html__( 'Design Options', 'bigshop' ),
						),
						array(
							'param_name'       => 'newsletter_custom_id',
							'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
							'type'             => 'uniqid',
							'edit_field_class' => 'hidden',
						),
					),
				)
			);

			/*Map New Custom menu*/

			$all_menu = array();
			$menus    = get_terms( 'nav_menu', array( 'hide_empty' => false ) );
			if ( $menus && count( $menus ) > 0 ) {
				foreach ( $menus as $m ) {
					$all_menu[ $m->name ] = $m->slug;
				}
			}
			vc_map(
				array(
					'name'        => esc_html__( 'Bigshop: Custom Menu', 'bigshop' ),
					'base'        => 'bigshop_custommenu', // shortcode
					'class'       => '',
					'category'    => esc_html__( 'Bigshop Elements', 'bigshop' ),
					'description' => esc_html__( 'Display a custom menu.', 'bigshop' ),
					'params'      => array(
						array(
							'type'        => 'select_preview',
							'heading'     => esc_html__( 'Layout', 'bigshop' ),
							'value'       => array(
								'default' => array(
									'alt' => 'Default',
									'img' => BIGSHOP_SHORTCODE_PREVIEW . 'custom_menu/default.jpg',
								),
							),
							'default'     => 'default',
							'admin_label' => true,
							'param_name'  => 'layout',
						),
						array(
							'type'        => 'textfield',
							'heading'     => esc_html__( 'Title', 'bigshop' ),
							'param_name'  => 'title',
							'description' => esc_html__( 'The title of shortcode', 'bigshop' ),
							'admin_label' => true,
							'std'         => '',
						),
						array(
							"type"        => "attach_image",
							"heading"     => esc_html__( "Image Banner", 'bigshop' ),
							"param_name"  => "menu_banner",
							"admin_label" => true,
							'dependency'  => array(
								'element' => 'layout',
								'value'   => array( 'layout1' ),
							),
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Menu', 'bigshop' ),
							'param_name'  => 'menu',
							'value'       => $all_menu,
							'admin_label' => true,
							'description' => esc_html__( 'Select menu to display.', 'bigshop' ),
						),
						array(
							'type'        => 'vc_link',
							'heading'     => esc_attr__( 'URL (Link)', 'bigshop' ),
							'param_name'  => 'link',
							'description' => esc_attr__( 'Add link.', 'bigshop' ),
							'dependency'  => array(
								'element' => 'layout',
								'value'   => array( 'layout1' ),
							),
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Extra class name", "bigshop" ),
							"param_name"  => "el_class",
							"description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "bigshop" ),
						),
						array(
							'type'       => 'css_editor',
							'heading'    => esc_html__( 'Css', 'bigshop' ),
							'param_name' => 'css',
							'group'      => esc_html__( 'Design Options', 'bigshop' ),
						),
						array(
							'param_name'       => 'custommenu_custom_id',
							'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
							'type'             => 'uniqid',
							'edit_field_class' => 'hidden',
						),
					),
				)
			);

			/*Map Brand*/

			vc_map(
				array(
					'name'                    => esc_html__( 'Bigshop: Brands', 'bigshop' ),
					'base'                    => 'bigshop_brands',
					'category'                => esc_html__( 'Bigshop Elements', 'bigshop' ),
					'description'             => esc_html__( 'Display  brands.', 'bigshop' ),
					'params'                  => array(
						array(
							'type'        => 'textfield',
							'admin_label' => true,
							'heading'     => esc_html__( 'Title', 'bigshop' ),
							'description' => esc_html__( 'Title of shortcode.', 'bigshop' ),
							'param_name'  => 'title',
						),
                        array(
                            'type'        => 'vc_link',
                            'heading'     => esc_attr__( 'Block URL (Link)', 'bigshop' ),
                            'param_name'  => 'block_link',
                        ),
                        array(
                            'type'          => 'param_group',
                            'param_name'    => 'brands',
                            'params' => array(
                                array(
                                    'type'          => 'textfield',
                                    'heading'       => esc_html__( 'Name', 'bigshop' ),
                                    'param_name'    => 'name',
                                    'admin_label' => true,
                                ),
                                array(
                                    'type'          => 'textfield',
                                    'heading'       => esc_html__( 'Link', 'bigshop' ),
                                    'param_name'    => 'link',
                                    'admin_label' => true,
                                ),
                                array(
                                    'type'          => 'attach_image',
                                    'heading'       => esc_html__( 'Logo', 'bigshop' ),
                                    'param_name'    => 'logo',
                                ),
                            ),
                            'group'       => esc_html__( 'Brands settings', 'bigshop' ),
                        ),
                        array(
                            'type'        => 'dropdown',
                            'heading'     => esc_html__( 'Items per row on Desktop', 'bigshop' ),
                            'param_name'  => 'boostrap_lg_items',
                            'value'       => array(
                                esc_html__( '1 item', 'bigshop' )  => '12',
                                esc_html__( '2 items', 'bigshop' ) => '6',
                                esc_html__( '3 items', 'bigshop' ) => '4',
                                esc_html__( '4 items', 'bigshop' ) => '3',
                                esc_html__( '5 items', 'bigshop' ) => '15',
                                esc_html__( '6 items', 'bigshop' ) => '2',
                            ),
                            'description' => esc_html__( '(Item per row on screen resolution of device >= 1200px and < 1500px )', 'bigshop' ),
                            'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
                            'std'         => '3',
                        ),
                        array(
                            'type'        => 'dropdown',
                            'heading'     => esc_html__( 'Items per row on landscape tablet', 'bigshop' ),
                            'param_name'  => 'boostrap_md_items',
                            'value'       => array(
                                esc_html__( '1 item', 'bigshop' )  => '12',
                                esc_html__( '2 items', 'bigshop' ) => '6',
                                esc_html__( '3 items', 'bigshop' ) => '4',
                                esc_html__( '4 items', 'bigshop' ) => '3',
                                esc_html__( '5 items', 'bigshop' ) => '15',
                                esc_html__( '6 items', 'bigshop' ) => '2',
                            ),
                            'description' => esc_html__( '(Item per row on screen resolution of device >=992px and < 1200px )', 'bigshop' ),
                            'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
                            'std'         => '3',
                        ),
                        array(
                            'type'        => 'dropdown',
                            'heading'     => esc_html__( 'Items per row on portrait tablet', 'bigshop' ),
                            'param_name'  => 'boostrap_sm_items',
                            'value'       => array(
                                esc_html__( '1 item', 'bigshop' )  => '12',
                                esc_html__( '2 items', 'bigshop' ) => '6',
                                esc_html__( '3 items', 'bigshop' ) => '4',
                                esc_html__( '4 items', 'bigshop' ) => '3',
                                esc_html__( '5 items', 'bigshop' ) => '15',
                                esc_html__( '6 items', 'bigshop' ) => '2',
                            ),
                            'description' => esc_html__( '(Item per row on screen resolution of device >=768px and < 992px )', 'bigshop' ),
                            'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
                            'std'         => '4',
                        ),
                        array(
                            'type'        => 'dropdown',
                            'heading'     => esc_html__( 'Items per row on Mobile', 'bigshop' ),
                            'param_name'  => 'boostrap_xs_items',
                            'value'       => array(
                                esc_html__( '1 item', 'bigshop' )  => '12',
                                esc_html__( '2 items', 'bigshop' ) => '6',
                                esc_html__( '3 items', 'bigshop' ) => '4',
                                esc_html__( '4 items', 'bigshop' ) => '3',
                                esc_html__( '5 items', 'bigshop' ) => '15',
                                esc_html__( '6 items', 'bigshop' ) => '2',
                            ),
                            'description' => esc_html__( '(Item per row on screen resolution of device >=480  add < 768px )', 'bigshop' ),
                            'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
                            'std'         => '6',
                        ),
                        array(
                            'type'        => 'dropdown',
                            'heading'     => esc_html__( 'Items per row on Mobile', 'bigshop' ),
                            'param_name'  => 'boostrap_ts_items',
                            'value'       => array(
                                esc_html__( '1 item', 'bigshop' )  => '12',
                                esc_html__( '2 items', 'bigshop' ) => '6',
                                esc_html__( '3 items', 'bigshop' ) => '4',
                                esc_html__( '4 items', 'bigshop' ) => '3',
                                esc_html__( '5 items', 'bigshop' ) => '15',
                                esc_html__( '6 items', 'bigshop' ) => '2',
                            ),
                            'description' => esc_html__( '(Item per row on screen resolution of device < 480px)', 'bigshop' ),
                            'group'       => esc_html__( 'Boostrap settings', 'bigshop' ),
                            'std'         => '12',
                        ),
						array(
							'heading'     => esc_html__( 'Extra Class Name', 'bigshop' ),
							'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'bigshop' ),
							'type'        => 'textfield',
							'param_name'  => 'el_class',
						),
						array(
							'type'       => 'css_editor',
							'heading'    => esc_html__( 'Css', 'bigshop' ),
							'param_name' => 'css',
							'group'      => esc_html__( 'Design Options', 'bigshop' ),
						),
						array(
							'param_name'       => 'brands_custom_id',
							'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
							'type'             => 'uniqid',
							'edit_field_class' => 'hidden',
						),
					),
				)
			);

			/* Map Google Map */
			vc_map(
				array(
					'name'        => esc_html__( 'Bigshop: Google Map', 'bigshop' ),
					'base'        => 'bigshop_googlemap', // shortcode
					'class'       => '',
					'category'    => esc_html__( 'Bigshop Elements', 'bigshop' ),
					'description' => esc_html__( 'Display a google map.', 'bigshop' ),
					'params'      => array(
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Title", 'bigshop' ),
							"param_name"  => "title",
							'admin_label' => true,
							"description" => esc_html__( "title.", 'bigshop' ),
							'std'         => 'Stores',
						),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__( "Sub Title", 'bigshop' ),
                            "param_name"  => "subtitle",
                            'admin_label' => true,
                            "description" => esc_html__( "Sub title.", 'bigshop' ),
                            'std'         => 'Find your closest Big.Shop',
                        ),

						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Map Height", 'bigshop' ),
							"param_name"  => "map_height",
							'admin_label' => true,
							'std'         => '400',
						),
						array(
							'type'       => 'dropdown',
							'heading'    => esc_html__( 'Maps type', 'bigshop' ),
							'param_name' => 'map_type',
							'value'      => array(
								esc_html__( 'ROADMAP', 'bigshop' )   => 'ROADMAP',
								esc_html__( 'SATELLITE', 'bigshop' ) => 'SATELLITE',
								esc_html__( 'HYBRID', 'bigshop' )    => 'HYBRID',
								esc_html__( 'TERRAIN', 'bigshop' )   => 'TERRAIN',
							),
							'std'        => 'ROADMAP',
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Longitude", 'bigshop' ),
							"param_name"  => "longitude",
							'admin_label' => true,
							"description" => esc_html__( "longitude.", 'bigshop' ),
							'std'         => '105.800286',
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Latitude", 'bigshop' ),
							"param_name"  => "latitude",
							'admin_label' => true,
							"description" => esc_html__( "latitude.", 'bigshop' ),
							'std'         => '21.587001',
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Zoom", 'bigshop' ),
							"param_name"  => "zoom",
							'admin_label' => true,
							"description" => esc_html__( "zoom.", 'bigshop' ),
							'std'         => '14',
						),
						array(
							"type"        => "textfield",
							"heading"     => esc_html__( "Extra class name", 'bigshop' ),
							"param_name"  => "el_class",
							"description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "bigshop" ),
						),
						array(
							'type'       => 'css_editor',
							'heading'    => esc_html__( 'Css', 'bigshop' ),
							'param_name' => 'css',
							'group'      => esc_html__( 'Design Options', 'bigshop' ),
						),
						array(
							'param_name'       => 'googlemap_custom_id',
							'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
							'type'             => 'uniqid',
							'edit_field_class' => 'hidden',
						),
					),
				)
			);
            /*Store info */
            vc_map(
                array(
                    'name'        => esc_html__( 'Bigshop: Store Info', 'bigshop' ),
                    'base'        => 'bigshop_storeinfo', // shortcode
                    'class'       => '',
                    'category'    => esc_html__( 'Bigshop Elements', 'bigshop' ),
                    'description' => esc_html__( 'Display a store info.', 'bigshop' ),
                    'params'      => array(
                        array(
                            'param_name'  => 'layout',
                            'heading'     => esc_html__( 'Layout', 'bigshop' ),
                            'type'        => 'dropdown',
                            'value'       => array(
                                esc_html__( 'Default', 'bigshop' ) => 'default',
                                esc_html__('Inline', 'bigshop')    => 'inline',
                            ),
                            'admin_label' => true,
                            'std'         => 'default',
                        ),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__( "Title", 'bigshop' ),
                            "param_name"  => "title",
                            'admin_label' => true,
                            "description" => esc_html__( "title.", 'bigshop' ),
                            'std'         => 'Contact us',
                        ),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__( "Address", 'bigshop' ),
                            "param_name"  => "address",
                            'admin_label' => true,
                            'std'         => "198 Rue Jeanne d'Arc,1988 Nancy, France",
                        ),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__( "Phone", 'bigshop' ),
                            "param_name"  => "phone",
                            'admin_label' => true,
                            'std'         => "+(00) 123 456 789",
                        ),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__( "Email", 'bigshop' ),
                            "param_name"  => "email",
                            'admin_label' => true,
                            'std'         => "Support@bigshop.com",
                        ),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__( "Extra class name", 'bigshop' ),
                            "param_name"  => "el_class",
                            "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "bigshop" ),
                        ),
                        array(
                            'type'       => 'css_editor',
                            'heading'    => esc_html__( 'Css', 'bigshop' ),
                            'param_name' => 'css',
                            'group'      => esc_html__( 'Design Options', 'bigshop' ),
                        ),
                        array(
                            'param_name'       => 'storeinfo_custom_id',
                            'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
                            'type'             => 'uniqid',
                            'edit_field_class' => 'hidden',
                        ),
                    ),
                )
            );
            /* Map New Social */
            $socials     = array();
            $all_socials = bigshop_get_option( 'user_all_social' );
            $i           = 1;
            if ( $all_socials ) {
                foreach ( $all_socials as $key => $value )
                    $socials[ $value[ 'title_social' ] ] = $key;
            }
            vc_map(
                array(
                    'name'        => esc_html__( 'Bigshop: Socials', 'bigshop' ),
                    'base'        => 'bigshop_socials', // shortcode
                    'class'       => '',
                    'category'    => esc_html__( 'Bigshop Elements', 'bigshop' ),
                    'description' => esc_html__( 'Display a social list.', 'bigshop' ),
                    'params'      => array(
                        array(
                            'type'        => 'dropdown',
                            'heading'     => esc_html__( 'Select style', 'bigshop' ),
                            'value'      => array(
                                esc_html__( 'Default', 'bigshop' )   => 'default',
                            ),
                            'default'     => 'default',
                            'admin_label' => true,
                            'param_name'  => 'style',
                        ),
                        array(
                            'type'       => 'textfield',
                            'heading'    => esc_html__( 'Title', 'bigshop' ),
                            'param_name' => 'title',
                        ),
                        array(
                            'param_name' => 'text_align',
                            'heading'    => esc_html__( 'Text align', 'bigshop' ),
                            'type'       => 'dropdown',
                            'value'      => array(
                                esc_html__( 'Left', 'bigshop' )   => 'text-left',
                                esc_html__( 'Right', 'bigshop' )  => 'text-right',
                                esc_html__( 'Center', 'bigshop' ) => 'text-center',
                            ),
                            'std'        => 'text-left',
                        ),
                        array(
                            'type'       => 'checkbox',
                            'heading'    => esc_html__( 'Display on', 'bigshop' ),
                            'param_name' => 'use_socials',
                            'class'      => 'checkbox-display-block',
                            'value'      => $socials,
                        ),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__( "Extra class name", "bigshop" ),
                            "param_name"  => "el_class",
                            "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "bigshop" ),
                        ),
                        array(
                            'type'       => 'css_editor',
                            'heading'    => esc_html__( 'Css', 'bigshop' ),
                            'param_name' => 'css',
                            'group'      => esc_html__( 'Design Options', 'bigshop' ),
                        ),
                        array(
                            'param_name'       => 'socials_custom_id',
                            'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
                            'type'             => 'uniqid',
                            'edit_field_class' => 'hidden',
                        ),
                    ),
                )
            );
            /*Section title */
            vc_map(
                array(
                    'name'        => esc_html__( 'Bigshop: Title', 'bigshop' ),
                    'base'        => 'bigshop_title', // shortcode
                    'class'       => '',
                    'category'    => esc_html__( 'Bigshop Elements', 'bigshop' ),
                    'description' => esc_html__( 'Display a store info.', 'bigshop' ),
                    'params'      => array(
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__( "Title", 'bigshop' ),
                            "param_name"  => "title",
                            'admin_label' => true,
                            "description" => esc_html__( "title.", 'bigshop' ),
                            'std'         => '',
                        ),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__( "Subtitle", 'bigshop' ),
                            "param_name"  => "subtitle",
                            'admin_label' => true,
                        ),

                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__( "Extra class name", 'bigshop' ),
                            "param_name"  => "el_class",
                            "description" => esc_html__( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "bigshop" ),
                        ),
                        array(
                            'type'       => 'css_editor',
                            'heading'    => esc_html__( 'Css', 'bigshop' ),
                            'param_name' => 'css',
                            'group'      => esc_html__( 'Design Options', 'bigshop' ),
                        ),
                        array(
                            'param_name'       => 'title_custom_id',
                            'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
                            'type'             => 'uniqid',
                            'edit_field_class' => 'hidden',
                        ),
                    ),
                )
            );

            /* Map Team Member */
            vc_map(
                array(
                    'name'                    => esc_html__( 'Bigshop: Team Members', 'bigshop' ),
                    'base'                    => 'bigshop_team_member',
                    'category'                => esc_html__( 'Bigshop Elements', 'bigshop' ),
                    'params'                  => array(
                        array(
                            'param_name'  => 'style',
                            'heading'     => esc_html__( 'Layout', 'bigshop' ),
                            'type'        => 'dropdown',
                            'value'       => array(
                                esc_html__( 'Default', 'bigshop' ) => 'default',
                            ),
                            'admin_label' => true,
                            'std'         => 'default',
                        ),
                        array(
                            'type'          => 'param_group',
                            'param_name'    => 'team_members',
                            'params' => array(
                                array(
                                    'type'          => 'attach_image',
                                    'heading'       => esc_html__( 'Avatar', 'bigshop' ),
                                    'param_name'    => 'avatar',
                                ),
                                array(
                                    'type'          => 'textfield',
                                    'heading'       => esc_html__( 'Member name', 'bigshop' ),
                                    'param_name'    => 'name',
                                ),
                                array(
                                    'type'          => 'textfield',
                                    'heading'       => esc_html__( 'Position', 'bigshop' ),
                                    'param_name'    => 'position',
                                )
                            )
                        ),
                        /* Owl */
                        array(
                            'type'        => 'dropdown',
                            'value'       => array(
                                esc_html__('Yes', 'bigshop') => 'true',
                                esc_html__('No', 'bigshop')  => 'false'
                            ),
                            'std'         => 'false',
                            'heading'     => esc_html__('AutoPlay', 'bigshop'),
                            'param_name'  => 'autoplay',
                            'group'       => esc_html__('Carousel settings', 'bigshop'),
                            'admin_label' => false,
                        ),
                        array(
                            'type'        => 'dropdown',
                            'value'       => array(
                                esc_html__('No', 'bigshop')  => 'false',
                                esc_html__('Yes', 'bigshop') => 'true'
                            ),
                            'std'         => 'false',
                            'heading'     => esc_html__('Navigation', 'bigshop'),
                            'param_name'  => 'navigation',
                            'description' => esc_html__("Show buton 'next' and 'prev' buttons.", 'bigshop'),
                            'group'       => esc_html__('Carousel settings', 'bigshop'),
                            'admin_label' => false,
                        ),
                        array(
                            'type'        => 'dropdown',
                            'heading'     => esc_html__( 'Navigation position', 'bigshop' ),
                            'param_name'  => 'owl_navigation_position',
                            'value'       => array(
                                esc_html__('Default', 'bigshop')      => '',
                                esc_html__('Center', 'bigshop')       => 'nav-center',
                                esc_html__('Top Left', 'bigshop')     => 'nav-top-left',
                                esc_html__('Top right', 'bigshop')    => 'nav-top-right',
                                esc_html__('Bottom right', 'bigshop') => 'nav-bottom-right',
                            ),
                            'std'=>'',
                            'group'       => esc_html__( 'Carousel settings', 'bigshop' ),
                            "dependency"  => array("element" => "navigation", "value" => array( 'true' )),
                        ),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__("Postion Top", 'bigshop'),
                            "param_name"  => "owl_navigation_position_top",
                            "std"       => -60,
                            'group'       => esc_html__( 'Carousel settings', 'bigshop' ),
                            'admin_label' => false,
                            "dependency"  => array(
                                "element" => "owl_navigation_position", "value" => array('nav-top-left','nav-top-right')
                            ),
                        ),
                        array(
                            'type'        => 'dropdown',
                            'value'       => array(
                                esc_html__('Yes', 'bigshop') => 'true',
                                esc_html__('No', 'bigshop')  => 'false'
                            ),
                            'std'         => 'false',
                            'heading'     => esc_html__('Loop', 'bigshop'),
                            'param_name'  => 'loop',
                            'description' => esc_html__("Inifnity loop. Duplicate last and first items to get loop illusion.", 'bigshop'),
                            'group'       => esc_html__('Carousel settings', 'bigshop'),
                            'admin_label' => false,
                        ),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__("Slide Speed", 'bigshop'),
                            "param_name"  => "slidespeed",
                            "value"       => "200",
                            "description" => esc_html__('Slide speed in milliseconds', 'bigshop'),
                            'group'       => esc_html__('Carousel settings', 'bigshop'),
                            'admin_label' => false,
                        ),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__("Margin", 'bigshop'),
                            "param_name"  => "margin",
                            "value"       => "30",
                            "description" => esc_html__('Distance( or space) between 2 item', 'bigshop'),
                            'group'       => esc_html__('Carousel settings', 'bigshop'),
                            'admin_label' => false,
                        ),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__("The items on desktop (Screen resolution of device >= 1200px )", 'bigshop'),
                            "param_name"  => "lg_items",
                            "value"       => "4",
                            'group'       => esc_html__('Carousel settings', 'bigshop'),
                            'admin_label' => false,
                        ),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__("The items on desktop (Screen resolution of device >= 992px < 1200px )", 'bigshop'),
                            "param_name"  => "md_items",
                            "value"       => "3",
                            'group'       => esc_html__('Carousel settings', 'bigshop'),
                            'admin_label' => false,
                        ),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__("The items on tablet (Screen resolution of device >=768px and < 992px )", 'bigshop'),
                            "param_name"  => "sm_items",
                            "value"       => "2",
                            'group'       => esc_html__('Carousel settings', 'bigshop'),
                            'admin_label' => false,
                        ),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__("The items on mobile landscape(Screen resolution of device >=480px and < 768px)", 'bigshop'),
                            "param_name"  => "xs_items",
                            "value"       => "2",
                            'group'       => esc_html__('Carousel settings', 'bigshop'),
                            'admin_label' => false,
                        ),
                        array(
                            "type"        => "textfield",
                            "heading"     => esc_html__("The items on mobile (Screen resolution of device < 480px)", 'bigshop'),
                            "param_name"  => "ts_items",
                            "value"       => "1",
                            'group'       => esc_html__('Carousel settings', 'bigshop'),
                            'admin_label' => false,
                        ),
                        array(
                            'heading'     => esc_html__( 'Extra Class Name', 'bigshop' ),
                            'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'bigshop' ),
                            'type'        => 'textfield',
                            'param_name'  => 'el_class',
                        ),
                        array(
                            'type'       => 'css_editor',
                            'heading'    => esc_html__( 'Css', 'bigshop' ),
                            'param_name' => 'css',
                            'group'      => esc_html__( 'Design Options', 'bigshop' ),
                        ),
                        array(
                            'param_name'       => 'team_member_custom_id',
                            'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
                            'type'             => 'uniqid',
                            'edit_field_class' => 'hidden',
                        )
                    )
                )
            );

            /* Map  Progress Bar */
            vc_map(
                array(
                    'name'                    => esc_html__( 'Bigshop:  Progress Bar', 'bigshop' ),
                    'base'                    => 'bigshop_progress_bar',
                    'category'                => esc_html__( 'Bigshop Elements', 'bigshop' ),
                    'params'                  => array(
                        array(
                            'param_name'  => 'style',
                            'heading'     => esc_html__( 'Layout', 'bigshop' ),
                            'type'        => 'dropdown',
                            'value'       => array(
                                esc_html__( 'Default', 'bigshop' ) => 'default',
                            ),
                            'admin_label' => true,
                            'std'         => 'default',
                        ),
                        array(
                            'type'          => 'param_group',
                            'param_name'    => 'progress_bars',
                            'params' => array(
                                array(
                                    'type'          => 'textfield',
                                    'heading'       => esc_html__( 'Label', 'bigshop' ),
                                    'param_name'    => 'label',
                                ),
                                array(
                                    'type'          => 'textfield',
                                    'heading'       => esc_html__( 'Value', 'bigshop' ),
                                    'param_name'    => 'value',
                                ),
                                array(
                                    "type" => "colorpicker",
                                    "heading" => __( "Bar Background", "bigshop" ),
                                    "param_name" => "bar_background",
                                    "value" => '#f1b400',
                                )
                            )
                        ),
                        array(
                            'type'       => 'css_editor',
                            'heading'    => esc_html__( 'Css', 'bigshop' ),
                            'param_name' => 'css',
                            'group'      => esc_html__( 'Design Options', 'bigshop' ),
                        ),
                        array(
                            'param_name'       => 'progress_bar_custom_id',
                            'heading'          => esc_html__( 'Hidden ID', 'bigshop' ),
                            'type'             => 'uniqid',
                            'edit_field_class' => 'hidden',
                        )
                    )
                )
            );

		}
	}

	new Bigshop_Visual_Composer();
}
VcShortcodeAutoloader::getInstance()->includeClass( 'WPBakeryShortCode_VC_Tta_Accordion' );
class WPBakeryShortCode_Bigshop_Tabs extends WPBakeryShortCode_VC_Tta_Accordion{};
class WPBakeryShortCode_Bigshop_Container extends WPBakeryShortCodesContainer{};