<?php
/* ==================== HOOK SHOP ==================== */

/* Remove Div cover content shop */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

/* Custom shop control */

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

add_action( 'woocommerce_before_shop_loop', 'bigshop_top_control', 40 );

add_action( 'bigshop_woocommerce_before_loop_start', 'bigshop_shop_banners', 1 );

remove_action('woocommerce_after_shop_loop_item_title','woocommerce_template_loop_rating',5);
add_action('bigshop_before_shop_loop_item_group_button','woocommerce_template_loop_rating',5);
add_action('bigshop_before_shop_loop_item_group_button','woocommerce_template_loop_price',6);


/* Remove CSS */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/* Custom Product Thumbnail */
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'bigshop_template_loop_product_thumbnail', 10 );

/*Custom product per page*/
add_filter( 'loop_shop_per_page', 'bigshop_loop_shop_per_page', 20 );
add_filter( 'woof_products_query', 'bigshop_woof_products_query', 20 );

/* ==================== CART PAGE ==================== */

remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
add_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display', 30 );

/*Remove woocommerce_template_loop_product_link_open */
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

/*Custom product name*/
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title' );
add_action( 'woocommerce_shop_loop_item_title', 'bigshop_template_loop_product_title', 10 );

/* Add countdown in product */
add_action( 'bigshop_display_product_countdown_in_loop', 'bigshop_display_product_countdown_in_loop', 1 );

/* Stock status */
add_action( 'bigshop_woo_get_stock_status', 'bigshop_woo_get_stock_status', 1 );

/* Custom flash icon */
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'bigshop_group_flash', 5 );



/* ==================== HOOK PRODUCT ==================== */

/* WC_Vendors */
if ( class_exists( 'WC_Vendors' ) && class_exists( 'WCV_Vendor_Shop' ) ) {
	// Add sold by to product loop before add to cart
	if ( WC_Vendors::$pv_options->get_option( 'sold_by' ) ) {
		remove_action( 'woocommerce_after_shop_loop_item', array( 'WCV_Vendor_Shop', 'template_loop_sold_by' ), 9 );
		add_action( 'woocommerce_shop_loop_item_title', array( 'WCV_Vendor_Shop', 'template_loop_sold_by' ), 1 );
	}
}



if ( !function_exists( 'bigshop_loop_shop_per_page' ) ) {
	function bigshop_loop_shop_per_page()
	{
		$bigshop_woo_products_perpage = bigshop_get_option( 'product_per_page', '9' );

		return $bigshop_woo_products_perpage;
	}
}

if ( !function_exists( 'bigshop_woof_products_query' ) ) {
	function bigshop_woof_products_query( $wr )
	{
		$bigshop_woo_products_perpage = bigshop_get_option( 'product_per_page', '9' );
		$wr[ 'posts_per_page' ]       = $bigshop_woo_products_perpage;

		return $wr;
	}
}


/* CUSTOM PRODUCT TITLE */
if ( !function_exists( 'bigshop_template_loop_product_title' ) ) {
	function bigshop_template_loop_product_title()
	{
		$title_class = array( 'product-name product_title' );
		?>
        <h5 class="<?php echo esc_attr( implode( ' ', $title_class ) ); ?>">
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </h5>
		<?php
	}
}

/* CUSTOM PAGINATION */
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
add_action( 'woocommerce_after_shop_loop', 'bigshop_custom_pagination', 10 );

if ( !function_exists( 'bigshop_custom_pagination' ) ) {
	function bigshop_custom_pagination()
	{
		global $wp_query;

		if ( $wp_query->max_num_pages <= 1 ) {
			return;
		}
		?>
        <nav class="woocommerce-pagination pagination">
			<?php
			echo paginate_links( apply_filters( 'woocommerce_pagination_args', array(
						'base'      => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
						'format'    => '',
						'add_args'  => false,
						'current'   => max( 1, get_query_var( 'paged' ) ),
						'total'     => $wp_query->max_num_pages,
						'prev_text' => esc_html__( 'Prev', 'bigshop' ),
						'next_text' => esc_html__( 'Next', 'bigshop' ),
						'type'      => 'plain',
						'end_size'  => 1,
						'mid_size'  => 1,
					)
				)
			);
			?>
        </nav>
		<?php
	}
}


/* CUSTOM PRODUCT THUMBNAIL */
if ( !function_exists( 'bigshop_template_loop_product_thumbnail' ) ) {

	function bigshop_template_loop_product_thumbnail(){
        global $product;
        $bigshop_enable_lazy = bigshop_get_option('bigshop_enable_lazy','1');
        $bigshop_using_two_image = bigshop_get_option('bigshop_using_two_image','1');
        $thumb_inner_class = array('thumb-inner');
        /*GET SIZE IMAGE SETTING*/
        $w = 400;
        $h = 400;
        $crop = true;
        $size = wc_get_image_size( 'shop_catalog' );
        if( $size ){
            $w = $size['width'];
            $h = $size['height'];
            if( !$size['crop'] ){
                $crop = false;
            }
        }
        $w = apply_filters( 'bigshop_shop_pruduct_thumb_width', $w);
        $h = apply_filters( 'bigshop_shop_pruduct_thumb_height', $h);
        $back_image = '';
        if( $bigshop_using_two_image =="yes"){
            $attachment_ids = $product->get_gallery_image_ids();
            if( $attachment_ids && is_array( $attachment_ids ) ){
                $image_back_thumb = bigshop_resize_image( $attachment_ids[0], null, $w, $h, $crop, true, false );
                $back_image = '<img width="'.$image_back_thumb['width'].'" height="'.$image_back_thumb['height'].'" class="attachment-product-thumbnail" src="'.$image_back_thumb['url'].'" alt="" />';
            }
            if( $back_image ){
                $thumb_inner_class[] = 'has-back-image';
            }
        }
        ob_start();
        ?>
        <div class="<?php echo esc_attr( implode(' ', $thumb_inner_class) );?>">
            <a class="thumb-link" href="<?php the_permalink();?>">
                <?php
                    $image_thumb = bigshop_resize_image( get_post_thumbnail_id($product->get_id()), null, $w, $h, $crop, true, false );
                ?>
                <?php if( $bigshop_enable_lazy == '1'):?>
                    <img width="<?php echo esc_attr( $image_thumb['width'] ); ?>" height="<?php echo esc_attr( $image_thumb['height'] ); ?>" class="attachment-post-thumbnail wp-post-image lazy owl-lazy" src="<?php echo trailingslashit ( get_template_directory_uri() ). '/assets/images/1x1.jpg'?>" data-src="<?php echo esc_attr( $image_thumb['url'] ) ?>" data-original="<?php echo esc_attr( $image_thumb['url'] ) ?>" alt="" />
                <?php else:?>
                    <img width="<?php echo esc_attr( $image_thumb['width'] ); ?>" height="<?php echo esc_attr( $image_thumb['height'] ); ?>" class="attachment-post-thumbnail wp-post-image" src="<?php echo esc_attr( $image_thumb['url'] ) ?>"   alt="" />
                <?php endif;?>
            </a>
        </div>
        <?php
        echo ob_get_clean();
	}
}


/* TOP CONTROL */
if ( !function_exists( 'bigshop_top_control' ) ) {
	function bigshop_top_control()
	{
		?>
        <div class="shop-top-control clear-list">
			<?php
			woocommerce_result_count();
			woocommerce_catalog_ordering();
			bigshop_shop_view_more();

			?>
        </div>
		<?php
	}
}
/* SET VIEW MORE */
if ( isset($_POST["shop_display_mode"])) {
	session_start();
	$_SESSION[ 'shop_display_mode' ] = $_POST["shop_display_mode"];
}
/* VIEW MORE */
if ( !function_exists( 'bigshop_shop_view_more' ) ) {
	function bigshop_shop_view_more()
	{
		$shop_display_mode = bigshop_get_option( 'shop_page_layout', 'grid' );
		if ( isset( $_SESSION[ 'shop_display_mode' ] ) ) {
			$shop_display_mode = $_SESSION[ 'shop_display_mode' ];
		}
		?>
        <div class="grid-view-mode">
            <span class="title"><?php esc_html_e('View as','bigshop');?></span>
            <a data-mode="grid" class="modes-mode mode-grid display-mode <?php if ( $shop_display_mode == "grid" ): ?>active<?php endif; ?>" href="javascript:void(0)">
                <i class="fa fa-th" aria-hidden="true"></i>

            </a>
            <a data-mode="list"
               class="modes-mode mode-list display-mode <?php if ( $shop_display_mode == "list" ): ?>active<?php endif; ?>" href="javascript:void(0)">
                <i class="fa fa-th-list" aria-hidden="true"></i>
            </a>
        </div>
		<?php
	}
}

/* SHOP BANNER */
if ( !function_exists( 'bigshop_shop_banners' ) ) {
	function bigshop_shop_banners()
	{

		$enable_shop_banner = bigshop_get_option( 'enable_shop_banner' );
		$woo_shop_banner    = bigshop_get_option( 'woo_shop_banner' );
		$banner_class       = array( 'banner-shop' );

		if ( $enable_shop_banner == 1 && !is_single() ) :
			$attachment = wp_get_attachment_image_src( $woo_shop_banner, 'full' ); ?>
            <div class="<?php echo esc_attr( implode( ' ', $banner_class ) ); ?>">
                <img src="<?php echo esc_url( $attachment[ 0 ] ); ?>" alt="">
            </div>
		<?php else :
			return;
		endif;
	}
}

/* QUICK VIEW */
if ( class_exists( 'YITH_WCQV_Frontend' ) ) {
	// Class frontend
	$enable           = get_option( 'yith-wcqv-enable' ) == 'yes' ? true : false;
	$enable_on_mobile = get_option( 'yith-wcqv-enable-mobile' ) == 'yes' ? true : false;
	// Class frontend
	if ( ( !wp_is_mobile() && $enable ) || ( wp_is_mobile() && $enable_on_mobile && $enable ) ) {
		remove_action( 'woocommerce_after_shop_loop_item', array( YITH_WCQV_Frontend::get_instance(), 'yith_add_quick_view_button' ), 15 );
		remove_action( 'yith_wcwl_table_after_product_name', array( YITH_WCQV_Frontend::get_instance(), 'yith_add_quick_view_button' ), 15,0 );
		add_action( 'bigshop_function_shop_loop_item_quickview', array( YITH_WCQV_Frontend::get_instance(), 'yith_add_quick_view_button' ), 5 );
	}
}

/* WISH LIST */
if ( class_exists( 'YITH_WCWL' ) && get_option( 'yith_wcwl_enabled' ) == 'yes' ) {
	if ( !function_exists( 'bigshop_wc_loop_product_wishlist_btn' ) ) {
		function bigshop_wc_loop_product_wishlist_btn()
		{
			if ( shortcode_exists( 'yith_wcwl_add_to_wishlist' ) ) {
				echo do_shortcode( '[yith_wcwl_add_to_wishlist product_id="' . get_the_ID() . '"]' );
			}
		}
	}
	add_action( 'bigshop_function_shop_loop_item_wishlist', 'bigshop_wc_loop_product_wishlist_btn', 1 );
}

/* COMPARE */
if ( class_exists( 'YITH_Woocompare' ) && get_option( 'yith_woocompare_compare_button_in_products_list' ) == 'yes' ) {
	global $yith_woocompare;
	$is_ajax = ( defined( 'DOING_AJAX' ) && DOING_AJAX );
	if ( $yith_woocompare->is_frontend() || $is_ajax ) {
		if ( $is_ajax ) {
			if ( !class_exists( 'YITH_Woocompare_Frontend' ) ) {
				if ( file_exists( YITH_WOOCOMPARE_DIR . 'includes/class.yith-woocompare-frontend.php' ) ) {
					require_once( YITH_WOOCOMPARE_DIR . 'includes/class.yith-woocompare-frontend.php' );
				}
			}
			$yith_woocompare->obj = new YITH_Woocompare_Frontend();
		}
		/* Remove button */
		remove_action( 'woocommerce_after_shop_loop_item', array( $yith_woocompare->obj, 'add_compare_link' ), 20 );
		/* Add compare button */
		if ( !function_exists( 'bigshop_wc_loop_product_compare_btn' ) ) {
			function bigshop_wc_loop_product_compare_btn()
			{
				if ( shortcode_exists( 'yith_compare_button' ) ) {
					echo do_shortcode( '[yith_compare_button product_id="' . get_the_ID() . '"]' );
				} // End if ( shortcode_exists( 'yith_compare_button' ) )
				else {
					if ( class_exists( 'YITH_Woocompare_Frontend' ) ) {
						$YITH_Woocompare_Frontend = new YITH_Woocompare_Frontend();
						echo do_shortcode( '[yith_compare_button product_id="' . get_the_ID() . '"]' );
					}
				}
			}
		}
		add_action( 'bigshop_function_shop_loop_item_compare', 'bigshop_wc_loop_product_compare_btn', 1 );
	}
}
/* Compare in single product  */
if( class_exists('YITH_Woocompare') && get_option('yith_woocompare_compare_button_in_product_page') == 'yes' ){
    global $yith_woocompare;
    $is_ajax = ( defined( 'DOING_AJAX' ) && DOING_AJAX );
    if( $yith_woocompare->is_frontend() || $is_ajax ) {
        if( $is_ajax ){
            if( !class_exists( 'YITH_Woocompare_Frontend' ) ){
                $file_name = YITH_WOOCOMPARE_DIR . 'includes/class.yith-woocompare-frontend.php';
                if( file_exists( $file_name ) ){
                    require_once( $file_name );
                }
            }
            $yith_woocompare->obj = new YITH_Woocompare_Frontend();
        }
        remove_action( 'woocommerce_after_shop_loop_item', array( $yith_woocompare->obj, 'add_compare_link' ), 20 );
        remove_action( 'woocommerce_single_product_summary', array( $yith_woocompare->obj, 'add_compare_link' ), 35 );
        add_action( 'woocommerce_after_add_to_cart_button', array( $yith_woocompare->obj, 'add_compare_link' ), 5 );
    }
}

/* Wislist in single product */
add_filter('yith_wcwl_positions','bigshop_single_product_wislist_button_positions',99,1);
if( !function_exists('bigshop_single_product_wislist_button_positions')){
    function bigshop_single_product_wislist_button_positions( $positions ){


        if( isset( $positions['add-to-cart']['hook'] )){
            $positions['add-to-cart']['hook'] = 'woocommerce_after_add_to_cart_button';
        }
        if( isset( $positions['add-to-cart']['priority'] )){
            $positions['add-to-cart']['priority'] = 10;
        }

        return $positions;
    }
}

/* GROUP NEW FLASH */
if ( !function_exists( 'bigshop_group_flash' ) ) {
	function bigshop_group_flash()
	{
		global $product;
		?>
        <div class="flash">
			<?php
			woocommerce_show_product_loop_sale_flash();
			bigshop_show_product_loop_new_flash();
			?>
        </div>
		<?php
	}
}

if ( !function_exists( 'bigshop_show_product_loop_new_flash' ) ) {
	/**
	 * Get the sale flash for the loop.
	 *
	 * @subpackage    Loop
	 */
	function bigshop_show_product_loop_new_flash()
	{
		wc_get_template( 'loop/new-flash.php' );
	}
}


if ( !function_exists( 'bigshop_get_percent_discount' ) ) {
	function bigshop_get_percent_discount()
	{
		global $product;
		$percent = '';
		if ( $product->is_on_sale() ) {
			if ( $product->is_type( 'variable' ) ) {
				$available_variations = $product->get_available_variations();
				$maximumper           = 0;
				$minimumper           = 0;
				$percentage           = 0;

				for ( $i = 0; $i < count( $available_variations ); ++$i ) {
					$variation_id = $available_variations[ $i ][ 'variation_id' ];

					$variable_product1 = new WC_Product_Variation( $variation_id );
					$regular_price     = $variable_product1->get_regular_price();
					$sales_price       = $variable_product1->get_sale_price();
					if ( $regular_price > 0 && $sales_price > 0 ) {
						$percentage = round( ( ( ( $regular_price - $sales_price ) / $regular_price ) * 100 ), 0 );
					}

					if ( $minimumper == 0 ) {
						$minimumper = $percentage;
					}
					if ( $percentage > $maximumper ) {
						$maximumper = $percentage;
					}

					if ( $percentage < $minimumper ) {
						$minimumper = $percentage;
					}
				}
				if ( $minimumper == $maximumper ) {
					$percent .= '-' . $minimumper . '%';
				} else {
					$percent .= '-(' . $minimumper . '-' . $maximumper . ')%';
				}

			} else {
				if ( $product->get_regular_price() > 0 && $product->get_sale_price() > 0 ) {
					$percentage = round( ( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 ), 0 );
					$percent    .= '-' . $percentage . '%';
				}
			}
		}

		return $percent;
	}
}

/* GROUP NEW FLASH */

/* STOCK STATUS */
if ( !function_exists( 'bigshop_woo_get_stock_status' ) ) {
	function bigshop_woo_get_stock_status()
	{
		global $product;
		?>
        <span class="stock">
            <span class="fa fa-check"></span><?php $product->is_in_stock() ? esc_html_e( ' In Stock', 'bigshop' ) : esc_html_e( ' Out Of Stock', 'bigshop' ); ?>
        </span>
		<?php
	}
}

/* COUNTDOWN IN LOOP */
if ( !function_exists( 'bigshop_display_product_countdown_in_loop' ) ) {
	function bigshop_display_product_countdown_in_loop()
	{
		global $product;
		$date = bigshop_get_max_date_sale( $product->get_id() );
		?>
		<?php if ( $date > 0 ):
		$y = date( 'Y', $date );
		$m    = date( 'm', $date );
		$d    = date( 'd', $date );
		$h    = date( 'h', $date );
		$i    = date( 'i', $date );
		$s    = date( 's', $date );
		?>
        <div class="product-count-down">
            <div class="bigshop-countdown" data-y="<?php echo esc_attr( $y ); ?>"
                 data-m="<?php echo esc_attr( $m ); ?>"
                 data-d="<?php echo esc_attr( $d ); ?>" data-h="<?php echo esc_attr( $h ); ?>"
                 data-i="<?php echo esc_attr( $i ); ?>" data-s="<?php echo esc_attr( $s ); ?>"></div>
        </div>
	<?php endif; ?>
		<?php
	}
}

// GET DATE SALE
if ( !function_exists( 'bigshop_get_max_date_sale' ) ) {
	function bigshop_get_max_date_sale( $product_id )
	{
		$time = 0;
		// Get variations
		$args          = array(
			'post_type'   => 'product_variation',
			'post_status' => array( 'private', 'publish' ),
			'numberposts' => -1,
			'orderby'     => 'menu_order',
			'order'       => 'asc',
			'post_parent' => $product_id,
		);
		$variations    = get_posts( $args );
		$variation_ids = array();
		if ( $variations ) {
			foreach ( $variations as $variation ) {
				$variation_ids[] = $variation->ID;
			}
		}
		$sale_price_dates_to = false;

		if ( !empty( $variation_ids ) ) {
			global $wpdb;
			$sale_price_dates_to = $wpdb->get_var( "
        SELECT
        meta_value
        FROM $wpdb->postmeta
        WHERE meta_key = '_sale_price_dates_to' and post_id IN(" . join( ',', $variation_ids ) . ")
        ORDER BY meta_value DESC
        LIMIT 1
    "
			);

			if ( $sale_price_dates_to != '' ) {
				return $sale_price_dates_to;
			}
		}

		if ( !$sale_price_dates_to ) {
			$sale_price_dates_to = get_post_meta( $product_id, '_sale_price_dates_to', true );

			if ( $sale_price_dates_to == '' ) {
				$sale_price_dates_to = '0';
			}

			return $sale_price_dates_to;
		}
	}
}

/* AJAX UPDATE WISH LIST */
if ( !function_exists( ( 'bigshop_update_wishlist_count' ) ) ) {
	function bigshop_update_wishlist_count()
	{
		if ( function_exists( 'YITH_WCWL' ) ) {
			wp_send_json( YITH_WCWL()->count_products() );
		}
	}

	// Wishlist ajaxify update
	add_action( 'wp_ajax_bigshop_update_wishlist_count', 'bigshop_update_wishlist_count' );
	add_action( 'wp_ajax_nopriv_bigshop_update_wishlist_count', 'bigshop_update_wishlist_count' );
}


if( !function_exists('bigshop_mini_cart')){
    function bigshop_mini_cart(){
        get_template_part( 'template-parts/header-mini', 'cart' );
    }
}
/* AJAX MINI CART */

if ( !function_exists( 'bigshop_cart_link_fragment' ) ) {
	/**
	 * Cart Fragments
	 * Ensure cart contents update when products are added to the cart via AJAX
	 *
	 * @param  array $fragments Fragments to refresh via AJAX.
	 * @return array            Fragments to refresh via AJAX
	 */
	add_filter( 'woocommerce_add_to_cart_fragments', 'bigshop_cart_link_fragment' );

	function bigshop_cart_link_fragment( $fragments )
	{
		ob_start();
		?>
        <div class="block-minicart bigshop-mini-cart">
			<?php get_template_part( 'template-parts/header-mini', 'cart' ); ?>
        </div>
		<?php
		$fragments[ 'div.bigshop-mini-cart' ] = ob_get_clean();

		return $fragments;
	}
}

/* REMOVE CART ITEM */
if ( !function_exists( 'bigshop_remove_cart_item_via_ajax' ) ) {
	add_action( 'wp_ajax_bigshop_remove_cart_item_via_ajax', 'bigshop_remove_cart_item_via_ajax' );
	add_action( 'wp_ajax_nopriv_bigshop_remove_cart_item_via_ajax', 'bigshop_remove_cart_item_via_ajax' );

	function bigshop_remove_cart_item_via_ajax()
	{

		$response = array(
			'message'        => '',
			'fragments'      => '',
			'cart_hash'      => '',
			'mini_cart_html' => '',
			'err'            => 'no',
		);

		$cart_item_key = isset( $_POST[ 'cart_item_key' ] ) ? sanitize_text_field( $_POST[ 'cart_item_key' ] ) : '';
		$nonce         = isset( $_POST[ 'nonce' ] ) ? trim( $_POST[ 'nonce' ] ) : '';

		if ( $cart_item_key == '' || $nonce == '' ) {
			$response[ 'err' ] = 'yes';
			wp_send_json( $response );
		}

		if ( ( wp_verify_nonce( $nonce, 'woocommerce-cart' ) ) ) {

			if ( $cart_item = WC()->cart->get_cart_item( $cart_item_key ) ) {
				WC()->cart->remove_cart_item( $cart_item_key );

				$product = wc_get_product( $cart_item[ 'product_id' ] );

				$item_removed_title = apply_filters( 'woocommerce_cart_item_removed_title', $product ? sprintf( _x( '&ldquo;%s&rdquo;', 'Item name in quotes', 'bigshop' ), $product->get_name() ) : esc_html__( 'Item', 'bigshop' ), $cart_item );

				// Don't show undo link if removed item is out of stock.
				if ( $product->is_in_stock() && $product->has_enough_stock( $cart_item[ 'quantity' ] ) ) {
					$removed_notice = sprintf( esc_html__( '%s removed.', 'bigshop' ), $item_removed_title );
					$removed_notice .= ' <a href="' . esc_url( WC()->cart->get_undo_url( $cart_item_key ) ) . '">' . esc_html__( 'Undo?', 'bigshop' ) . '</a>';
				} else {
					$removed_notice = sprintf( esc_html__( '%s removed.', 'bigshop' ), $item_removed_title );
				}

				wc_add_notice( $removed_notice );
			}
		} else {
			$response[ 'message' ] = esc_html__( 'Security check error!', 'bigshop' );
			$response[ 'err' ]     = 'yes';
			wp_send_json( $response );
		}

		ob_start();

		get_template_part( 'template-parts/header-mini', 'cart' );

		$mini_cart = ob_get_clean();

		$response[ 'fragments' ]      = apply_filters( 'woocommerce_add_to_cart_fragments', array(
				'div.widget_shopping_cart_content' => '<div class="widget_shopping_cart_content">' . $mini_cart . '</div>',
			)
		);
		$response[ 'cart_hash' ]      = apply_filters( 'woocommerce_add_to_cart_hash', WC()->cart->get_cart_for_session() ? md5( json_encode( WC()->cart->get_cart_for_session() ) ) : '', WC()->cart->get_cart_for_session() );
		$response[ 'mini_cart_html' ] = $mini_cart;

		wp_send_json( $response );

		die();
	}
}

add_filter( 'woocommerce_show_page_title' , 'bigshop_hide_page_title' );
/**
 * woo_hide_page_title
 *
 * Removes the "shop" title on the main shop page
 *
 * @access      public
 * @since       1.0
 * @return      void
 */
if( !function_exists('bigshop_hide_page_title')){
    function bigshop_hide_page_title() {
        return false;
    }
}

add_filter( 'woocommerce_breadcrumb_defaults', 'bigshop_woocommerce_breadcrumb_defaults', 10, 1 );
if( !function_exists( 'bigshop_woocommerce_breadcrumb_defaults')){
    function bigshop_woocommerce_breadcrumb_defaults($args){
        $args['delimiter'] ='';
        return $args;
    }
}

add_filter('woocommerce_product_additional_information_heading','bigshop_remove_tab_content_title',10,1);
add_filter('woocommerce_product_description_heading','bigshop_remove_tab_content_title',10,1);
if( !function_exists('bigshop_remove_tab_content_title')){
    function bigshop_remove_tab_content_title( $title){
        return '';
    }
}

add_action( 'bigshop_product_loop_excerpt', 'bigshop_product_loop_excerpt_callback' );
if( !function_exists( 'bigshop_product_loop_excerpt_callback' ) ){
    function bigshop_product_loop_excerpt_callback(){
        global $product;
        ?>
        <div class="excerpt"><?php echo wp_trim_words( get_the_excerpt( $product->get_id() ), 33, null ); ?></div>
        <?php
        
    }
}

add_action( 'bigshop_function_shop_loop_product_thumbnails', 'bigshop_function_shop_loop_product_thumbnails_callback' );
if( !function_exists( 'bigshop_function_shop_loop_product_thumbnails_callback' ) ){
    function bigshop_function_shop_loop_product_thumbnails_callback(){
        global $product;
        
        $attachment_ids = $product->get_gallery_image_ids();
        
        if( has_post_thumbnail( $product->get_id() ) ){
            array_unshift( $attachment_ids, get_post_thumbnail_id( $product->get_id() ) );
        }
        
        $list_images = array();
        
        if ( is_array( $attachment_ids ) && !empty( $attachment_ids ) ) {
        	foreach ( $attachment_ids as $attachment_id ) {
        		$full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
                $resize_image = bigshop_resize_image( $attachment_id, null, apply_filters( 'bigshop_product6_list_thumb_width', 60 ), apply_filters( 'bigshop_product6_list_thumb_height', 68 ), true );
                
        		$attributes      = array(
        			'title'                   => get_post_field( 'post_title', $attachment_id ),
        			'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
        			'data-src'                => (isset( $resize_image['url'] ) && $resize_image['url'] != '') ? $resize_image['url'] : '',
                    'data-width'              => (isset( $resize_image['width'] ) && $resize_image['width'] != '') ? $resize_image['width'] : '',
                    'data-height'             => (isset( $resize_image['height'] ) && $resize_image['height'] != '') ? $resize_image['height'] : '',
        			'data-large_image'        => $full_size_image[0],
        		);
                $list_images[] = $attributes;
        	}
        }
        
        if( is_array( $list_images ) && !empty( $list_images ) ){
            ?>
            <ul class="bigshop_product6_loop_thumbs owl-carousel" data-items="3" data-margin="10" data-nav="true" data-loops="true">
                <?php foreach( $list_images as $image ){ ?>
                    <li class="img_thumb">
                        <img data-large_url="<?php echo esc_url( $image['data-large_image'] ); ?>" alt="<?php echo esc_attr( $image['title'] ); ?>" class="thumb" src="<?php echo esc_url( $image['data-src'] ); ?>" width="<?php echo esc_attr( $image['data-width'] ); ?>" height="<?php echo esc_attr( $image['data-height'] ); ?>" />
                    </li>
                <?php }//Foreach ?>
            </ul>
            <?php
        }//is_array
    }
}


function bigshop_woocommerce_price_html( $price, $product ){
    return preg_replace('@(<del>.*?</del>).*?(<ins>.*?</ins>)@misx', '$2 $1', $price);
}

add_filter( 'woocommerce_get_price_html', 'bigshop_woocommerce_price_html', 100, 2 );

add_filter('woocommerce_registration_errors', 'bigshop_registration_errors_validation', 10,3);
function bigshop_registration_errors_validation($reg_errors, $sanitized_user_login, $user_email) {
	global $woocommerce;
	if ( isset( $_POST['repassword'] ) ) {
	    if( strcmp( $_POST['password'], $_POST['repassword'] ) !== 0 ){
	         return new WP_Error( 'registration-error', esc_html__( 'Password and re-password do not match.', 'bigshop' ) );
	    }
		
	}
	return $reg_errors;
}