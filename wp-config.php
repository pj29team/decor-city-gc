<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wpdatabase');

/** MySQL database username */
define('DB_USER', 'wordpress_user');

/** MySQL database password */
define('DB_PASSWORD', 'P*1NJsVHVNpuop');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Jw]drGZM:sU0rxOLy<*FiB66sA[@r*4ODC}ftQ#fR#VPBzf7R(E2yBKmdurdj&Cc');
define('SECURE_AUTH_KEY',  'C:kMv Ons~:sO*tE-%gs9x `HB^`LM;=[*XQq#)/F>+*{xT/8Z`/Y8Nu5BD-Mm+m');
define('LOGGED_IN_KEY',    'nug2WFT{K/^D[zd,|3IyA2F+TBb@Jeq[(P_fKXdxlq$<Rro1mZ)S|.g3X HkwZQ8');
define('NONCE_KEY',        'a|FX++W_W9jxxG y[3K#CD>4E5H8@ZUa3AQDQs::*bzjb;A3NOS$8N_QPz!<|LO-');
define('AUTH_SALT',        'S7cEd5yHmM{=AMPVr|9T-g70VntSa8lhn$MBSu^&*GP,F5DZg1MKj|855[a|1Vj>');
define('SECURE_AUTH_SALT', 'BQ)(V0GSJ<7[j>X}|!<%1OfYrN[%:+ZOLS;]POtDj[Ea}|,tH?Y-lw^daO+FN^&5');
define('LOGGED_IN_SALT',   '${<^wTn|nMV!je+/FJLAOC~}D |Q5O-$`v M}*6h/53}3W7J++3o98GX /`d%~6|');
define('NONCE_SALT',       '<|?<Z_-gKRf_^:^Tnt lPunqf<wdY+P^[@vF(6t]f5-p1N}a*zOX&<0`U#LnbRxs');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpdeco_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
